#!/bin/sh

# This script creates a tag for the current commit, by incrementing the current tag value, and pushes it to the remote repository

# first arg decribes the type of release (major, minor, patch)
release_type=$1

echo "Release type: $release_type"

git tag -d $(git tag -l) # delete all local tags
git pull # fetch all remote tags

current_tag=$(git describe --tags `git rev-list --tags --max-count=1`)
echo "Current tag: $current_tag"

major=$(echo $current_tag | cut -d. -f1)
minor=$(echo $current_tag | cut -d. -f2)
patch=$(echo $current_tag | cut -d. -f3)

if [ "$release_type" == "major" ]; then
  major=$((major+1))
  minor=0
  patch=0
elif [ "$release_type" == "minor" ]; then
  minor=$((minor+1))
  patch=0
elif [ "$release_type" == "patch" ]; then
  patch=$((patch+1))
else
  echo "Invalid release type. Must be one of: major, minor, patch"
  return 1
fi

new_tag="$major.$minor.$patch"
echo "Creating tag: $new_tag"

changelog=$(git log --pretty=oneline HEAD...$current_tag --decorate=False | grep "Merge branch"  | grep -v 'https' |  awk '{print $4}' | sed 's/-/ /g'| rev | cut -c2- | rev | sed "s/'/- #/g")
echo "Changelog:
$changelog
"


git tag -a $new_tag -m "$changelog"


git push origin $new_tag
