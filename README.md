# ICAT+

ICAT+ exposes a set of restful webservices and currently depends on [ICAT](https://icatproject.org/).

## Documentation

Please refer to the documentation:

- [Documentation](https://icat.gitlab-pages.esrf.fr/icat-plus/)

- [Coverage report](https://icat.gitlab-pages.esrf.fr/icat-plus/coverage/)

## License

The source code of EXI2 is licensed under the [MIT](LICENSE.md) license.
