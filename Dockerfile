FROM harbor.esrf.fr/isdd/alpine-node-wkhtmltopdf:12.14.1-0.12.5-small
RUN apk add --no-cache tzdata
ENV TZ=Europe/Paris

ENV NODE_ENV production

WORKDIR /icat
COPY . /icat

RUN apk --no-cache add --virtual build-dependencies git \
  && npm run configure \
  && npm install \
  && apk del build-dependencies \
  && apk --no-cache add curl bash

EXPOSE 8000

ENTRYPOINT ["/bin/sh", "/icat/entrypoint"]
CMD ["npm", "start"]

HEALTHCHECK --start-period=60s --timeout=40s --retries=2 CMD /icat/healthcheck
