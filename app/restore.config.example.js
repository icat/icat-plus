/**
 * restore email configuration file
 */
module.exports = {
  enabled: process.env.RESTORE_ENABLED ? !!+process.env.RESTORE_ENABLED : true,
  fromAddress: "restorerequests@esrf.fr",
  subject: "[ESRF Data Restoration] Dataset restoration request",
  smtp: {
    host: "smtp.esrf.fr",
    port: 25,
  },
  dataPortalURL: "https://data.esrf.fr/investigation/{investigationId}/datasets/{datasetId}",
};
