/**
 * ActiveMQ nmessages configuration
 */
module.exports = {
  enabled: process.env.MESSAGE_ENABLED ? !!+process.env.MESSAGE_ENABLED : true,
  queueHost: "dau-dm-07.esrf.fr",
  queuePort: 61613,
  logbookNotificationQueueName: "icatLogbookReport",
  syncUserQueueName: "icatSyncUser",
};
