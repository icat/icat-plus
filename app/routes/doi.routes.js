module.exports = (app) => {
  const doiController = require("../controllers/doi.controller.js");
  const datasetController = require("../controllers/datasets.controller.js");
  const auth = require("../authentication/icat.js");
  const { ERROR, sendError } = require("../errors.js");
  const isParticipant = async (sessionId, datasetIds, dataCollectionId) => {
    let datasets = [];
    let investigationUsers = [];
    let dataCollections = [];
    if (dataCollectionId) {
      [datasets, investigationUsers, dataCollections] = await doiController.isParticipantByDataCollectionId(sessionId, dataCollectionId);
    } else if (datasetIds && datasetIds.length > 0) {
      [datasets, investigationUsers] = await doiController.isParticipantByDatasetId(sessionId, datasetIds);
    } else {
      throw new Error(`No datasetIds or dataCollectionId found`);
    }
    return {
      datasets,
      investigationUsers,
      dataCollections,
    };
  };
  /**
   * @swagger
   * /doi/{sessionId}/mint:
   *   post:
   *     summary: It mints a DOI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: DOI information
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              allOf:
   *                 - $ref: '#/components/schemas/doiData'
   *                 - type: object
   *                   properties:
   *                      authors:
   *                          $ref: '#/components/schemas/authors'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/doiCreated'
   *       '400':
   *         description: Bad request
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.post("/doi/:sessionId/mint", auth.requiresSession, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { title, abstract, authors, dataCollectionId, keywords, citationPublicationDOI, referenceURL } = req.body;
      let { datasetIdList } = req.body;

      global.gLogger.info("Mint", { title, abstract, authors, datasetIdList, dataCollectionId });
      const { datasets, investigationUsers, dataCollections } = await isParticipant(sessionId, datasetIdList, dataCollectionId);
      if (dataCollectionId) {
        datasetIdList = datasets.map((dataset) => dataset.id);
      }
      doiController.mint(
        sessionId,
        datasets,
        investigationUsers[0].name,
        investigationUsers[0].fullName,
        {
          datasetIds: datasetIdList,
          title,
          abstract,
          authors,
          reservedDataCollectionId: dataCollectionId,
          dataCollections,
          keywords,
          citationPublicationDOI,
          referenceURL,
        },
        res
      );
    } catch (e) {
      global.gLogger.error(e);
      return sendError(ERROR.NO_PARTICIPANT.code, ERROR.NO_PARTICIPANT.message, new Error(ERROR.NO_PARTICIPANT.message), res);
    }
  });

  /**
   * @swagger
   * /doi/{prefix}/{suffix}/datasets:
   *   get:
   *     summary: Returns a set of datasets given a DOI.
   *     parameters:
   *       - in: path
   *         description : prefix of the doi
   *         name: prefix
   *         schema :
   *           type : string
   *         example : 10.15151
   *         required: true
   *       - in: path
   *         description : suffix of the doi
   *         name: suffix
   *         schema :
   *           type : string
   *         example : ESRF-ES-142846529
   *         required: true
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - $ref: '#/components/parameters/sortOrder'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/dataset'
   *       '400':
   *         description: Bad request
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.get("/doi/:prefix/:suffix/datasets", datasetController.getDatasetsByDOI);

  /**
   * @swagger
   * /doi/{prefix}/{suffix}/json-datacite:
   *   get:
   *     summary: Returns a document in a json-datacite format with the description of the DOI
   *     parameters:
   *       - in: path
   *         description : prefix of the doi
   *         name: prefix
   *         schema :
   *           type : string
   *         example : 10.15151
   *         required: true
   *       - in: path
   *         description : suffix of the doi
   *         name: suffix
   *         schema :
   *           type : string
   *         example : ESRF-ES-142846529
   *         required: true
   *     responses:
   *       '200':
   *         description: 'Return a json-datacite compliant format'
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/json-datacite'
   *       '400':
   *         description: Bad request
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.get("/doi/:prefix/:suffix/json-datacite", doiController.getJSONDataciteByDOI);

  /**
   * @swagger
   * /doi/{prefix}/{suffix}/reports:
   *   get:
   *     summary: Returns the list of the experimental reports coming from the user portal and information needed for the display
   *     parameters:
   *       - in: path
   *         description : prefix of the doi
   *         name: prefix
   *         schema :
   *           type : string
   *         example : 10.15151
   *         required: true
   *       - in: path
   *         description : suffix of the doi
   *         name: suffix
   *         schema :
   *           type : string
   *         example : ESRF-ES-142846529
   *         required: true
   *     responses:
   *       '200':
   *         description: 'Return experimental reports information'
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/expreports'
   *       '400':
   *         description: Bad request
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.get("/doi/:prefix/:suffix/reports", doiController.getExperimentalReportsByDOI);

  /**
   * @swagger
   * /doi/{sessionId}/reserve:
   *   post:
   *     summary: It reserves a DOI
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: reserve DOI information
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  Array with the datasetIds
   *              properties:
   *                datasetIdList:
   *                  type: array
   *                  items:
   *                    type: number
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/doiCreated'
   *       '400':
   *         description: Bad request
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.post("/doi/:sessionId/reserve", auth.requiresSession, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { datasetIdList } = req.body;

      global.gLogger.info("Reserve DOI", { datasetIdList });
      const { datasets, investigationUsers } = await isParticipant(sessionId, req.body.datasetIdList);
      doiController.reserveDOI(sessionId, datasets, investigationUsers[0], datasetIdList, res);
    } catch (e) {
      global.gLogger.error(e);
      return sendError(ERROR.NO_PARTICIPANT.code, ERROR.NO_PARTICIPANT.message, new Error(ERROR.NO_PARTICIPANT.message), res);
    }
  });

  /**
   * @swagger
   * /doi/{sessionId}/reserved-dois:
   *   get:
   *     summary: Returns the list of reserved DOIs for a given user
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: 'Return reserved DOIs'
   *         $ref: '#/components/responses/datacollections'
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - DOI
   */
  app.get("/doi/:sessionId/reserved-dois", auth.requiresSession, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const dataCollections = await doiController.getReservedDois(sessionId);
      return res.send(dataCollections);
    } catch (error) {
      global.gLogger.error(error);
      return sendError(500, "Failed get the reserved DOIs", error, res);
    }
  });
};
