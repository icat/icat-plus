const { sendError } = require("../errors.js");

module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const userpreferencescontroller = require("../controllers/userpreferences.controller.js");
  /**
   * @swagger
   * /users/{sessionId}/preferences:
   *   get:
   *     summary: Returns the list of the user's preferences, user from the sessionId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/userpreferences'
   *       '500':
   *          description: Unexpected error.
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - USERS
   */
  app.get("/users/:sessionId/preferences", auth.requiresSession, async (req, res) => {
    try {
      const userPreferences = await userpreferencescontroller.getUserPreferences(req.session);
      return res.send(userPreferences);
    } catch (error) {
      global.gLogger.error(error);
      return sendError(500, "Failed get the user preferences", error, res);
    }
  });

  /**
   * @swagger
   * /users/{sessionId}/preferences:
   *   put:
   *     summary: Update the user's preferences, user from the sessionId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: userpreferences
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *               $ref: '#/components/schemas/userpreferences'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/userpreferences'
   *       '500':
   *          description: Unexpected error.
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - USERS
   */
  app.put("/users/:sessionId/preferences", auth.requiresSession, async (req, res) => {
    try {
      const userPreferences = await userpreferencescontroller.updateUserPreferences(req.session, req.body);
      return res.send(userPreferences);
    } catch (error) {
      global.gLogger.error(error);
      return sendError(500, "Failed update the user preferences", error, res);
    }
  });
};
