module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const controller = require("../controllers/ewoks.controller.js");
  /**
   * @swagger
   * /ewoks/{sessionId}/jobs:
   *   get:
   *     summary: Returns a list of the jobs
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *       - $ref: '#/components/parameters/querySampleIds'
   *       - $ref: '#/components/parameters/queryJobId'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - in: query
   *         description: list of status, comma separated
   *         name: status
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *       - in: query
   *         description: search over status, proposal, sample, datasetname, step name
   *         name: search
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum : [_id, createdAt, updatedAt, status]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - in: query
   *         description: return all jobs the user access to, false by default
   *         name: all
   *         schema:
   *           type: boolean
   *     responses:
   *       '200':
   *         description : It success
   *       '500':
   *          description: Unexpected error
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - EWOKS
   */
  app.get(
    "/ewoks/:sessionId/jobs",
    auth.isEwoksEnabled,
    (req, res, next) => (req.query.investigationId ? auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next) : auth.requiresSession(req, res, next)),
    (req, res) => {
      const { investigationId, all } = req.query;
      const { isAdministrator } = req.session;
      const loadAllJobs = all !== undefined && all.toLowerCase() === "true";
      if ((!investigationId && !isAdministrator) || (investigationId && !loadAllJobs) || (isAdministrator && !loadAllJobs)) {
        return controller.getJobsByUser(req, res);
      }
      return controller.getJobs(req, res, []);
    }
  );

  /**
   * @swagger
   * /ewoks/{sessionId}/workflows:
   *   get:
   *     summary: Returns a list with the content of the graph of each workflow
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description : It success
   *       '500':
   *          description: Unexpected error
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - EWOKS
   */
  app.get("/ewoks/:sessionId/workflows", auth.isEwoksEnabled, auth.requiresSession, (req, res) => {
    return controller.getWorkflows(res);
  });

  /**
   * @swagger
   * /ewoks/{sessionId}/jobs:
   *   post:
   *     summary: Launch a job
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *       - in: query
   *         name: identifier
   *         required: true
   *         schema:
   *           type : string
   *         description : Identifier of the workflow
   *     requestBody:
   *       description: The dictionary with the input parameters
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              properties:
   *                name:
   *                  type: string
   *     responses:
   *       '200':
   *         description : It success
   *       '400':
   *          description: Bad request
   *          $ref: '#/components/responses/error400'
   *       '500':
   *          description: Unexpected error
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - EWOKS
   */
  app.post("/ewoks/:sessionId/jobs", auth.isEwoksEnabled, auth.allowLaunchJob, (req, res) => {
    const { sessionId } = req.params;
    const { datasetIds, identifier } = req.query;
    return controller.run(sessionId, { username: req.session.username, identifier, datasetIds, input: req.body }, res);
  });

  /**
   * @swagger
   * /ewoks/{sessionId}/jobs:
   *   put:
   *     summary: updates a job
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: id
   *         schema:
   *           type : string
   *         description : Mongo identifier of the job in ICAT+
   *       - in: query
   *         name: status
   *         schema:
   *           type : string
   *         description : Status of the workflow
   *     requestBody:
   *       description: Job
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description: the status of the job, the step where the job is currently processing, a log reported by ewoks.
   *              properties:
   *                status:
   *                  type: string
   *                step:
   *                  type: string
   *                log:
   *                  type: object
   *     responses:
   *       '200':
   *         description : It success
   *       '400':
   *          description: Bad request
   *          $ref: '#/components/responses/error400'
   *       '500':
   *          description: Unexpected error
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - EWOKS
   */
  app.put("/ewoks/:sessionId/jobs", auth.isEwoksEnabled, auth.requiresSession, (req, res) => {
    const { id } = req.query;
    const { step, status, logs } = req.body;
    return controller.update({ session: req.session, id, status, step, logs }, res);
  });
};
