const { isJSONString } = require("../controllers/helpers/helper.controller.js");

module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const parcelController = require("../controllers/parcel.controller.js");

  /**
   * @swagger
   * /tracking/{sessionId}/setup:
   *   get:
   *     summary: Returns the the list of container types and parameters allowed to define experiment and processing plans
   *     description : Given an investigationId it returns the whole container type, experiment and processing plan configuration. This configuration depends on the investigation (and more precisely the instrument where the experiment is carried out)
   *     parameters:
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/setup'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get("/tracking/:sessionId/setup", auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { investigationId } = req.query;
      const setup = await parcelController.getSetup(sessionId, investigationId);
      res.send(setup);
    } catch (e) {
      global.gLogger.error(e);
      res.status(500).send(e);
    }
  });

  /**
   * @swagger
   * /tracking/{sessionId}/parcel:
   *   get:
   *     summary: Lists parcels
   *     description : Lists parcels
   *     parameters:
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/requiredQueryShipmentId'
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryParcelId'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/parcelStatusQuery'
   *       - $ref: '#/components/parameters/search'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/parcels'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/parcel",
    (req, res, next) => {
      if (!req.query.investigationId) {
        auth.requiresSession(req, res, next);
      } else {
        auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
      }
    },
    parcelController.getParcelsBy
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/labels:
   *   get:
   *     summary: Returns a file with the labels
   *     description : It triggers the download of a PDF file with the labels. This method is only available when the status is APPROVED.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/parcelId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: 'Return a PDF file with the labels'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/labels",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.getLabels
  );

  /**
   * @swagger
   * /tracking/{sessionId}/parcel:
   *   put:
   *     summary: It does create or update a new parcel
   *     description : It does create or update a new parcel
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/requiredQueryShipmentId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/parcel'
   *       '400':
   *         description: Bad request. Some parameters are missing.
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put("/tracking/:sessionId/parcel", auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { investigationId, shipmentId } = req.query;
      const { username } = req.session;
      const parcel = await parcelController.createOrUpdateParcel(sessionId, investigationId, shipmentId, username, req.body);
      res.send(parcel);
    } catch (e) {
      if (isJSONString(e.message)) {
        const customError = JSON.parse(e.message);
        if (customError.code) {
          return res.status(customError.code).send(customError);
        }
      }
      return res.status(500).send(e);
    }
  });

  /**
   * @swagger
   * /tracking/{sessionId}/parcel:
   *   post:
   *     summary: It does create or update a new parcel
   *     description : It does create or update a new parcel
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/requiredQueryShipmentId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/parcel'
   *       '400':
   *         description: Bad request. Some parameters are missing.
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post("/tracking/:sessionId/parcel", auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, async (req, res) => {
    try {
      const { sessionId } = req.params;
      const { investigationId, shipmentId } = req.query;
      const { username } = req.session;
      const parcel = await parcelController.createOrUpdateParcel(sessionId, investigationId, shipmentId, username, req.body);
      res.send(parcel);
    } catch (e) {
      if (isJSONString(e.message)) {
        const customError = JSON.parse(e.message);
        if (customError.code) {
          return res.status(customError.code).send(customError);
        }
      }
      return res.status(500).send(e);
    }
  });

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/parcel/status/{status}:
   *   put:
   *     summary: Modifies a the status of a parcel
   *     description : The requester should be a participant, instrument scientists or manager.
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcelStatus'
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/parcelId'
   *       - $ref: '#/components/parameters/parcelStatus'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/status/:status",
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    parcelController.setParcelStatus
  );

  /**
   * @swagger
   * /tracking/{sessionId}/shipment/id/{shipmentId}/parcel:
   *   delete:
   *     summary: Deletes a parcel
   *     description : Deletes an existing parcel. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/shipmentId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/parcel'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete("/tracking/:sessionId/parcel", auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, parcelController.deleteParcel);
};
