module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const addressController = require("../controllers/address.controller.js");
  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/address:
   *   get:
   *     summary: Returns a list of addresses associated to an investigation
   *     description : Return a list of address associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/addresses'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/investigation/id/:investigationId/address",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    addressController.getAddressesByInvestigationId
  );

  /**
   * @swagger
   * /tracking/{sessionId}/address:
   *   get:
   *     summary: Returns all addresses associated to an user
   *     description : Return a list of address associated to an user. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/addresses'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */

  app.get("/tracking/:sessionId/address", auth.requiresSession, addressController.getAddressesByUsername);

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/address:
   *   post:
   *     summary: Creates a address
   *     description : Creates a address associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/address'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post(
    "/tracking/:sessionId/investigation/id/:investigationId/address",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    addressController.createAddress
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/address:
   *   put:
   *     summary: Modifies an existin address
   *     description : Modifies an existing address. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/address'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/address",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    addressController.updateAddress
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/address:
   *   delete:
   *     summary: Deletes an existin address
   *     description : Deletes an existing address. It does not delete the record from the data but change the status to removed. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/address'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete(
    "/tracking/:sessionId/investigation/id/:investigationId/address",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    addressController.deleteAddress
  );
};
