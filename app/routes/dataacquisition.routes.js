module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const logbook = require("../controllers/logbook.controller.js");
  const investigationController = require("../controllers/investigations.controller.js");
  const datasetController = require("../controllers/datasets.controller.js");
  const { DatasetAdapter, asyncGetSession } = require("../api/icat.js");
  const { ERROR, sendError } = require("../errors.js");
  const doiController = require("../controllers/doi.controller.js");
  const { getSession } = require("../authentication/helper.authentication.js");

  const datasetAccessController = require("../controllers/datasetaccess.controller.js");

  const connectAsDataAcquisitionUser = async (req, res, next) => {
    try {
      const dataAcquisitionUserSession = await asyncGetSession(global.gServerConfig.icat.authorizations.dataacquisition.user);
      req.params.sessionId = dataAcquisitionUserSession.sessionId;
      return next();
    } catch {
      global.gLogger.error(`connectAsDataAcquisitionUser failed`);
      return sendError(ERROR.LOGIN_FAILED.code, ERROR.LOGIN_FAILED.message, ERROR.LOGIN_FAILED, res);
    }
  };

  /**
   * This allows to use createInvestigationNotification with the former deprecated methods which use path params
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  const supportDeprecatedMethods = (req, res, next) => {
    req.params.investigationName = req.query.investigationName;
    req.params.instrumentName = req.query.instrumentName;
    req.params.investigationId = req.query.investigationId;
    next();
  };

  /**
   * @swagger
   * /dataacquisition/{apiKey}/mint:
   *   post:
   *     summary: It mints a DOI
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *     requestBody:
   *       description: DOi information
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              allOf:
   *                 - $ref: '#/components/schemas/doiData'
   *                 - type: object
   *                   properties:
   *                      authors:
   *                          $ref: '#/components/schemas/authorslist'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/doiCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         description: Authorization information is missing or invalid
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Internal error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/mint", auth.requiresApiKey, async (req, res) => {
    try {
      let { sessionId } = req.params;
      const { title, abstract, authors, keywords, citationPublicationDOI, referenceURL } = req.body;
      let { datasetIdList } = req.body;

      /** it might happens that it arrives as string */
      if (!Array.isArray(datasetIdList)) {
        datasetIdList = JSON.parse(datasetIdList);
      }

      global.gLogger.info("Mint", { title, abstract, authors, datasetIdList });

      const authorsList = authors
        .split(";")
        .map((author) => {
          const [name = "", surname = "", orcid = ""] = author.split(",");
          return {
            surname: surname.trim(),
            name: name.trim(),
            orcid: orcid ? orcid.trim() : null,
          };
        })
        .filter((author) => author.name && author.surname);

      // If sessionId is the key then we log as minter
      const data = await asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
      req.params.sessionId = data.sessionId;
      sessionId = data.sessionId;
      req.session = await getSession(req);

      if (req.session.isMinter) {
        const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIdList });
        doiController.mint(
          sessionId,
          datasets,
          req.session.name,
          req.session.fullName,
          { datasetIds: datasetIdList, title, abstract, authors: authorsList, keywords, citationPublicationDOI, referenceURL },
          res
        );
      } else {
        sendError(ERROR.BAD_PARAMS.code, ERROR.BAD_PARAMS.message, new Error(ERROR.BAD_PARAMS.message), res);
      }
    } catch (e) {
      global.gLogger.error(e);
      return sendError(ERROR.NO_PARTICIPANT.code, ERROR.NO_PARTICIPANT.message, new Error(ERROR.NO_PARTICIPANT.message), res);
    }
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/base64:
   *   post:
   *     summary: Uploads an image in base64 format to the investigation electronic logbook
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: proposal's name
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       description: Encoded image
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  Comma separated user names and String with the image encoded in base64.
   *              properties:
   *                base64:
   *                  type: string
   *                  example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *                usernames:
   *                  type: string
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         description: Authorization information is missing or invalid
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         description: Authorization information is missing or invalid
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Internal error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/base64", auth.requiresApiKey, connectAsDataAcquisitionUser, auth.requiresSession, (req, res) => {
    const { base64, datasetId, software, machine, tag } = req.body;
    const { sessionId } = req.params;
    const { instrumentName, investigationName } = req.query;
    return logbook.createfrombase64(sessionId, { investigationName, instrumentName, base64, datasetId, software, machine, tag }, res);
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/notification:
   *   post:
   *     summary: Adds an event to the logbook (for an investigation or beamline logbook). It could also be a machine event
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: name of the investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         description: Authorization information is missing or invalid
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         description: Authorization information is missing or invalid
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Internal error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/notification", auth.requiresApiKey, supportDeprecatedMethods, (req, res) => {
    /** This allows to use createInvestigationNotification with the former deprecated methods */
    if (req.params.investigationName) return logbook.createInvestigationNotification(req, res);
    if (req.params.instrumentName) return logbook.createBeamlineNotification(req, res);
    return logbook.createBroadcastNotification(req, res);
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/investigation:
   *   get:
   *     summary: Get investigations
   *     description : |
   *        If ids, it returns the investigations corresponding to these ids.
   *        If filter is embargoed, it returns all investigations which are under embargoed, ie. with releaseDate is after today.
   *        If filter is released, it returns all investigations which are open, ie. with releaseDate is before today and with a DOI.
   *        If filter is instrumentscientist, it returns all investigations where user is instrumentScientitst.
   *        If filter is partipant, it returns all investigations where user has reading permissions.
   *        If no filter, it returns all user's investigations.
   *
   *        Instrument allows to filter investigations by instrument name.
   *
   *        StartDate and EndDate allows to filter investigations which occur during this period.
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist, released, embargoed]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *       - in: query
   *         name: instrumentName
   *         description : It filters the investigations by instrument name
   *         schema:
   *           type : string
   *       - in: query
   *         name: startDate
   *         example : 2019-01-01
   *         description : It filters the investigations for which startDate or endDate is after this startDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: endDate
   *         example : 2019-01-31
   *         description : It filters the investigations for which startDate is lower than this endDate - not combined with ids
   *         schema:
   *           type : string
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [NAME,INSTRUMENT,STARTDATE,TITLE,SUMMARY,RELEASEDATE,DOI]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - in: query
   *         description: It is the name of the proposal or investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         description: Comma separated investigation identifiers list
   *         name: ids
   *         schema:
   *           type: string
   *       - in: query
   *         name: time
   *         example : 2019-01-01T08:00:00
   *         description : It filters the investigations for which time is between startDate and endDate
   *         schema:
   *           type : string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/investigation'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *     tags:
   *       - DataAcquisition
   */
  app.get("/dataacquisition/:apiKey/investigation", auth.requiresApiKey, connectAsDataAcquisitionUser, auth.requiresSession, investigationController.getInvestigationsBySessionId);

  /**
   * @swagger
   * /dataacquisition/{apiKey}/dataset:
   *   get:
   *     summary: Gets datasets
   *     description: Gets datasets for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         name: investigationId
   *         description : Identifier of the investigation
   *         schema:
   *           type : string
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE|SAMPLENAME|NAME]
   *       - $ref: '#/components/parameters/sortOrder'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         description: Authorization information is missing or invalid.
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error.
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */

  app.get(
    "/dataacquisition/:apiKey/dataset",
    auth.requiresApiKey,
    connectAsDataAcquisitionUser,
    auth.requiresSession,
    supportDeprecatedMethods,
    (req, _, next) => {
      req.query.datasetType = req.query.datasetType ? req.query.datasetType : "acquisition";
      req.query.investigationIds = req.query.investigationId;
      next();
    },
    datasetController.getDatasetsBy
  );

  /**
   * @swagger
   * /dataacquisition/{apiKey}/dataset/restore:
   *   post:
   *     summary: Update the restore status of a dataset access and send an email to the users who asked for the restoration
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         description: datasetId
   *         name: datasetId
   *         schema:
   *           type: number
   *     requestBody:
   *       $ref: '#/components/requestBodies/restoreInfo'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/datasetaccesses'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         description: Authorization information is missing or invalid.
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/dataset/restore", auth.requiresApiKey, (req, res) => {
    return datasetAccessController.updateRestoreStatus(req.query.datasetId, req.body.level, req.body.message, res);
  });

  /**
   * @swagger
   * /dataacquisition/{apiKey}/events/move:
   *   post:
   *     summary: move the events from one investigation to another
   *     parameters:
   *       - $ref: '#/components/parameters/apiKey'
   *       - in: query
   *         required: true
   *         description: investigationId of the source event
   *         name: sourceInvestigationId
   *         schema:
   *           type: number
   *       - in: query
   *         required: true
   *         description: investigationId of the destination event
   *         name: destinationInvestigationId
   *         schema:
   *           type: number
   *     responses:
   *       '200':
   *         description: Events moved successfully
   *         $ref: '#/components/responses/modifiedObject'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - DataAcquisition
   */
  app.post("/dataacquisition/:apiKey/events/move", auth.requiresApiKey, logbook.moveEvents);
};
