module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const shipmentController = require("../controllers/shipment.controller.js");

  /**
   * @swagger
   * /tracking/{sessionId}/shipment:
   *   get:
   *     summary: Lists shipments associated to an investigation
   *     description : Lists shipments associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/requiredQueryInvestigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryShipmentId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/shipments'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.get(
    "/tracking/:sessionId/shipment",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    (req, res, next) => {
      const { investigationId } = req.query;
      if (investigationId) return auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
      /** just in case error in the code downstream  a user can not filter investigation in the investigation logbook*/
      next();
    },
    shipmentController.getShipmentsBy
  );

  /**
   * @swagger
   * /tracking/{sessionId}/shipment:
   *   post:
   *     summary: Creates a shipment associated to an investigation
   *     description : Creates a shipment associated to an investigation. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/requiredQueryInvestigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post("/tracking/:sessionId/shipment", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, shipmentController.createShipment);

  /**
   * @swagger
   * /tracking/{sessionId}/shipment:
   *   put:
   *     summary: Modifies a shipment associated to an investigation
   *     description : Modifies an existing shipment. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/requiredQueryInvestigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put("/tracking/:sessionId/shipment", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, shipmentController.updateShipment);

  /**
   * @swagger
   * /tracking/{sessionId}/shipment:
   *   delete:
   *     summary: Deletes a shipment
   *     description : Deletes an existing shipment. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/requiredQueryInvestigationId'
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/shipment'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete("/tracking/:sessionId/shipment", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, shipmentController.deleteShipment);
};
