const investigationController = require("../controllers/investigations.controller.js");
const datasetController = require("../controllers/datasets.controller.js");
const sampleController = require("../controllers/samples.controller.js");
const dataCollectionController = require("../controllers/datacollections.controller.js");
const datafileController = require("../controllers/datafile.controller.js");
const userController = require("../controllers/user.controller.js");
const instrumentController = require("../controllers/instrument.controller.js");
const parameterController = require("../controllers/parameter.controller.js");
const auth = require("../authentication/icat.js");
const datasetParameterController = require("../controllers/datasetparameter.controller.js");
const { isUndefined } = require("../controllers/helpers/helper.controller.js");
const { ERROR } = require("../errors.js");
const techniqueController = require("../controllers/technique.controller.js");
const { getSession } = require("../authentication/helper.authentication.js");
const { isValid, parse } = require("date-fns");

module.exports = (app) => {
  /**
   * @swagger
   * /catalogue/parameters:
   *   get:
   *     summary: Returns all parameters
   *     description : Return a list of parameters that are registered in ICAT
   *     parameters:
   *       - in: query
   *         name: parameterTypeId
   *         schema:
   *           type: integer
   *       - in: query
   *         name: name
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/parameter'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/parameters", parameterController.getParameters);

  /**
   * @swagger
   * /catalogue/instruments:
   *   get:
   *     summary: Returns all instruments
   *     description : Return a list of instruments that are registered in ICAT
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrument'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/instruments", instrumentController.getInstruments);

  /**
   * @swagger
   * /catalogue/{sessionId}/instruments:
   *   get:
   *     summary: Returns all instruments linked to the user
   *     description : |
   *       Returns instruments linked to the user, with instruments linked to the scientist if the user is beamline responsible, all instruments if administrator
   *
   *       if the filter is participant, it returns only the instruments linked to the user
   *       if the filter is instrumentscientist, it returns only the instruments linked to the scientist if the user is beamline responsible
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrument'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/instruments", auth.requiresSession, instrumentController.getInstrumentsBySession);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   get:
   *     summary: Gets users involved in a investigation
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation/id/:investigationId/investigationusers", auth.requiresSession, investigationController.getInvestigationUserByInvestigation);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   post:
   *     summary: Add users to an investigation. Only allowed to PI, LocalContact and Beamline staff
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     requestBody:
   *       description: User name
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  user's name in the ICAT database
   *              properties:
   *                name:
   *                  type: string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.post(
    "/catalogue/:sessionId/investigation/id/:investigationId/investigationusers",
    auth.requiresSession,
    auth.allowsGrantInvestigationAccessManager,
    investigationController.addInvestigationUserToInvestigation
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
   *   delete:
   *     summary: Deletes a user from an investigation. Only allowed to PI, LocalContact and Beamline staff
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : integer
   *         required: true
   *     requestBody:
   *       description: Username
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  user's name in the ICAT database
   *              properties:
   *                name:
   *                  type: string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/investigationusers'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.delete(
    "/catalogue/:sessionId/investigation/id/:investigationId/investigationusers",
    auth.requiresSession,
    auth.allowsGrantInvestigationAccessManager,
    investigationController.deleteInvestigationUserToInvestigation
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation:
   *   get:
   *     summary: Get investigations by sessionId
   *     description : |
   *        If ids, it returns the investigations corresponding to these ids.
   *
   *        If filter is embargoed, it returns all investigations which are under embargoed, ie. with releaseDate is after today.
   *        If filter is released, it returns all investigations which are open, ie. with releaseDate is before today and with a DOI.
   *        If filter is instrumentscientist, it returns all investigations where user is instrumentScientitst.
   *        If filter is partipant, it returns all investigations where user has reading permissions.
   *        If no filter, it returns all user's investigations.
   *
   *        Instrument allows to filter investigations by instrument name.
   *
   *        StartDate and EndDate allows to filter investigations which occur during this period.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInstrumentName'
   *       - in: query
   *         description: It is the name of the proposal or investigation
   *         name: investigationName
   *         schema:
   *           type: string
   *       - in: query
   *         name: ids
   *         schema:
   *           type : string
   *         description : Comma separated investigation identifiers list
   *       - in: query
   *         name: filter
   *         schema:
   *           type : string
   *           enum: [partipant, instrumentscientist, released, embargoed]
   *         example : participant
   *         description : Filters the investigations attached to an user
   *       - in: query
   *         name: startDate
   *         example : 2019-01-01
   *         description : It filters the investigations for which startDate or endDate is after this startDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: endDate
   *         example : 2019-01-31
   *         description : It filters the investigations for which startDate is lower than this endDate - not combined with ids
   *         schema:
   *           type : string
   *       - in: query
   *         name: username
   *         description: It filters the investigations for which the user is a participant (user.name prefix)
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [NAME,INSTRUMENT,STARTDATE,TITLE,SUMMARY,RELEASEDATE,DOI]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - in: query
   *         description: flag to load information about access to the experiments' datasets
   *         name: withHasAccess
   *         schema:
   *           type: boolean
   *           default: false
   *       - $ref: '#/components/parameters/elementParametersQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/investigation'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation", auth.requiresSession, investigationController.getInvestigationsBySessionId);

  /**
   * @swagger
   * /catalogue/{sessionId}/datacollection:
   *   get:
   *     summary: Gets all datacollection or for a given datasetId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: Metadata catalogue dataset identifier
   *         name: datasetId
   *         schema :
   *           type : string
   *         example : 132123
   *       - in: query
   *         description: type of the data collection
   *         name: type
   *         schema:
   *           type: string
   *           enum: [document|datacollection]
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [DOI,DATE]
   *       - $ref: '#/components/parameters/sortOrder'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/datacollection'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/datacollection", auth.requiresSession, dataCollectionController.getDataCollectionsBy);

  /**
   * Return true if date matches the format
   */
  const checkDate = (date, format) => {
    const parsedDate = parse(date, format, new Date());
    return isValid(parsedDate);
  };
  /**
   * @swagger
   * /catalogue/{sessionId}/datasetparameter:
   *   get:
   *     summary: Returns the parameters
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationIdList'
   *       - $ref: '#/components/parameters/queryInstrumentName'
   *       - $ref: '#/components/parameters/sampleId'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *       - in: query
   *         description: the name of the parameter type to filter by. It must be only one parameter for json, and a comma-separated list for csv (2 values min.)
   *         name: name
   *         schema:
   *           type: string
   *       - in: query
   *         description: the type of dataset to filter by
   *         name: datasetType
   *         schema:
   *           type: string
   *           enum: [acquisition, processed]
   *       - in: query
   *         description: format the data will be returned. json is the default format
   *         name: format
   *         schema:
   *           type: string
   *           enum: [json, csv]
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datasetparameter'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get(
    "/catalogue/:sessionId/datasetParameter",
    (req, res, next) => {
      const { startDate, endDate, instrumentName, investigationId } = req.query;
      const isByInstrumentName = !(isUndefined(instrumentName) || isUndefined(startDate) || isUndefined(endDate));
      if (isUndefined(investigationId) && isByInstrumentName === false) {
        return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
      }
      /** it is done by instrument then check date format */
      if (isUndefined(investigationId)) {
        const format = "yyyy-MM-dd";
        if (!checkDate(startDate, format) || !checkDate(endDate, format)) {
          global.gLogger.error(`Start or end date format does not match ${format}`, req.query);
          return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
        }
      }
      next();
    },
    datasetParameterController.getDatasetParameter
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/datasetparameter/values:
   *   get:
   *     summary: Returns the values of the parameters
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationIdList'
   *       - $ref: '#/components/parameters/queryInstrumentName'
   *       - $ref: '#/components/parameters/sampleId'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *       - in: query
   *         description: the name of the parameter type to filter by
   *         name: name
   *         schema:
   *           type: string
   *       - in: query
   *         description: the type of dataset to filter by
   *         name: datasetType
   *         schema:
   *           type: string
   *           enum: [acquisition, processed]
   *       - $ref: '#/components/parameters/elementParametersQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/responses/datasetparameters'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get(
    "/catalogue/:sessionId/datasetParameter/values",
    (req, res, next) => {
      const { startDate, endDate, instrumentName, investigationId } = req.query;
      const isByInstrumentName = !(isUndefined(instrumentName) || isUndefined(startDate) || isUndefined(endDate));
      if (isUndefined(investigationId) && isByInstrumentName === false) {
        return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
      }
      /** it is done by instrument then check date format */
      if (isUndefined(investigationId)) {
        const format = "yyyy-MM-dd";
        if (!checkDate(startDate, format) || !checkDate(endDate, format)) {
          global.gLogger.error(`Start or end date format does not match ${format}`, req.query);
          return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
        }
      }
      next();
    },
    datasetParameterController.getDatasetParameterValues
  );

  /**
   * @swagger
   * /catalogue/investigation/name/{investigationName}/normalize:
   *   get:
   *     summary: Most of users don't write their proposal names using the same convention so it might happen that a given proposal can be written in many different ways. This method will homogenize such name.  Example IH-LS-0001, ihls0001, ih-LS-0001 are normalized like IH-LS-0001.
   *     parameters:
   *       - in: path
   *         description : name of the investigation to normalize
   *         name: investigationName
   *         schema :
   *           type : string
   *         example : ihls0001
   *         required: true
   *     responses:
   *       '200':
   *         description : It returns a string with the normalized proposal name
   *         content:
   *           application/json:
   *             schema :
   *               type : string
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/investigation/name/:investigationName/normalize", investigationController.normalize);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset:
   *   get:
   *     summary: Gets datasets for a given investigationId or for a given datasetIds of for a sampleId or for a beamline
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description : Metadata catalogue investigation identifier list (comma separated)
   *         name: investigationIds
   *         schema :
   *           type : string
   *         example : 132123,1523433
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *       - $ref: '#/components/parameters/sampleId'
   *       - in: query
   *         description: name of the beamline or the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *         example: ID01
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: type of the dataset
   *         name: datasetType
   *         schema:
   *           type: string
   *       - in: query
   *         description: true if processed datasets will be populated
   *         name: nested
   *         schema:
   *           type: boolean
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum :  [STARTDATE,SAMPLENAME,NAME,SAMPLEDATE]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - $ref: '#/components/parameters/elementParametersQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/dataset", (req, res) => {
    return datasetController.getDatasetsBy(req, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/timeline:
   *   get:
   *     summary: Gets datasets timeline for a given investigationId or sampleId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/sampleId'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/timeline'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/dataset/timeline", auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, async (req, res) => {
    try {
      const { investigationId, sampleId } = req.query;
      const { sessionId } = req.params;
      if (isUndefined(investigationId) && isUndefined(sampleId)) {
        return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
      }
      const session = await getSession(req);
      const isAdministrator = session.isAdministrator;
      const timelines = await datasetController.getDatasetsTimelineBy(sessionId, investigationId, sampleId, isAdministrator);
      res.send(timelines);
    } catch (error) {
      global.gLogger.error(error);
      res.status(500).send("Failed to get timeline");
    }
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/timeline:
   *   get:
   *     summary: Gets investigation timeline for a given instrumentname
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInstrument'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/timeline'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/investigation/timeline", auth.allowsInstrumentScientistsOrAdministrators, async (req, res) => {
    try {
      const { instrument } = req.query;
      const { sessionId } = req.params;
      if (isUndefined(instrument)) {
        return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
      }
      const timelines = await investigationController.getTimelineBy(sessionId, instrument);
      res.send(timelines);
    } catch (error) {
      global.gLogger.error(error);
      res.status(500).send("Failed to get timeline");
    }
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/id/{investigationId}/sample:
   *   get:
   *     deprecated : true
   *     summary: Gets samples for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/investigation/id/:investigationId/sample", auth.requiresSession, sampleController.getSamplesByInvestigationId);

  /**
   * @swagger
   * /catalogue/{sessionId}/samples:
   *   get:
   *     summary: Gets samples
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/queryInstrumentName'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - $ref: '#/components/parameters/querySampleIds'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *       - $ref: '#/components/parameters/datasetTypeQuery'
   *       - $ref: '#/components/parameters/havingAcquisitionDatasetsQuery'
   *       - $ref: '#/components/parameters/includeDatasetsQuery'
   *       - $ref: '#/components/parameters/nestedQuery'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum : [DATE,NAME]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - $ref: '#/components/parameters/elementParametersQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/sample'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/samples", auth.requiresSession, sampleController.getSamplesBy);

  /**
   * @swagger
   * /catalogue/{sessionId}/samples/{sampleId}/page:
   *   get:
   *     summary: Gets sample page for a given sample
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/pathRequiredSampleId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/queryInstrumentName'
   *       - $ref: '#/components/parameters/querySampleIds'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *       - $ref: '#/components/parameters/havingAcquisitionDatasetsQuery'
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *           enum : [DATE,NAME]
   *       - $ref: '#/components/parameters/sortOrder'
   *       - $ref: '#/components/parameters/elementParametersQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/meta'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/samples/:sampleId/page", auth.requiresSession, sampleController.getSamplePage);

  /**
   * @swagger
   * /catalogue/{sessionId}/sampleParameters/{id}:
   *   put:
   *     summary: Update sample parameter (description only if it is an editable sample or only sample comment)
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: path
   *         description : Metadata Catalogue parameter identifier
   *         name: id
   *         schema :
   *           type : integer
   *         required: true
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *         required: true
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateSampleParameter'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: string
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.put("/catalogue/:sessionId/sampleParameters/:id", auth.requiresSession, auth.allowsEmbargoedInvestigationUserOrAdministratorOrInstrumentScientists, (req, res) => {
    return sampleController.updateSampleParameter(req.params.sessionId, req.query.investigationId, req.params.id, req.body.value, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/samples/{sampleId}/sampleParameters:
   *   post:
   *     summary: Create sample parameter (description only if it is an editable sample, or sample comment only)
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/pathRequiredSampleId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *         required: true
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateSampleParameter'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: string
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.post(
    "/catalogue/:sessionId/samples/:sampleId/sampleParameters",
    auth.requiresSession,
    auth.allowsEmbargoedInvestigationUserOrAdministratorOrInstrumentScientists,
    (req, res) => {
      return sampleController.createSampleParameter(req.params.sessionId, req.params.sampleId, req.body.name, req.body.value, res);
    }
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset:
   *   get:
   *     deprecated : true
   *     summary: Get a list of dataset from a datasetId list
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetIds'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetIds/dataset", auth.requiresSession, (req, res) => {
    req.query.datasetIds = req.params.datasetIds;
    return datasetController.getDatasetsById(req, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset_document:
   *   get:
   *     summary: Get a list of dataset documents from a datasetId list. A dataset document is used for indexing a dataset in elastic search. It unflatters dataset's parameters.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetIds'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset_document'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetIds/dataset_document", auth.requiresSession, datasetController.getDatasetsDocumentsById);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/startdate/{startDate}/enddate/{endDate}/dataset_document:
   *   get:
   *     summary: Get a list of dataset documents by date range. A dataset document is used for indexing a dataset in elastic search. It unflatters dataset's parameters.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDate'
   *       - $ref: '#/components/parameters/endDate'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/dataset_document'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/startdate/:startDate/enddate/:endDate/dataset_document", auth.requiresSession, datasetController.getDatasetsDocumentsByDateRange);

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetId}/datacollection:
   *   get:
   *     deprecated : true
   *     summary: Returns datacollections related to a given dataset along with a datasetId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetId'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datacollection'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get(
    "/catalogue/:sessionId/dataset/id/:datasetId/datacollection",
    auth.requiresSession,
    (req, _, next) => {
      req.query.datasetId = req.params.datasetId;
      next();
    },
    dataCollectionController.getDataCollectionsBy
  );

  /**
   * @swagger
   * /catalogue/{sessionId}/dataset/id/{datasetId}/datafile:
   *   get:
   *     deprecated : true
   *     summary: Get datafiles for a given dataset id. Method replaced by /catalogue/{sessionId}/datafile
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/datasetId'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/dataset/id/:datasetId/datafile", auth.requiresSession, (req, res) => {
    req.query.datasetId = req.params.datasetId;
    return datafileController.getDatafilesByDatasetId(req, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/datafile:
   *   get:
   *     summary: Get datafiles for a given dataset id.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryDatasetId'
   *       - $ref: '#/components/parameters/limit'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/search'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/datafile", auth.requiresSession, datafileController.getDatafilesByDatasetId);

  /**
   * @swagger
   * /catalogue/{sessionId}/user:
   *   get:
   *     summary: Get user list of ICAT (Restricted to administrators)
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/user'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/user", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.getUsers);

  /**
   * @swagger
   * /catalogue/{sessionId}/investigation/{investigationId}/user:
   *   get:
   *     summary: Get user list of ICAT if the user if PI or LocalContact of the investigation, or InstrumentScientist or Administrator
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/user'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/investigation/:investigationId/user", auth.requiresSession, auth.allowsGrantInvestigationAccessManager, userController.getUsers);

  /**
   * @swagger
   * /catalogue/{sessionId}/user/name/{name}/instrumentscientist:
   *   get:
   *     summary: Get instrumentScientist by user name
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - in: path
   *         description : ICAT's user name
   *         name: name
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/datafile'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.get("/catalogue/:sessionId/user/name/:name/instrumentscientist", auth.requiresSession, auth.allowAdministrators, userController.getInstrumentScientistsByUserName);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   get:
   *     summary: Get all InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: name of the beamline or the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *         example: ID01
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrumentscientist'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.get("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.getInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   put:
   *     summary: It creates InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: List of Usernames and instruments
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:   Comma separated user names and Comma separated instrument names
   *              properties:
   *                usernames:
   *                  type: string
   *                instrumentnames:
   *                  type: string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/instrumentscientist'
   *       '403':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.put("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.createInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/instrumentscientist:
   *   delete:
   *     summary: It deletes InstrumentScientists
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: List of usernames and instrumentScientistsId
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:   Comma separated user names and comma separated instrumentScientist ids
   *              properties:
   *                usernames:
   *                  type: string
   *                instrumentscientistsid:
   *                  type: string
   *     responses:
   *       '200':
   *          description: OK
   *       '403':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - User Management
   */
  app.delete("/catalogue/:sessionId/instrumentscientist", auth.requiresSession, auth.allowsInstrumentScientistsOrAdministrators, userController.deleteInstrumentScientists);

  /**
   * @swagger
   * /catalogue/{sessionId}/datasetparameter:
   *   put:
   *     summary: Update a value of a dataset parameter
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the parameter
   *         name: parameterId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateParameter'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: string
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.put("/catalogue/:sessionId/datasetparameter", auth.requiresSession, auth.allowAdministrators, (req, res) => {
    return datasetParameterController.updateParameter(req.params.sessionId, req.query.parameterId, req.body.value, res);
  });

  /**
   * @swagger
   * /catalogue/{sessionId}/datafile/download:
   *   get:
   *     summary: Downloads a file given its filepath.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         name: filepath
   *         description : File path
   *         schema :
   *           type : string
   *         required: true
   *       - in: query
   *         description: Metadata catalogue dataset identifier
   *         name: datasetId
   *         schema :
   *           type : string
   *         example : 132123
   *         required: true
   *       - in: query
   *         description: Metadata catalogue investigation identifier
   *         name: investigationId
   *         schema :
   *           type : string
   *         example : 132123
   *         required: true
   *     responses:
   *       '200':
   *         description : 'Ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '404':
   *         $ref: '#/components/responses/error404'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */

  app.get("/catalogue/:sessionId/datafile/download", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists, (req, res) => {
    return datafileController.download(req.params.sessionId, req.query.filepath, req.query.datasetId, res);
  });

  /**
   * @swagger
   * /catalogue/techniques:
   *   get:
   *     summary: Returns all techniques
   *     description : Return a list of techniques that are registered in ICAT
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/technique'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/techniques", techniqueController.getTechniques);

  /**
   * @swagger
   * /catalogue/{sessionId}/files/upload:
   *   post:
   *     summary: Upload a file and information linked to a sample
   *     description : upload a file (like .pdb) and information (filetype, groupName, etc.) and link it to a sample
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryRequiredSampleId'
   *     requestBody:
   *       description: File to be uploaded and information related to the file
   *       required: true
   *       content:
   *          multipart/form-data:
   *            schema:
   *              type: object
   *              description: file and associated metadata
   *              required:
   *                - file
   *                - fileType
   *              properties:
   *                file:
   *                  type: file
   *                fileType:
   *                  type: string
   *                groupName:
   *                  type: string
   *                multiplicity:
   *                  type: number
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *                 $ref: '#/components/schemas/sampleinformation'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.post("/catalogue/:sessionId/files/upload", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientistsForSample, sampleController.uploadFile);

  /**
   * @swagger
   * /catalogue/{sessionId}/files:
   *   get:
   *     summary: Get the list of the files linked to a sample
   *     description : Returns the list of files and associated metadata linked to a sample
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryRequiredSampleId'
   *       - in: query
   *         description: groupName filter
   *         name: groupName
   *         schema :
   *           type : string
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *                $ref: '#/components/schemas/sampleinformation'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/files", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientistsForSample, sampleController.getSampleFiles);

  /**
   * @swagger
   * /catalogue/{sessionId}/files/download:
   *   get:
   *     summary: Download the file associated to a sample
   *     description : Returns the file indentified by the resourceId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryRequiredSampleId'
   *       - in: query
   *         description: file resourceId
   *         name: resourceId
   *         schema :
   *           type : string
   *         required:
   *            true
   *     responses:
   *       '200':
   *         description: OK
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Catalogue
   */
  app.get("/catalogue/:sessionId/files/download", auth.requiresSession, auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientistsForSample, sampleController.downloadFile);
};
