module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const actionsController = require("../controllers/actions.controller.js");

  /**
   * @swagger
   * /actions/{sessionId}/logbook/instrumentname:
   *   put:
   *     summary: Actions related to the field instrumentName of the logbook
   *     description : Actions related to the field instrumentName of the logbook
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the action
   *         name: action
   *         schema:
   *           type: string
   *           enum: [fill]
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/actions'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Actions
   */
  app.put("/actions/:sessionId/logbook/instrumentname", auth.requiresSession, auth.allowAdministrators, actionsController.doActionOnInstrumentName);
};
