module.exports = (app) => {
  const controller = require("../controllers/oaipmh.controller.js");

  /**
   * @swagger
   * /oaipmh/request:
   *   get:
   *     summary: Proxy to OAI-PMH server
   *     responses:
   *       '200':
   *         description: OK
   *     tags:
   *       - OAI-PMH
   */
  app.get("/oaipmh/request", controller.request);
};
