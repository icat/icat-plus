const { getPathBy } = require("../api/icat.js");

module.exports = (app) => {
  const controller = require("../controllers/h5grove.controller.js");
  const { ERROR, sendError } = require("../errors");
  const { getDatafilesByIds } = require("../api/icat");

  /**
   * Checks that the parameters datafileId is present as query param
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @returns
   */
  const requireDatafile = (req, res, next) => {
    global.gLogger.debug("[h5grove] requireDatafile", { datafileId: req.query.datafileId });
    if (req.query.datafileId) {
      return next();
    }
    return sendError(ERROR.NO_DATAFILEID.code, ERROR.NO_DATAFILEID.message, new Error(ERROR.NO_DATAFILEID.message), res);
  };

  /**
   * Cache to store the paths of the datafile requested to H5Grove
   * We need this cache because Hibou does a request for each entry (ids needs >1s to resolve each) which makes this very slow
   * It is composed by datafileCache[sessionId][datafileId] so it assumes that for a single user's session the file path will not changed
   */
  const datafileCache = {};

  /**
   * It retrieves the datafile from ICAT
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @returns
   */
  const getDataFile = async (req, res, next) => {
    const { sessionId, datafileId } = req.query;
    global.gLogger.debug("[h5grove] Request datafile", { queryParams: JSON.stringify(req.query), datafileId });
    const datafiles = await getDatafilesByIds(sessionId, datafileId);
    global.gLogger.debug("[h5grove] getDataFile", { count: datafiles.length });
    if (datafiles && datafiles.length > 0) {
      const dataFile = datafiles[0].Datafile;
      req.location = dataFile.location;
      global.gLogger.info("File location", { location: dataFile.location });

      if (!datafileCache[sessionId] || !datafileCache[sessionId][datafileId]) {
        if (!datafileCache[sessionId]) {
          datafileCache[sessionId] = {};
        }
        datafileCache[sessionId][datafileId] = await getPathBy(sessionId, datafileId, null, true);
        global.gLogger.info("Paths are retrieved from getPathBy via IDS", { location: dataFile.location, paths: datafileCache[sessionId][datafileId] });
      }
      const paths = datafileCache[sessionId][datafileId];

      global.gLogger.debug("[h5grove] getPathBy", { datafileId, paths });
      if (paths.length > 0) {
        req.location = paths[0];
        return next();
      }
      global.gLogger.debug("[h5grove] getPathByDatafileIds. datafile is archived", datafileId, paths);
    }
    return sendError(ERROR.NO_DATAFILE.code, ERROR.NO_DATAFILE.message, new Error(ERROR.NO_DATAFILE.message), res);
  };

  /**
   * @swagger
   * /h5grove/meta:
   *   get:
   *     summary: It redirects to h5grove
   *     parameters:
   *       - $ref: '#/components/parameters/querySessionId'
   *       - in: query
   *         description : Id of the datafile
   *         name: datafileId
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '404':
   *         $ref: '#/components/responses/error404'
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - H5Grove
   */
  app.get("/h5grove/meta", requireDatafile, getDataFile, (req, res) => {
    return controller.request(req, req.location, res);
  });

  /**
   * @swagger
   * /h5grove/attr:
   *   get:
   *     summary: It redirects to h5grove
   *     parameters:
   *       - $ref: '#/components/parameters/querySessionId'
   *       - in: query
   *         description : Id of the datafile
   *         name: datafileId
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '404':
   *         $ref: '#/components/responses/error404'
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - H5Grove
   */
  app.get("/h5grove/attr", requireDatafile, getDataFile, (req, res) => {
    return controller.request(req, req.location, res);
  });

  /**
   * @swagger
   * /h5grove/data:
   *   get:
   *     summary: It redirects to h5grove
   *     parameters:
   *       - $ref: '#/components/parameters/querySessionId'
   *       - in: query
   *         description : Id of the datafile
   *         name: datafileId
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '404':
   *         $ref: '#/components/responses/error404'
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - H5Grove
   */
  app.get("/h5grove/data", requireDatafile, getDataFile, (req, res) => {
    return controller.request(req, req.location, res);
  });

  /**
   * @swagger
   * /h5grove/stats:
   *   get:
   *     summary: It redirects to h5grove
   *     parameters:
   *       - $ref: '#/components/parameters/querySessionId'
   *       - in: query
   *         description : Id of the datafile
   *         name: datafileId
   *         schema :
   *           type : string
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '404':
   *         $ref: '#/components/responses/error404'
   *       '500':
   *         description: Unexpected error
   *     tags:
   *       - H5Grove
   */
  app.get("/h5grove/stats", requireDatafile, getDataFile, (req, res) => {
    return controller.request(req, req.location, res);
  });
};
