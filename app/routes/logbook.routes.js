const { replaceImageSrc, getServerURI, toHTML, toText } = require("../controllers/helpers/helper.logbook.controller.js");
const { getParamAsDateString, getParamAsTimeString } = require("../controllers/helpers/helper.controller.js");
const { ERROR } = require("../errors.js");

module.exports = (app) => {
  /** PDF generation response timeout */
  const TIMEOUT = 1000 * 60 * 10;

  const auth = require("../authentication/icat.js");
  const logbook = require("../controllers/logbook.controller.js");
  const tag = require("../controllers/tag.controller.js");

  const allowAccessToInstrumentScientistOrUsersOrReleased = (req, res, next) => {
    /** This allows to implement allowing the access to beamline scientist to the logbook of the beamline */
    if (!req.query.investigationId && req.query.instrumentName) {
      return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
    }
    return auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser(req, res, next);
  };

  /**
   * @swagger
   * /logbook/{sessionId}/event/createfrombase64:
   *   post:
   *     summary: Create an event given a base64 string
   *     description : this is used mainly when uploading a photo from the mobile device. It will create a new event (in an investigation or in a beamline logbook) that will contain the image.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       description: Encoded image
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  String with the image encoded in base64. It is the full base64 with the data:image/type;<base64>
   *              properties:
   *                base64:
   *                  type: string
   *                  example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/event/createfrombase64", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, (req, res) => {
    const { base64, datasetId, software, machine, tag } = req.body;
    const { username, fullName } = req.session;
    return logbook.createfrombase64(
      req.params.sessionId,
      { investigationId: req.query.investigationId, instrumentName: req.query.instrumentName, username, fullName, base64, datasetId, software, machine, tag },
      res
    );
  });

  /**
   * Parses and gets the parameters needed in order to build the find query of the events
   * @param {*} req
   * @returns
   */
  function getListEventsParametersFromRequest(req) {
    const {
      types,
      investigationId,
      instrumentName,
      sortBy = "_id",
      sortOrder = "-1",
      skip = 0,
      limit,
      search,
      filterInvestigation = false,
      date,
      tags,
      startTime,
      endTime,
    } = req.query;

    return {
      investigationId,
      instrumentName,
      sortBy,
      sortOrder,
      skip,
      limit,
      search,
      filterInvestigation,
      date,
      tags,
      startTime,
      endTime,
      types,
      formattedDate: getParamAsDateString(date),
      formattedStartTime: getParamAsTimeString(startTime),
      formattedEndTime: getParamAsTimeString(endTime),
    };
  }
  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   get:
   *     summary: Lists events
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/sortOrder'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: format of the returned events, json by default
   *         name: format
   *         schema:
   *           type: string
   *           enum: [json, pdf, txt]
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *       - $ref: '#/components/parameters/tags'
   *       - $ref: '#/components/parameters/startTimeQuery'
   *       - $ref: '#/components/parameters/endTimeQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/events'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get(
    "/logbook/:sessionId/event",
    auth.requiresSession,
    (req, res, next) => {
      const { investigationId, instrumentName } = req.query;
      if (!investigationId && !instrumentName) return auth.allowAdministrators(req, res, next);
      if (!investigationId) return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
      /** just in case error in the code downstream  a user can not filter investigation in the investigation logbook*/
      req.query.filterInvestigation = false;
      return allowAccessToInstrumentScientistOrUsersOrReleased(req, res, next);
    },
    async (req, res) => {
      try {
        const { sessionId } = req.params;
        const { format = "json", investigationId } = req.query;
        const events = await logbook.getEvents(getListEventsParametersFromRequest(req));

        switch (format.toLowerCase()) {
          case "pdf":
            req.setTimeout(TIMEOUT);
            return logbook.sendPDF(toHTML(events, sessionId, getServerURI(req.headers.host, req.secure)), req, sessionId, investigationId, res);
          case "txt": {
            const fileName = await logbook.getLogbookFileName(sessionId, investigationId);
            res.attachment(fileName);
            res.type("txt");
            return res.send(toText(events));
          }
          default:
            return res.send(replaceImageSrc(events, sessionId, getServerURI(req.headers.host, req.secure)));
        }
      } catch (err) {
        global.gLogger.error(err);
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving events.",
        });
      }
    }
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/remove:
   *   put:
   *     summary: Flag events as removed
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *       - $ref: '#/components/parameters/tags'
   *       - $ref: '#/components/parameters/startTimeQuery'
   *       - $ref: '#/components/parameters/endTimeQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/events'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put(
    "/logbook/:sessionId/event/remove",
    auth.requiresSession,
    auth.allowAdministrators,
    (req, res, next) => {
      if (!req.query.investigationId) {
        return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
      }
      next();
    },
    async (req, res) => {
      try {
        const events = await logbook.getEvents(getListEventsParametersFromRequest(req));
        const removedEventList = await logbook.remove(req.query.investigationId, events);
        res.send(removedEventList);
      } catch (e) {
        res.send(e);
      }
    }
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/restore:
   *   put:
   *     summary: Flag events as restored by setting removed = false for each event. InvestigationId is mandatory
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: field to be sorted by
   *         name: sortBy
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *       - $ref: '#/components/parameters/tags'
   *       - $ref: '#/components/parameters/startTimeQuery'
   *       - $ref: '#/components/parameters/endTimeQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/events'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put(
    "/logbook/:sessionId/event/restore",
    auth.requiresSession,
    auth.allowAdministrators,
    (req, res, next) => {
      if (!req.query.investigationId) {
        return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
      }
      next();
    },
    async (req, res) => {
      try {
        const events = await logbook.getEvents(getListEventsParametersFromRequest(req), { includeRemoved: true });
        const restoredEventList = await logbook.restore(req.query.investigationId, events);
        res.send(restoredEventList);
      } catch (e) {
        res.send(e);
      }
    }
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/count:
   *   get:
   *     summary: Count events
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *       - $ref: '#/components/parameters/tags'
   *       - $ref: '#/components/parameters/startTimeQuery'
   *       - $ref: '#/components/parameters/endTimeQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/totalNumber'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get(
    "/logbook/:sessionId/event/count",
    auth.requiresSession,
    (req, res, next) => {
      global.gLogger.debug(JSON.stringify(req.query));
      if (!req.query.investigationId && !req.query.instrumentName) return auth.allowAdministrators(req, res, next);
      if (!req.query.investigationId) return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
      /** just in case error in the code downstream  a user can not filter investigation in the investigation logbook*/
      req.query.filterInvestigation = false;
      return allowAccessToInstrumentScientistOrUsersOrReleased(req, res, next);
    },
    logbook.countEvents
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/page:
   *   get:
   *     summary: Gets the number of page where the event is
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: event identifier
   *         name: _id
   *         schema:
   *           type: string
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - $ref: '#/components/parameters/sortOrder'
   *       - $ref: '#/components/parameters/skip'
   *       - $ref: '#/components/parameters/limit'
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - in: query
   *         description: filter events on a specific date - format YYYY-MM-DD
   *         name: date
   *         schema:
   *            type: string
   *       - $ref: '#/components/parameters/tags'
   *       - $ref: '#/components/parameters/startTimeQuery'
   *       - $ref: '#/components/parameters/endTimeQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/meta'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get(
    "/logbook/:sessionId/event/page",
    auth.requiresSession,
    (req, res, next) => {
      if (!req.query.investigationId && !req.query.instrumentName) return auth.allowAdministrators(req, res, next);
      if (!req.query.investigationId) return auth.allowsInstrumentScientistsOrAdministrators(req, res, next);
      return allowAccessToInstrumentScientistOrUsersOrReleased(req, res, next);
    },
    logbook.getPageByEventId
  );

  /**
   * @swagger
   * /logbook/{sessionId}/event/dates:
   *   get:
   *     summary: Lists events dates
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *       - in: query
   *         description: comma separated list of type-category events. Example NOTIFICATION-ERROR, NOTIFICATION-INFO
   *         name: types
   *         schema:
   *           type: string
   *       - in: query
   *         description: If true the logs with investigationId will not be retrieved
   *         name: filterInvestigation
   *         schema:
   *           type: boolean
   *       - $ref: '#/components/parameters/search'
   *       - $ref: '#/components/parameters/tags'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/dates'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/event/dates", auth.requiresSession, allowAccessToInstrumentScientistOrUsersOrReleased, logbook.findAllDatesByEvents);

  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   post:
   *     summary: Creates an event
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/event", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, logbook.createEvent);

  /**
   * @swagger
   * /logbook/{sessionId}/event:
   *   put:
   *     summary: Updates an event
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToUpdate'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put("/logbook/:sessionId/event", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, logbook.updateEvent);

  /**
   * @swagger
   * /logbook/{sessionId}/stats/investigation:
   *   get:
   *     summary: Lists statistics on events for each investigation that are allocated in the date range
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/logbookInvestigationStats'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/stats/investigation", auth.requiresSession, auth.allowAdministrators, logbook.getEventsStatistics);

  /**
   * @swagger
   * /logbook/{sessionId}/stats/count:
   *   get:
   *     summary: Counts number of event for date range
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/startDateQuery'
   *       - $ref: '#/components/parameters/endDateQuery'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/logbookInvestigationStatsCount'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/stats/count", auth.requiresSession, auth.allowAdministrators, logbook.getEventsCount);

  /**
   * @swagger
   * /logbook/{sessionId}/tag:
   *   get:
   *     summary: GET all tags available, (ie used in the investigation or not) for a given logbook including global, beamline specific and investigation specific tags. Tags available but not used are also retrieved.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/tags'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.get("/logbook/:sessionId/tag", auth.requiresSession, allowAccessToInstrumentScientistOrUsersOrReleased, tag.getTags);

  /**
   * @swagger
   * /logbook/{sessionId}/tag:
   *   post:
   *     summary: Create an investigation tag for a given investigationId
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the investigation
   *         name: investigationId
   *         schema:
   *           type: string
   *       - in: query
   *         description: name of the instrument
   *         name: instrumentName
   *         schema:
   *           type: string
   *     requestBody:
   *       $ref: '#/components/requestBodies/createTag'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/tagCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:sessionId/tag", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, tag.createTag);

  /**
   * @swagger
   * /logbook/{sessionId}/tag:
   *   put:
   *     summary: Update an investigation tag
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - $ref: '#/components/parameters/tagIdQuery'
   *     requestBody:
   *       $ref: '#/components/requestBodies/updateTag'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/tagUpdated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.put("/logbook/:sessionId/tag", auth.requiresSession, auth.allowAccessToInstrumentScientistOrUsers, tag.updateTag);

  /**
   * @swagger
   * /logbook/{apiKey}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event/createfrombase64:
   *   post:
   *     deprecated: false
   *     summary: Create an event given a base64 string. It requires the API token. This is used on the spec beamlines from the metadata manager
   *     parameters:
   *       - $ref: '#/components/parameters/investigationName'
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/apiKey'
   *     requestBody:
   *       description: Encoded image
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description:  Comma separated user names and String with the image encoded in base64.
   *              properties:
   *                base64:
   *                  type: string
   *                  example: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=
   *                usernames:
   *                  type: string
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:apiKey/investigation/name/:investigationName/instrument/name/:instrumentName/event/createfrombase64", auth.requiresApiKey, (req, res) => {
    const { base64, datasetId, software, machine, tag } = req.body;
    const { sessionId, instrumentName, investigationName } = req.params;
    return logbook.createfrombase64(sessionId, { investigationName, instrumentName, base64, datasetId, software, machine, tag }, res);
  });

  /**
   * @swagger
   * /logbook/{apiKey}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event:
   *   post:
   *     deprecated : false
   *     summary: This is used on the spec beamlines from the metadata manager
   *     parameters:
   *       - $ref: '#/components/parameters/investigationName'
   *       - $ref: '#/components/parameters/instrumentName'
   *       - $ref: '#/components/parameters/apiKey'
   *     requestBody:
   *       $ref: '#/components/requestBodies/eventToCreate'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/eventCreated'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Logbook
   */
  app.post("/logbook/:apiKey/investigation/name/:investigationName/instrument/name/:instrumentName/event", auth.requiresApiKey, (req, res) => {
    return logbook.createInvestigationNotification(req, res);
  });
};
