const idsController = require("../controllers/ids.controller.js");
const auth = require("../authentication/icat.js");

module.exports = (app) => {
  /**
   * @swagger
   * /ids/{sessionId}/datasets/status:
   *   get:
   *     summary: Return the IDS status of the datasets specified by the datasetIds along with a sessionId.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description : Metadata catalogue dataset identifier list (comma separated)
   *         name: datasetIds
   *         schema :
   *           type : string
   *         example : 132123,1523433
   *         required: true
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               items:
   *                 $ref: '#/components/schemas/status'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - IDS
   */
  app.get("/ids/:sessionId/datasets/status", auth.requiresSession, (req, res) => {
    idsController.getStatusByDatasetIds(req.params.sessionId, req.query.datasetIds, res);
  });

  /**
   * @swagger
   * /ids/{sessionId}/path:
   *   get:
   *     summary: Returns the location of the files or datasets
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *       - $ref: '#/components/parameters/queryDatafileIds'
   *       - $ref: '#/components/parameters/queryInvestigationId'
   *       - in: query
   *         description: if true only the locations that currently exists will be returned
   *         name: isonline
   *         schema:
   *           type: boolean
   *           default: false
   *     responses:
   *       '200':
   *          description : 'List of locations where the datafiles or datasets can be located'
   *          schema:
   *            type: array
   *            items:
   *              type: string
   *          Example: "['/data/visitor/mx1234/bm01/sample/dataset1', '/data/visitor/mx1234/bm01/sample/dataset2']"
   *       '400':
   *         $ref: '#/components/responses/error400'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - IDS
   */
  app.get("/ids/:sessionId/path", auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, (req, res) => {
    const { sessionId } = req.params;
    const { datafileIds, datasetIds, isonline } = req.query;
    return idsController.getPathBy(sessionId, datafileIds, datasetIds, isonline, res);
  });

  /**
   * @swagger
   * /ids/{sessionId}/data/download:
   *   get:
   *     summary: Redirects to IDS server to download datasets or datafiles
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - in: query
   *         description: identifier of the datasets
   *         name: datasetIds
   *         schema:
   *           type: array
   *           items:
   *              $ref: '#/components/schemas/dataset'
   *       - in: query
   *         description: identifier of the datafiles
   *         name: datafileIds
   *         schema:
   *           type: array
   *           items:
   *              $ref: '#/components/schemas/datafile'
   *     responses:
   *       '200':
   *          description : 'Ok'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - IDS
   */
  app.get("/ids/:sessionId/data/download", auth.requiresSession, (req, res) => {
    return idsController.downloadData(req.params.sessionId, req.query.datasetIds, req.query.datafileIds, res);
  });

  /**
   * @swagger
   * /ids/{sessionId}/datasets/restore:
   *   post:
   *     summary: Records dataset restoration request by a user and redirects to IDS server to download datasets or datafiles
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/queryDatasetIds'
   *     requestBody:
   *       description: User information
   *       content:
   *          application/json:
   *            schema:
   *              type: object
   *              description: User identifier and email (if anonymous)
   *              properties:
   *                name:
   *                  type: string
   *                email:
   *                  type: string
   *     responses:
   *       '200':
   *          description : 'Ok'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - IDS
   */
  app.post("/ids/:sessionId/datasets/restore", auth.requiresSession, (req, res) => {
    return idsController.restoreData(req.params.sessionId, req.query.datasetIds, req.body.name, req.body.email, res);
  });
};
