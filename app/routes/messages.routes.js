const { sendError } = require("../errors.js");

module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const messagescontroller = require("../controllers/messages.controller.js");
  /**
   * @swagger
   * /messages:
   *   get:
   *     summary: Returns the messages
   *     parameters:
   *       - in: query
   *         description: type of the message
   *         name: type
   *         schema:
   *           type: string
   *           enum : [INFORMATION]
   *     responses:
   *       '200':
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/message'
   *       '500':
   *          description: Unexpected error.
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - MESSAGES
   */
  app.get("/messages", async (req, res) => {
    try {
      const messages = await messagescontroller.getMessages(req.query.type);
      return res.send(messages);
    } catch (error) {
      global.gLogger.error(error);
      return sendError(500, "Failed get the message", error, res);
    }
  });

  /**
   * @swagger
   * /messages/{sessionId}/information:
   *   put:
   *     summary: Update an information message
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     requestBody:
   *       description: message
   *       required: true
   *       content:
   *          application/json:
   *            schema:
   *               $ref: '#/components/schemas/message'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/message'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *          description: Unexpected error.
   *          $ref: '#/components/responses/error500'
   *     tags:
   *       - MESSAGES
   */
  app.put("/messages/:sessionId/information", auth.allowAdministrators, async (req, res) => {
    try {
      const message = await messagescontroller.updateInformationMessage(req.body);
      return res.send(message);
    } catch (error) {
      global.gLogger.error(error);
      return sendError(500, "Failed update the information message", error, res);
    }
  });
};
