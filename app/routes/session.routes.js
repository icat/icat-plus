const session = require("../controllers/session.controller.js");
const { ERROR } = require("../errors.js");

module.exports = (app) => {
  /**
   * @swagger
   * /session/{sessionId}:
   *   get:
   *     summary: Gets information about the session in ICAT
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/sessionInformation'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Session
   */
  app.get("/session/:sessionId", session.getSessionInformation);

  /**
   * @swagger
   * /session/{sessionId}:
   *   delete:
   *     summary: Log out
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '200':
   *         description: Ok
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Session
   */
  app.delete("/session/:sessionId", session.doLogout);

  /**
   * @swagger
   * /session:
   *   post:
   *     summary: This is used to log into the app
   *     requestBody:
   *       description: The credentials to log in
   *       content:
   *         'application/json':
   *           schema:
   *             $ref: '#/components/schemas/credentials'
   *       required: true
   *     responses:
   *       '200':
   *         description: Information about the newly created session
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/session'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *     tags:
   *       - Session
   */
  app.post("/session", session.login);

  /**
   * @swagger
   * /session/{sessionId}:
   *   put:
   *     summary: This is used to refresh the current session. It returns code 204 if refreshed
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *     responses:
   *       '204':
   *         description: Session is refreshed
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '500':
   *         description: Unexpected error
   *         $ref: '#/components/responses/error500'
   *       '501':
   *         $ref: '#/components/responses/error501'
   *     tags:
   *       - Session
   */
  app.put(
    "/session/:sessionId",
    (req, res, next) => {
      if (global.gServerConfig.session.allowRefresh) {
        return next();
      }
      return res.status(ERROR.NO_SESSION_REFRESH_ALLOWED.code).send(ERROR.NO_SESSION_REFRESH_ALLOWED.message);
    },
    session.refreshSession
  );
};
