module.exports = (app) => {
  const auth = require("../authentication/icat.js");
  const itemController = require("../controllers/item.controller.js");

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/item:
   *   post:
   *     summary: Creates an item associated to a parcel
   *     description : Creates an item associated to an parcel. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/parcelId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/item'
   *     responses:
   *       '200':
   *         description: OK
   *         $ref: '#/components/responses/shipment'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.post(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    itemController.addItem
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/item/{itemId}:
   *   put:
   *     summary: Edits an item
   *     description : Edits an existing item. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/parcelId'
   *       - $ref: '#/components/parameters/itemId'
   *     requestBody:
   *       $ref: '#/components/requestBodies/item'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.put(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item/:itemId",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    itemController.updateItem
  );

  /**
   * @swagger
   * /tracking/{sessionId}/investigation/id/{investigationId}/parcel/id/{parcelId}/item/{itemId}:
   *   delete:
   *     summary: Deletes an item
   *     description : Deletes an existing item. The requester should be a participant, instrument scientists or manager.
   *     parameters:
   *       - $ref: '#/components/parameters/sessionId'
   *       - $ref: '#/components/parameters/investigationId'
   *       - $ref: '#/components/parameters/parcelId'
   *       - $ref: '#/components/parameters/itemId'
   *     responses:
   *       '200':
   *         description: 'ok'
   *       '400':
   *         description: Bad request
   *         $ref: '#/components/responses/error400'
   *       '401':
   *         $ref: '#/components/responses/error401'
   *       '403':
   *         $ref: '#/components/responses/error403'
   *       '501':
   *         description: 'Not implemented yet'
   *     tags:
   *       - Tracking
   */
  app.delete(
    "/tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item/:itemId",
    auth.requiresSession,
    auth.allowsInvestigationUserOrAdministratorsOrInstrumentScientists,
    itemController.deleteItem
  );
};
