const { InvestigationAdapter, getInvestigationTypes, getParameterTypeList, getUsersByGroupName, getInstruments, getUsers, asyncGetSession } = require("../api/icat.js");
const NodeCache = require("node-cache");

const checkperiod = 2 * 60;
const stdTTL = 24 * 60 * 60; // 1 day
/**
 * This retrieves the investigation types from ICAT and store them in the cache
 */
const initInvestigationTypes = async () => {
  try {
    global.gLogger.debug("initInvestigationTypes");
    const investigationTypes = await getInvestigationTypes(global.gServerConfig.icat.authorizations.parameterListReader.user);
    global.appCache.set("investigationTypes", investigationTypes, stdTTL);
    global.gLogger.info("InvestigationTypes are retrieved.", {
      investigationTypes: investigationTypes.map((type) => {
        return type.InvestigationType.name;
      }),
    });
  } catch (e) {
    global.gLogger.error(e);
  }
};

const initParameterTypeList = async () => {
  global.gLogger.debug("initParameterTypeList");
  const parameterList = await getParameterTypeList(global.gServerConfig.icat.authorizations.parameterListReader.user);
  global.gLogger.info("ParameterTypeList retrieved for global cache", { parametersCount: parameterList.length });
  global.appCache.set("parameterTypes", parameterList, stdTTL);
  const parametersDictionary = {};
  for (let i = 0; i < parameterList.length; i++) {
    parametersDictionary[parameterList[i].ParameterType.id] = parameterList[i].ParameterType.name;
  }
  global.appCache.set("parametersDictionary", parametersDictionary, stdTTL);
};

const getAdministrators = () => {
  return global.appCache.get("administrators");
};

const initAdministrators = async () => {
  global.gLogger.debug("initAdministrators");
  const users = await getUsersByGroupName(
    global.gServerConfig.icat.authorizations.adminUsersReader.user,
    global.gServerConfig.icat.authorizations.adminUsersReader.administrationGroups
  );

  global.appCache.set("administrators", users, stdTTL);
  const administrators = getAdministrators().map((u) => {
    return `${u.User.fullName}( ${u.User.name} )`;
  });
  global.gLogger.info("getUsersByGroupName is set.", { administrators });
};

const getMinters = () => {
  return global.appCache.get("minters");
};

const initMinters = async () => {
  global.gLogger.debug("initMinters");
  const users = await getUsersByGroupName(global.gServerConfig.icat.authorizations.adminUsersReader.user, global.gServerConfig.icat.authorizations.minting.group);
  global.appCache.set("minters", users, stdTTL);
  const minters = getMinters().map((u) => {
    return `${u.User.fullName}( ${u.User.name} )`;
  });
  global.gLogger.info("getUsersByGroupName is set.", { minters });
};

const initInstruments = async () => {
  global.gLogger.debug("initInstruments");
  const instruments = await getInstruments(global.gServerConfig.icat.authorizations.adminUsersReader.user);
  global.instruments = instruments.map((i) => {
    return i.Instrument;
  });
  global.gLogger.info("[cache] Retrieved instruments", { count: instruments.length });
};

const initUsers = async () => {
  global.gLogger.debug("initUsers");
  const users = await getUsers(global.gServerConfig.icat.authorizations.adminUsersReader.user);
  const usersDictionary = {};
  for (let i = 0; i < users.length; i++) {
    usersDictionary[users[i].User.name] = users[i].User;
  }
  global.appCache.set("usersDictionary", usersDictionary, stdTTL);
  global.gLogger.info("[cache] Retrieved users", { count: users.length });
  global.users = usersDictionary;
};

const initInvestigations = async () => {
  global.gLogger.debug("initInvestigations");
  const data = await asyncGetSession(global.gServerConfig.icat.authorizations.investigationReader.user);
  const investigations = await InvestigationAdapter.getInvestigationsBy(data.sessionId, { limit: 2000000 }, "", 2000000);
  global.gLogger.info("investigations retrieved", { count: investigations.length });
  global.appCache.set("investigations", investigations, stdTTL);
  global.investigations = investigations;
  global.investigationsKeys = {};
  global.investigations.forEach((investigation) => {
    global.investigationsKeys[investigation.id] = investigation;
  });
};

const initAuthenticators = () => {
  /** Object that gets the authentications based on the sessionId */
  global.gLogger.info("Init authenticators");
  global.authenticators = {};
};

/** map that contains the user by name pattern, as it registered in the mongo database (eg. siteId or username/siteId) */
const initUsersUserByPattern = () => {
  global.gLogger.info("Init users by pattern name");
  global.usersByPattern = {};
};

exports.init = async () => {
  global.gLogger.info("Init application cache");
  global.appCache = new NodeCache({ checkperiod });
  await this.loadCache();
  global.gLogger.info("Application cache has been initialized");
  global.appCache.on("expired", async (key) => {
    if (key === "authenticators") {
      initAuthenticators();
    }
    if (key === "investigationTypes") {
      await initInvestigationTypes();
    }
    if (key === "parametersDictionary") {
      await initParameterTypeList();
    }
    if (key === "administrators") {
      await initAdministrators();
    }
    if (key === "minters") {
      await initMinters();
    }
    if (key === "usersDictionary") {
      await initUsers();
    }
    if (key === "instruments") {
      await initInstruments();
    }
    if (key === "investigations") {
      await initInvestigations();
    }
    if (key === "usersByPattern") {
      initUsersUserByPattern();
    }
  });
};

exports.loadCache = async () => {
  initAuthenticators();
  initUsersUserByPattern();
  await initInvestigationTypes();
  await initParameterTypeList();
  await initAdministrators();
  await initMinters();
  await initUsers();
  await initInstruments();
  await initInvestigations();
};
