const icat = require("../api/icat.js");
const { sendSyncUserMessage } = require("../controllers/helpers/helper.message.controller.js");

/** Given a username and a key in the cache if will return true if the username exists in the list of users */
const isUserInGroup = (username, group) => {
  return global.appCache.get(group).find((u) => {
    return u.User.name === username;
  });
};
/**
 * Returns true if the sessionId belongs to a user that is administrator.
 */
const isAdministrator = (username) => {
  return isUserInGroup(username, "administrators");
};

const isMinter = (username) => {
  return isUserInGroup(username, "minters");
};

/** returns all investigations of the database */
exports.getInvestigations = () => global.investigations;

/**
 * This method intializes the cache of a user
 */
exports.initSession = async (sessionId) => {
  try {
    if (sessionId) {
      if (global.API_KEY !== sessionId) {
        const data = await icat.asyncGetSessionInformation(sessionId);
        const { userName, remainingMinutes } = data;
        const [instrumentScientists, usersWithSamePrefix] = await Promise.all([
          icat.getInstrumentScientistsByUserName(sessionId, userName),
          this.getUserByUsernamePrefix(sessionId, userName),
        ]);
        const user = await this.getUserByName(userName, sessionId);
        const isInstrumentScientist = instrumentScientists && instrumentScientists.length > 0;
        const credential = {
          name: userName,
          username: userName,
          fullName: user.fullName,
          affiliation: user.affiliation,
          lifeTimeMinutes: remainingMinutes,
          expirationTime: new Date(new Date().getTime() + remainingMinutes * 60000),
          isAdministrator: !!isAdministrator(userName),
          isInstrumentScientist: !!isInstrumentScientist,
          isMinter: !!isMinter(userName),
          sessionId,
          usersByPrefix: usersWithSamePrefix,
        };
        return credential;
      }

      return {
        name: "API_KEY",
        username: "API_KEY",
        fullName: "API_KEY",
        lifeTimeMinutes: 0,
        isAdministrator: false,
        isInstrumentScientist: false,
        sessionId,
        usersByPrefix: [],
      };
    }

    throw Error("Session can not be initialized. Session id is empty?");
  } catch (e) {
    global.gLogger.error("Unable to initialize the user'session", { sessionId, error: e.message });
  }
};

/**
 * Returns the user from the cache or from the DB if he/she is not in cache
 * @param {*} name
 * @param {*} sessionId
 * @returns
 * @throws error if user not found in the cache or in DB
 */
exports.getUserByName = async (name, sessionId) => {
  if (global.users[name] === undefined) {
    if (!sessionId) {
      const response = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.adminUsersReader.user);
      sessionId = response.sessionId;
    }
    const userFromDB = await icat.getUserByName(sessionId, name);
    if (userFromDB === undefined || userFromDB.length === 0) {
      throw Error(`User ${name} not found `);
    }
    global.users[name] = userFromDB[0];
    if (name.indexOf("/null") > -1) {
      sendSyncUserMessage(userFromDB[0]);
    }
  }
  return global.users[name];
};

exports.getUserByUsernamePrefix = async (sessionId, userName) => {
  const users = await icat.getUserByUsernamePrefix(sessionId, this.extractUserNamePrefix(userName), userName);
  return users;
};

exports.extractUserNamePrefix = (userName) => {
  return userName ? userName.split("/")[0] : "";
};

exports.getInvestigationTypes = () => global.appCache.get("investigationTypes");

exports.getParameterTypes = () => {
  return global.appCache.get("parameterTypes");
};

/**
 * It returns a dictionary id: name
 * Example:
 * '127988408': 'InstrumentSource_current_start',
 */
exports.getParameterTypeDictionary = () => {
  return global.appCache.get("parametersDictionary");
};

/**
 * Given a fullName (alex/1234 or suffix 1234 it will find and return the user in the cache)
 * @param {*} searchName
 * @returns
 */
exports.getUserFromCacheBySuffixOrFullName = (searchName) => {
  if (global.usersByPattern[searchName]) {
    return global.usersByPattern[searchName];
  }
  return this.getUserByNamePattern(searchName);
};

/**
 * Get user fullname by name  or by suffix.
 * @param {string} nameOrSuffix name or suffix of the user.
 * @returns {string} fullname if it exists. Unknown if nameOrSuffix is null; nameOrSuffix otherwise.
 */
exports.getUserFullNameByName = (nameOrSuffix) => {
  if (!nameOrSuffix) {
    return "Unknown";
  }
  if (global.users[nameOrSuffix]) {
    return global.users[nameOrSuffix].fullName;
  }

  const user = this.getUserFromCacheBySuffixOrFullName(nameOrSuffix);
  return user ? user.fullName : nameOrSuffix;
};

/**
 * returns the first user name corresponding to a specified pattern, undefined if no user found
 * @param {*} namePattern - Example '/123456'
 * @returns the user name,
 */
exports.getUserByNamePattern = (namePattern) => {
  const fullNames = Object.keys(global.users);
  const name = fullNames
    .filter((fullName) => {
      return fullName === namePattern || fullName.endsWith(`/${namePattern}`);
    })
    .shift();
  /** Storing in the memory for next time */
  if (global.users[name]) {
    global.usersByPattern[namePattern] = global.users[name];
    return global.users[name];
  }
  return undefined;
};

exports.getStats = () => global.appCache.getStats();

exports.getKeys = () => global.appCache.keys();

exports.getDataByKey = (key) => global.appCache.mget([key]);

exports.getLifeTimeMinutes = () => global.appCache.get("lifetimeMinutes");

/**
 * Return the object in the cache by sessionId
 * In case it is not available then it will init the cache
 */
exports.getSessionById = async (sessionId) => {
  let session = global.appCache.get(sessionId);
  if (!session) {
    global.gLogger.debug("No session found for sessionId and will be initialized", sessionId);
    session = await this.initSession(sessionId);
    global.gLogger.debug("Session has been initialized", sessionId);
  }
  return session;
};

exports.getParameterTypeById = (id) => {
  const attribute = global.appCache.get("parametersDictionary");
  if (attribute) {
    return attribute[id];
  }

  return null;
};

exports.getInstruments = () => {
  return global.instruments;
};

exports.getInstrumentByName = (name) => {
  return global.instruments.find((i) => i.name === name);
};

exports.setKey = async (sessionId, key, value) => {
  let session = await this.getSessionById(sessionId);
  if (!session) {
    global.gLogger.info("{}  => cache", { sessionId });
    this.setSession(sessionId);
    session = await this.getSessionById(sessionId);
  }

  session[key] = value;
  global.gLogger.info(`${key} => cache`, { sessionId, key });
  global.appCache.set(sessionId, session);
};

exports.cache = global.appCache;
