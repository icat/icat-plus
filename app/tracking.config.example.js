const MXExperimentPlan = [
  {
    header: "Pin barcode",
    name: "pin_barcode",
    type: "string",
    description: "Barcode identifier",
  },
  {
    header: "Exp. Type",
    name: "workflowType",
    type: "select",
    values: ["MXPressE", "MXPressF", "MXPressO", "MXPressF", "MXPressE_SAD", "MXScore", "MXPressM", "MXPressP", "MXPressP_SAD"],
    description: "Specifies the type of experiment to be performed for automatic data collection.",
  },
  {
    header: "a",
    name: "unit_cell_a",
    type: "string",
  },
  {
    header: "b",
    name: "unit_cell_b",
    type: "string",
  },
  {
    header: "c",
    name: "unit_cell_c",
    type: "string",
  },
  {
    header: "Alpha",
    name: "unit_cell_alpha",
    type: "string",
  },
  {
    header: "Beta",
    name: "unit_cell_beta",
    type: "string",
  },
  {
    header: "Gamma",
    name: "unit_cell_gamma",
    type: "string",
  },
  {
    header: "Initial resolution",
    name: "initial_resolution",
    type: "string",
    description: "Resolution that the detector will be set to for mesh scans and default data collection or MXPressO",
  },
  {
    header: "Radiation-sensitivity",
    name: "radiation_sensitivity",
    type: "string",
    description: "BEST input in the case of highly radiation-sensitive or insensitive crystals (0.5–2.0 - low to high sensitivity)",
  },
  {
    header: "Required completeness",
    name: "required_completeness",
    type: "string",
    description: "% of Completeness required (value between 1 and 100)",
  },
  {
    header: "Required multiplicity",
    name: "required_multiplicity",
    type: "string",
    description: "Multiplicity of the final data set",
  },
  {
    header: "Number of positions",
    name: "number_positions",
    type: "string",
    description: "For multiple crystals on your support or for helical data collection (MXPressP) (Default: 1 (5 for MXPressP and P_SAD)",
  },
  {
    header: "Beam diameter",
    name: "beam_diameter",
    type: "string",
    description: "Select appropriate beam size for crystals (if blank will be dynamically adapted to crystal size) - value in µm",
  },
  {
    header: "Tot. Rot. Angle",
    name: "total_rotation_angle",
    type: "string",
    description:
      "Select required rotation range for data collection (Default: MXPressE - minimum required; MXPressE_SAD - 360°; MXPressO - 180°; MXPressF - 180°; MXPressP - 180°/positions; MXPressP_SAD - 360°/positions) ",
  },
];

const MXProcessingPlan = [
  {
    name: "pipelines",
    header: "Pipelines",
    type: "table",
    colored: true,
    description: "List of which pipelines should be automatically run on the data collected, and their parameters.",
    columns: [
      {
        name: "pipeline_name",
        header: "Pipeline name",
        type: "select",
        values: ["grenades_fastproc", "grenades_parallelproc", "EDNAproc", "autoProc", "XDSapp"],
      },
      {
        header: "Force S.G",
        name: "forceSpaceGroup",
        type: "string",
        description: "Select space group for strategy calculation and autoprocessing if cell not known or no PDB",
      },
      {
        header: "MR",
        name: "mr",
        type: "select",
        values: ["True", "False"],
        description: "Should we automatically run MR phasing on this pipeline's result?",
      },
      {
        header: "SAD",
        name: "sad",
        type: "select",
        values: ["True", "False"],
        description: "Should we automatically run SAD phasing on this pipeline's result?",
      },
      {
        header: "Reference reflection",
        name: "reference",
        type: "reflection",
        fileType: "pdb",
      },
      {
        header: "Search models",
        name: "search_models",
        type: "table",
        colored: true,
        columns: [
          {
            header: "PDB file",
            name: "pdb",
            fileType: "pdb",
          },
          {
            header: "PDB group",
            name: "pdb_group",
            type: "sampleFileGroup",
          },
        ],
      },
      {
        header: "UniProt IDs",
        name: "uniprot_ids",
        type: "string",
        description: "Comma-separated list of UniProt IDs.",
      },
      {
        header: "Smiles",
        name: "smiles",
        type: "string",
      },
      {
        header: "Program Arguments",
        name: "programArguments",
        type: "string",
        description: "Arguments passed to the processing pipeline",
      },
    ],
  },
  {
    name: "cutoffs",
    header: "Cutoffs",
    type: "table",
    colored: true,
    description: "List of cutoffs that should be applied for processing.",
    columns: [
      {
        name: "shell",
        header: "Shell",
        type: "select",
        values: [
          {
            label: "Inner",
            value: "inner",
          },
          {
            label: "Outer",
            value: "outer",
          },
          {
            label: "Overall",
            value: "overall",
          },
        ],
      },
      {
        name: "parameter",
        header: "Parameter",
        type: "select",
        values: [
          {
            label: "Completeness",
            value: "completeness",
          },
          {
            label: "Resolution low",
            value: "resolution_limit_low",
          },
          {
            label: "Resolution high",
            value: "resolution_limit_high",
          },
          {
            label: "Rmeas",
            value: "r_meas_all_IPlus_IMinus",
          },
          {
            label: "I/s(I)",
            value: "mean_I_over_sigI",
          },
          {
            label: "CC 1/2",
            value: "cc_half",
          },
          {
            label: "CC Ano",
            value: "cc_ano",
          },
        ],
      },
      {
        header: "Value",
        name: "value",
        type: "string",
      },
    ],
  },
  {
    name: "statistics_program",
    header: "Statistics Program",
    type: "select",
    values: ["MRFANA"],
  },
];

const MXContainersType = [
  { name: "SPINE", capacity: 10 },
  { name: "UNIPUCK", capacity: 16 },
  { name: "BOX", capacity: 1 },
];

const STATUS = {
  CREATED: "CREATED",
  SCHEDULED: "SCHEDULED",
  SENT: "SENT",
  STORES: "STORES",
  BEAMLINE: "BEAMLINE",
  BACK_STORES: "BACK_STORES",
  BACK_USER: "BACK_USER",
  ON_HOLD: "ON_HOLD",
  DESTROYED: "DESTROYED",
  REMOVED: "REMOVED",
};

const FlexHCDDualChangerType = [{ name: "FlexHCDDual" }];
const FlexHCDUnipuckPlate = [{ name: "FlexHCDUnipuckPlate" }];

/**
 * sample tracking configuration file
 */
module.exports = {
  STATUS,
  sampleChangerTypes: {
    "ID23-1": FlexHCDUnipuckPlate,
    "ID23-2": FlexHCDUnipuckPlate,
    "ID30A-1": FlexHCDUnipuckPlate,
    "ID30A-2": FlexHCDDualChangerType,
    "ID30A-3": MXContainersType,
    ID30B: FlexHCDDualChangerType,
  },
  containerTypes: {
    "ID23-1": MXContainersType,
    "ID23-2": MXContainersType,
    "ID30A-1": MXContainersType,
    "ID30A-2": MXContainersType,
    "ID30A-3": MXContainersType,
    ID30B: MXContainersType,
  },
  experimentPlan: {
    "ID23-1": MXExperimentPlan,
    "ID23-2": MXExperimentPlan,
    "ID30A-1": MXExperimentPlan,
    "ID30A-3": MXExperimentPlan,
  },
  processingPlan: {
    "ID23-1": MXProcessingPlan,
    "ID23-2": MXProcessingPlan,
    "ID30A-1": MXProcessingPlan,
    "ID30A-3": MXProcessingPlan,
  },
  qrCode: {
    server: "https://data.esrf.fr",
  },
  labels: {
    "ID23-1": "DewarModel",
    "ID23-2": "DewarModel",
    "ID30A-1": "DewarModel",
    "ID30A-2": "DewarModel",
    "ID30A-3": "DewarModel",
    ID30B: "DewarModel",
  },
  /** Address of the facility where the experiments will be conducted */
  facilityAddress: {
    name: "ESRF Stores",
    surname: "",
    address: "71 avenue des Martyrs",
    city: "Grenoble",
    postalCode: "38000",
    email: "",
    phoneNumber: "+33 (0)4 76 88 2733",
    fax: "+33 (0)4 76 88 2347",
    country: "France",
  },

  notifications: {
    enabled: process.env.TRACKING_ENABLED ? !!+process.env.TRACKING_ENABLED : true,
    fromAddress: "dataportal@esrf.fr",
    replyTo: "sampletransport@esrf.fr",
    smtp: {
      host: "smtp.esrf.fr",
      port: 25,
    },
    siteName: "ESRF",
    subjectPrefix: "ESRF Parcel Tracking",
    storesEmail: "sampletransport@esrf.fr",
    proposalTypeEmails: [
      { proposal: ["IN", "IM"], email: "industry@esrf.fr" },
      { proposal: ["IX", "FX", "OA"], email: "mxind@esrf.fr" },
    ],
    parcelURLTemplate: "https://data.esrf.fr/investigation/{investigationId}/logistics/parcel/{parcelId}",
    logisticsURLTemplate: "https://data.esrf.fr/investigation/{investigationId}/logistics",
    /**
     * statuses describe to whom the emails should be sent for each different status of a parcel
     */
    statuses: {
      [STATUS.SENT]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
      [STATUS.STORES]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: false,
        replyToRoles: ["Local contact"],
      },
      [STATUS.BEAMLINE]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: false,
        replyToRoles: ["Local contact"],
      },
      [STATUS.BACK_STORES]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
      [STATUS.BACK_USER]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
      [STATUS.ON_HOLD]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
    },
  },
};
