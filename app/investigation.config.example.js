/**
 * investigation configuration file
 */
module.exports = {
  sampleParameter: {
    // name of the type (ParameterType) which is used to store the sample description
    description: "Sample_description",
    // name of the type (ParameterType) which is used to store that the sample cannot be editable.
    // For example, at the ESRF, if the sample has the parameter Id, it cannot be editable.
    nonEditable: "Id",
    // name of the type (ParameterType) which is used to store the sample comment
    comment: "Sample_notes",
  },
  mintDOIEmail: {
    enabled: process.env.EMAIL_MINT_DOI_ENABLED ? !!+process.env.EMAIL_MINT_DOI_ENABLED : true,
    recipients: ["bodin@esrf.fr", "demariaa@esrf.fr", "andy.gotz@esrf.fr"],
    fromAddress: "icat@esrf.fr",
    subject: "[ESRF DOI minted] DOI minted",
    smtp: {
      host: "smtp.esrf.fr",
      port: 25,
    },
  },
};
