const icat = require("../api/icat.js");
const { ERROR, sendError } = require("../errors.js");
const { getInstrumentsBySessionId, getInstrumentsForUser, getInstrumentsForInstrumentScientist } = require("../api/icat.js");
const instrumentParser = require("../api/parsers/instrumentparser.js");

/**
 * Get all instruments
 */
exports.getInstruments = async (_req, res) => {
  try {
    const data = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.parameterListReader.user);
    const instruments = await icat.getInstrumentsBySessionId(data.sessionId);
    return res.send(instrumentParser.parse(instruments));
  } catch (error) {
    global.gLogger.error(error);
    res.status(ERROR.FAILED_TO_RETRIEVE_INSTRUMENTS.code).send(ERROR.FAILED_TO_RETRIEVE_INSTRUMENTS.message);
  }
};

/**
 * Returns all instruments linked to a user
 *    - if administrator: all instruments
 *    - if instrument scientists: all instruments linked to the instrumentScientist and all instruments linked to his/her investigations
 *    - all instruments linked to the user's investigations
 * @param {*} req
 * @param {*} res
 * @returns array of instruments
 */
exports.getInstrumentsBySession = async (req, res) => {
  const { session, params, query } = req;
  const { sessionId } = params;
  const { filter } = query;
  const { isAdministrator, name } = session;
  global.gLogger.debug("getInstruments. ", { sessionId, filter });
  try {
    let instruments = [];
    if (isAdministrator) {
      instruments = await getInstrumentsBySessionId(sessionId);
    } else {
      if (filter) {
        instruments = await getInstrumentsByRole(sessionId, name, filter);
      } else {
        const [instrumentsUser, instrumentsScientists] = await Promise.all([getInstrumentsForUser(sessionId, name), getInstrumentsForInstrumentScientist(sessionId, name)]);
        instruments = [...new Set([...instrumentsUser, ...instrumentsScientists])];
      }
    }
    return res.send(instrumentParser.parse(instruments));
  } catch (error) {
    return sendError(500, "Failed to getInstruments", error, res);
  }
};

async function getInstrumentsByRole(sessionId, username, role) {
  let instruments = [];
  switch (role.toUpperCase()) {
    case "PARTICIPANT":
      instruments = await getInstrumentsForUser(sessionId, username);
      break;
    case "INSTRUMENTSCIENTIST":
      instruments = await getInstrumentsForInstrumentScientist(sessionId, username);
      break;
    default:
      throw new Error(`Parameter filter is invalid. filter=${role}. Valid values:[PARTICIPANT|INSTRUMENTSCIENTIST]`);
  }
  return instruments;
}
