const request = require("request");
const { getDBByInvestigationId } = require("./helpers/panosc.parser");
const { asyncGetSession, DatasetAdapter } = require("../api/icat");
const { parse } = require("../api/parsers/datasetitemparser");

exports.request = (req, res) => {
  let { server, port } = req.params;

  server = server ? req.params.server : global.gServerConfig.panosc.search.server.url;
  port = port ? req.params.port : global.gServerConfig.panosc.search.server.port;

  const url = `${server}:${port}${req.url.replace("/hoa/", "/")}`; //`${global.gServerConfig.panosc.search.server.url}:${global.gServerConfig.panosc.search.server.port}${req.url}`;
  global.gLogger.info(`[SEARCH-API] Request: ${url}`);

  request(url)
    .on("error", (e) => {
      global.gLogger.error(`[SEARCH-API]${e.message}`);
      res.status(500).send({
        message: e.message,
      });
    })
    .pipe(res);
};

exports.getByInvestigationId = async (investigationId, parameters, res) => {
  global.gLogger.info(`[SEARCH-API] getByInvestigationId`, { investigationId });
  try {
    const db = await getDBByInvestigationId(investigationId, parameters);
    res.status(200).send(db);
  } catch (e) {
    global.gLogger.error(e);
    res.status(500).send(e);
  }
};

/**
 * Gets public datasets by date and then it parses into items for the scoring database
 * @param {*} startDate
 * @param {*} endDate
 * @param {*} response
 */
exports.getItemsByDate = async (startDate, endDate) => {
  global.gLogger.debug("getItemsByDate", { startDate, endDate });
  const data = await asyncGetSession(global.gServerConfig.icat.anonymous);
  const { sessionId } = data;
  const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { startDate, endDate });
  global.gLogger.debug(`Retrieved ${datasets.length} datasets`, { startDate, endDate });
  return parse(datasets);
};
