const { asyncGetSession, DatasetAdapter } = require("../api/icat.js");
const { parse } = require("../api/parsers/datasetdocumentparser.js");
const { sendError, ERROR } = require("../errors.js");
const { getParamAsEscapedLowerString, getSortOrder, chunk, isUndefined } = require("./helpers/helper.controller.js");
const OUTPUT_DATASET_IDS_PARAMETER_NAME = "output_datasetIds";
const FULL_OUTPUT_DATASET_IDS_PARAMETER_NAME = "__full_output_datasetIds";
const MAX_CHUNK_DATASETS_RETRIEVAL = 500;

/**
 * This method retrieves by changes a list of datasets based on the dataset Ids
 * @param {*} sessionId
 * @param {*} outputDatasetIdsList
 * @returns
 */
async function retrieveDatasetsByChunks(sessionId, datasetIds) {
  const result = await Promise.all(
    chunk(datasetIds, MAX_CHUNK_DATASETS_RETRIEVAL).map((datasetId) => {
      return DatasetAdapter.getDatasetsBy(sessionId, { datasetId });
    })
  );
  let outputDatasets = [];
  for (const outputDataset of result) {
    outputDatasets = outputDatasets.concat(outputDataset);
  }
  return outputDatasets;
}
/**
 * Returns a list of unique output datasetsIds
 * @param {*} datasets
 */
function getOutputDatasetIds(datasets) {
  let outputDatasetIdsList = datasets.map((dataset) => dataset.parameters.filter((p) => p.name === FULL_OUTPUT_DATASET_IDS_PARAMETER_NAME).map((p) => p.value)).flat();
  outputDatasetIdsList = Array.from(new Set(outputDatasetIdsList));
  /* this can give results like : [ "123957469", "123957450 123957485", "123957434" ] */
  return outputDatasetIdsList.map((e) => e.split(" ")).flat();
}
/**
 * This method will retrieve the datasets that are referred by the dataset parameter output_datasetIds
 * @param {*} sessionId
 * @param {*} datasets
 * @returns it returns the list of output datasets
 */
async function findOutputDatasets(sessionId, datasets) {
  let outputDatasetIdsList = getOutputDatasetIds(datasets);
  const outputDatasetLength = outputDatasetIdsList.length;
  let outputDatasets = await retrieveDatasetsByChunks(sessionId, outputDatasetIdsList);

  /** This is a patch until we understand the problem. I think that currrently it might be a problem with the ingester or with the ingestion done.
   * Problem is that __full_output_datasetIds from the acquisition datasets do not contains all datasets this is why it has to be done twice
   * The next lines could then be removed
   */
  outputDatasetIdsList = getOutputDatasetIds(outputDatasets);

  /* This prevents to retrieve twice when the same datasets **/
  if (outputDatasetLength === outputDatasetIdsList.length) {
    return outputDatasets;
  }
  const outputDatasetsTree = await retrieveDatasetsByChunks(sessionId, outputDatasetIdsList);
  for (const outputDataset of outputDatasetsTree) {
    outputDatasets = outputDatasets.concat(outputDataset);
  }

  return outputDatasets;
}

function fillOutputDatasetField(datasets) {
  /** Adding datasets to the list of datasets */
  datasets.forEach((ds) => {
    if (ds.parameters && ds.parameters.length > 0) {
      const output_datasetIds = ds.parameters.find((p) => p.name === OUTPUT_DATASET_IDS_PARAMETER_NAME);
      if (output_datasetIds) {
        ds.outputDatasets = datasets.filter((outputDataset) => output_datasetIds.value.indexOf(outputDataset.id) !== -1);
      }
    }
  });
  return datasets;
}
/**
 * Given a list of datasets this method will popupalte the ooutputDatasets array based on the output_datasetIds parameter
 * @param {*} sessionId
 * @param {*} datasets
 * @returns
 */
exports.populateOutputDatasets = async (sessionId, datasets) => {
  /** Getting output datasets and merging them*/
  const outputDatasets = await findOutputDatasets(sessionId, datasets);
  if (outputDatasets.length > 0) {
    fillOutputDatasetField(datasets.concat(outputDatasets));
    return datasets;
  }
  return datasets;
};

/**
 * this method retrieves the most important parameters of the datasets in order to build meanful statistics.
 * It reduces dramatically the length of the payload compared to retrieving by dataset
 */
exports.getDatasetsTimelineBy = async (sessionId, investigationId, sampleId, isAdministrator) => {
  global.gLogger.debug("getDatasetsTimelineBy. ", {
    sessionId,
    investigationId,
    sampleId,
    isAdministrator,
  });

  const timelines = isAdministrator
    ? await DatasetAdapter.getTimelineForAdminBy(sessionId, {
        investigationId,
        sampleId,
      })
    : await DatasetAdapter.getTimelineBy(sessionId, {
        investigationId,
        sampleId,
      });

  return timelines;
};

/**
 * Get datasets given different criteria. At least for an investigationId or datasetIds or sampleId
 */
exports.getDatasetsBy = async (req, res) => {
  try {
    const { datasetIds, sortBy, sortOrder, investigationIds, datasetType, sampleId, nested, parameters, instrumentName } = req.query;
    let { limit, skip, search } = req.query;
    const { sessionId } = req.params;
    limit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
    skip = parseInt(skip) || 0;
    search = getParamAsEscapedLowerString(search);

    if (isUndefined(investigationIds) && isUndefined(sampleId) && isUndefined(datasetIds) && isUndefined(instrumentName)) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }

    global.gLogger.debug("getDatasetsBy", {
      sessionId,
      datasetIds,
      investigationIds,
      sampleId,
      instrumentName,
      limit,
      skip,
      search,
      sortBy,
      sortOrder,
      datasetType,
      nested,
      parameters,
    });
    const formattedSortBy = getDatasetParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);

    let datasets = [];
    // retrieve all datasets for a given sample to speed up the request.
    if (sampleId && !datasetIds && !instrumentName && nested === "true" && datasetType === "acquisition" && req.query.limit === undefined) {
      datasets = await DatasetAdapter.getDatasetsBy(sessionId, {
        investigationIds,
        sampleId,
        search,
        sortBy: formattedSortBy,
        sortOrder: formattedSortOrder,
        parameters,
      });
      datasets = fillOutputDatasetField(datasets).filter((d) => d.type === datasetType);
    } else {
      datasets = await DatasetAdapter.getDatasetsBy(sessionId, {
        investigationIds,
        sampleId,
        instrumentName,
        limit,
        skip,
        search,
        sortBy: formattedSortBy,
        sortOrder: formattedSortOrder,
        datasetType,
        datasetId: datasetIds,
        parameters,
      });
      if (nested === "true") {
        datasets = await this.populateOutputDatasets(sessionId, datasets);
      }
    }

    res.send(datasets);
  } catch (error) {
    sendError(500, `Failed to get getDatasetsBy. ${error.message}`, error, res);
  }
};

function getDatasetParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "NAME":
        sortFilter = "dataset.name";
        break;
      case "STARTDATE":
        sortFilter = "dataset.startDate";
        break;
      case "SAMPLEDATE":
        sortFilter = "sample.modTime";
        break;
      case "SAMPLENAME":
        sortFilter = "sample.name";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[STARTDATE|SAMPLENAME|NAME|SAMPLEDATE]`);
    }
  }
  return sortFilter;
}

/**
 * Get dataset from a list of datasetId
 */
exports.getDatasetsById = async (req, res) => {
  try {
    const { sessionId } = req.params;
    const { datasetIds } = req.query;
    let datasets = await DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIds });
    /** This populates the output Datasets */
    datasets = await this.populateOutputDatasets(sessionId, datasets);

    res.send(datasets);
  } catch (error) {
    sendError(500, "Failed to getDatasetsById", error, res);
  }
};

/**
 * Get dataset documents from a list of datasetId
 */
exports.getDatasetsDocumentsById = async (req, res) => {
  try {
    const datasets = await DatasetAdapter.getDatasetsBy(req.params.sessionId, { datasetId: req.params.datasetIds });
    res.send(parse(datasets));
  } catch (error) {
    sendError(500, "Failed to getDatasetsById", error, res);
  }
};

/** Get a list of datasetDocuments filtered by start and end date.  Format of dates are YYYY-MM-DD */
exports.getDatasetsDocumentsByDateRange = async (req, res) => {
  const { sessionId, startDate, endDate } = req.params;
  global.gLogger.debug("getDatasetsDocumentsByDateRange", { startDate, endDate });
  try {
    const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { startDate, endDate });
    res.send(parse(datasets));
  } catch (error) {
    sendError(500, "Failed to getDatasetsDocumentsByDateRange", error, res);
  }
};

exports.getDatasetsByDOI = async (req, res) => {
  try {
    let { prefix, suffix } = req.params;
    const { limit, skip, search, sortBy, sortOrder } = req.query;
    const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
    const searchSkip = parseInt(skip) || 0;
    const searchText = getParamAsEscapedLowerString(search);

    /** Queries are case sensitive */
    prefix = prefix ? prefix.toUpperCase() : prefix;
    suffix = suffix ? suffix.toUpperCase() : suffix;

    global.gLogger.debug("getDatasetsByDOI. ", {
      prefix,
      suffix,
      searchLimit,
      searchSkip,
      searchText,
      sortBy,
      sortOrder,
    });
    const formattedSortBy = getDatasetParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);

    const data = await asyncGetSession(global.gServerConfig.icat.authorizations.notifier.user);
    const datasets = await findDatasetsByDOI(data.sessionId, `${prefix}/${suffix}`, searchLimit, searchSkip, searchText, formattedSortBy, formattedSortOrder);

    res.send(datasets);
  } catch (error) {
    sendError(500, "Failed to getDatasetsByDOI", error, res);
  }
};

async function findDatasetsByDOI(sessionId, doi, limit, skip, search, sortBy, sortOrder) {
  global.gLogger.debug("findDatasetsByDOI. ", { doi, limit, skip, search, sortBy, sortOrder });
  const hrstart = process.hrtime();
  const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { doi, limit, skip, search, sortBy, sortOrder });

  global.gLogger.debug("Final time to findDatasetsByDOI: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return datasets;
}
