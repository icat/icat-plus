const UserPreferences = require("../models/userpreferences.model.js");
const { isJSONString } = require("./helpers/helper.controller.js");

exports.getUserPreferences = async (session) => {
  const username = session.name;
  global.gLogger.debug(`getUserPreferences username = ${username}`);

  let preferences = await UserPreferences.find({ username }).lean();
  if (preferences.length > 0 && preferences[0]._id) {
    preferences = await UserPreferences.findById(preferences[0]._id);
  } else {
    preferences = new UserPreferences({ username });
  }
  return preferences;
};

function getInternalFields() {
  return ["createdAt", "updatedAt", "__v"];
}

exports.updateUserPreferences = async (session, body) => {
  const username = session.name;
  global.gLogger.debug(`updateUserPreferences for username = ${username} body=${JSON.stringify(body)}`);
  const set = {};
  Object.keys(body).forEach((field) => {
    if (getInternalFields().indexOf(field) === -1) {
      if (isJSONString(body[field])) {
        getInternalFields().forEach((f) => delete body[field][f]);
      }
      if (getInternalFields().indexOf(field) === -1) {
        set[field] = body[field];
      }
    }
  });

  const conditions = { username };
  const update = {
    $set: set,
  };
  const options = { upsert: true, omitUndefined: true };
  await UserPreferences.updateOne(conditions, update, options);
  const preferences = await this.getUserPreferences(session);

  return preferences;
};
