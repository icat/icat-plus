const axios = require("axios-https-proxy-fix");
const template = require("../doi/datacite.template.js");
const {
  InvestigationAdapter,
  asyncGetSession,
  getDataCollectionsByInvestigationId,
  asyncGetInvestigationUserBySessionId,
  updateDataCollectionDOI,
  mint,
  DatasetAdapter,
} = require("../api/icat.js");
const cache = require("../cache/cache.js");
const { sendError, ERROR } = require("../errors.js");
const { isUndefined, fromISOToYear, fromISOToDate, dateFormat } = require("./helpers/helper.controller.js");
const { isReleased } = require("./helpers/helper.logbook.controller.js");
const icat = require("../api/icat.js");
const { sendEmail } = require("./helpers/helper.email.js");

const PANET_URL = "http://purl.org/pan-science/PaNET/";

function getDataCiteSubjects(investigation, investigationDescription) {
  const subjects = [
    {
      subject: investigationDescription,
      subjectScheme: "Proposal Type Description",
    },
    {
      subject: investigation.name,
      subjectScheme: "Proposal",
    },
  ];
  if (investigation && investigation.instrument) {
    subjects.push({
      subject: investigation.instrument.name,
      subjectScheme: "Instrument",
    });
  }
  return subjects;
}

function formatUserName(user) {
  return `${user.familyName}, ${user.givenName}`;
}

function getDataCiteCreators(investigationUsers) {
  const creators = [];
  const creatorsName = [];

  investigationUsers
    .filter((investigationUser) => investigationUser.role === "Participant" || investigationUser.role === "Principal investigator")
    .map((investigationUser) => investigationUser.user)
    .sort((u1, u2) => (u1.familyName > u2.familyName ? 1 : -1))
    .forEach((user) => {
      if (!isUserInCreatorsList(user.name, creatorsName)) {
        creators.push({
          name: formatUserName(user),
          nameIdentifiers: getOrcidNameIdentifier(user.orcidId),
        });
        creatorsName.push(user.name);
      }
    });
  return { creators, creatorsName };
}

function getOrcidNameIdentifier(orcid) {
  return orcid
    ? [
        {
          schemeUri: "https://orcid.org",
          nameIdentifierScheme: "ORCID",
          nameIdentifier: `https://orcid.org/${orcid}`,
        },
      ]
    : undefined;
}

function addContributorsByRole(investigationUsers, creators, contributors, role, contributorType, excludeFromCreators = false) {
  investigationUsers
    .filter((investigationUser) => investigationUser.role === role)
    .map((investigationUser) => investigationUser.user)
    .filter((user) => !excludeFromCreators || !isUserInCreatorsList(user.name, creators))
    .sort((u1, u2) => u1.familyName.localeCompare(u2.familyName))
    .forEach((user) => {
      contributors.push({
        name: formatUserName(user),
        nameIdentifiers: getOrcidNameIdentifier(user.orcidId),
        contributorType,
      });
    });
}

function getDataCiteContributors(investigationUsers, creators) {
  const contributors = [];
  addContributorsByRole(investigationUsers, creators, contributors, "Local contact", "DataCollector", true);
  addContributorsByRole(investigationUsers, creators, contributors, "Principal investigator", "ProjectManager");
  addContributorsByRole(investigationUsers, creators, contributors, "Scientist", "ProjectMember");
  return contributors;
}

function isUserInCreatorsList(name, creators) {
  return creators.map((creator) => creator).indexOf(name) > -1;
}

function getRelatedIdentifier(dataCollection) {
  return {
    relatedIdentifier: dataCollection.doi,
    relatedIdentifierType: "DOI",
    relationType: "IsSupplementedBy",
  };
}

function getDataCiteJson(doi, investigation, relatedDataCollections) {
  const datacite = {};
  datacite.doi = doi;
  datacite.url = global.gServerConfig.datacite.landingPage + doi;
  datacite.publicationYear = fromISOToYear(investigation.releaseDate);
  datacite.creators = [];

  if (investigation.investigationUsers) {
    const { creators, creatorsName } = getDataCiteCreators(investigation.investigationUsers);
    datacite.creators = creators;
    datacite.contributors = getDataCiteContributors(investigation.investigationUsers, creatorsName);
  }

  datacite.titles = [
    {
      title: investigation.title,
    },
  ];

  datacite.descriptions = [
    {
      description: investigation.summary,
      descriptionType: "Abstract",
    },
  ];

  function getResourceType(doi) {
    if (doi.toUpperCase().indexOf("-DC-") > -1) {
      return "Datacollection";
    }
    return "Experiment Session";
  }

  datacite.types = {
    resourceTypeGeneral: "Dataset",
    resourceType: getResourceType(doi),
  };
  const investigationType = cache.getInvestigationTypes().find((o) => {
    return o.InvestigationType.name === investigation.type.name;
  });

  let investigationDescription = "";
  if (investigationType) {
    if (investigationType.InvestigationType) {
      investigationDescription = investigationType.InvestigationType.description;
    }
  }

  datacite.publisher = global.gServerConfig.datacite.publisher;
  datacite.subjects = getDataCiteSubjects(investigation, investigationDescription);

  datacite.dates = [
    {
      date: fromISOToDate(investigation.startDate, "yyyy-MM-dd"),
      dateType: "Collected",
    },
    {
      date: fromISOToYear(investigation.releaseDate),
      dateType: "Issued",
    },
  ];

  if (relatedDataCollections && relatedDataCollections.length > 0) {
    datacite.relatedIdentifiers = [];
    relatedDataCollections.forEach((dataCollection) => {
      datacite.relatedIdentifiers.push(getRelatedIdentifier(dataCollection));
    });
  }
  return datacite;
}

exports.getJSONDataciteByDOI = async (req, res) => {
  const { prefix, suffix } = req.params;
  const doi = `${req.params.prefix}/${suffix}`;
  try {
    if (prefix) {
      if (suffix) {
        global.gLogger.debug("getJSONDataciteByDOI", { doi });
        const auth = await asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
        const investigations = await InvestigationAdapter.getInvestigationsBy(
          auth.sessionId,
          { doi },
          "investigation.investigationUsers investigationUsers, investigationUsers.user user"
        );
        if (investigations && investigations.length > 0) {
          const inv = investigations[0];
          const relatedDataCollections = await getDataCollectionsByInvestigationId(auth.sessionId, inv.id);
          return res.status(200).send(getDataCiteJson(doi, inv, relatedDataCollections));
        }
        return res.status(500).send("No investigation found");
      }
    }
  } catch (e) {
    global.gLogger.error(e);
    return res.status(500).send();
  }
  return res.status(400).send();
};

/**
 * Return true if it investigationId corresponds to a participant of the investigation
 */
function isInvestigationInInvestigationUserList(investigationId, investigationUsers) {
  return investigationUsers.find((investigationUser) => investigationUser.investigationId === investigationId);
}

/**
 * returns the list of investigations and investigationUser for a given user (provided by the sessionId) and for a list of datasetIds
 * throws an error if the user cannot access to one of the datasets (not a participant of the corresponding investigation)
 * @param {sessionId} sessionId
 * @param {datasetIds} datasetIds
 */
exports.isParticipantByDatasetId = async (sessionId, datasetIds) => {
  const [datasets, investigationUsers] = await Promise.all([
    //InvestigationAdapter.getInvestigationsBy(sessionId, { datasetId: datasetIds }),
    DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIds }),
    asyncGetInvestigationUserBySessionId(sessionId),
  ]);
  if (investigationUsers.length === 0) {
    throw new Error(`investigationUsers is empty ${sessionId}`);
  }

  if (datasets.length === 0) {
    throw new Error(`No datasets found ${sessionId}`);
  }

  const investigationIds = datasets.map((dataset) => dataset.investigation.id); //getInvestigationsIds(investigations);

  /** Foreach investigationId we check that user is a participant*/
  for (let i = 0; i < investigationIds.length; i++) {
    if (!isInvestigationInInvestigationUserList(investigationIds[i], investigationUsers)) {
      throw new Error(`investigation is not in investigationUser list ${investigationIds[i]}`);
    }
  }

  return [datasets, investigationUsers];
};

exports.mint = async function (sessionId, datasets, name, fullName, params, res) {
  const { datasetIds, title, abstract, authors, reservedDataCollectionId, dataCollections, keywords, citationPublicationDOI, referenceURL } = params;
  global.gLogger.debug("Starts mint", {
    sessionId,
    datasetIds,
    title,
    abstract,
    authors: JSON.stringify(authors),
    dataCollectionId: reservedDataCollectionId,
    keywords: JSON.stringify(keywords),
    citationPublicationDOI,
    referenceURL,
  });

  try {
    const investigationIds = datasets.map((dataset) => dataset.investigation.id);
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId: investigationIds });

    const investigationNames = [...new Set(investigations.map((o) => o.name))];
    const instrumentsNames = [...new Set(investigations.map((o) => o.instrument.name))];

    const datasetTechniques = await icat.getDatasetTechniques(sessionId, datasetIds);
    const techniques = datasetTechniques
      .map((datasetTechnique) => datasetTechnique.DatasetTechnique.technique)
      .filter((value, index, self) => self.findIndex((v) => v.pid === value.pid) === index);
    global.gLogger.debug("[Mint] Allowed", { investigationIds, investigationNames, title, instrumentsNames });

    global.gLogger.debug("[Mint] Creating a datacollection", { datasetIds, title, abstract, investigationNames, instrumentsNames, name, fullName, reservedDataCollectionId });
    let dataCollectionId = reservedDataCollectionId;
    if (reservedDataCollectionId) {
      if (!dataCollections || dataCollections.length === 0) {
        const errorMessage = `[Mint] Error while minting from a reserved DOI, no dataCollection found! reservedDataCollectionId=${reservedDataCollectionId}`;
        global.gLogger.error(errorMessage);
        return sendError(500, "An error ocurred during the minting", errorMessage, res);
      }
      const dataCollection = dataCollections[0];
      await icat.createDataCollectionParameters(dataCollection, title, abstract, investigationNames, instrumentsNames, name, fullName);
    } else {
      dataCollectionId = await createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, name, fullName);
      dataCollectionId = String(dataCollectionId[0]);
      global.gLogger.debug("[Mint] Created datacollection", { dataCollectionId });
    }
    global.gLogger.debug("[Mint] Minting DOI", { dataCollectionId, title });

    const data = { title, abstract, authors, datasetIdList: datasetIds, investigationNames, instrumentsNames, techniques, keywords, citationPublicationDOI, referenceURL };
    const doi = await mintDOI(dataCollectionId, data);

    await updateDataCollectionDOI(global.gServerConfig.icat.authorizations.minting.user, parseInt(dataCollectionId), doi);
    await sendBespokeDOIEmail(doi, { title, abstract, authors, datasets, keywords, citationPublicationDOI, referenceURL });

    res.send({ message: doi });
  } catch (e) {
    global.gLogger.error(e.message);
    global.gLogger.error(e);
    sendError(500, "An error ocurred during the minting", e, res);
  }
};

function getContext(doi, doiData) {
  const { title, abstract, authors, datasets, keywords, citationPublicationDOI, referenceURL } = doiData;
  const doiDate = dateFormat(new Date(), "dd/MM/yyyy HH:mm");
  const listOfAuthors = authors.map((author) => (author.creatorName ? author.creatorName : `${author.surname}, ${author.name}`));
  const datasetInformation = datasets.map(
    (dataset) => `${dataset.investigation.name} ${dataset.investigation.investigationInstruments[0].instrument.name} ${dataset.name} ${dataset.location}`
  );
  return {
    doiDate,
    doi,
    title,
    abstract,
    authors: listOfAuthors,
    datasetInformation,
    keywords,
    citationPublicationDOI: citationPublicationDOI ? `https://doi.org/${citationPublicationDOI}` : undefined,
    referenceURL,
  };
}

async function sendBespokeDOIEmail(doi, doiData) {
  try {
    const { title, abstract, authors } = doiData;
    global.gLogger.debug("sendBespokeDOIEmail:", { doi, title, abstract, authors: JSON.stringify(authors) });
    const investigationConfig = global.gInvestigationConfig;
    const { enabled, fromAddress, subject, recipients, smtp } = investigationConfig.mintDOIEmail;

    const email = {
      from: fromAddress,
      to: recipients,
      subject,
      template: "email.doiminted",
      context: getContext(doi, doiData),
    };
    await sendEmail(email, smtp, enabled, "doi minted");
  } catch (error) {
    throw new Error(`Failed to send doi minted email ${error}`);
  }
}

/**
 * Create a datacollection in the metadata manager for given dataset
 * @param {*} minterCredential
 * @returns {Promise}
 */
async function createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) {
  const dataCollectionId = await mint(
    global.gServerConfig.icat.authorizations.minting.user,
    datasetIds,
    title,
    abstract,
    investigationNames,
    instrumentsNames,
    mintedByName,
    mintedByFullName
  );
  global.gLogger.debug("DatacollectionId has been created", { dataCollectionId });
  return dataCollectionId;
}

/**
 * Mint the DOI at datacite
 * @param {*} dataCollectionId dataCollection identifier
 * @param {*} data expect to have { title, abstract, authors, datasetIdList, investigationNames, instrumentsNames, techniques, keywords, citationPublicationDOI, referenceURL }
 * @return {Promise}
 */
async function mintDOI(dataCollectionId, data) {
  const { landingPage, username, password, proxy, mds } = global.gServerConfig.datacite;
  const { title, abstract, authors, datasetIdList, investigationNames, instrumentsNames, techniques, keywords, citationPublicationDOI, referenceURL } = data;
  const doi = buildDOI(dataCollectionId);
  const url = landingPage + doi;

  const doiData = { title, abstract, authors, investigationNames, instrumentsNames, techniques, keywords, citationPublicationDOI, referenceURL };
  const xmls = generateXML(doi, dataCollectionId, doiData);
  global.gLogger.info("Creation of the XML from the DOI to be minted as requested by datacite", {
    xmls,
    doi,
    title,
    abstract,
    datasetIdList,
    investigationNames,
    instrumentsNames,
    techniques: JSON.stringify(techniques),
  });
  // mint metadata
  await axios({
    method: "post",
    url: `${mds}/metadata`,
    headers: { "content-type": "application/xml", charset: "UTF-8" },
    auth: { username, password },
    proxy: { host: proxy.host, port: proxy.port },
    params: {},
    data: xmls,
  });

  global.gLogger.info("[SUCCESS] DOI metadata has been minted on datacite");
  // register doi
  await axios({
    method: "post",
    url: `${mds}/doi`,
    auth: { username, password },
    proxy: { host: proxy.host, port: proxy.port },
    params: { doi, url },
  });
  global.gLogger.info(`[SUCCESS] DOI has been registered on datacite: ${doi}`);
  return doi;
}

function isValidOrcid(orcid) {
  return orcid === undefined ? false : /^(\d{4}-){3}\d{3}(\d|X)$/.test(orcid);
}

function getOrcidXML(orcid) {
  return isValidOrcid(orcid) ? `<nameIdentifier schemeURI="http://orcid.org/" nameIdentifierScheme="ORCID">${orcid}</nameIdentifier>` : "";
}

function generateXML(doi, dataCollectionId, doiData) {
  const { title, abstract, authors, investigationNames, instrumentsNames, techniques, keywords, citationPublicationDOI, referenceURL } = doiData;
  /** This function is a string is blank */
  function isBlank(str) {
    return !str || /^\s*$/.test(str);
  }

  /** validation */
  if (authors) {
    if (authors.length === 0) {
      throw "List of authors is empty";
    }
  } else {
    throw "No authors found";
  }

  if (isBlank(title)) {
    throw "Title should not be empty";
  }

  if (isBlank(abstract)) {
    throw "Abstract should not be empty";
  }

  if (!dataCollectionId) {
    throw "It was not possible to create a data collection on ICAT";
  }

  /** Parsing authors */
  let authorsXML = "";
  for (let i = 0; i < authors.length; i++) {
    const orcid = getOrcidXML(authors[i].orcid);
    const creatorName = authors[i].creatorName ? authors[i].creatorName : `${authors[i].surname},${authors[i].name}`;
    authorsXML = `${authorsXML}<creator><creatorName nameType="Personal">${creatorName}</creatorName><givenName>${authors[i].name}</givenName><familyName>${authors[i].surname}</familyName>${orcid}</creator>`;
  }

  const xmlTechniques = getXmlTechniques(techniques);
  const xmlKeywords = getXmlKeywords(keywords);
  const relatedIndentifiers = getXmlRelatedIndentifiers(citationPublicationDOI, referenceURL);

  const releaseYear = new Date().getFullYear();
  const xml = template.xml
    .replace("%DOI%", doi)
    .replace("%PUBLICATION_YEAR%", new Date().getFullYear())
    .replace("%AUTHORS%", authorsXML)
    .replace("%TITLE%", title)
    .replace("%ABSTRACT%", abstract)
    .replace("%INSTRUMENT_NAME%", instrumentsNames)
    .replace("%INVESTIGATION_NAME%", investigationNames)
    .replace("%KEYWORDS%", xmlKeywords)
    .replace("%TECHNIQUES%", xmlTechniques)
    .replace("%RELEASE_YEAR%", releaseYear)
    .replace("%RELATEDIDENTIFIERS%", relatedIndentifiers);

  return xml;
}

function getXmlTechniques(techniques) {
  if (!techniques || techniques.length === 0) {
    return "";
  }
  return techniques.map((technique) => getXmlTechnique(technique)).join("\n");
}

function getTechniquePId(pid) {
  const separatorIndex = pid.indexOf(":");
  if (separatorIndex > -1) {
    return pid.substring(separatorIndex + 1, pid.length);
  }
  return pid;
}

function getXmlTechnique(technique) {
  const pid = getTechniquePId(technique.pid);
  return `<subject subjectScheme="PaNET" schemeURI="${PANET_URL}PaNET.owl" valueURI="${PANET_URL}${pid}" classificationCode="${pid}">${technique.name}</subject>`;
}

function getXmlKeywords(keywords) {
  if (!keywords || keywords.length === 0) {
    return "";
  }
  return keywords.map((keyword) => getXmlKeyword(keyword)).join("\n");
}

function getXmlKeyword(keyword) {
  return `<subject xml:lang="en">${keyword}</subject>`;
}

function getXmlRelatedIndentifiers(citationPublicationDOI, referenceURL) {
  if ((!citationPublicationDOI || citationPublicationDOI.trim().length === 0) && (!referenceURL || referenceURL.trim().length === 0)) {
    return "";
  }
  const citation = citationPublicationDOI ? `<relatedIdentifier relationType="IsCitedBy" relatedIdentifierType="DOI">${citationPublicationDOI}</relatedIdentifier>` : "";
  const reference = referenceURL ? `<relatedIdentifier relationType="References" relatedIdentifierType="URL">${referenceURL}</relatedIdentifier>` : "";
  return `<relatedIdentifiers>${citation} ${reference}</relatedIdentifiers>`;
}

/**
 * Returns the experimental reports information for a given DOI. Calls the User Portal to retrieve the list of reports
 * The DOI could be a Session DOI (only 1 proposal linked), or a DataCollection DOI (could have multiple proposals linked)
 * @param {*} req
 * @param {*} res
 * @returns for each proposal linked to the DOI: the proposal name, if experiment is under emmbargo, the number of reports and the list of reports
 */
exports.getExperimentalReportsByDOI = async (req, res) => {
  const { prefix, suffix } = req.params;
  const doi = `${req.params.prefix}/${suffix}`;
  global.gLogger.debug("getExperimentalReportsByDOI", { doi });
  try {
    if (isUndefined(prefix) || isUndefined(suffix)) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const auth = await asyncGetSession(global.gServerConfig.icat.authorizations.investigationReader.user);
    let investigations = [];
    if (doi.toUpperCase().indexOf("-DC-") > -1) {
      const datasets = await DatasetAdapter.getDatasetsBy(auth.sessionId, { doi });
      const allInv = datasets.map((dataset) => dataset.investigation);
      allInv.forEach((investigation) => {
        const inv = investigations.map((i) => i.id).filter((id) => id === investigation.id);
        if (inv.length === 0) {
          investigations.push(investigation);
        }
      });
    } else {
      investigations = await InvestigationAdapter.getInvestigationsBy(auth.sessionId, { doi });
    }
    if (investigations.length === 0) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
    }
    const reports = [];
    for (let i = 0; i < investigations.length; i++) {
      const investigation = investigations[i];
      const isUnderEmbargo = !isReleased(investigation);
      const expReports = await this.getExperimentalReports(investigation);
      const nbReports = expReports.length;
      reports.push({
        proposal: investigation.name,
        underEmbargo: isUnderEmbargo,
        nbReports,
        reports: expReports,
      });
    }
    return res.send(reports);
  } catch (e) {
    global.gLogger.error(e);
    return res.status(500).send();
  }
};

/**
 * Calls the User Portal web service to retrieve the experimental reports linked to a given investigation
 * @param {*} investigation
 * @returns
 */
exports.getExperimentalReports = async (investigation) => {
  const proposalCode = investigation.type.name;
  const proposalNumber = investigation.name.substring(proposalCode.length + 1);
  const beamline = investigation.instrument ? investigation.instrument.name : undefined;
  const sessionDate = dateFormat(investigation.startDate, "yyyy-MM-dd'T'HH:mm");
  const url = global.gServerConfig.experimentalReports.server;
  const params = new URLSearchParams();
  params.append("categoryCode", proposalCode);
  params.append("categoryCounter", proposalNumber);
  if (beamline && sessionDate) {
    params.append("beamline", beamline);
    params.append("date", sessionDate);
  }
  const response = await axios.get(`${url}?${decodeURIComponent(params.toString())}`);
  return response.data;
};

/**
 * Reserve a DOI for a list of datasets
 * @param {*} sessionId
 * @param {*} datasets
 * @param {*} investigationUser
 * @param {*} datasetIdList
 * @param {*} res
 */
exports.reserveDOI = async function (sessionId, datasets, investigationUser, datasetIdList, res) {
  const datasetIds = datasetIdList;
  const name = investigationUser.name;
  const fullName = investigationUser.fullName;
  global.gLogger.debug("[Reserve DOI]", { sessionId, datasetIds, name });

  try {
    const investigationIds = datasets.map((dataset) => dataset.investigation.id);
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId: investigationIds });

    const investigationNames = [...new Set(investigations.map((o) => o.name))];
    const instrumentsNames = [...new Set(investigations.map((o) => o.instrument.name))];

    global.gLogger.debug("[Reserve DOI] Allowed", { investigationIds, investigationNames, instrumentsNames });

    global.gLogger.debug("[Reserve DOI] Creating a datacollection", { datasetIds, investigationNames, instrumentsNames, name, fullName });
    let dataCollectionId = await createDataCollection(datasetIds, undefined, undefined, investigationNames, instrumentsNames, name, fullName);
    global.gLogger.debug("[Reserve DOI] Created datacollection", { dataCollectionId });
    dataCollectionId = String(dataCollectionId[0]);
    const doi = buildDOI(dataCollectionId);

    res.send({ message: doi });
  } catch (e) {
    global.gLogger.error(e.message);
    global.gLogger.error(e);
    sendError(500, "An error ocurred during the DOI reservation", e, res);
  }
};

function buildDOI(dataCollectionId) {
  const { prefix, suffix } = global.gServerConfig.datacite;
  return `${prefix}/${suffix}${dataCollectionId}`;
}

/**
 * Return the list of reserved DOIs based on the session
 * @param {*} sessionId
 * @returns
 */
exports.getReservedDois = async function (sessionId) {
  global.gLogger.debug(`getReservedDois sessionId = ${sessionId}`);
  const sessionInformation = await icat.asyncGetSessionInformation(sessionId);
  const username = sessionInformation.userName;
  global.gLogger.debug(`getReservedDois username = ${username}`);
  const data = await icat.DataCollectionAdapter.getDataCollectionsBy(sessionId, { hasNoDOIs: true, usernameParticipant: username });
  return data;
};

/**
 * Check the user from the session is a participant of the experiments linked to the datasets of a given dataCollection
 * @param {*} sessionId
 * @param {*} dataCollectionId
 * @returns
 */
exports.isParticipantByDataCollectionId = async (sessionId, dataCollectionId) => {
  const dataCollections = await icat.DataCollectionAdapter.getDataCollectionsBy(sessionId, { dataCollectionId, hasNoDOIs: true });

  if (dataCollections.length === 0) {
    throw new Error(`No dataCollections found ${sessionId}`);
  }

  const datasetIds = dataCollections[0].dataCollectionDatasets.map((item) => item.dataset.id);

  const [datasets, investigationUsers] = await this.isParticipantByDatasetId(sessionId, datasetIds);
  return [datasets, investigationUsers, dataCollections];
};
