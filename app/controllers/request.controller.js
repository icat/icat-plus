const Request = require("../models/request.model.js");
const { getSession } = require("../authentication/helper.authentication.js");
const { isUndefined } = require("./helpers/helper.controller.js");

/**
 * Calculates in ms the difference between start and now
 * @param {*} start
 * @returns
 */
const getDurationInMilliseconds = (start) => {
  const NS_PER_SEC = 1e9;
  const NS_TO_MS = 1e6;
  const diff = process.hrtime(start);

  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

/**
 * It fills the Request table if the duration > slowQueriesDuration
 * @param {*} sessionId
 */
async function saveRequest(start, req) {
  try {
    if (req.method === "OPTIONS") {
      return;
    }
    const durationInMilliseconds = getDurationInMilliseconds(start);
    const session = await getSession(req);
    if (session === null || isUndefined(session)) {
      return;
    }
    const { isAdministrator = null, isInstrumentScientist, username, sessionId } = session;
    const { investigationIds, sampleId, nested, datasetType, investigationId } = req.query;

    if (durationInMilliseconds > global.slowQueriesDuration) {
      global.gLogger.debug(`${req.method} ${unescape(req.originalUrl)} [CLOSED] ${durationInMilliseconds.toLocaleString()} ms`);
      await new Request({
        sessionId,
        method: req.method,
        url: req.originalUrl,
        isAdministrator,
        isInstrumentScientist,
        username,
        investigationIds,
        sampleId,
        nested,
        datasetType,
        investigationId,
        duration: durationInMilliseconds,
      }).save();
    }
  } catch (e) {
    global.gLogger.error(e);
  }
}

module.exports = {
  saveRequest,
};
