const { getDatasetParameterById, updateDatasetParameter, getDatasetParameter } = require("../api/icat.js");
const { ERROR } = require("../errors.js");
const { sendError } = require("../errors.js");
const { parse } = require("../api/parsers/datasetParameter.js");

async function getDatasetParameterBySessionId(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, includeDatasets, parameters }) {
  const data = await getDatasetParameter(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, includeDatasets, parameters });

  return data.map((datasetParameter) => {
    const { type, stringValue, id, dataset } = datasetParameter.DatasetParameter;

    return parse(
      type.name,
      stringValue,
      id ? id : "",
      type.units,
      dataset ? dataset.id : "",
      dataset ? (dataset.sample ? dataset.sample.id : undefined) : undefined,
      dataset ? (dataset.sample ? dataset.sample.name : undefined) : undefined,
      datasetParameter.DatasetParameter.createTime
    );
  });
}
/**
 *
 * @param {*} parametersValuesList {"name":"TOMO_energy","value":"75.0","id":1452806502,"units":"keV","datasetId":"","createTime":"2024-01-16T15:04:41.355+01:00"}
 */
function toCSV(arrays) {
  const datasetMap = new Map();
  const nameSet = new Set();

  // Group by datasetId and collect unique names
  arrays.forEach((arr) => {
    arr.forEach((item) => {
      if (!datasetMap.has(item.datasetId)) {
        datasetMap.set(item.datasetId, { createTime: item.createTime, values: {} });
      }
      datasetMap.get(item.datasetId).values[item.name] = item.value;
      nameSet.add(item.name);
    });
  });

  // Create CSV rows
  const names = Array.from(nameSet);
  const rows = [];

  datasetMap.forEach((value, key) => {
    const row = [key, value.createTime];
    names.forEach((name) => {
      row.push(value.values[name] || ""); // Fill with empty string if name not found
    });
    rows.push(row.join("\t"));
  });

  // Add header
  const header = ["datasetId", "createTime", ...names].join("\t");

  // Combine header and rows
  const csvContent = [header, ...rows].join("\n");
  return csvContent;
}

exports.getDatasetParameter = async (req, res) => {
  const { sessionId } = req.params;
  const { investigationId, name, sampleId, datasetType, instrumentName, startDate, endDate, format = "json" } = req.query;
  global.gLogger.debug("getDatasetParameter", { sessionId, investigationId, name, sampleId, datasetType, format });

  try {
    const parameters = name ? name.split(",") : [];
    if (parameters.length > 1 && format === "json") {
      return res.status(500).send("JSON format is not compatible with multiple metadata parameters");
    }
    if (parameters.length < 2 && format === "csv") {
      return res.status(500).send("Two parameter names are needed for csv");
    }
    /** includeDatasets will be slower but it is needed for csv */
    const includeDatasets = format === "csv";
    let result = [];
    if (parameters.length > 1) {
      /** do multiple parameters in parallel */
      result = await Promise.all(
        parameters.map((name) => getDatasetParameterBySessionId(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, includeDatasets }))
      );
    } else {
      result = await getDatasetParameterBySessionId(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, includeDatasets });
    }

    if (format === "json") {
      return res.send(result);
    }
    return res.send(toCSV(result));
  } catch (e) {
    sendError(500, "Failed to getDatasetParameter", e, res);
  }
};

/**
 * Extracts unique values from an array of result objects.
 *
 * This function takes an array of arrays of objects (results) and extracts the unique
 * 'value' field from each inner array. It returns an array where each element is an array
 * of unique values corresponding to each inner array of result objects.
 *
 * @param {Array<Array<DatasetParameter>>} results - An array of arrays of result objects. Each result object is expected to have a 'value' property.
 * @returns {Array<String>} - An array of sorted arrays, each containing the name of the parameter and an array of unique values
 */
function toValues(results) {
  const values = [];
  results.forEach((result) => {
    if (result.length > 0) {
      values.push({
        name: result[0].name,
        values: Array.from(new Set(result.map((r) => r.value))).sort((a, b) => a - b),
      });
    }
  });

  return values;
}
exports.getDatasetParameterValues = async (req, res) => {
  const { sessionId } = req.params;
  const { investigationId, name, sampleId, datasetType, instrumentName, startDate, endDate, parameters } = req.query;
  global.gLogger.debug("getDatasetParameterValues", { sessionId, investigationId, name, sampleId, datasetType, parameters });

  try {
    const names = name ? name.split(",") : [];

    let results = [];
    if (names.length > 1) {
      /** do multiple parameters in paralell */
      results = await Promise.all(
        names.map((name) => getDatasetParameterBySessionId(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, parameters }))
      );
    } else {
      const row = await getDatasetParameterBySessionId(sessionId, { investigationId, name, sampleId, datasetType, startDate, endDate, instrumentName, parameters });
      results.push(row);
    }

    return res.send(toValues(results));
  } catch (e) {
    sendError(500, "Failed to getDatasetParameter", e, res);
  }
};

/**
 * Update dataset parameter
 */
exports.updateParameter = async (sessionId, parameterId, value, res) => {
  global.gLogger.debug("updateParameter", { parameterId, value });

  if (!parameterId || !value) {
    return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
  }

  try {
    parameterId = parseInt(parameterId);
    const parameter = await getDatasetParameterById(sessionId, parameterId);
    if (parameter.length === 0) {
      return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
    }
    await updateDatasetParameter(sessionId, parameterId, value);
    res.send({ parameterId, value });
  } catch (error) {
    sendError(500, "Failed to Failed to update Parameter", error, res);
  }
};
