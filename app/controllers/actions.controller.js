const Event = require("../models/event.model.js");
const { InvestigationAdapter } = require("../api/icat.js");

/**
 * It fills the instrumentName for each log that has no instrumentName
 * @param {*} sessionId
 */
async function updateInstrumentName(sessionId, res) {
  /** Get events with no instrumentName, with investigationId and that is not an attachment */
  const query = { $and: [{ instrumentName: null }, { investigationId: { $ne: null } }, { type: { $ne: "attachment" } }] };

  /** Get logs where instrumentName is null and type is not attachment */
  const count = await Event.countDocuments(query);

  /** Get ids of the investigations of the logs with no instrumentName */
  const investigationIds = await Event.distinct("investigationId", query);

  global.gLogger.debug("Investigations that will be updated", { investigations: investigationIds.length });

  /** For each investigation update the events */
  for (let i = 0; i < investigationIds.length; i++) {
    try {
      const investigationId = investigationIds[i];
      const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
      if (investigations.length > 0) {
        const instrumentName = investigations[0].instrument.name;
        if (instrumentName) {
          /** Get the events with no instrumentName by investigationId so it will care gently of the database */
          const clone = JSON.parse(JSON.stringify(query));
          clone.$and.push({ investigationId });
          const result = await Event.updateMany(clone, { instrumentName: instrumentName.toUpperCase() }, { timestamps: false });
          global.gLogger.debug("Updated", {
            investigationName: investigations[0].name,
            investigationId,
            instrumentName,
            updated: result.nModified,
            matched: result.n,
          });
        }
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  }

  try {
    const missing = await Event.countDocuments(query);
    res.send({
      missing,
      count,
      updated: count - missing,
    });
  } catch (error) {
    global.gLogger.error(error);
    res.status(500).send("Failed to updateInstrumentName");
  }
}

/**
 * Do actions on the instrumentName field of the logbook
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function doActionOnInstrumentName(req, res) {
  const { sessionId } = req.params;
  const { action } = req.query;
  const { username, isAdministrator } = req.session;

  global.gLogger.debug("doActionOnInstrumentName", { sessionId, username, isAdministrator, action });

  switch (action) {
    case "fill":
      global.gLogger.debug("Do Fill");
      return res.send(await updateInstrumentName(sessionId, res));
    default:
      return res.status(500).send("Invalid action");
  }
}

module.exports = {
  doActionOnInstrumentName,
};
