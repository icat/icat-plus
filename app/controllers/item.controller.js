const Parcel = require("../models/parcel.model.js");
const Item = require("../models/item.model.js");
const helper = require("./helpers/helper.tracking.controller.js");
const { SampleAdapter } = require("../api/icat.js");

async function addItemToParcel(parcelId, investigationId, item, res) {
  try {
    const parcels = await Parcel.find({ _id: parcelId, investigationId }).populate("items");

    if (parcels.length === 0) {
      throw new Error("addItemToParcel: No parcel found");
    }
    const parcel = parcels[0];
    const savedItem = await new Item(item).save();

    parcel.items.push(savedItem);
    const newParcel = await Parcel.findOneAndUpdate({ _id: parcel._id }, { items: parcel.items }, { new: true }).populate({ path: "items", match: { status: { $ne: "REMOVED" } } });
    res.send(newParcel);
  } catch (error) {
    helper.sendError(500, "Failed to addItemToParcel", error, res);
  }
}

async function getSampleInformationBySampleId(sessionId, sampleId) {
  if (sampleId) {
    const samples = await SampleAdapter.getSamplesBy(sessionId, { sampleIds: sampleId });
    if (samples[0]) {
      return {
        sampleId: samples[0].id,
        sampleName: samples[0].name,
      };
    }
  }
  return {
    sampleId: null,
    sampleName: null,
  };
}
/**
 * If item is of type samplesheet then it gets the information of the sample from ICAt and fills sampleId and sampleName
 * otherwise the fields sampleId and sampleName are removed
 * @param {*} item
 * @returns
 */
async function getSampleSheetInformationByItem(sessionId, item) {
  //**Checks on items that are sample sheet */
  if (item.type === "SAMPLESHEET") {
    if (item.sampleId == null) {
      throw "addItem: No sampleId has been found on item of type sample sheet";
    }
    /** Case sample is a sample sheet */
    const { sampleId, sampleName } = await getSampleInformationBySampleId(sessionId, item.sampleId);

    item.sampleId = sampleId;
    item.sampleName = sampleName;
    global.gLogger.debug("SampleSheet found", {
      sampleId: item.sampleId,
      sampleName,
    });
  } else {
    item.sampleId = null;
    item.sampleName = null;
  }
  return item;
}

async function addItem(req, res) {
  const { sessionId, parcelId, investigationId } = req.params;
  let item = req.body;
  global.gLogger.debug("addItem", { sessionId, item: JSON.stringify(item), parcelId, investigationId });

  try {
    item = await getSampleSheetInformationByItem(sessionId, item);
    return await addItemToParcel(parcelId, investigationId, item, res);
  } catch (e) {
    helper.sendError(500, "Failed to addItem", e, res);
  }
}

async function updateItem(req, res) {
  const { sessionId, investigationId, parcelId, itemId } = req.params;
  global.gLogger.debug("updateItem", { sessionId, investigationId, parcelId, itemId, item: req.body });

  const { name, description, comments, sampleId, type } = req.body;

  try {
    const sampleInformation = await getSampleInformationBySampleId(sessionId, sampleId);

    const item = await Item.findOneAndUpdate(
      { _id: itemId },
      {
        name,
        description,
        comments,
        sampleId: sampleInformation.sampleId,
        sampleName: sampleInformation.sampleName,
        type,
      },
      { new: true }
    );
    res.send(item);
  } catch (error) {
    helper.sendError(500, "Failed to updateItem", error, res);
  }
}

async function deleteItem(req, res) {
  const { sessionId, investigationId, parcelId, itemId } = req.params;
  global.gLogger.debug("deleteItem", { sessionId, investigationId, parcelId, itemId });

  try {
    const response = await Parcel.updateOne({ investigationId, _id: parcelId }, { $pull: { items: itemId } }, { safe: true });
    if (!response.ok) {
      throw Error("UpdateOne from deleted item returned not ok");
    }
    const parcel = await helper.findParcels({ investigationId, _id: parcelId });
    const responseRemoved = await Item.deleteOne({ _id: req.params.itemId });
    if (!responseRemoved.ok) {
      throw Error("Delete item from collection returned not ok");
    }
    res.send(parcel);
  } catch (error) {
    helper.sendError(500, "Failed to deleteItem", error, res);
  }
}

module.exports = {
  addItem,
  updateItem,
  deleteItem,
};
