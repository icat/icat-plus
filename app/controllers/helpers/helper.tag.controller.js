const Tag = require("../../models/tag.model.js");

/**
 * Returns the investigation, instrument and global tags which name is passed as parameter
 * @param {*} investigationId id of the investigation of null if not investigation scope
 * @param {*} instrumentName name of the instrument or null if not instrument scope
 * @param {*} name name of the tag to be found
 * @returns
 */
exports.findTagByName = async (investigationId, instrumentName, name) => {
  const $or = [
    { name, investigationId: undefined, instrumentName },
    { name, investigationId: undefined, instrumentName: undefined },
  ];
  if (investigationId) {
    $or.push({ name, investigationId });
  }
  const data = Tag.find({ $or });
  return data;
};
