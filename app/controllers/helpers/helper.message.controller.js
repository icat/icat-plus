const Stomp = require("stomp-client");
const { dateFormat } = require("./helper.controller");

function sendMessageToMessageBroker(queueName, notification) {
  const config = global.gMessageConfig;
  try {
    if (config.enabled) {
      const stompClient = new Stomp(config.queueHost, config.queuePort);
      stompClient.connect(
        () => {
          stompClient.publish(`/queue/${queueName}`, JSON.stringify(notification));
          stompClient.disconnect();
        },
        (e) => global.gLogger.error("ERROR in sending message to message broker stompConnection", { message: e.message })
      );
    } else {
      global.gLogger.info("Broker messages are disabled and no message will be sent");
    }
  } catch (e) {
    global.gLogger.error("ERROR in sendMessageToMessageBroker", { message: e.message });
  }
}

exports.sendLogbookMessage = (event) => {
  const config = global.gMessageConfig;
  const tags = event.tag ? event.tag.map((t) => t.name) : [];
  const notification = {
    id: event._id,
    creationDate: dateFormat(event.creationDate, "yyyy-MM-ddTHH:mm:ss"),
    machine: event.machine,
    software: event.software,
    content: event.content,
    type: event.type,
    category: event.category,
    investigationName: event.investigationName,
    investigationId: event.investigationId,
    instrumentName: event.instrumentName,
    tags,
  };

  sendMessageToMessageBroker(config.logbookNotificationQueueName, notification);
};

exports.sendSyncUserMessage = (user) => {
  const config = global.gMessageConfig;
  const syncUser = {
    name: user.name,
    email: user.email,
    firstName: user.givenName,
    lastName: user.familyName,
  };

  sendMessageToMessageBroker(config.syncUserQueueName, syncUser);
};
