const Parcel = require("../../models/parcel.model.js");
const Address = require("../../models/address.model.js");
const Shipment = require("../../models/shipment.model.js");
const { InvestigationAdapter } = require("../../api/icat.js");
const { isUndefined } = require("../../controllers/helpers/helper.controller.js");
/**
 *
 * @param {*} shipment Shipment Mongoose object. Example: let shipment = new Shipment(req.body);
 * @param {*} investigationId
 * @param {*} sessionId
 */
async function createShipment(shipment, investigationId, sessionId) {
  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
  shipment.investigationName = investigations[0].name;
  shipment.investigationId = investigations[0].id;
  const newShipment = await new Shipment(shipment).save();
  const shipments = await findShipmentsBy({ investigationId, _id: newShipment._id });
  return shipments[0];
}

async function createAddress(address, investigationId, sessionId, username) {
  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
  if (investigations.length > 0) {
    address.investigationName = investigations[0].name;
    address.createdBy = username;
    address.investigationId = investigationId;
    const newAdress = await new Address(address).save();
    return newAdress;
  }
}

function sanitizeShipmentAddresses(shipment) {
  const sanitizedShipment = shipment;

  if (shipment.defaultReturnAddress && shipment.defaultReturnAddress.status === "REMOVED") {
    sanitizedShipment.set("defaultReturnAddress", undefined);
  }

  if (shipment.defaultShippingAddress && shipment.defaultShippingAddress.status === "REMOVED") {
    sanitizedShipment.set("defaultShippingAddress", undefined);
  }

  return sanitizedShipment;
}

async function findShipmentsBy(query) {
  const shipments = await Shipment.find(query).populate("defaultReturnAddress").populate("defaultShippingAddress").exec();
  return shipments.map(sanitizeShipmentAddresses);
}

function findNotRemovedShipmentsBy(query) {
  return findShipmentsBy({ ...query, status: { $ne: "REMOVED" } });
}

function sendError(code, message, e, res) {
  if (e) {
    global.gLogger.error(e);
    global.gLogger.error(message, {
      error: e.message,
    });
  }
  res.status(code).send({
    message: e.message || message,
  });
}

async function findNotRemovedParcels(query, skip, limit) {
  /** next statement has no effect AFAIK */
  query["statuses.status"] = { $ne: "REMOVED" };
  const parcels = await findParcels(query, skip, limit);
  return parcels;
}

async function findParcels(query, skip, limit) {
  try {
    skip = isUndefined(skip) ? 0 : parseFloat(skip);
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : parseFloat(limit);

    const parcels = await Parcel.find(query).skip(skip).limit(limit).sort({ _id: -1 }).populate({ path: "items" });
    return parcels.map((parcel) => parcel.toObject({ virtuals: true }));
  } catch (e) {
    global.gLogger.error(e);
    throw e;
  }
}

module.exports = {
  createShipment,
  createAddress,
  findShipmentsBy,
  findNotRemovedShipmentsBy,
  sendError,
  findNotRemovedParcels,
  findParcels,
};
