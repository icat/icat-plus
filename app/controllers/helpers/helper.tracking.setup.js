const { NO_INVESTIGATION_FOUND } = require("../../errors.js");
const { InvestigationAdapter, SampleAdapter } = require("../../api/icat.js");
const Parcel = require("../../models/parcel.model.js");
/**
 * Returns the setup (containerTypes, experiment and processing plan) needed to populate a parcel
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.getSetupConfigurationBy = async (sessionId, investigationId) => {
  const [investigations, samples] = await Promise.all([
    InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId }),
    SampleAdapter.getSamplesBy(sessionId, { investigationId }),
  ]);
  if (!investigations || investigations.length === 0) {
    throw new Error(NO_INVESTIGATION_FOUND.message);
  }
  // we need to return only the samplesheets `filter((s) => s.parameters.length > 0)`
  const sampleSheets = samples
    .filter((s) => s.parameters.find((p) => p.name === "Id"))
    .map((sample) => {
      return { label: sample.name, id: sample.id, value: sample.id };
    });

  const investigation = investigations[0];
  const instrumentName = investigation.instrument.name;

  return {
    sampleChangerType: global.gTrackingConfig.sampleChangerTypes ? global.gTrackingConfig.sampleChangerTypes[instrumentName] || [] : [],
    containerTypes: global.gTrackingConfig.containerTypes[instrumentName] || [],
    experimentPlan: global.gTrackingConfig.experimentPlan[instrumentName] || [],
    processingPlan: global.gTrackingConfig.processingPlan[instrumentName] || [],
    instrumentName,
    sampleDescription: [
      {
        header: "Type",
        name: "type",
        type: "select",
        required: true,
        colored: false,
        values: [
          { label: "SAMPLE", value: "SAMPLESHEET" },
          { label: "EQUIPMENT", value: "TOOL" },
          { label: "OTHER", value: "OTHER" },
        ],
      },
      {
        header: "Short name",
        name: "sampleId",
        type: "select",
        values: sampleSheets,
        description: "Short name is required for item type SAMPLE. Select one of the SAMPLE FORMS declared in the A form using the pull down menu.",
      },
      {
        header: "Name",
        name: "name",
        type: "string",
        required: true,
        colored: false,
        fillIncrement: true,
        description: "Unique name for each individual sample, tool or other item.",
      },
      {
        header: "Description",
        name: "description",
        type: "string",
        required: false,
      },
      {
        name: "comments",
        header: "Comments",
        type: "string",
      },
    ],
  };
};

/**
 * Compares two arrays of strings and returns the differences.
 *
 * @param {string[]} arr1 - The first array.
 * @param {string[]} arr2 - The second array.
 * @returns {string[]} - An array containing the differences between arr1 and arr2.
 */
function compare(arr1, arr2) {
  const diff = [];
  arr1.forEach((item) => {
    if (!arr2.includes(item)) {
      diff.push(item);
    }
  });
  return diff;
}

/**
 * This function checks that all experiment and processing parameters are valid. It will thrown an error if difference is found
 * @param {*} sessionId
 * @param {*} parcel
 */
exports.checkParcelParameters = async (sessionId, parcel) => {
  const setup = await this.getSetupConfigurationBy(sessionId, parcel.investigationId);
  const { experimentPlan, processingPlan } = setup;

  const objParcel = new Parcel(parcel);

  const diffExperiment = compare(
    objParcel.getExperimentPlanKeys(),
    experimentPlan.map((e) => e.name)
  );

  const diffProcessing = compare(
    objParcel.getProcessingPlanKeys(),
    processingPlan.map((e) => e.name)
  );

  if (diffExperiment.length > 0 || diffProcessing.length > 0) {
    throw new Error(
      JSON.stringify({ code: 400, message: `parameters are invalid:${diffExperiment} ${diffProcessing}`, experimentPlan: diffExperiment, processingPlan: diffProcessing })
    );
  }
};
