"use strict";

// const fs = require('fs');
const QRCode = require("qrcode");
const path = require("path");
const fs = require("fs");
const handlebars = require("handlebars");
const { ROLE_PRINCIPAL_INVESTIGATOR, ROLE_LOCAL_CONTACT } = require("../../constants");
const { dateFormat } = require("./helper.controller");
const { parse } = require("date-fns");
const wkhtmltopdf = require("wkhtmltopdf");

const serverConfig = global.gServerConfig;
const trackingConfig = global.gTrackingConfig;

const LOGO = `${serverConfig.server.url}/ESRF-Logo-CMYK.png`;
const LOGO_DEWAR = `${serverConfig.server.url}/pdf/dewarLabel.png`;
const LOGO_SCISSORS = `${serverConfig.server.url}/pdf/scissors.png`;
const LOGO_WARNING = `${serverConfig.server.url}/pdf/warning.png`;
const LOGO_PRINT = `${serverConfig.server.url}/pdf/printLabel.png`;
const LOGO_ARROW_UP = `${serverConfig.server.url}/pdf/arrowUp.png`;

function getAddress(address) {
  if (!address) return "";
  return {
    ...address,
    city: address.city.toUpperCase(),
    country: address.country ? address.country.toUpperCase() : "",
  };
}

function getHTML(shipment, parcel, investigation, shippingAddress, esrfAddress, investigationUsers) {
  const instrumentName = investigation.instrument.name;
  const labelsModel = global.gTrackingConfig.labels ? global.gTrackingConfig.labels[instrumentName] : null;
  const localContacts = getUsersWithRole(investigationUsers, ROLE_LOCAL_CONTACT);
  const mainProposer = getUsersWithRole(investigationUsers, ROLE_PRINCIPAL_INVESTIGATOR);
  const qrContent = `${trackingConfig.qrCode.server}/investigation/${investigation.id}/parcel/${parcel._id}?fromBarCode=true`;

  return QRCode.toDataURL(qrContent, { width: 110, margin: 0 }).then((qrCode) => {
    return labelsModel && labelsModel === "DewarModel"
      ? getHTMLForDewarModel(qrCode, shippingAddress, esrfAddress, investigation, parcel, localContacts, mainProposer, shipment)
      : getHTMLForGenericModel(qrCode, shippingAddress, esrfAddress, investigation, parcel, localContacts, shipment);
  });
}

function getHTMLForGenericModel(qrCode, shippingAddress, esrfAddress, investigation, parcel, localContacts, mainProposer, shipment) {
  const templatePath = path.join("./app/views/", "labels.handlebars");
  const templateContent = fs.readFileSync(templatePath, "utf-8");
  const template = handlebars.compile(templateContent);
  const html = template(getLabelsContext(investigation, qrCode, shippingAddress, esrfAddress, parcel, localContacts, mainProposer, shipment));
  return html;
}

function getLabelsContext(investigation, qrCode, shippingAddress, esrfAddress, parcel, localContacts, shipment) {
  const parcels = shipment.parcels.filter((parcel) => parcel.status !== "REMOVED");
  const localContactsArray = localContacts.split(",");
  return {
    facilityLogo: `${LOGO}`,
    qrCode,
    outwardFromAddress: getAddress(shippingAddress),
    parcelName: parcel.name,
    nbParcels: parcels.length,
    proposal: investigation.name,
    beamline: investigation.instrument.name,
    startDate: dateFormat(investigation.startDate, "dd-MM-yyyy"),
    localContacts: localContactsArray,
    facilityAddress: esrfAddress ? getAddress(esrfAddress) : "",
    containsDangerousGoods: parcel.containsDangerousGoods,
    storageConditions: parcel.storageConditions === "Other" ? parcel.comments : `${parcel.storageConditions}${parcel.comments ? ` - ${parcel.comments}` : ""}`,
    returnAddress: getAddress(parcel.returnAddress),
    hasMultipleParcels: parcels.length > 1,
    hasMultipleLocalContacts: localContactsArray.length > 1,
  };
}

function getLabelsDewarModelContext(investigation, qrCode, shippingAddress, esrfAddress, parcel, localContacts, mainProposer, shipment) {
  const parcels = shipment.parcels.filter((parcel) => parcel.status !== "REMOVED");
  const returnAddress = parcel.returnAddress;
  const puckNames = parcel.content.filter((item) => item.type === "CONTAINER").map((item) => item.name);
  const returnParcelInformation = parcel.returnParcelInformation;
  const formattedDate = dateFormat(parse(investigation.startDate, "yyyy-MM-dd'T'HH:mm:ss.SSSxxx", new Date()), "dd/MM/yyyy");
  const midpoint = Math.ceil(puckNames.length / 2);
  const pucks1 = puckNames.slice(0, midpoint);
  const pucks2 = puckNames.slice(midpoint);
  const nbPucks = parcel.content.filter((item) => item.type === "CONTAINER").length;
  const nbSamples = parcel.content.reduce((acc, container) => {
    return acc + container.content.filter((item) => item.type === "SAMPLESHEET").length;
  }, 0);
  return {
    facilityLogo: `${LOGO}`,
    dewarLogo: `${LOGO_DEWAR}`,
    scissorsLogo: `${LOGO_SCISSORS}`,
    warningLogo: `${LOGO_WARNING}`,
    printLogo: `${LOGO_PRINT}`,
    arrowUpLogo: `${LOGO_ARROW_UP}`,
    esrfAddress,
    beamline: investigation.instrument.name,
    proposalName: investigation.name,
    mainProposer,
    returnLab: returnAddress ? returnAddress.companyName : "",
    returnCity: returnAddress ? returnAddress.city : "",
    dewarName: `${parcel.name}`,
    sessionDate: `${formattedDate}`,
    localContacts,
    senderAddress: `${shippingAddress.name} ${shippingAddress.surname}, ${shippingAddress.companyName}, ${shippingAddress.city}, ${shippingAddress.country}`,
    senderPhone: `${shippingAddress.phoneNumber}`,
    nbParcels: parcels.length,
    returnLabContact: returnAddress ? `${returnAddress.name} ${returnAddress.surname}` : "",
    returnAddress: returnAddress ? `${returnAddress.companyName} - ${returnAddress.city}` : "",
    returnCountry: returnAddress ? `${returnAddress.country}` : "",
    courierCompany: returnParcelInformation ? returnParcelInformation.courierCompany : "",
    courierAccount: returnParcelInformation ? returnParcelInformation.courierAccount : "",
    instructions: parcel.returnInstructions ? parcel.returnInstructions : "n/a",
    plannedReturnDate: returnParcelInformation && returnParcelInformation.plannedPickupDate ? returnParcelInformation.plannedPickupDate : "n/a",
    qrCode,
    pucks1,
    pucks2,
    nbPucks,
    nbSamples,
  };
}

function getHTMLForDewarModel(qrCode, shippingAddress, esrfAddress, investigation, parcel, localContacts, mainProposer, shipment) {
  const templatePath = path.join("./app/views/", "labels.dewarmodel.handlebars");
  const templateContent = fs.readFileSync(templatePath, "utf-8");
  const template = handlebars.compile(templateContent);
  const html = template(getLabelsDewarModelContext(investigation, qrCode, shippingAddress, esrfAddress, parcel, localContacts, mainProposer, shipment));
  return html;
}

function getUsersWithRole(users, role) {
  if (users && users.length > 0) {
    const usersWithRole = users.filter((user) => user.role === role);
    if (usersWithRole) {
      const distinctFullNames = [...new Set(usersWithRole.map((user) => user.fullName))];
      return distinctFullNames.join(", ");
    }
  }
  return "";
}

async function getLabels(response, shipment, parcel, investigation, investigationUsers) {
  const esrfAddress = trackingConfig.facilityAddress;
  const shippingAddress = parcel.shippingAddress;

  try {
    const html = await getHTML(shipment, parcel, investigation, shippingAddress, esrfAddress, investigationUsers);
    const pdfBuffer = await generatePDF(html);

    const date = dateFormat(investigation.startDate, "dd-MM-yyyy");
    response.header("Content-Disposition", `attachment; filename="${parcel.name}-${parcel.investigationName}-${date}.pdf"`);
    pdfBuffer.pipe(response);
  } catch (error) {
    global.gLogger.error("PDF has not been sent", { error, stack: error.stack });
    response.status(500).send(`PDF generation failed ${error.stack}`);
  }
}

async function generatePDF(html) {
  const pdf = wkhtmltopdf(html, {
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    disableSmartShrinking: true, // use CSS width/height
    dpi: 300, // https://github.com/wkhtmltopdf/wkhtmltopdf/issues/3256#issuecomment-653282428
  });
  global.gLogger.debug("PDF has been sent");
  return pdf;
}

exports.getLabels = getLabels;
