const { asyncGetSession, getInvestigationUserByInvestigationId, DatasetAdapter, SampleAdapter, getDatafilesByDatasetId, InvestigationAdapter } = require("../../api/icat.js");
const { transform } = require("node-json-transform");
const { chunk } = require("./helper.controller.js");

/** Mapping between Dataset@ICAT to DatasetSample@PANOSC  */
const DatasetSampleMap = {
  item: { id: "id", datasetId: "id", sampleId: "sampleId" },
};

/** Mapping between Dataset@ICAT to Dataset@PANOSC  */
const DatasetMap = {
  item: {
    pid: "id",
    id: "id",
    title: "name",
    creationDate: "startDate",
    documentId: "investigation.doi",
    instrumentId: "investigation.investigationInstruments[0].instrument.id",
  },
  each(item) {
    item.isPublic = true;
    return item;
  },
};

/** Mapping between investigationUsers@ICAT and Member@PANOSC */
const MemberMap = {
  item: { id: "name", role: "role", personId: "name", documentId: "investigationId" },
};

/** Mapping between files@ICAT and File@PANOSC */
const FileMap = {
  item: { id: "Datafile.id", name: "Datafile.name", datasetId: "Datafile.datasetId", path: "Datafile.location", size: "Datafile.fileSize" },
};

/** Mapping between investigationUsers@ICAT and Person@PANOSC */
const PersonMap = {
  item: { id: "name" },
  each(item) {
    item.fullName = "name";
    return item;
  },
};

/** Mapping between sample@ICAT and sample@PANOSC */
const SampleMap = {
  item: {
    pid: "Sample.id",
    id: "Sample.id",
    name: "Sample.name",
  },
  each(item) {
    return item;
  },
};

/** Mapping between investigation@ICAT and document@PANOSC */
const DocumentMap = {
  item: {
    doi: "doi",
    id: "id",
    title: "title",
    summary: "summary",
    startDate: "startDate",
    endDate: "endDate",
    releaseDate: "releaseDate",
  },
  each(item) {
    if (item.doi) {
      item.doi.trim().length > 0 ? (item.pid = item.doi) : (item.pid = item.id);
    }
    item.isPublic = true;
    item.type = "proposal";
    item.license = "CC-BY-4.0";
    return item;
  },
};

/** Mapping between Instrument@ICAT and Instrument@PANOSC */
const InstrumentMap = {
  item: { pid: "id", id: "id", name: "name" },
  each(item) {
    item.facility = "ESRF";
    return item;
  },
};

const getDatasetsByInvestigations = async (sessionId, investigations, parameters) => {
  let datasetList = [];
  for (let i = 0; i < investigations.length; i++) {
    const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { investigationIds: investigations[i].id, parameters });
    datasetList = datasetList.concat(datasets);
  }
  return datasetList;
};

const getUsersByInvestigations = async (sessionId, investigations) => {
  let arr = [];
  for (let i = 0; i < investigations.length; i++) {
    const items = await getInvestigationUserByInvestigationId(sessionId, investigations[i].id);
    arr = arr.concat(items);
  }
  return arr;
};

const getSamplesByInvestigations = async (sessionId, investigations) => {
  let sampleList = [];
  for (let i = 0; i < investigations.length; i++) {
    const samples = SampleAdapter.getSamplesBy(sessionId, { investigationId: investigations[i].id });
    sampleList = sampleList.concat(samples);
  }
  return sampleList;
};

/** Returns the array mapped into the mapping passed as parameter and as a dictionary which key is PID*/
const parse = (arr, map, key) => {
  if (!key) key = "pid";
  return Object.assign({}, ...transform(arr, map).map((o) => ({ [o[key]]: o })));
};

const parseInstruments = (investigations) => {
  const instruments = investigations.map((investigation) => {
    if (investigation.instrument) {
      return investigation.instrument;
    }
    return null;
  });
  const instrumentIds = [
    ...new Set(
      instruments.map((instrument) => {
        return instrument.id;
      })
    ),
  ];
  const uniqueInstruments = instrumentIds.map((id) => instruments.find((i) => i.id === id));
  return parse(uniqueInstruments, InstrumentMap);
};

const getFiles = async (sessionId, datasets) => {
  const datasetIds = datasets.map((d) => d.id);
  const result = await Promise.all(
    chunk(datasetIds, 150).map((datasetId) => {
      return getDatafilesByDatasetId(sessionId, datasetId);
    })
  );
  const fileList = result.flat();
  fileList.forEach((file) => {
    file.Datafile.datasetId = file.Datafile.dataset.id;
  });
  return fileList;
};

const parseParameters = (datasets) => {
  const datasetParameters = [];
  datasets.forEach((dataset) => {
    for (const key in dataset.parameters) {
      datasetParameters.push(dataset.parameters[key]);
    }
  });

  const params = {};
  datasetParameters.forEach((p) => {
    params[p.id] = p;
  });

  return params;
};

exports.getDBByInvestigationId = async (investigationId, parameters) => {
  const { sessionId } = await asyncGetSession(global.gServerConfig.icat.authorizations.investigationReader.user);
  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId, parameters });
  return parseInvestigationsIntoPanoscModel(sessionId, investigations, parameters);
};

async function parseInvestigationsIntoPanoscModel(sessionId, investigations, datasetparameters) {
  const datasets = await getDatasetsByInvestigations(sessionId, investigations, datasetparameters);
  const samples = await getSamplesByInvestigations(sessionId, investigations);
  const datasetSample = parse(datasets, DatasetSampleMap, "id");
  const investigationUsers = await getUsersByInvestigations(sessionId, investigations);
  const files = await getFiles(sessionId, datasets);

  const parameters = parseParameters(datasets);

  return {
    ids: {
      AccessToken: 1,
      ACL: 1,
      Affiliation: 1,
      Dataset: datasets.length,
      DatasetSample: datasetSample.length,
      DatasetTechnique: 0,
      Document: investigations.length,
      File: files.length,
      Instrument: 16,
      Member: investigationUsers.length,
      Parameter: parameters.length,
      Person: investigationUsers.length,
      Role: 1,
      RoleMapping: 1,
      Sample: samples.length,
      Technique: 3,
      User: 1,
    },
    models: {
      AccessToken: {},
      ACL: {},
      File: parse(files, FileMap, "id"),
      Parameter: parameters,
      Document: parse(investigations, DocumentMap),
      Member: parse(investigationUsers, MemberMap, "id"),
      Person: parse(investigationUsers, PersonMap, "id"),
      Instrument: parseInstruments(investigations),
      DatasetSample: datasetSample,
      Sample: parse(samples, SampleMap),
      Dataset: parse(datasets, DatasetMap),
      DatasetTechnique: {},
      Technique: {},
      Affiliation: {},
    },
  };
}
