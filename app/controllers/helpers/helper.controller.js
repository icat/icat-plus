const { format, parse, isValid, parseISO } = require("date-fns");
const ObjectId = require("mongoose").Types.ObjectId;

exports.dateFormat = (strDate, strFormat) => {
  return format(new Date(strDate), strFormat);
};

exports.fromISOToYear = (date) => {
  return this.fromISOToDate(date, "yyyy");
};

exports.fromISOToDate = (date, format) => {
  const formattedDate = parseISO(date);
  return this.dateFormat(formattedDate, format);
};

/**
 * returns the string param as a date, undefined if not valid
 * @param {*} date
 * @param {*} withLastTimeOfDay if true add the last time of the day (23:59:59) at the end of the string
 * @returns the formatted date as `YYYY-MM-DD`, undefined if the date is not valid
 */
exports.getParamAsDateString = (date, withLastTimeOfDay) => {
  const dateFormat = "yyyy-MM-dd";
  const parsedDate = parse(date, dateFormat, new Date());
  if (!isValid(parsedDate)) {
    return undefined;
  }
  const formattedDate = format(parsedDate, dateFormat);
  return withLastTimeOfDay ? `${formattedDate} 23:59:59` : formattedDate;
};

/**
 * returns the string param as a time, undefined if not valid
 * @param {*} time
 * @returns
 */
exports.getParamAsTimeString = (time) => {
  const timeFormat = "yyyy-MM-dd'T'HH:mm:ss";
  const parsedTime = parse(time, timeFormat, new Date());
  if (!isValid(parsedTime)) {
    return undefined;
  }
  return format(parsedTime, timeFormat);
};

/**
 * returns the string in lower case and remove simple quote
 * @param {*} str
 * @returns the string in lower case, where simple quote is removed.
 */
exports.getParamAsEscapedLowerString = (str) => {
  return str ? str.toLowerCase().replace(/[']/gi, "") : "";
};

/**
 * returns the sortOrder 1 or -1 if sortBy is defined, -1 otherwise
 * @param {*} sortBy
 * @param {*} sortOrder
 * @returns 1 or -1
 */
exports.getSortOrder = (sortBy, sortOrder) => {
  return sortBy && sortOrder && (sortOrder === "1" || sortOrder === "-1") ? Number(sortOrder) : -1;
};

/**
 * Chunks an array by size
 * @param {*} array
 * @param {*} size
 * @returns
 */
exports.chunk = (array, size) => {
  const chunked_arr = [];
  for (let i = 0; i < array.length; i++) {
    const last = chunked_arr[chunked_arr.length - 1];
    if (!last || last.length === size) {
      chunked_arr.push([array[i]]);
    } else {
      last.push(array[i]);
    }
  }
  return chunked_arr;
};

/**
 * returns true if the value is undefined or equals to 'undefined'
 * @param {*} value
 * @returns
 */
exports.isUndefined = (value) => {
  return !value || value === "undefined";
};

exports.isJSONString = (str) => {
  try {
    JSON.parse(str);
    return true;
  } catch {
    return false;
  }
};

/** Converts a date as a string into an ObjectID see https://docs.mongodb.com/manual/reference/method/ObjectId/ */
exports.getObjectIdFromStrDate = (strDate) => {
  global.gLogger.debug(`getObjectIdFromStrDate strDate=${strDate}`);
  return ObjectId(parseDateToId(new Date(strDate)));
};

/**
 * Converts the date into id
 * see converter https://steveridout.github.io/
 */
function parseDateToId(date) {
  return `${Math.floor(date.getTime() / 1000).toString(16)}0000000000000000`;
}
