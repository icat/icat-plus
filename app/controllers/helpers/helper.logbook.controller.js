const { createModel } = require("mongoose-gridfs");
const fs = require("fs");
const Tag = require("../../models/tag.model.js");
const Event = require("../../models/event.model.js");
const CONSTANTS = require("../../constants");
const { ERROR } = require("../../errors.js");
const ObjectId = require("mongoose").Types.ObjectId;
const { InvestigationAdapter } = require("../../api/icat");
const cache = require("../../cache/cache.js");
const { findTagByName } = require("./helper.tag.controller.js");
const { EVENT_TYPE_BROADCAST } = require("../../constants");
const { isInvestigationUnderEmbargo } = require("../../helpers/investigation.js");
const { ExceptionHandler } = require("winston");
const { dateFormat, getObjectIdFromStrDate } = require("./helper.controller.js");
const { addDays, format } = require("date-fns");
/**
 * This is a helper that converts a comma separated list of type-category into an array of objects
 * Example:
 * Input: "annotation-comment, notification-error"
 * Output: [{type:"annotation", category:"comment"}, {type:"notification", category:"error"}]
 * @param {*} types
 */
exports.parseTypesToTypeCategory = (types) => {
  return types
    ? types.split(",").map((categoryRaw) => {
        const [type, category] = categoryRaw.toLowerCase().split("-");
        return {
          type,
          category,
        };
      })
    : [];
};

/** This is the way to fill automatically the fullName without need to use virtuals that will not work with the function lean() */
const fillFullName = (e) => {
  while (e) {
    e.fullName = cache.getUserFullNameByName(e.username);
    e = e.previousVersionEvent;
  }
  return e;
};

/*************************************************
 * DATABASE RELATED
 * ***********************************************/

exports.getEventPage = async (_id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder, tags, startTime, endTime) => {
  global.gLogger.debug("getEventPage", { _id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder, startTime, endTime });

  /** Otherwise the buildEventFindQuery will do (investigationId && instrumentName == undefined) */
  if (investigationId) {
    instrumentName = null;
  }

  if (!investigationId && !instrumentName) {
    instrumentName = null;
    investigationId = null;
  }

  // determine min and max event id to load broadcast events
  let minEventId;
  let maxEventId;
  if (this.isSearchingBroadcastEvents(types)) {
    [minEventId, maxEventId] = await this.getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
  }

  const totalEventsQuery = await this.buildEventFindQuery({ investigationId, filterInvestigation, instrumentName, types, date, minEventId, maxEventId, tags, startTime, endTime });
  const pageQuery = { $and: [{ _id: Number(sortOrder) === 1 ? { $lte: _id } : { $gte: _id } }, totalEventsQuery] };

  limit = parseFloat(limit) || global.gServerConfig.icat.maxQueryLimit;

  function getCountCriteria() {
    if (investigationId) return { investigationId };
    return { instrumentName };
  }
  /** Retrieve the number of events with the search, the result of the search with the skip/limit, and the whole logbook to calculate the indexes */
  const [total, position, events] = await Promise.all([Event.countDocuments(getCountCriteria()), this.count(pageQuery), Event.find({ _id }).populate("tag").lean()]);

  global.gLogger.debug("Response", { total, position, limit, events, index: Math.ceil(position / limit) });

  if (events && events.length === 1) {
    const e = events[0];
    fillFullName(e);
    // TODO index and limit can be removed when we using only data2 - index and currentPage are different as page (starting at 0 or 1)
    e.meta = {
      page: {
        total,
        limit,
        index: Math.ceil(position / limit),
        totalPages: Math.ceil(total / limit),
        currentPage: Math.floor(position / limit),
      },
    };

    return e.meta;
  }
};

/**
 * converts newlines to line breaks
 * #594: the lib used contains a bug, so we do the replacement here
 * @param {*} str
 * @param {*} isXhtml
 * @returns
 */
exports.nl2br = (str, isXhtml) => {
  const breakTag = isXhtml || typeof isXhtml === "undefined" ? "<br />" : "<br>";
  return `${str}`.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, `$1${breakTag}`);
};

/**
 * Translate a plain text formated event content to html formated event content when necessary.
 * This operates only on event content which have a plain text formated content  + no html formated content + plain text syntax which needs to be translated such as \n.
 * This concerns notifications or annotations sent from consoles.
 * @param {array} content original event content [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }]
 * @returns {array} updated event content; null when there is no content.
 * returned example: [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }, {format:'html', text: '<p>this is a comment on one line.</p><p>This is another line</p>' },]
 */
exports.translateEventContentToHtml = (content) => {
  /** If content comes as string then try to parse it */
  try {
    content = JSON.parse(content);
  } catch {
    //global.gLogger.warn("Object could not be converted to JSON");
  }
  if (content && content instanceof Array && content.length > 0) {
    if (!content.some((item) => item.format === "html")) {
      // there is no text in html format
      const plainTextEventContent = content.find((item) => item.format === "plainText");

      if (plainTextEventContent) {
        if (plainTextEventContent.text.split("\n").length > 1) {
          content.push({
            format: "html",
            text: this.nl2br(plainTextEventContent.text, false),
          });
        }
        return content;
      }
    }
    return content;
  }
  return null;
};

/**
 *
 * @param {*} find
 * @param {*} sort
 * @param {*} skip
 * @param {*} limit
 * @param {*} paginationMetadata True if the pagination metadata (number of page, indeces, etc...) will be included in the results
 * @param {*} findForTotalEvents
 * @returns
 */
exports.findEvents = async (find, sort, skip, limit) => {
  const startTime = process.hrtime(); // For measuring execution time

  skip = parseFloat(skip) || 0;
  limit = parseFloat(limit) || global.gServerConfig.icat.maxQueryLimit;

  global.gLogger.debug("[findEvents] Find clause is:", { find: JSON.stringify(find) });
  const events = await Event.find(find).populate("tag").sort(sort).skip(skip).limit(limit).lean();
  global.gLogger.debug("[findEvents] Database ops finished time to find: ", { events: events.length, find: JSON.stringify(find) });

  events.forEach((e) => {
    fillFullName(e);
  });

  global.gLogger.debug(`[findEvents] Find finished successfully. It took ${process.hrtime(startTime)[0]} seconds`, { find: JSON.stringify(find) });
  return events;
};

exports.count = async (find) => {
  const startTime = process.hrtime(); // For measuring execution time
  global.gLogger.debug("[countEvents] Find clause is:", { find: JSON.stringify(find) });
  const count = await Event.countDocuments(find);

  global.gLogger.debug(`[Count Events] Find finished successfully. It took ${process.hrtime(startTime)[0]} seconds`, { find: JSON.stringify(find) });
  return count;
};

/**
 * If tag already exists with same name in the scope (investigation, instrument or global) then tag will no be saved
 * otherwise a new tag will be created in teh database
 */
exports.saveTags = async (tags) => {
  global.gLogger.debug("saveTags", { tags: JSON.stringify(tags) });
  const savedTags = [];

  if (tags) {
    for (const tag of tags) {
      const name = tag.name.toLowerCase();
      const { description, investigationId, instrumentName, color } = tag;
      const tags = await findTagByName(investigationId, instrumentName, name);
      if (tags.length > 0) {
        savedTags.push(tags[0]);
      } else {
        const createdTag = await new Tag({ name, description, color, instrumentName, investigationId }).save();
        savedTags.push(createdTag);
      }
    }
  }
  return savedTags;
};

/**
 * Return the instrumentName attached to the investigation
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.getInstrumentNameByInvestigationId = async (sessionId, investigationId) => {
  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
    return investigations[0].instrument.name.toLowerCase();
  } catch {
    return null;
  }
};

/**
 * Given a mongoose event it will be saved in the database.
 * Category, type will be converted to lower case
 * Creation date will be set to now if null
 * @param {*} event
 * @returns The event created with the tags populated
 */
exports.saveEvent = async (event) => {
  if (event.previousVersionEvent) {
    if (event.type !== event.previousVersionEvent.type) {
      throw ERROR.TYPE_OF_EVENT_NOT_MODIFICABLE;
    }
    if (event.category !== event.previousVersionEvent.category) {
      throw ERROR.CATEGORY_OF_EVENT_NOT_MODIFICABLE;
    }
  }

  event = await event.save();
  event = await Event.populate(event, { path: "tag" });
  return event;
};

/**
 * This method creates a temporal file and will return the file path
 */
function getTemporalPathforBase64File(base64) {
  let temporalFolder = "/tmp";
  if (global.gServerConfig.server.temporalFolder) {
    temporalFolder = global.gServerConfig.server.temporalFolder;
  } else {
    global.gLogger.debug("Temporal Folder is not defined in the configuration file");
  }
  global.gLogger.debug("Using temporal folder", { path: temporalFolder });
  const filePath = `${temporalFolder}/${new Date().valueOf()}_camera.png`;
  global.gLogger.info("Creating temporal file", { filePath });
  /** Writting temporal file */
  fs.writeFile(filePath, base64.replace(/^data:image\/png;base64,/, ""), "base64", (error) => {
    if (error) {
      return global.gLogger.error(error);
    }
  });
  return filePath;
}

/**
 * Returns the URL of a pre-uploaded file
 * @param {string} req.params.sessionId session identifier
 * @param {string} fileId file identifier stored in mongo gridFS
 **/
function getFileDownloadURL(sessionId, fileId) {
  return `${global.gServerConfig.server.url}/resource/${sessionId}/file/download?resourceId=${fileId}`;
}

exports.createBase64Event = async (sessionId, investigationId, base64, datasetId, software, machine, username, fullName, investigationName, instrumentName, tag) => {
  const Attachment = createModel({
    modelName: "fs",
  });
  /** Creating stream */
  const temporalFilePath = getTemporalPathforBase64File(base64);
  global.gLogger.debug("Creating temporal file with base64", { temporalFilePath });
  global.gLogger.debug("Reading temporal file", { path: temporalFilePath });
  const readStream = fs.createReadStream(temporalFilePath);
  global.gLogger.debug("File is read", { path: temporalFilePath });

  readStream.on("error", (error) => {
    global.gLogger.error(error);
    throw new ExceptionHandler(error);
  });
  /** Wrapper to convert from promised based to await function */
  function writeFile(filename, contentType, readStream) {
    return new Promise((resolve, reject) => {
      Attachment.write({ filename, contentType }, readStream, (error, file) => {
        if (error) {
          global.gLogger.error("Error", error);
          return reject(error);
        }
        resolve({ error, file });
      });
    }).catch((error) => {
      return { error };
    });
  }
  const { error, file } = await writeFile(temporalFilePath, "application/octet-stream", readStream);
  if (error) {
    global.gLogger.error(error);
    throw new ExceptionHandler(error);
  }
  global.gLogger.debug("File is written by using gridFs", {
    id: file.id,
    investigationId,
    type: CONSTANTS.EVENT_TYPE_ATTACHMENT,
    category: CONSTANTS.EVENT_CATEGORY_FILE,
  });

  let attachment = new Event({
    investigationId,
    investigationName,
    datasetId,
    type: CONSTANTS.EVENT_TYPE_ATTACHMENT,
    category: CONSTANTS.EVENT_CATEGORY_FILE,
    text: "",
    tag,
    filename: temporalFilePath,
    username,
    software,
    machine,
    instrumentName,
    file: file._id,
    contentType: "application/octet-stream",
  });

  attachment = await this.saveEvent(attachment);
  fs.unlinkSync(temporalFilePath);
  global.gLogger.debug("Attachment is created in gridFS and removed temporal file", { id: file._id, temporalFilePath });

  let event = new Event({
    investigationId,
    investigationName,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    tag,
    username,
    previousVersionEvent: null,
    content: this.translateEventContentToHtml([
      {
        format: "plainText",
        text: "",
      },
      {
        format: "html",
        text: `<p><img src="${getFileDownloadURL(sessionId, attachment._id)}" /></p>`,
      },
    ]),
    instrumentName,
  });
  event = await this.saveEvent(event);
  global.gLogger.debug("Event has created and linked to attachment successfully", { id: event._id });
  event.username = fullName;
  return event;
};

/***************************************
 * Non-DB helpers
 ****************************************/
// Build the HTML text
function getTextColorByCategoryType(category, type) {
  if (type === CONSTANTS.EVENT_TYPE_BROADCAST) {
    return "black";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_INFO) {
    return "blue";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_ERROR) {
    return "red";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_COMMANDLINE) {
    return "gray";
  }
  if (category === CONSTANTS.EVENT_CATEGORY_DEBUG) {
    return "purple";
  }
  return "black";
}

// Build the HTML text

function getFontStyleByType(type) {
  if (type === CONSTANTS.EVENT_TYPE_BROADCAST) {
    return "italic";
  }
  return "normal";
}

function layoutEventInPDF(eventFormattedContent, date, event) {
  if (eventFormattedContent.text) {
    const dateColor = "#4D4D4D";
    const dateComponent = `<span style='color:${dateColor}'> [${format(date, "yyyy-MM-dd")}] </span>`;
    const timeComponent = `<span style='color:${dateColor}'> [${format(date, "HH:mm:ss")}] </span>`;

    return (
      `<div style='text-align:left;font-size:12pt;'>` +
      `<div style='float:left; padding-right:10px;font-size:10pt;'>${timeComponent}${dateComponent}</div>` +
      `<div style='color:${getTextColorByCategoryType(event.category, event.type)}; font-style: ${getFontStyleByType(
        event.type
      )}; word-break: break-word;' >${eventFormattedContent.text.replace(/"/g, '"')}</div>` +
      ` </div>`
    );
  }
}

/**
 * For notifications, the event will be serialized taken into account his comments
 * @param {*} event
 * @returns
 */
function getEventToSerialize(event) {
  const eventsToConvert = [];
  if (event) {
    // For notifications, get the original version of the event
    if (event.type === CONSTANTS.EVENT_TYPE_NOTIFICATION) {
      if (event.previousVersionEvent) {
        let originalVersion = event;
        try {
          while (originalVersion.previousVersionEvent) {
            originalVersion = originalVersion.previousVersionEvent;
          }
          eventsToConvert.push(originalVersion);
        } catch (e) {
          global.gLogger.error(e);
        }
      }
    }

    eventsToConvert.push(event);
  }
  return eventsToConvert;
}

/**
 * Get HTML formated text for a given event
 * @param {Event} event event to extract the HTML text from
 * @returns {string} html formated text
 */
exports.getHTMLTextByEvent = (event) => {
  let finalHTMLText = "<html><head><style>body {white-space:normal; } img {max-width: 100%;height: auto;}</style><meta charset='utf-8'></head><body>";
  if (event) {
    const eventsToConvert = getEventToSerialize(event);

    for (let i = 0; i < eventsToConvert.length; i++) {
      const event = eventsToConvert[i];

      if (event.content) {
        const HTMLContent = event.content.find((o) => {
          if (o.format) {
            return o.format === CONSTANTS.EVENT_CONTENT_HTML_FORMAT;
          }
          return null;
        });
        if (HTMLContent) {
          finalHTMLText = finalHTMLText + layoutEventInPDF(HTMLContent, new Date(event.creationDate), event);
        } else {
          const plainTextContent = event.content.find((o) => {
            if (o.format) {
              return o.format === CONSTANTS.EVENT_CONTENT_PLAINTEXT_FORMAT;
            }
            return null;
          });
          if (plainTextContent) {
            plainTextContent.text = `${plainTextContent.text}`;
            finalHTMLText = finalHTMLText + layoutEventInPDF(plainTextContent, new Date(event.creationDate), event);
          }
        }
        finalHTMLText += "<br/>";
      }
    }
    finalHTMLText += "</body></html>";
  }
  return finalHTMLText;
};

exports.replaceImgSrcInText = (text, sessionId, serverURI) => {
  if (!text) {
    return text;
  }
  /** The following regular expression:
   * extracts eventual presence of image style or other img options in p1
   * extracts eventId which needs to be updated into p2
   * input:
   * ....<img src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... style=.... />
   * OR ....<img style= ..... src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... />
   * Note: Extraction occurs everywhere the pattern is found in stringifiedEvents. Even when the serverURI is the same as what we expect, the replacement
   * occurs because the whole API has changed and needs to be replaced completely.
   */
  text = text.replace(/<img([^>]*)src="\S+\/investigations\/\d+\/events\/(\S+)\/file?\S+"/g, (match, p1, p2) => {
    if (match && p2) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download?resourceId=${p2}"`;
    }
  });

  // Replace img src using OLD API surounded by single quotes or \"
  text = text.replace(/<img([^>]*)src='?(?:")?\S+\/file\/id\/(\S+)\/investigation\S+'?(?:")?/g, (match, p1, p2) => {
    if (match && p2) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download?resourceId=${p2}"`;
    }
  });

  // Replace img src using last API without investigation Id "
  text = text.replace(/<img([^>]*)src='?(?:")?\S+\/file\/download/g, (match, p1) => {
    if (match && p1) {
      return `<img${p1}src="${serverURI}/resource/${sessionId}/file/download`;
    }
  });

  return text;
};

exports.replaceImageSrcInEvent = (event, sessionId, serverURI) => {
  if (event.content) {
    event.content.forEach((content) => {
      if (content.text) {
        content.text = this.replaceImgSrcInText(content.text, sessionId, serverURI);
      }
    });
  }
  if (event.previousVersionEvent) {
    this.replaceImageSrcInEvent(event.previousVersionEvent, sessionId, serverURI);
  }
};

/**
 * Rebuild the image src value with proper API and a valid sessionId for a set of events
 * @param {Array} events all events to be investigated
 * @param {String} sessionId new session identifier to be used in img src
 * @param {String} investigationId investigation identifier
 * @param {String} serverURI http protocol and servername to use example: https://icatplus.esrf.fr
 * @return {Array} updated events
 */
exports.replaceImageSrc = (events, sessionId, serverURI) => {
  events.forEach((event) => {
    this.replaceImageSrcInEvent(event, sessionId, serverURI);
  });
  return events;
};

function getEventAsPlainText(event) {
  const events = getEventToSerialize(event);

  const plainTextEvents = events.map((event) => {
    if (event.content) {
      const plainTextContent = event.content.find((c) => c.format === CONSTANTS.EVENT_CONTENT_PLAINTEXT_FORMAT);
      return `${dateFormat(event.creationDate, "yyyy-MM-dd HH:mm:ss")}: ${plainTextContent.text}`;
    }
    return "";
  });
  return plainTextEvents.reduce((accumulator, currentValue) => `${accumulator + currentValue}\n`, "");
}

/**
 * Converts events into a string
 * @param {*} events
 * @returns
 */
exports.toText = (events) => {
  let text = "";
  if (events) {
    if (events.length > 0) {
      events.forEach((event) => {
        text = `${text}${getEventAsPlainText(event)}`;
      }, this);
    }
  }
  return text;
};
/**
 * Concatenate all events into a single string as html format
 * @param {Array} events list of events to be concatenated
 * @param {string} serverURI protocol and server name of icat+
 * @returns {string} all events in HTML format
 */
exports.toHTML = (events, sessionId, serverURI) => {
  let html = "";
  if (events) {
    if (events.length > 0) {
      // replaces image src value with a new serverURI, new sessionId and using a new API
      events = this.replaceImageSrc(events, sessionId, serverURI);
      events.forEach(function (event) {
        const eventHTML = this.getHTMLTextByEvent(event);
        html = `${html}${eventHTML}`;
      }, this);
    }
  }
  return html;
};

/**
 * Get the serverURI
 * @param {object} req the request object
 * @returns {string} the server URI such as https://icatplus.esrf.fr:8000
 */
exports.getServerURI = (host, isSecure) => {
  return (isSecure ? "https://" : "http://") + host;
};

async function searchTagsByNameAndInvestigation(tagName, investigationId) {
  const tags = await Tag.find({ $and: [{ $or: [{ investigationId }, { investigationId: null }] }, { name: tagName }] });
  return tags;
}

/**
 * True if types contains type broadcast
 * @param {*} types
 * @returns
 */
exports.isSearchingBroadcastEvents = (types) => {
  return !types || types.toLowerCase().indexOf(EVENT_TYPE_BROADCAST) !== -1;
};

/**
 * builds the query to find broadcast events between 2 event ids, depending on different categories
 * @param {*} categories
 * @param {*} minEventId
 * @param {*} maxEventId
 * @param {*} search
 * @param {*} tags
 * @param {*} dateCondition
 * @param {*} starTimeCond
 * @param {*} endTimeCond
 * @returns
 */
exports.buildBroadcastQuery = (categories, minEventId, maxEventId, search, tags, dateCondition, startTimeCond, endTimeCond) => {
  const $and = [{ investigationId: null }, { instrumentName: null }];
  if (minEventId) {
    $and.push({ _id: { $gte: minEventId } });
  }
  if (maxEventId) {
    $and.push({ _id: { $lte: maxEventId } });
  }
  if (search) {
    $and.push({ $or: search });
  }
  const categoryFilter = categories
    .filter((category) => category.type === EVENT_TYPE_BROADCAST)
    .map((category) => {
      if (category.type && category.category) {
        return category;
      }
      return { type: category.type };
    });

  if (categoryFilter.length > 0) {
    $and.push({ $and: categoryFilter });
  } else {
    $and.push({ type: EVENT_TYPE_BROADCAST });
  }

  if (tags) {
    $and.push(tags);
  }
  if (dateCondition) {
    $and.push(dateCondition);
  }
  if (startTimeCond) {
    $and.push(startTimeCond);
  }
  if (endTimeCond) {
    $and.push(endTimeCond);
  }

  return { $and };
};

function getIds(items) {
  return items.map((i) => new ObjectId(i._id));
}

/**
 * build the find query to retrieve events depending on investigation or instrument and type of events and date
 * eventFilters = {investigationId, filterInvestigation, instrumentName, types, date, search, minEventId, maxEventId, tags, starTime, endTime}
 * @param {*} filters
 * @param {*} options
 * @returns
 */
exports.buildEventFindQuery = async (filters, options = { includeRemoved: false }) => {
  const { investigationId, filterInvestigation, instrumentName, types, date, search, minEventId, maxEventId, tags, startTime, endTime } = filters;
  const { includeRemoved } = options;

  let $and = [];

  if (investigationId) {
    $and.push({
      $or: [{ investigationId: parseFloat(investigationId) }],
    });
  }

  if (filterInvestigation === "true" && !investigationId) {
    $and = [{ investigationId: null }];
  }

  if (instrumentName && instrumentName !== undefined && instrumentName !== "undefined") {
    $and.push({ $or: [{ instrumentName: instrumentName.toLowerCase() }, { instrumentName: instrumentName.toUpperCase() }] });
  }

  const categories = this.parseTypesToTypeCategory(types);
  if (types) {
    $and.push({
      $or: categories.map((category) => {
        if (category.type && category.category) {
          return { type: category.type, category: category.category };
        }
        return { type: category.type };
      }),
    });
  }

  let dateCond;
  if (date) {
    const startDate = dateFormat(date, "yyyy-MM-dd");
    const endDate = addDays(new Date(startDate), 1);
    dateCond = { _id: { $gte: getObjectIdFromStrDate(startDate), $lt: getObjectIdFromStrDate(endDate) } };
    $and.push(dateCond);
  }

  let startTimeCond;
  if (startTime) {
    startTimeCond = { _id: { $gte: getObjectIdFromStrDate(dateFormat(startTime, "yyyy-MM-dd'T'HH:mm:ss")) } };
    $and.push(startTimeCond);
  }

  let endTimeCond;
  if (endTime) {
    endTimeCond = { _id: { $lte: getObjectIdFromStrDate(dateFormat(endTime, "yyyy-MM-dd'T'HH:mm:ss")) } };
    $and.push(endTimeCond);
  }

  let searchParameters;
  if (search) {
    /** Regular exp to search case insensitive */
    const regExp = new RegExp(`${search}(?![^<img].*/>)`, "i");
    /** Find tags that belongs to the investigations */
    const investigationTags = await searchTagsByNameAndInvestigation(regExp, investigationId);

    /** Create query to search in the content.text */
    searchParameters = ["content.text"].map((param) => {
      const obj = {};
      obj[param] = { $regex: regExp };
      return obj;
    });

    /* Adding the search in the tags **/
    if (investigationTags.length > 0) searchParameters.push({ tag: { $in: getIds(investigationTags) } });
    $and.push({ $or: searchParameters });

    global.gLogger.debug("Search query", { find: JSON.stringify($and) });
  }

  // search by tags
  let tagsCond;
  if (tags) {
    const tagRegex = tags.split(",").map((t) => new RegExp(t, "i"));
    const tagsWithName = await searchTagsByNameAndInvestigation({ $in: tagRegex }, investigationId);
    tagsCond = { tag: { $in: getIds(tagsWithName) } };
    $and.push(tagsCond);
  }

  /** Hide the removed events **/
  if (includeRemoved !== true) {
    $and.push({ $or: [{ removed: { $exists: false } }, { removed: false }] });
  }

  let find = { $and };

  if (this.isSearchingBroadcastEvents(types) && minEventId && maxEventId) {
    const broadcastQuery = this.buildBroadcastQuery(categories, minEventId, maxEventId, searchParameters, tagsCond, dateCond, startTimeCond, endTimeCond);
    find = { $or: [{ $and }, broadcastQuery] };
  }

  return find;
};

/**
 * retrieve all distinct dates of events that match a query
 * @param {*} query
 * @returns an array of {_id: "YYYY-MM-DD", event_count: number}
 */
const getDatesByQuery = async (query) => {
  const group = { $group: { _id: { $substr: ["$creationDate", 0, 10] }, event_count: { $sum: 1 } } };
  const match = { $match: query };
  let dates = await Event.aggregate([match, group, { $sort: { _id: -1 } }]);
  dates = dates.filter((d) => d._id !== "");
  return dates;
};

/**
 * retrieve all distinct dates of events that match a query
 * @param {*} eventFilters
 * @returns an array of {_id: "YYYY-MM-DD", event_count: number}
 */
exports.getDates = async (eventFilters) => {
  const { investigationId, filterInvestigation, instrumentName, types, search, minEventId, maxEventId, tags } = eventFilters;
  const query = await this.buildEventFindQuery({ investigationId, filterInvestigation, instrumentName, types, search, minEventId, maxEventId, tags });
  return getDatesByQuery(query);
};

exports.getMinAndMaxIdForBroadcastEvents = async (investigationId, instrumentName) => {
  const $and = [];

  if (investigationId) {
    $and.push({
      $or: [{ investigationId: parseFloat(investigationId) }],
    });
  }

  if (instrumentName) {
    $and.push({ $or: [{ instrumentName: instrumentName.toLowerCase() }, { instrumentName: instrumentName.toUpperCase() }] });
  }

  if ($and.length > 0) {
    const [minEvent, maxEvent] = await Promise.all([Event.find({ $and }).sort({ _id: 1 }).limit(1).lean(), Event.find({ $and }).sort({ _id: -1 }).limit(1).lean()]);
    return [minEvent.length > 0 ? minEvent[0]._id : undefined, maxEvent.length > 0 ? maxEvent[0]._id : undefined];
  }

  return [0, Number.MAX_SAFE_INTEGER];
};

/**
 * Returns true if the investigation is released
 * @param {*} investigation
 * @returns
 */
exports.isReleased = (investigation) => {
  return !isInvestigationUnderEmbargo(investigation);
};
