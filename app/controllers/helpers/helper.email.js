const nodemailer = require("nodemailer");
const config = global.gTrackingConfig;
const { getInvestigationUserByInvestigationId } = require("../../api/icat.js");
const STATUS = require("../../models/status.js");
const { ROLE_LOCAL_CONTACT } = require("../../constants.js");
const { dateFormat } = require("./helper.controller.js");
const path = require("path");
const hbs = require("nodemailer-express-handlebars");

function getSubject(investigation, parcel) {
  const siteName = config.notifications.siteName;
  let subject = `[${config.notifications.subjectPrefix}] `;

  switch (parcel.status) {
    case STATUS.SENT:
      subject = `${subject}Sent to ${siteName}`;
      break;
    case STATUS.STORES:
      subject = `${subject}Received at ${siteName} Stores`;
      break;
    case STATUS.BEAMLINE:
      subject = `${subject}Arrived on the Beamline`;
      break;
    case STATUS.BACK_STORES:
      subject = `${subject}Back to ${siteName} Stores`;
      break;
    case STATUS.BACK_USER:
      subject = `${subject}Sent to User`;
      break;
    case STATUS.ON_HOLD:
      subject = `${subject}On Hold at Stores`;
      break;
    default:
      subject = `${subject}Parcel changed status to ${parcel.status}`;
      break;
  }
  subject = `${subject}: ${investigation.name} ${investigation.instrument.name} ${formatDate(investigation.startDate)} ${parcel.name}`;
  return subject;
}

function formatDate(strDate) {
  return strDate ? dateFormat(strDate, "dd-MM-yyyy") : undefined;
}

function itemTypeToDisplay(itemType) {
  if (itemType === "SAMPLESHEET") {
    return "Sample";
  } else if (itemType === "TOOL") {
    return "Equipment";
  }
  return itemType;
}

function getContext(usersWithRoles, investigation, parcel) {
  const { status, returnAddress } = parcel;
  const siteName = config.notifications.siteName;
  const localContacts = getUsersByRoles(usersWithRoles, ROLE_LOCAL_CONTACT);
  const currentDate = formatDate(new Date(), "dd-MM-yyyy");

  let beforeParcelPresentation = "The parcel";
  let afterParcelPresentation = "changed status to ${parcel.status}.";
  let hasNotification = false;
  let hasReturnAddress = false;

  let returnAddressName = "";
  let returnAddressSurname = "";
  let returnAddressCompanyName = "";
  let returnAddressAddress = "";
  let returnAddressPostalCode = "";
  let returnAddressCity = "";
  let returnAddressRegion = "";
  let returnAddressCountry = "";

  if (returnAddress) {
    returnAddressName = returnAddress.name;
    returnAddressSurname = returnAddress.surname;
    returnAddressCompanyName = returnAddress.companyName;
    returnAddressAddress = returnAddress.address;
    returnAddressPostalCode = returnAddress.postalCode;
    returnAddressCity = returnAddress.city.toUpperCase();
    returnAddressRegion = returnAddress.region;
    returnAddressCountry = returnAddress.country ? returnAddress.country.toUpperCase() : "";
  }
  const parcelURL = config.notifications.parcelURLTemplate.replace("{investigationId}", investigation.id).replace("{parcelId}", parcel._id);
  const investigationURL = config.notifications.logisticsURLTemplate.replace("{investigationId}", investigation.id);

  const parcelContent = parcel.content.map((item) => ({
    name: item.name,
    type: itemTypeToDisplay(item.type),
  }));

  let hasContactContent = false;
  let hasLocalContactContent = false;
  let hasParcelReturn = false;
  const returnForwarderName = parcel.returnParcelInformation ? parcel.returnParcelInformation.forwarderName : "";
  const plannedPickupDate = parcel.returnParcelInformation ? formatDate(parcel.returnParcelInformation.plannedPickupDate) : "Not specified";
  let signature = `${siteName} stores`;
  const storesEmail = config.notifications.storesEmail;

  if (status === STATUS.SENT) {
    beforeParcelPresentation = "You have sent the parcel";
    afterParcelPresentation = ".";
    hasNotification = true;
    hasContactContent = true;
  }

  if (status === STATUS.STORES) {
    afterParcelPresentation = `has been received by the ${siteName} on ${currentDate}.`;
    hasLocalContactContent = true;
  }

  if (status === STATUS.BEAMLINE) {
    afterParcelPresentation = "<b> has ARRIVED on the BEAMLINE</b>.";
    hasLocalContactContent = true;
    signature = `${investigation.instrument.name} team`;
  }

  if (status === STATUS.BACK_STORES) {
    afterParcelPresentation = `<b>has arrived BACK at the ${siteName} Stores, and is ready to be shipped</b>.`;
    hasParcelReturn = parcel.returnParcelInformation && parcel.returnParcelInformation.returnType === "USER_ORGANIZE_RETURN";
  }

  if (status === STATUS.BACK_USER) {
    let forwarder = "";
    const returnParcelInformation = parcel.returnParcelInformation;
    const { forwarderName, forwarderAccount } = returnParcelInformation ? returnParcelInformation : {};
    if (returnParcelInformation && (forwarderName || forwarderAccount)) {
      forwarder = `with the forwarder ${forwarderName ? forwarderName : "(not defined)"} and the tracking number ${forwarderAccount ? forwarderAccount : "(not defined)"}`;
    }
    afterParcelPresentation = `<b>has been SENT to the RETURN ADDRESS you have defined</b> ${forwarder}.</p>`;
    hasReturnAddress = true;
    hasContactContent = true;
  }

  if (status === STATUS.ON_HOLD) {
    afterParcelPresentation = `is awaiting <b>the forwarder/transporter shipping return labels</b> to be sent back to you. <b>Please send them to</b> <a href="mailto:${storesEmail}">${storesEmail}</a> <b>to proceed.</b>`;
    hasContactContent = true;
  }

  return {
    beforeParcelPresentation,
    parcelName: parcel.name,
    investigationName: investigation.name,
    investigationStartDate: dateFormat(investigation.startDate, "dd-MM-yyyy"),
    instrumentName: investigation.instrument.name,
    afterParcelPresentation,
    hasNotification,
    siteName,
    hasReturnAddress,
    returnAddressName,
    returnAddressSurname,
    returnAddressCompanyName,
    returnAddressAddress,
    returnAddressPostalCode,
    returnAddressCity,
    returnAddressRegion,
    returnAddressCountry,
    parcelURL,
    investigationURL,
    localContacts,
    parcelContent,
    hasDangerousGoods: parcel.containsDangerousGoods,
    hasContactContent,
    hasLocalContactContent,
    hasParcelReturn,
    returnForwarderName,
    plannedPickupDate,
    signature,
    storesEmail,
  };
}

/**
 * Returns the list of users with a given role
 * @param {*} users
 * @param {*} roles
 * @returns
 */
function getUsersByRoles(users, roles) {
  return users.filter((user) => user.email !== null && roles.toString().toUpperCase().includes(user.role.toUpperCase()));
}

/**
 * Returns an array of emails for specific proposals
 * @param {*} investigation
 * @returns
 */
function getRecipientsForSpecificProposals(investigation) {
  const investigationName = investigation.name.toUpperCase();
  const recipients = [];
  for (const { proposal, email } of config.notifications.proposalTypeEmails) {
    if (proposal.some((code) => investigationName.startsWith(code.toUpperCase()))) {
      recipients.push(email);
    }
  }
  return recipients;
}

/**
 * Returns the list of recipients for the tracking emails, depending on the roles, and proposal types
 * @param {*} usersWithRoles
 * @param {*} parcel
 * @param {*} investigation
 * @returns
 */
function getRecipients(usersWithRoles, parcel, investigation, configStatus) {
  global.gLogger.debug("getRecipients", { parcelId: parcel._id, status: parcel.status, roles: config.notifications.statuses[parcel.status].roles });
  const storesEmail = [];
  if (configStatus.isEmailToStores) {
    storesEmail.push(config.notifications.storesEmail);
  }
  const recipients = [...usersWithRoles.map((user) => user.email), ...storesEmail, ...getRecipientsForSpecificProposals(investigation)];

  // Remove duplicates
  return [...new Set(recipients)];
}

function getReplyTo(configStatus, usersWithRoles) {
  return configStatus.replyToRoles ? getUsersByRoles(usersWithRoles, configStatus.replyToRoles).map((user) => user.email) : config.notifications.replyTo;
}

exports.sendParcelEmail = async (sessionId, investigation, parcel) => {
  const configStatus = config.notifications.statuses[parcel.status] || undefined;
  if (!configStatus) {
    global.gLogger.debug(`No email configured for the status ${parcel.status}. No email sent.`);
    return Promise.resolve(`No email configured for the status ${parcel.status}. No email sent.`);
  }
  const users = await getInvestigationUserByInvestigationId(sessionId, parcel.investigationId);
  const roles = configStatus.roles || [];
  const usersWithRoles = getUsersByRoles(users, roles);

  const email = {
    from: config.notifications.fromAddress,
    replyTo: getReplyTo(configStatus, usersWithRoles),
    to: getRecipients(usersWithRoles, parcel, investigation, configStatus),
    subject: getSubject(investigation, parcel),
    template: "email.parcel",
    context: getContext(usersWithRoles, investigation, parcel),
  };
  await this.sendEmail(email, config.notifications.smtp, config.notifications.enabled, "parcel");
};

/**
 * send email from a template
 * @param {*} email
 * @param {*} smtp
 * @param {*} enabled
 * @param {*} notificationType
 */
exports.sendEmail = async (email, smtp, enabled, notificationType) => {
  try {
    // point to the template folder
    const handlebarOptions = {
      viewEngine: {
        partialsDir: path.resolve("./app/views/"),
        defaultLayout: false,
      },
      viewPath: path.resolve("./app/views/"),
    };

    global.gLogger.debug(`Email=${JSON.stringify(email)}`);

    const transport = nodemailer.createTransport({
      host: smtp.host,
      port: smtp.port,
    });

    transport.use("compile", hbs(handlebarOptions));

    global.gLogger.debug("Email is about to be sent", JSON.stringify(email));

    if (enabled) {
      await transport.sendMail(email);
    } else {
      global.gLogger.info(`Email notifications for ${notificationType} are disabled and no email will be sent`);
    }
  } catch (error) {
    throw new Error(`Failed to send ${notificationType} email ${error}`);
  }
};
