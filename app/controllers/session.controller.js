const icat = require("../api/icat.js");
const cache = require("../cache/cache.js");
const { ERROR, sendError } = require("../errors.js");
/**
 * Get information about the session.
 * see doc for details on the corresponding API method /session/{sessionId}
 *
 * @param Object req the http request
 * @param Object res the http response
 * @param {function} next the next function
 */
exports.getSessionInformation = async (req, res) => {
  try {
    const { sessionId } = req.params;
    const data = await icat.asyncGetSessionInformation(sessionId);
    data.user = await cache.initSession(sessionId, data.userName);
    res.send(data);
  } catch (error) {
    return sendError(500, "Error when retrieving the session information. Your session might be expired or it is invalid.", error, res);
  }
};

/**
 * Login to ICAT+
 * see doc for details on the corresponding API method /session
 *
 * @param request the http request
 * @param response the http response
 * @param {function} next the next function
 */
exports.login = async (req, res) => {
  try {
    const { username, usernameSuffix, password, plugin } = req.body;
    const session = await icat.asyncGetSession({
      plugin,
      credentials: [
        {
          username,
        },
        { password },
        { usernameSuffix },
      ],
    });
    if (session.sessionId) {
      global.gLogger.info("Login done successfully. ", {
        username,
        plugin,
        usernameSuffix,
        origin: req.get("origin"),
        host: req.get("host"),
        ip: req.socket ? req.socket.remoteAddress : "",
      });
      const user = await cache.initSession(session.sessionId);

      return res.send(user);
    }
  } catch {
    return res.status(ERROR.LOGIN_FAILED.code).send(ERROR.LOGIN_FAILED.message);
  }
  return res.status(ERROR.LOGIN_FAILED.code).send(ERROR.LOGIN_FAILED.message);
};

/**
 * Does log out of the session
 * @param {*} req
 * @param {*} res
 */
exports.doLogout = async (req, res) => {
  try {
    const { sessionId } = req.params;
    global.gLogger.debug("Logged out", { sessionId });
    await icat.logout(sessionId);
    res.sendStatus(200);
  } catch (error) {
    return sendError(500, "Error when logging out. Your session might be expired or it is invalid.", error, res);
  }
};

/**
 * refreshes the icat session
 * @param {*} req
 * @param {*} res
 */
exports.refreshSession = async (req, res) => {
  try {
    const { sessionId } = req.params;
    global.gLogger.debug("refreshSession", { sessionId });
    const response = await icat.refreshSession(sessionId);
    const { status } = response;
    if (status < 300) {
      return res.status(response.status).send();
    }
    throw new Error();
  } catch (error) {
    return sendError(500, "Error when refreshing the session", error, res);
  }
};
