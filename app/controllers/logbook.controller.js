const wkhtmltopdf = require("wkhtmltopdf");
const { getInstrumentByName } = require("../cache/cache.js");
const Event = require("../models/event.model.js");
const investigationHelper = require("../helpers/investigation");
const { InvestigationAdapter, asyncGetSession } = require("../api/icat.js");
const investigationController = require("./investigations.controller.js");

const {
  getInstrumentNameByInvestigationId,
  createBase64Event,
  translateEventContentToHtml,
  saveEvent,
  saveTags,
  findEvents,
  getDates,
  buildEventFindQuery,
  getEventPage,
  getMinAndMaxIdForBroadcastEvents,
  isSearchingBroadcastEvents,
  count,
  isReleased,
} = require("./helpers/helper.logbook.controller.js");
const { ERROR } = require("../errors.js");
const { EVENT_TYPE_ANNOTATION, EVENT_TYPE_NOTIFICATION, EVENT_TYPE_BROADCAST } = require("../constants");
const { getParamAsDateString, isUndefined, getParamAsTimeString } = require("./helpers/helper.controller.js");
const { sendLogbookMessage } = require("./helpers/helper.message.controller.js");
const path = require("path");
const moment = require("moment-timezone");

/** Query to search event statistics based on start and end dates and type (annotation or notification) */
const STATS_QUERY = (start, end, type) => [
  { $match: { type, creationDate: { $gte: start, $lt: end } } },
  { $group: { _id: { instrumentName: "$instrumentName", investigationId: "$investigationId", investigationName: "$investigationName" }, count: { $sum: 1 } } },
];

/** Query counts the number of event per day */
const STATS__COUNT_QUERY = (start, end, type) => [
  { $match: { creationDate: { $gte: start, $lt: end }, type } },
  {
    $group: {
      _id: { $dateToString: { format: "%Y-%m-%d", date: "$creationDate" } },
      count: { $sum: 1 },
    },
  },
  {
    $sort: {
      _id: -1,
    },
  },
];

async function getEventStatistics(start, end) {
  const [all, annotations, notifications] = await Promise.all([
    await Event.aggregate([
      { $match: { creationDate: { $gte: start, $lt: end } } },
      {
        $group: {
          _id: { investigationId: "$investigationId" },
          count: { $sum: 1 },
        },
      },
    ]),
    await Event.aggregate(STATS_QUERY(start, end, EVENT_TYPE_ANNOTATION)),
    await Event.aggregate(STATS_QUERY(start, end, EVENT_TYPE_NOTIFICATION)),
  ]);

  const getCount = (annotation, annotations) => {
    const found = annotations.find((r) => {
      return r._id.investigationId === annotation._id.investigationId;
    });
    if (found) return found.count;
  };
  all.forEach((stats) => {
    stats.investigationId = stats._id.investigationId;
    const investigation = global.investigationsKeys[stats._id.investigationId];
    if (investigation) {
      const { title, parameters, startDate, endDate, name } = investigation;
      stats.title = title;
      stats.name = name;
      stats = Object.assign(stats, parameters);
      stats.startDate = startDate;
      stats.endDate = endDate;
      stats.instrument = investigation.instrument.name;
      stats.annotations = getCount(stats, annotations);
      stats.notifications = getCount(stats, notifications);
    }
  });

  all.sort((a, b) => a.investigationId - b.investigationId);
  return all;
}

/**
 * This method receives the json tag and stores in the database
 * @param {*} tag json object or string representation. Example: [{name : "myTag"}]
 * @param {*} investigationId optional
 * @param {*} instrumentName optional
 * @returns an array with the tags saved
 */
const createNotificationTag = async (tag, investigationId, instrumentName) => {
  let tags = [];
  if (tag) {
    try {
      tag = JSON.parse(tag);
    } catch {
      global.gLogger.debug("createNotificationTag. Could not convert list of tags to JSON", { tag });
    }
    /** Get unique elements of the tag list by name */
    const uniqueTagList = tag.filter((t, index, self) => index === self.findIndex((p) => p.name === t.name));

    uniqueTagList.forEach((tag) => {
      tag.name = tag.name.toLowerCase();
      if (investigationId) tag.investigationId = investigationId;
      if (instrumentName) tag.instrumentName = instrumentName;
    });
    tags = await saveTags(uniqueTagList);
  }
  return tags;
};

/**
 * Given an investigationId and a list of events, this method will set the field `removed` of each event to the value isRemoved
 * When an event is removed then event.removed = true otherwise event.removed should be false or not exist
 */
async function updateRemovedField(investigationId, events, isRemoved) {
  const removedEventList = [];
  for await (const event of events) {
    const doc = await Event.findOneAndUpdate(
      { investigationId, _id: event._id },
      { removed: isRemoved },
      {
        new: true,
      }
    );
    const { investigationName, removed, _id } = doc;
    global.gLogger.debug(`Event ${doc._id} has been flagged as removed`, { _id, investigationName, removed, investigationId });
    removedEventList.push(doc);
  }
  return removedEventList;
}
/**
 * This method will flag the field removed = true from the events passed as parameters if they belong to the investigation filtered by investigationId
 */
exports.remove = async (investigationId, events) => {
  global.gLogger.debug("remove events", { length: events.length });
  const removedEventList = await updateRemovedField(investigationId, events, true);
  return removedEventList;
};

/**
 * This method will flag the field removed = false from the events passed as parameters if they belong to the investigation filtered by investigationId
 */
exports.restore = async (investigationId, events) => {
  global.gLogger.debug("remove events", { length: events.length });
  const removedEventList = await updateRemovedField(investigationId, events, false);
  return removedEventList;
};

exports.getEventsCount = async (req, res) => {
  const { startDate, endDate, type } = req.query;
  global.gLogger.debug("getEventsCount", { startDate, endDate, type, query: JSON.stringify(STATS__COUNT_QUERY(new Date(startDate), new Date(endDate), type)) });
  if (!startDate || !endDate) {
    return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
  }
  try {
    const statistics = await Event.aggregate(STATS__COUNT_QUERY(new Date(startDate), new Date(endDate), type));
    res.send(statistics);
  } catch (e) {
    return res.send(e);
  }
};
/**
 * Endpoint to retrieve statistics of the events + the parameters of the investigation
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.getEventsStatistics = async (req, res) => {
  const { startDate, endDate } = req.query;
  if (!startDate || !endDate) {
    return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
  }

  try {
    const statistics = await getEventStatistics(new Date(startDate), new Date(endDate));
    res.send(statistics);
  } catch (e) {
    return res.send(e);
  }
};
/**
 * This method moves the events from one investigation to an other. It changes the investigationId, the investigationName and the instrumentName.
 * It returns the number of modified events
 * Example of returned information: { n: 2, nModified: 2, ok: 1 }

 * @param {*} req
 * @param {*} res
 */
exports.moveEvents = async (req, res) => {
  const { sourceInvestigationId, destinationInvestigationId } = req.query;
  const { sessionId } = await asyncGetSession(global.gServerConfig.icat.authorizations.investigationReader.user);

  global.gLogger.info("moveEvents ", { sourceInvestigationId, destinationInvestigationId });

  if (!sourceInvestigationId || !destinationInvestigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId: destinationInvestigationId });
    if (!investigations || investigations.length === 0) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send({ message: ERROR.NO_INVESTIGATION_FOUND.message });
    }
    let instrumentName = await getInstrumentNameByInvestigationId(sessionId, destinationInvestigationId);
    if (instrumentName) {
      instrumentName = instrumentName.toUpperCase();
    }

    const conditions = { investigationId: sourceInvestigationId };
    const update = {
      $set: {
        investigationId: destinationInvestigationId,
        investigationName: investigations[0].name,
        instrumentName,
      },
    };
    const options = { multi: true, upsert: false };
    const updatedEventsInformation = await Event.updateMany(conditions, update, options);
    res.send(updatedEventsInformation);
  } catch (e) {
    global.gLogger.error(e);
    res.status(500).send({
      message: e.message || "Some error occurred while moving events investigation",
    });
  }
};

exports.createTags = async (tag, investigationId, instrumentName) => {
  let tagsDocuments = null;
  try {
    if (tag) {
      tagsDocuments = await createNotificationTag(tag, investigationId, instrumentName);
      return tagsDocuments;
    }
  } catch (error) {
    global.gLogger.debug("Error during the creation of the tags", { error, tag });
  }
};

/**
 * This method will create an attachment and a annotation from a base64 encoded image
 * The event can be created in a session logbook (investigationId or {investigationName and instrumentName}) or a beamline logbook (instrumentName)
 * @param {string} sessionId session identifier
 * @param {string} base64Event = {investigationId, investigationName, instrumentName, username, fullName, base64, datasetId, software, machine, tag}
 * @param {string} res
 **/
exports.createfrombase64 = async (sessionId, base64Event, res) => {
  const { username, fullName, base64, datasetId, software, machine, tag } = base64Event;
  let { investigationId, investigationName, instrumentName } = base64Event;
  global.gLogger.info("createfrombase64", { investigationId, investigationName, instrumentName, username, fullName, datasetId, software, machine, tag });

  if (isUndefined(investigationId) && isUndefined(investigationName) && isUndefined(instrumentName)) {
    return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
  }
  if (!base64) {
    global.gLogger.error("createfrombase64 No base64 parameter", { base64 });
    return res.status(ERROR.NO_BASE64_BAD_PARAMS.code).send({ message: ERROR.NO_BASE64_BAD_PARAMS.message });
  }
  try {
    // Check if instrument exists
    if (instrumentName) {
      instrumentName = instrumentName.toUpperCase();
      if (!getInstrumentByName(instrumentName)) {
        return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
      }
    }
    // Check if investigation exists and is not released
    if (!isUndefined(investigationId) || !isUndefined(investigationName)) {
      const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId, investigationName, instrumentName });
      if (investigations.length === 0) {
        return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
      }
      investigationName = investigations[0].name;
      investigationId = investigations[0].id;
      if (isReleased(investigations[0])) {
        return res.status(ERROR.NO_ALLOWED_EDIT_EVENT.code).send({ message: ERROR.NO_ALLOWED_EDIT_EVENT.message });
      }
    }
    const tagsDocuments = await this.createTags(tag, investigationId, instrumentName);

    const eventCreated = await createBase64Event(
      sessionId,
      investigationId,
      base64,
      datasetId,
      software,
      machine,
      username,
      fullName,
      investigationName,
      instrumentName,
      tagsDocuments
    );
    res.send(eventCreated);
  } catch (e) {
    global.gLogger.error("createfrombase64", { error: e.message });
    res.status(500).send({ message: e.message || "Some error occurred while creating the Attachment" });
  }
};

/**
 * Creates an event in the logbook linked to an investigation or an instrument
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.query.investigationId investigation identifier
 * @param {string} req.query.instrumentName investigation identifier
 * @param {object} req.body event to be created in the logbook
 **/
exports.createEvent = async (req, res) => {
  try {
    const { name } = req.session;
    const { sessionId } = req.params;
    const { investigationId } = req.query;
    let { instrumentName } = req.query;
    const { datasetId, datasetName, tag, title, type, category, filename, fileSize, software, machine, creationDate, content, removed } = req.body;

    global.gLogger.info("createEvent", {
      investigationId,
      name,
      sessionId,
      instrumentName,
    });

    if (isNaN(investigationId) && !instrumentName) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
    }

    let investigationName = null;
    if (investigationId) {
      const investigation = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
      if (investigation.length > 0) {
        investigationName = investigation[0].name;
      }
      if (isReleased(investigation[0])) {
        return res.status(ERROR.NO_ALLOWED_EDIT_EVENT.code).send({ message: ERROR.NO_ALLOWED_EDIT_EVENT.message });
      }
    }
    /** Check instrumentName exists */
    if (instrumentName) {
      if (!getInstrumentByName(instrumentName.toUpperCase())) {
        return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
      }
      instrumentName = instrumentName.toUpperCase();
    }

    if (!isNaN(investigationId)) {
      instrumentName = await getInstrumentNameByInvestigationId(sessionId, investigationId);
      if (instrumentName) {
        instrumentName = instrumentName.toUpperCase();
      }
    }

    const event = new Event({
      investigationId,
      investigationName,
      datasetId,
      datasetName,
      tag,
      title,
      removed, // There are little chances that an event is created as removed but this is needed to the unit tests
      type,
      category,
      filename,
      fileSize,
      username: name,
      software,
      machine,
      instrumentName,
      creationDate,
      content: translateEventContentToHtml(content),
      previousVersionEvent: null,
    });

    const eventCreated = await saveEvent(event);
    sendLogbookMessage(eventCreated);
    res.send(eventCreated);
  } catch (e) {
    global.gLogger.error(e);
    res.status(500).send({
      message: e.message || "Some error occurred while creating the event",
    });
  }
};

function parseCreationDate(date) {
  if (date) {
    if (moment(date, moment.ISO_8601).isValid() || moment(date, moment.RFC_2822).isValid()) {
      return moment.tz(date, "Europe/Paris").toDate();
    }

    // the date is neither ISO_8601 nor RFC_2822 format. There is such a case in the tests 'Mon Mar 09 2020 15:24:38 GMT+0400 (CET)'
    return date;
  }
  return new Date();
}

/**
 * Create a new logbook event.
 * @param {string} req.params.instrumentName instrument name
 * @param {string} req.params.sessionId the magic token
 */
exports.createBeamlineNotification = async (req, res) => {
  const { instrumentName } = req.params;
  const { tag, creationDate, datasetName, title, type, category, datasetId, filename, fileSize, software, machine, username, content } = req.body;
  global.gLogger.info("createBeamlineNotification ", { instrumentName });

  if (isUndefined(instrumentName)) {
    return res.status(ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.message });
  }
  const localcreationDate = parseCreationDate(creationDate);
  try {
    const tags = await createNotificationTag(tag, null, instrumentName);
    try {
      const event = new Event({
        datasetId,
        datasetName,
        tag: tags,
        title,
        type,
        category,
        filename,
        fileSize,
        username,
        instrumentName: instrumentName.toUpperCase(),
        software,
        machine,
        creationDate: localcreationDate,
        content: translateEventContentToHtml(content),
        previousVersionEvent: null,
      });

      try {
        const eventCreated = await saveEvent(event);
        sendLogbookMessage(eventCreated);
        res.send(eventCreated);
      } catch (error) {
        global.gLogger.error("createBeamlineNotification. There was an error when saving", {
          error: error.message,
        });
        res.status(422).send({
          message: error.message || "Some error occurred while saving the event.",
        });
      }
    } catch (e) {
      global.gLogger.error("createBeamlineNotification saving event", { error: e.message });
      return res.status(400).send(`createBeamlineNotification: ${e.message}`);
    }
  } catch (e) {
    return res.status(400).send(e.message);
  }
};

/**
 * Create a new event without investigationId. Instead it uses investigationName and instrumentName.
 * @param {string} req.params.investigationName investigation name
 * @param {string} req.params.instrumentName instrument name
 * @param {string} req.params.sessionId the magic token
 */
exports.createInvestigationNotification = async (req, res) => {
  const { instrumentName } = req.params;
  let { investigationName } = req.params;

  const { tag, creationDate, datasetName, title, type, category, datasetId, filename, fileSize, software, machine } = req.body;
  global.gLogger.info("createInvestigationNotification ", { investigationName, instrumentName });

  if (isUndefined(investigationName)) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  try {
    investigationName = investigationHelper.normalize(investigationName);
  } catch {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
  }

  if (isUndefined(instrumentName)) {
    return res.status(ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.NO_INSTRUMENTNAME_BAD_PARAMS.message });
  }

  const localcreationDate = parseCreationDate(creationDate);

  try {
    const investigation = await investigationController.getInvestigationByNameAndInstrument(investigationName, instrumentName, localcreationDate);

    if (!investigation) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
    }

    if (isReleased(investigation)) {
      return res.status(ERROR.NO_ALLOWED_EDIT_EVENT.code).send(ERROR.NO_ALLOWED_EDIT_EVENT.message);
    }

    const { name, id } = investigation;
    const tags = await createNotificationTag(tag, investigation.id, instrumentName);
    // Create a Event
    const event = new Event({
      investigationName: name,
      investigationId: id,
      datasetId,
      datasetName,
      tag: tags,
      title,
      type,
      category,
      filename,
      fileSize,
      username: req.body.username,
      instrumentName: instrumentName.toUpperCase(),
      software,
      machine,
      creationDate: localcreationDate,
      content: translateEventContentToHtml(req.body.content),
      previousVersionEvent: null,
    });

    try {
      const eventCreated = await saveEvent(event);
      sendLogbookMessage(eventCreated);
      res.send(eventCreated);
    } catch (error) {
      global.gLogger.error("createInvestigationNotification. There was an error when saving", {
        error: error.message,
      });
      res.status(422).send({
        message: error.message || "Some error occurred while saving the event.",
      });
    }
  } catch (e) {
    global.gLogger.error("createInvestigationNotification", { message: e.message });
    return res.status(400).send(`createInvestigationNotification ${e.message}`);
  }
};

/**
 * Update an event
 * @param {string} req.params.investigationId investigation identifier
 * @param {string} req.params.sessionId sessin identifier
 * @param {string} req.body.event the updated event
 */
exports.updateEvent = async (req, res) => {
  const { sessionId } = req.params;
  const { investigationId, instrumentName } = req.query;
  const { name } = req.session;
  const event = req.body;

  global.gLogger.debug("updateEvent", { investigationId, name, instrumentName });
  if (!investigationId && !instrumentName) {
    return res.status(400).send({ message: "investigationId and instrumentName can not be empty" });
  }

  if (!event) {
    return res.status(400).send({ message: "Event can not be empty" });
  }

  if (!event._id) {
    return res.status(400).send({ message: "_id can not be empty" });
  }

  // We are sure that the event has been allowed by the middleware
  event.investigationId = investigationId;

  try {
    if (investigationId) {
      const investigation = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
      if (isReleased(investigation[0])) {
        return res.status(ERROR.NO_ALLOWED_EDIT_EVENT.code).send({ message: ERROR.NO_ALLOWED_EDIT_EVENT.message });
      }
    }

    const currentEventVersion = await Event.findById(event._id).populate("tag");

    global.gLogger.info("Found event to modify.", {
      _id: currentEventVersion._id,
    });
    // create a copy of the current version as it is curretnyl stored in the DB
    const currentEventVersionCopy = JSON.parse(JSON.stringify(currentEventVersion));

    // update event fields
    currentEventVersion.investigationId = event.investigationId || null;
    currentEventVersion.datasetId = event.datasetId || null;
    currentEventVersion.tag = event.tag || [];
    currentEventVersion.title = event.title || "";
    currentEventVersion.type = event.type || null;
    currentEventVersion.category = event.category || null;
    currentEventVersion.filename = event.filename || null;
    currentEventVersion.fileSize = event.fileSize || null;
    currentEventVersion.username = name;
    currentEventVersion.software = event.software;
    currentEventVersion.machine = event.machine;
    currentEventVersion.creationDate = event.creationDate;
    currentEventVersion.content = event.content;
    /** instrument can not be modifed since the creation and the original is kept*/
    currentEventVersion.instrumentName = currentEventVersionCopy.instrumentName;

    //keep event history
    currentEventVersion.previousVersionEvent = currentEventVersionCopy;

    const savedEvent = await saveEvent(currentEventVersion);
    res.send(savedEvent);
  } catch (error) {
    global.gLogger.error("Update event error", { error: JSON.stringify(error) });
    res.status(500).send({ message: error || "Error was produced when finding the event" });
  }
};

exports.sendPDF = async (html, req, sessionId, investigationId, response) => {
  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
    if (investigations && investigations.length > 0) {
      const investigation = investigations[0];
      const { title, name } = investigation;
      const instrument = investigation.instrument.name;
      const prefix = name ? `${name}_` : "";
      const filename = `${prefix}${instrument}_elogbook.pdf`;

      /** This is need because we want to access to the ESRF logo that is in the public folder as a img so we need to build the http URL */
      const serverURL = `http://localhost:${global.gServerConfig.server.port}`;
      const searchParams = `?title=${title}&instrument=${instrument}&proposal=${name}&logo=${serverURL}/static/pdf/esrf_logo.jpg`;

      const header = `file://${path.resolve("./app/views/header.html")}${searchParams}`;
      const footer = `file://${path.resolve("./app/views/footer.html")}${searchParams}`;
      global.gLogger.debug("PDF Process has started");
      const pdf = wkhtmltopdf(html, {
        pageSize: "A4",
        orientation: "Portrait",
        marginTop: 30,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        "header-html": header,
        "footer-html": footer,
        "header-spacing": 5,
      });
      global.gLogger.debug("PDF Process has finished");
      response.writeHead(200, {
        "Content-Type": "application/pdf",
        "Content-disposition": `filename=${filename}`,
      });
      pdf.pipe(response);
    } else {
      throw new Error("No investigation found");
    }
  } catch (error) {
    global.gLogger.error(error);
  }
};

/**
 * Returns the meta field of an entry in the logbook
 * @param {*} req
 * @param {*} res
 */
exports.getPageByEventId = async (req, res) => {
  const { _id, investigationId, instrumentName, sortOrder = "-1", limit, filterInvestigation = false, date, tags, startTime, endTime } = req.query;
  let { types } = req.query;
  types = getTypesOrDefault(types);
  const formattedStartTime = getParamAsTimeString(startTime);
  const formattedEndTime = getParamAsTimeString(endTime);
  global.gLogger.debug("getPageByEventId", {
    investigationId,
    instrumentName,
    sortOrder,
    limit,
    types,
    filterInvestigation,
    tags,
    startTime: formattedStartTime,
    endTime: formattedEndTime,
  });
  const page = await getEventPage(_id, investigationId, limit, filterInvestigation, instrumentName, types, date, sortOrder, tags, startTime, endTime);
  return res.send(page);
};

exports.countEvents = async (req, res) => {
  const { investigationId, instrumentName, search, format = "json", filterInvestigation = false, date, tags, startTime, endTime } = req.query;
  let { types } = req.query;
  const { username, fullName, isAdministrator, isInstrumentScientist } = req.session;

  const formattedDate = getParamAsDateString(date);
  const formattedStartTime = getParamAsTimeString(startTime);
  const formattedEndTime = getParamAsTimeString(endTime);
  global.gLogger.debug("countEvents", {
    investigationId,
    instrumentName,
    search,
    types,
    format,
    username,
    fullName,
    isAdministrator,
    isInstrumentScientist,
    filterInvestigation,
    formattedDate,
    formattedStartTime,
    formattedEndTime,
  });

  types = getTypesOrDefault(types);

  // determine min and max event id to load broadcast events
  let minEventId;
  let maxEventId;
  if (isSearchingBroadcastEvents(types)) {
    [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
  }

  try {
    const find = await buildEventFindQuery({
      investigationId,
      filterInvestigation,
      instrumentName,
      types,
      date: formattedDate,
      search,
      minEventId,
      maxEventId,
      tags,
      startTime: formattedStartTime,
      endTime: formattedEndTime,
    });

    const nbEvents = await count(find);
    return res.send([{ totalNumber: nbEvents }]);
  } catch (err) {
    global.gLogger.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while counting events.",
    });
  }
};

function getTypesOrDefault(types) {
  if (isUndefined(types)) {
    const defaultTypes = [EVENT_TYPE_ANNOTATION, EVENT_TYPE_NOTIFICATION, EVENT_TYPE_BROADCAST];
    types = defaultTypes.join();
  }
  return types;
}

/**
 * Returns the name of the file of the electronic logbook
 * @param {*} sessionId
 * @param {*} investigationId
 */
exports.getLogbookFileName = async (sessionId, investigationId) => {
  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
    if (investigations && investigations.length > 0) {
      const instrument = investigations[0].instrument ? investigations[0].instrument.name : "";
      return `${investigations[0].name}_${instrument}.txt`;
    }
  } catch (e) {
    global.gLogger.error("Error finding the investigation", { e });
  }
  return "e-logbook.txt";
};

/**
 * Returns a list of events based in the url query params
 * @param {*} filter
 * @param {*} options
 * @returns
 */
exports.getEvents = async (
  { investigationId, instrumentName, sortBy, sortOrder, skip, limit, search, filterInvestigation, tags, types, formattedDate, formattedStartTime, formattedEndTime },
  options = {}
) => {
  const { includeRemoved = false } = options;

  const sort = {};
  sort[sortBy] = isUndefined(sortOrder) ? -1 : sortOrder;

  types = getTypesOrDefault(types);

  // determine min and max event id to load broadcast events
  let minEventId;
  let maxEventId;
  if (isSearchingBroadcastEvents(types)) {
    [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
  }

  const find = await buildEventFindQuery(
    {
      search,
      sort,
      investigationId,
      limit,
      skip,
      filterInvestigation,
      instrumentName,
      types,
      date: formattedDate,
      sortOrder,
      minEventId,
      maxEventId,
      tags,
      startTime: formattedStartTime,
      endTime: formattedEndTime,
    },
    { includeRemoved }
  );
  const events = await findEvents(find, sort, skip, limit);
  return events;
};

/**
 * Returns a list of dates of all events based on the url query params. For each date, the number of events is given.
 * List of {"_id": "YYYY-DD-MM", event_count: ""}
 * @param {*} req
 * @param {*} res
 */
exports.findAllDatesByEvents = async (req, res) => {
  const { investigationId, instrumentName, search, filterInvestigation = false, tags } = req.query;
  let { types } = req.query;

  global.gLogger.debug("findAllDatesByEvents", {
    investigationId,
    instrumentName,
    search,
    types,
    filterInvestigation,
  });

  types = getTypesOrDefault(types);
  try {
    // determine min and max event id to load broadcast events
    let minEventId;
    let maxEventId;
    if (isSearchingBroadcastEvents(types)) {
      [minEventId, maxEventId] = await getMinAndMaxIdForBroadcastEvents(investigationId, instrumentName);
    }
    const dates = await getDates({ investigationId, filterInvestigation, instrumentName, types, search, minEventId, maxEventId, tags });
    return res.send(dates);
  } catch (err) {
    global.gLogger.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving events.",
    });
  }
};

/**
 * Create a new logbook broadcast event.
 * @param {string} req.params.sessionId the magic token
 */
exports.createBroadcastNotification = async (req, res) => {
  const { tag, creationDate, title, type, category, datasetId, software, machine, username, content } = req.body;
  global.gLogger.info("createBroadcastNotification ");

  const localcreationDate = parseCreationDate(creationDate);
  if (!type || type.toLowerCase() !== EVENT_TYPE_BROADCAST) {
    global.gLogger.error(`createBroadcastNotification error, the type is not ${EVENT_TYPE_BROADCAST}, but ${type}`);
    return res.status(ERROR.INVALID_EVENT_TYPE.code).send(ERROR.INVALID_EVENT_TYPE.message);
  }
  try {
    const tags = await createNotificationTag(tag);
    try {
      const event = new Event({
        datasetId,
        tag: tags,
        title,
        type: type.toLowerCase(),
        category,
        username,
        software,
        machine,
        creationDate: localcreationDate,
        content: translateEventContentToHtml(content),
        previousVersionEvent: null,
      });

      try {
        const eventCreated = await saveEvent(event);
        sendLogbookMessage(eventCreated);
        res.send(eventCreated);
      } catch (error) {
        global.gLogger.error("createBroadcastNotification. There was an error when saving", {
          error: error.message,
        });
        res.status(422).send({
          message: error.message || "Some error occurred while saving the event.",
        });
      }
    } catch (e) {
      global.gLogger.error("createBroadcastNotification saving event", { error: e.message });
      return res.status(400).send(`createBroadcastNotification: ${e.message}`);
    }
  } catch (e) {
    return res.status(400).send(e.message);
  }
};
