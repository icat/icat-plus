const request = require("request");

exports.request = (req, res) => {
  const url = `${global.gServerConfig.oaipmh.server.url}:${global.gServerConfig.oaipmh.server.port}${req.url}`;
  global.gLogger.info(`[OAI-PMH] Request${url}`);
  request(url)
    .on("error", (e) => {
      global.gLogger.error(`[OAI-PMH]${e.message}`);
      res.status(500).send({
        message: e.message,
      });
    })
    .pipe(res);
};
