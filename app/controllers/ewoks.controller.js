const axios = require("axios");
const { ERROR } = require("../errors.js");
const Job = require("../models/job.model.js");
const icat = require("../api/icat.js");
const { isUndefined, getObjectIdFromStrDate, dateFormat } = require("./helpers/helper.controller.js");
const { setMetaDataTo } = require("../helpers/pagination.js");

const FULL_INPUT_DATASET_IDS_PARAMETER_NAME = "__full_input_datasetIds";
const EWOKS_SERVER = global.gServerConfig.ewoks.server;
const EXECUTE_URL = `${EWOKS_SERVER}/execute`;
const WORKFLOW_LIST_URL = `${EWOKS_SERVER}/workflows/descriptions`;

async function update({ id, status, step, session, logs }, res) {
  try {
    global.gLogger.info("Update job", { id, status, step, logs });
    const query = { _id: id, userNames: { $in: session.username } };

    global.gLogger.debug("Update job query", { query: JSON.stringify(query) });

    const updateFields = {};
    if (status) {
      updateFields.status = status;
      updateFields.$push = { statuses: { status, createdAt: new Date(), updatedAt: new Date() } };
    }
    if (step) {
      updateFields.$push = { steps: { ...step, createdAt: new Date(), updatedAt: new Date() } };
    }
    if (logs) {
      global.gLogger.debug("Logs received", { logs: JSON.stringify(logs) });
      updateFields.$push = { logs: { log: logs, createdAt: new Date(), updatedAt: new Date() } };
    }

    const updated = await Job.findOneAndUpdate(query, updateFields, {
      new: true,
    });

    res.send(updated);
  } catch (e) {
    global.gLogger.error(e.message);
    res.status(500).send("Unable to update status", { message: e.message });
  }
}

async function getJobsByUser(req, res) {
  try {
    const { username } = req.session;
    const $and = [{ userNames: { $in: username } }];
    const jobs = await findJobs(req.query, $and);
    res.send(jobs);
  } catch (error) {
    global.gLogger.error(error);
    return res.status(500).send("Failed to get the list of ewoks jobs by user");
  }
}

/**
 * Returns the list of jobs
 * @param {*} req
 * @param {*} res
 * @returns
 */
async function getJobs(req, res) {
  try {
    const jobs = await findJobs(req.query, []);
    res.send(jobs);
  } catch {
    return res.status(500).send("Failed to get the list of ewoks jobs");
  }
}

/**
 * Returns the list of jobs
 * @param {*} params
 * @param {*} $and
 * @returns
 */
async function findJobs(params, $and) {
  try {
    const { investigationId, datasetIds, sampleIds, jobId, sortBy, sortOrder, status, startDate, endDate, search } = params;
    let { skip, limit } = params;
    global.gLogger.debug("getJobs", { investigationId, datasetIds, sampleIds, jobId, skip, limit, sortBy, sortOrder, status, startDate, endDate, search });
    const sort = {};
    sort[sortBy] = isUndefined(sortOrder) ? -1 : sortOrder;
    skip = isUndefined(skip) ? 0 : skip;
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : limit;

    if (investigationId) {
      $and.push({ "investigations.id": investigationId });
    }
    if (datasetIds) {
      $and.push({ datasetIds: { $in: datasetIds } });
    }
    if (sampleIds) {
      $and.push({ sampleIds: { $in: sampleIds } });
    }
    if (jobId) {
      $and.push({ jobId });
    }

    if (status) {
      const statusList = status.toUpperCase().split(",");
      $and.push({ status: { $in: statusList } });
    }
    if (startDate) {
      const sDate = dateFormat(startDate, "yyyy-MM-dd");
      if (sDate) {
        $and.push({ _id: { $gte: getObjectIdFromStrDate(sDate) } });
      }
    }
    if (endDate) {
      const eDate = dateFormat(endDate, "yyyy-MM-dd");
      if (eDate) {
        $and.push({ _id: { $lte: getObjectIdFromStrDate(eDate) } });
      }
    }
    if (!isUndefined(search) && search.trim() !== "") {
      const regex = new RegExp(`${search}`, "i");
      $and.push({
        $or: [
          { "investigations.name": { $in: regex } },
          { datasetNames: { $elemMatch: { $regex: regex } } },
          { sampleNames: { $elemMatch: { $regex: regex } } },
          { "steps.name": { $in: regex } },
          { status: { $in: regex } },
        ],
      });
    }

    const query = $and.length === 0 ? {} : { $and };
    global.gLogger.debug(`query=${JSON.stringify(query)}`);
    const [jobs, total] = await Promise.all([Job.find(query).sort(sort).skip(parseInt(skip)).limit(parseInt(limit)), Job.countDocuments(query)]);
    const allJobs = jobs.map((job) => job.toObject({ virtuals: true }));
    setMetaDataTo(allJobs, total, limit, skip);
    return allJobs;
  } catch (error) {
    global.gLogger.error(error);
    throw new Error(error);
  }
}

async function getWorkflows(res) {
  try {
    const response = await axios.get(WORKFLOW_LIST_URL);
    res.send(response.data.items);
  } catch {
    return res.status(500).send({
      message: `There was a problem connecting to ewoks: ${WORKFLOW_LIST_URL}`,
    });
  }
}
/**
 * This returns the hook that will be used by ewoks to inform about the status of the run
 * @param {*} sessionId
 * @param {*} _id
 * @returns
 */
function getCallBack(sessionId, _id) {
  return `${global.gServerConfig.server.url}/ewoks/${sessionId}/jobs?id=${_id}`;
}

/**
 * Return dataset.location is dataset.type !== processed otherwise it will return dataset.parameters.full_input_datasets
 * @param {*} dataset
 */
async function getInputsLocation(sessionId, dataset) {
  if (dataset.type === "acquisition") {
    return dataset.location;
  }
  const inputParameter = dataset.parameters.find((parameter) => parameter.name === FULL_INPUT_DATASET_IDS_PARAMETER_NAME);
  if (inputParameter) {
    const datasetIds = inputParameter.value;
    if (datasetIds) {
      const rawDatasets = await icat.DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIds.split(" ").toString() });
      if (rawDatasets) {
        if (rawDatasets.length === 1) {
          return rawDatasets[0].location;
        }
        return rawDatasets.map((d) => d.location).toString();
      }
    }
  }
}
/**
 * This method calls ewoks endpoint to run a job
 * @param {*} identifier
 * @param {*} worker_options
 * @param {*} execute_arguments
 * @returns
 */
async function execute(identifier, input, datasets, sessionId) {
  const url = `${EXECUTE_URL}/${identifier}`;

  input.raw_data_path = [];
  for (const i in datasets) {
    const rawLocation = await getInputsLocation(sessionId, datasets[i]);
    if (rawLocation) {
      input.raw_data_path.push(rawLocation);
    }
  }
  global.gLogger.debug("Submit job to ewoks", { identifier, url, raw_data_path: input.raw_data_path });

  const payload = {
    execute_arguments: {
      startargs: input,
    },
  };

  const response = await axios.post(url, payload, {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  });
  return { jobId: response.data.job_id };
}

/**
 * Launch a job
 * @param {*} sessionId
 * @param {*} res
 * @returns
 */
async function run(sessionId, { username, identifier, datasetIds, input }, res) {
  try {
    global.gLogger.info("Ewoks job run", { username, identifier, datasetIds, input: JSON.stringify(input) });

    if (!datasetIds) {
      global.gLogger.error(ERROR.NO_DATASETIDS_PARAM_FOUND.message, { identifier, datasetIds });
      return res.status(ERROR.NO_DATASETIDS_PARAM_FOUND.code).send(ERROR.NO_DATASETIDS_PARAM_FOUND.message);
    }
    if (!identifier) {
      global.gLogger.error(ERROR.NO_EWOKS_IDENTIFIER.message, { identifier });
      return res.status(400).send(ERROR.NO_EWOKS_IDENTIFIER.message);
    }

    /** Retrieve datasets */
    const datasets = await icat.DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIds });
    if (!datasets || datasets.length === 0 || datasetIds.split(",").length !== datasets.length) {
      return res.status(ERROR.NO_DATASETS_FOUND.code).send(ERROR.NO_DATASETS_FOUND.message);
    }

    let job = await new Job({
      investigations: datasets.map((dataset) => {
        return { name: dataset.investigation.name, id: dataset.investigation.id };
      }),
      datasetIds: datasets.map((dataset) => dataset.id),
      datasetNames: datasets.map((dataset) => dataset.name),
      sampleIds: datasets.map((dataset) => dataset.sampleId),
      sampleNames: datasets.map((dataset) => dataset.sampleName),
      identifier,
      userNames: [username],
      input,
    }).save();

    input.callback = getCallBack(sessionId, job._id);
    global.gLogger.debug("Callback is", { callback: input.callback });
    const { jobId } = await execute(identifier, input, datasets, sessionId);

    /** updating workflow and jobId */
    job = await Job.findOneAndUpdate(
      { _id: job._id },
      { jobId },
      {
        new: true,
      }
    );
    global.gLogger.debug("Job has been successfully submitted to ewoks", { jobId });
    res.send(job);
  } catch {
    res.status(500).send({
      message: "Job could not be ran",
    });
  }
}

module.exports = {
  getJobs,
  getJobsByUser,
  run,
  getWorkflows,
  EXECUTE_URL,
  update,
  getInputsLocation,
};
