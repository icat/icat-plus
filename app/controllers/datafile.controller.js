const { getDatafilesByDatasetId, countDatafileByDatasetId, findDatafileParameters } = require("../api/icat.js");
const { sendError, ERROR } = require("../errors.js");
const { getParamAsEscapedLowerString, isUndefined } = require("./helpers/helper.controller.js");
const fs = require("fs");
const path = require("path");

exports.getDatafilesByDatasetId = async (req, res) => {
  const { limit, skip, search, datasetId } = req.query;
  const { sessionId } = req.params;
  const searchLimit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
  const searchSkip = parseInt(skip) || 0;
  const searchText = getParamAsEscapedLowerString(search);
  global.gLogger.debug("getDatafilesByDatasetId. ", {
    datasetId,
    searchLimit,
    searchSkip,
    searchText,
  });
  try {
    const datafiles = await findDatafilesByDatasetId(sessionId, datasetId, searchLimit, searchSkip, searchText);
    res.send(datafiles);
  } catch (error) {
    sendError(500, "Failed to getDatafilesByDatasetId", error, res);
  }
};

async function findDatafilesByDatasetId(sessionId, datasetId, limit, skip, search) {
  global.gLogger.debug("findDatafilesByDatasetId. ", { limit, skip, search });
  const hrstart = process.hrtime();
  const [totalWithoutFilters, total, datafiles] = await Promise.all([
    countDatafileByDatasetId(sessionId, datasetId, undefined),
    countDatafileByDatasetId(sessionId, datasetId, search),
    getDatafilesByDatasetId(sessionId, datasetId, limit, skip, search),
  ]);
  setMetaDataToDatafile(datafiles, totalWithoutFilters, total, limit, skip);

  global.gLogger.debug("Final time to findDatafilesByDatasetId: ", { time: process.hrtime(hrstart)[1] / 1000000 });
  return datafiles;
}

function setMetaDataToDatafile(datafiles, totalWithoutFilters, total, limit, skip) {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;
  datafiles.forEach((d) => {
    d.meta = {
      page: {
        totalWithoutFilters,
        total,
        totalPages,
        currentPage,
      },
    };
  });
}

exports.download = async (sessionId, filepath, datasetId, res) => {
  global.gLogger.debug("Download file. ", {
    filepath,
    datasetId,
  });
  try {
    if (isUndefined(filepath) || isUndefined(datasetId)) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const datafileParameters = await findDatafileParameters(sessionId, datasetId, filepath);
    const onlineParameters = datafileParameters.filter((param) => param.DatafileParameter.type.name === "__online_location");
    if (onlineParameters.length > 0) {
      const onlineFilePath = onlineParameters[0].DatafileParameter.stringValue;
      const data = fs.readFileSync(onlineFilePath);
      let header = { "Content-Length": data.length };
      const extension = path.parse(filepath).ext;
      if (extension.toLocaleLowerCase() === ".svg") {
        header = { ...header, "Content-Type": "image/svg+xml" };
      }
      res.writeHead(200, header);
      return res.end(data);
    }
    return res.status(ERROR.FILE_NOT_FOUND.code).send(ERROR.FILE_NOT_FOUND.message);
  } catch (error) {
    sendError(500, "Failed to download file", error, res);
  }
};
