const { getStatusByDatasetIds, getDatafilesByDatasetId, countDatafileByDatasetId, getDatafilesByIds, getPathBy } = require("../api/icat.js");
const { downloadData } = require("../api/query.js");
const { ERROR, sendError } = require("../errors.js");
const DatasetAccessSchema = require("../models/datasetaccess.model.js");
const { findDatasetAccesses } = require("./datasetaccess.controller.js");
const STATUS = require("../models/datasetaccess.status.js");
const TYPE = require("../models/datasetaccess.type.js");
const { isUndefined } = require("./helpers/helper.controller");

exports.getStatusByDatasetIds = async (sessionId, datasetIds, res) => {
  try {
    global.gLogger.debug("getStatusByDatasetIds", { datasetIds });
    const status = await getStatusByDatasetIds(sessionId, datasetIds);
    res.send(status);
  } catch (error) {
    sendError(500, "Failed to get status of datasets", error, res);
  }
};

/**
 * Returns an array with the paths where the datafile or dataset are located
 * @param {*} sessionId
 * @param {*} datafileIds
 * @param {*} online if true then it will return the only if the path exists
 * @param {*} res
 */
exports.getPathBy = async (sessionId, datafileIds, datasetIds, online, res) => {
  try {
    global.gLogger.debug("getPathBy", { datafileIds, datasetIds, online });
    if (!datafileIds & !datasetIds) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const paths = await getPathBy(sessionId, datafileIds, datasetIds, online);
    res.send(paths);
  } catch (error) {
    sendError(500, "Failed to get the path of datafiles or datasets", error, res);
  }
};

/**
 * Given a name that corresponds to the column name of the table DataFile it returns the name of the file.
 * note that the column name is the path relative to the dataset location which means that can be something like: /subfolder/file.txt
 * In this case the function will return "file.txt"
 * @param {*} name
 * @param {*} res
 */
exports.getOutname = (name) => {
  try {
    const splitted = name.split("/");
    return splitted[splitted.length - 1];
  } catch {
    return name;
  }
};

exports.downloadData = async (sessionId, datasetIds, datafileIds, res) => {
  try {
    global.gLogger.debug("downloadData", { datasetIds, datafileIds });
    if (isUndefined(datasetIds) && isUndefined(datafileIds)) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    /** final name of the file to be downloaded only if there is a single file */
    let outname = null;

    /** Case when there is a single dataset */
    if (!isUndefined(datasetIds) && datasetIds.split(",").length === 1) {
      const nbFiles = await countDatafileByDatasetId(sessionId, datasetIds, undefined);
      if (parseInt(nbFiles) === 1) {
        const datafiles = await getDatafilesByDatasetId(sessionId, datasetIds);
        if (datafiles.length === 1) {
          datafileIds = datafiles[0].Datafile.id;
          datasetIds = undefined;
          outname = this.getOutname(datafiles[0].Datafile.name);
        }
      }
    }
    /** Case of no datasets */
    if (isUndefined(datasetIds)) {
      const datafiles = await getDatafilesByIds(sessionId, datafileIds);
      if (datafiles.length === 1) {
        outname = this.getOutname(datafiles[0].Datafile.name);
      }
    }

    res.redirect(downloadData(sessionId, datasetIds, datafileIds, outname));
  } catch (error) {
    sendError(500, "Failed to download data", error, res);
  }
};

/**
 * records the information that a user asked the restoration of a datasetId and redirects to the download data method to trigger the restoration
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} name
 * @param {*} email empty if not anonymous user
 * @param {*} res
 * @returns
 */
exports.restoreData = async (sessionId, datasetIds, name, email, res) => {
  try {
    global.gLogger.debug("restoreData", { datasetIds, name, email });
    if (!datasetIds || !name) {
      return res.status(ERROR.BAD_PARAMS.code).send(ERROR.BAD_PARAMS.message);
    }
    const ids = datasetIds.split(",");
    for (let i = 0; i < ids.length; i++) {
      const datasetAccesses = await findDatasetAccesses(ids[i], TYPE.RESTORE, STATUS.ONGOING, name);
      if (datasetAccesses.length === 0) {
        const datasetAccess = new DatasetAccessSchema({
          datasetId: ids[i],
          user: name,
          email,
        });

        await datasetAccess.save();
      }
    }

    const datasetAccesses = await findDatasetAccesses(datasetIds, TYPE.RESTORE, STATUS.ONGOING, name);
    if (datasetAccesses.length === 0) {
      const datasetAccess = new DatasetAccessSchema({
        datasetIds,
        user: name,
        email,
      });

      await datasetAccess.save();
    }
    res.redirect(downloadData(sessionId, datasetIds, undefined));
  } catch (error) {
    sendError(500, "Failed to restore data", error, res);
  }
};
