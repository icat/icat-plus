const { getParameterTypeList } = require("../api/icat.js");
const { parse } = require("../api/parsers/parameterparser.js");
const { ERROR } = require("../errors.js");
const { sendError } = require("./helpers/helper.tracking.controller.js");
/**
 * It returns the list of parameters or filtered by id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getParameters(req, res) {
  try {
    const { parameterTypeId, name } = req.query;
    const parameterList = await getParameterTypeList(global.gServerConfig.icat.authorizations.parameterListReader.user);
    if (parameterList) {
      if (parameterTypeId) {
        const parameter = parameterList.find((parameter) => parameter.ParameterType.id === parseFloat(parameterTypeId));
        if (parameter) return res.send(parse([parameter]));
        return sendError(ERROR.NO_PARAMETER_BAD_PARAMS.code, ERROR.NO_PARAMETER_BAD_PARAMS.message, new Error(ERROR.NO_PARAMETER_BAD_PARAMS.message), res);
      }
      if (name) {
        const parameter = parameterList.find((parameter) => parameter.ParameterType.name === name);
        if (parameter) return res.send(parse([parameter]));
        return sendError(ERROR.NO_PARAMETER_BAD_PARAMS.code, ERROR.NO_PARAMETER_BAD_PARAMS.message, new Error(ERROR.NO_PARAMETER_BAD_PARAMS.message), res);
      }
      res.send(parse(parameterList));
    } else {
      throw new Error("No parameters found");
    }
  } catch (error) {
    sendError(500, "Failed to getParameters", error, res);
  }
}

module.exports = {
  getParameters,
};
