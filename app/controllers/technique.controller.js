const icat = require("../api/icat.js");
const { ERROR } = require("../errors.js");
const techniqueParser = require("../api/parsers/techniqueparser.js");

/**
 * Get all techniques
 */
exports.getTechniques = async (_req, res) => {
  try {
    const data = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.parameterListReader.user);
    const techniques = await icat.getTechniquesBySessionId(data.sessionId);
    return res.send(techniqueParser.parse(techniques));
  } catch (error) {
    global.gLogger.error(error);
    res.status(ERROR.FAILED_TO_RETRIEVE_TECHNIQUES.code).send(ERROR.FAILED_TO_RETRIEVE_TECHNIQUES.message);
  }
};
