const { InvestigationAdapter, getInvestigationUserByInvestigationId, asyncGetSession, addInvestigationUser, deleteInvestigationUser } = require("../api/icat.js");
const { ROLE_COLLABORATOR, MARGIN_HOURS } = require("../constants.js");
const { ERROR, sendError } = require("../errors.js");
const investigationHelper = require("../helpers/investigation.js");
const { getUserByName } = require("../cache/cache.js");
const { getParamAsDateString, getParamAsEscapedLowerString, getSortOrder, dateFormat, isUndefined } = require("./helpers/helper.controller.js");
const { isReleased } = require("./helpers/helper.logbook.controller.js");
const { add, sub, isBefore, isAfter, isValid, parse } = require("date-fns");

function normalize(req, res) {
  const { investigationName } = req.params;
  if (investigationName) {
    return res.send(investigationHelper.normalize(investigationName));
  }
  return res.status(500).send("No investigation name given");
}

const getInvestigationByNameAndInstrumentBySessionId = async (sessionId, investigationName, instrumentName, createdAt) => {
  let investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { instrumentName, investigationName });
  global.gLogger.debug("Found investigations matching the name", { createdAt, investigationName, count: investigations.length });

  /** Filter investigation of type proposals */
  investigations = investigations.filter((investigation) => {
    return investigation.visitId !== "PROPOSAL";
  });
  global.gLogger.debug("Filtered investigations that are not proposals", { investigationName, count: investigations.length });

  /** if a single experiment then we retrieve it */
  if (investigations.length === 1) {
    global.gLogger.debug("Found a single session that is not a proposal");
    return investigations[0];
  }

  /** Find investigation by time slot and instrument */
  let investigation = investigations.find((investigation) => {
    const { visitId, instrument, startDate, endDate } = investigation;
    if (visitId !== "PROPOSAL" && instrument && instrument.name && instrumentName && instrument.name.toUpperCase() === instrumentName.toUpperCase()) {
      if (startDate && endDate) {
        const startTimeSlot = sub(new Date(startDate), { hours: MARGIN_HOURS });
        const endTimeSlot = add(new Date(endDate), { hours: MARGIN_HOURS });
        return isAfter(new Date(createdAt), startTimeSlot) && isBefore(new Date(createdAt), endTimeSlot);
      }
    }
    global.gLogger.warn("Investigation does not match", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return false;
  });

  if (investigation) {
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.info("Investigation found", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return investigation;
  }

  global.gLogger.warn("Search for a test proposal", { investigationName, instrumentName, createdAt });
  /** Find a test investigation, a test investigation has the name of the instrument in the visitId */
  investigation = investigations.find((investigation) => {
    if (investigation.visitId.toUpperCase() === instrumentName.toUpperCase()) {
      return true;
    }
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.warn("Investigation does not macth", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return false;
  });

  if (investigation) {
    const { startDate, endDate, visitId } = investigation;
    global.gLogger.info("Investigation found", { startDate, endDate, investigationName, instrumentName, createdAt, visitId });
    return investigation;
  }

  global.gLogger.error(`No proposal for ${investigationName} and ${instrumentName} could be allocated`);
  return null;
};

/**
 *  Returns a promise with a single investigation or reject if more than one have been found
 *
 * @param {investigationName} name of the investigation (proposal)
 * @param {instrumentName} name of the instrument (beamline)
 * @param {createdAt} time when the event was created
 */
const getInvestigationByNameAndInstrument = async (investigationName, instrumentName, createdAt) => {
  const data = await asyncGetSession(global.gServerConfig.icat.authorizations.notifier.user);
  const investigation = await getInvestigationByNameAndInstrumentBySessionId(data.sessionId, investigationName, instrumentName, createdAt);
  return investigation;
};

async function getInvestigationById(req, res) {
  try {
    const { investigationId } = req.params;
    const investigations = await InvestigationAdapter.getInvestigationsBy(req.params.sessionId, { investigationId });
    return res.send(investigations);
  } catch (error) {
    sendError(500, "Failed to getInvestigationById", error, res);
  }
}

async function getInvestigationUserByInvestigation(req, res) {
  try {
    const investigationUsers = await getInvestigationUserByInvestigationId(req.params.sessionId, req.params.investigationId);
    res.send(investigationUsers);
  } catch (error) {
    sendError(ERROR.FAILED_TO_RETRIEVE_INVESTIGATIONUSERS.code, ERROR.FAILED_TO_RETRIEVE_INVESTIGATIONUSERS.message, error, res);
  }
}

async function getInvestigationsBySessionId(req, res) {
  global.gLogger.debug("getInvestigationsBySessionId", req.query);
  const { filter, sortBy, sortOrder, investigationName, ids, username, parameters } = req.query;
  let { instrumentName, search, limit, skip, startDate, endDate, withHasAccess, time } = req.query;
  const { sessionId } = req.params;
  const sessionUsername = req.session.name;

  instrumentName = instrumentName ? instrumentName.toUpperCase() : "";
  startDate = getParamAsDateString(startDate, true);
  endDate = getParamAsDateString(endDate, true);
  const parsedTime = parse(time, "yyyy-MM-dd'T'HH:mm:ss", new Date());
  time = isValid(parsedTime) ? dateFormat(parsedTime, "yyyy-MM-dd'T'HH:mm:ss") : undefined;
  limit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
  skip = parseInt(skip) || 0;
  search = getParamAsEscapedLowerString(search);
  withHasAccess = withHasAccess ? withHasAccess.toLowerCase() === "true" : false;

  let searchAsNormalizedInvestigation;
  if (!isUndefined(search)) {
    try {
      searchAsNormalizedInvestigation = investigationHelper.normalize(search).toLowerCase();
      // eslint-disable-next-line no-unused-vars
    } catch (e) {}
  }

  try {
    const formattedSortBy = getInvestigationParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);

    const include =
      withHasAccess || username !== undefined
        ? "investigation.investigationUsers investigationUsers, investigationUsers.user user, instrument.instrumentScientists as instrumentScientists, instrumentScientists.user scuser"
        : undefined;
    const investigations = await InvestigationAdapter.getInvestigationsBy(
      sessionId,
      {
        startDate,
        endDate,
        limit,
        skip,
        search,
        sortBy: formattedSortBy,
        sortOrder: formattedSortOrder,
        instrumentName,
        filter,
        investigationName,
        investigationId: ids,
        username: sessionUsername,
        searchAsNormalizedInvestigation,
        time,
        investigationUsername: username,
        parameters,
      },
      include
    );
    if (withHasAccess) {
      investigations.forEach((investigation) => {
        investigation.canAccessDatasets = canUserAccessDatasets(sessionUsername, investigation, req.session.isAdministrator);
      });
    }
    return res.send(investigations);
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, "Failed to getInvestigationsBySessionId", error, res);
  }
}

function canUserAccessDatasets(username, investigation, isAdministrator) {
  const isParticipant = investigation.investigationUsers.map((u) => u.user.name).filter((u) => u === username).length > 0;
  const isInstrumentScientist = investigation.instrument.instrumentScientists.map((u) => u.name).filter((u) => u === username).length > 0;
  return isAdministrator || isReleased(investigation) || isParticipant || isInstrumentScientist;
}

function getInvestigationParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "NAME":
        sortFilter = "investigation.name";
        break;
      case "INSTRUMENT":
        sortFilter = "instrument.name";
        break;
      case "STARTDATE":
        sortFilter = "investigation.startDate";
        break;
      case "TITLE":
        sortFilter = "investigation.title";
        break;
      case "SUMMARY":
        sortFilter = "investigation.summary";
        break;
      case "RELEASEDATE":
        sortFilter = "investigation.releaseDate";
        break;
      case "DOI":
        sortFilter = "investigation.doi";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[NAME|INSTRUMENT|STARTDATE|TITLE|SUMMARY|RELEASEDATE|DOI]`);
    }
  }
  return sortFilter;
}

/**
 * Adds an user to an investigation. Only users with role Collaborator can be added
 * @param {*} req
 * @param {*} res
 * @returns
 */
const addInvestigationUserToInvestigation = async (req, res) => {
  try {
    const { sessionId, investigationId } = req.params;
    const { name } = req.body;
    global.gLogger.info("addInvestigationUserToInvestigation", { sessionId, investigationId, name });

    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });

    if (investigations.length !== 1) {
      return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
    }

    if (!name) {
      return res.status(ERROR.NO_USERS_BAD_PARAMS.code).send(ERROR.NO_USERS_BAD_PARAMS.message);
    }

    let user;
    try {
      user = await getUserByName(name, sessionId);
    } catch {
      return res.status(ERROR.NO_USERS_BAD_PARAMS.code).send(ERROR.NO_USERS_BAD_PARAMS.message);
    }

    global.gLogger.debug("User found", { user: JSON.stringify(user) });
    const investigationUser = {
      role: ROLE_COLLABORATOR,
      user: { id: user.id },
      investigation: { id: investigations[0].id },
    };

    try {
      const investigationUsers = await addInvestigationUser(sessionId, {
        InvestigationUser: investigationUser,
      });
      investigationUser.id = investigationUsers[0];
      return res.send(investigationUser);
    } catch (error) {
      return sendError(500, error.response.data.message, error, res);
    }
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, error, error, res);
  }
};

/**
 * Removes an user from an investigation. Only users with role Collaborator can be remove
 * @param {*} req
 * @param {*} res
 * @returns
 */
const deleteInvestigationUserToInvestigation = async (req, res) => {
  try {
    const { sessionId, investigationId } = req.params;
    const { name } = req.body;
    global.gLogger.info("deleteInvestigationUserToInvestigation", { sessionId, investigationId, name });

    let investigationUsers = await getInvestigationUserByInvestigationId(sessionId, [investigationId]);
    global.gLogger.debug(`Found ${investigationUsers.length} within the investigation`);
    /** Note that the id of the User becomes the name for ICAT */
    const collaboratorToBeRemoved = investigationUsers.filter((i) => {
      return i.role.toUpperCase() === ROLE_COLLABORATOR.toUpperCase() && i.name === name;
    });

    global.gLogger.debug(`Found ${collaboratorToBeRemoved.length} investigation users to be removed`);
    if (collaboratorToBeRemoved.length > 0) {
      await deleteInvestigationUser(sessionId, [{ InvestigationUser: { id: collaboratorToBeRemoved[0].id } }]);
      investigationUsers = await getInvestigationUserByInvestigationId(sessionId, [investigationId]);
      return res.status(200).send(investigationUsers);
    }
    return res.status(400).send();
  } catch (error) {
    global.gLogger.error(error);
    return sendError(500, "Failed to deleteInvestigationUserToInvestigation", error, res);
  }
};

/**
 * this method retrieves the most important parameters of the datasets in order to build meanful statistics.
 * It reduces dramatically the length of the payload compared to retrieving by dataset
 */
const getTimelineBy = async (sessionId, instrument) => {
  global.gLogger.debug("getTimelineBy. ", {
    sessionId,
    instrument,
  });

  const timelines = await InvestigationAdapter.getTimelineBy(sessionId, {
    instrument,
  });

  return timelines;
};

module.exports = {
  addInvestigationUserToInvestigation,
  deleteInvestigationUserToInvestigation,
  normalize,
  getInvestigationByNameAndInstrument,
  getInvestigationById,
  getInvestigationUserByInvestigation,
  getInvestigationsBySessionId,
  getTimelineBy,
};
