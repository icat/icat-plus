const cache = require("../cache/cache.js");
const appCache = require("../cache/appCache.js");

exports.getSessionById = (req, res) => {
  res.send(cache.getSessionById(req.query.sessionId));
};

exports.getStats = (req, res) => {
  res.send(cache.getStats());
};

exports.getCacheKeys = (req, res) => {
  res.send(cache.getKeys());
};

exports.getCacheKey = (req, res) => {
  res.send(cache.getDataByKey(req.params.key));
};

exports.reload = async (req, res) => {
  await appCache.loadCache();
  res.send({ message: "Ok" });
};
