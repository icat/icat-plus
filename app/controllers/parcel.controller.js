const Shipment = require("../models/shipment.model.js");
const Parcel = require("../models/parcel.model.js");
const { ERROR } = require("../errors.js");
const STATUS = require("../models/status.js");
const { findNotRemovedShipmentsBy, findParcels, sendError, findNotRemovedParcels } = require("./helpers/helper.tracking.controller.js");
const { sendParcelEmail } = require("./helpers/helper.email.js");
const { InvestigationAdapter, getInvestigationUserByInvestigationId, getInvestigationUserByInvestigationIdAndRole } = require("../api/icat.js");
const { setMetaDataTo } = require("../helpers/pagination.js");
const { getLabels } = require("./helpers/helper.labels.tracking.controller.js");
const { ROLE_LOCAL_CONTACT } = require("../constants.js");
const { isUndefined, getParamAsEscapedLowerString } = require("../controllers/helpers/helper.controller.js");
const { getSetupConfigurationBy, checkParcelParameters } = require("./helpers/helper.tracking.setup.js");

function isStatusValid(parcel, newStatus) {
  return Object.keys(STATUS).includes(newStatus);
}

const UPDATE_FIELDS_STATUS = {
  containsDangerousGoods: STATUS.SENT,
  shippingTrackingInformation: STATUS.SENT,
};

function getUpdateFields(status, body = {}) {
  return Object.fromEntries(
    Object.entries(UPDATE_FIELDS_STATUS)
      .map(([field, target]) => {
        const value = body[field];
        const shouldBeUpdated = target === status;
        const isUpdated = value !== undefined;
        if (shouldBeUpdated && !isUpdated) {
          throw new Error(`${field} is required when setting the status to ${status}`);
        } else if (!shouldBeUpdated && isUpdated) {
          throw new Error(`${field} should not be updated when setting the status to ${status}`);
        }
        return [field, value];
      })
      .filter(([, value]) => {
        return value !== undefined;
      })
  );
}

/**
 * It will find the parcel and will set the new status of the parcel
 */
async function setStatus(sessionId, investigationId, parcelId, status, body, username) {
  global.gLogger.info("SetStatus", { _id: parcelId, investigationId });
  const newStatus = status.toUpperCase();

  const parcels = await findParcels({
    _id: parcelId,
    investigationId,
  });

  if (parcels.length === 0) {
    throw new Error(`No parcels were found with investigationId: ${investigationId} and parcelId: ${parcelId}`);
  }
  global.gLogger.info("setStatus: parcel found", { _id: parcelId, investigationId });
  const parcel = parcels[0];

  if (!isStatusValid(parcel, newStatus)) {
    throw new Error(`New status ${newStatus} is not valid for parcelId ${parcelId} and status ${status}`);
  }

  const statusObject = {
    status: newStatus,
    createdBy: username,
    comments: body.comments,
  };

  const updateFields = {
    $push: { statuses: statusObject },
    localContactNames: await getLocalContactNameListByInvestigationId(sessionId, investigationId),
    currentStatus: newStatus,
    ...getUpdateFields(newStatus, body),
  };

  const updated = await Parcel.findOneAndUpdate({ _id: parcel._id }, updateFields, {
    new: true,
  }).populate({ path: "items", match: { status: { $ne: "REMOVED" } } });

  try {
    global.gLogger.info("App is going to send the emails", { sessionId });
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId: updated.investigationId });
    const data = await sendParcelEmail(sessionId, investigations[0], updated);
    global.gLogger.info("Emails have been sent", data);
    return updated.toObject({ virtuals: true });
  } catch (error) {
    global.gLogger.error("Error sending the emails", error);
    throw error;
  }
}

exports.setParcelStatus = async (req, res) => {
  const { sessionId, parcelId, investigationId, status } = req.params;
  global.gLogger.debug("setParcelStatus", { sessionId, investigationId, parcelId, status });

  try {
    const parcel = await setStatus(sessionId, investigationId, parcelId, status, req.body, req.session.name);
    res.send(parcel);
  } catch (e) {
    sendError(ERROR.PARCEL_FAILED_TO_SET_STATUS.code, ERROR.PARCEL_FAILED_TO_SET_STATUS.message, e, res);
  }
};

exports.getLabels = async (req, res) => {
  const { sessionId, investigationId, parcelId } = req.params;
  global.gLogger.info("getLabels", { sessionId, investigationId, parcelId });

  const parcels = await findNotRemovedParcels({
    investigationId,
    _id: req.params.parcelId,
  });

  const shipment = await Shipment.findById(parcels[0].shipmentId).populate({ path: "parcels", match: { status: { $ne: "REMOVED" } } });
  const response = await InvestigationAdapter.getInvestigationsBy(req.params.sessionId, { investigationId: req.params.investigationId });
  const investigationUsers = await getInvestigationUserByInvestigationId(req.params.sessionId, [req.params.investigationId]);
  await getLabels(res, shipment, parcels[0], response[0], investigationUsers);
};

async function getLocalContactNameListByInvestigationId(sessionId, investigationId) {
  const users = await getInvestigationUserByInvestigationIdAndRole(sessionId, investigationId, ROLE_LOCAL_CONTACT);
  return users.map((u) => u.name);
}

/**
 * Initial status when a parcel is created
 * @param {*} username
 * @returns
 */
const getInitialStatusByUsername = (username) => {
  return {
    statuses: [
      {
        status: STATUS.CREATED,
        createdBy: username,
      },
      {
        status: STATUS.SCHEDULED,
        createdBy: username,
      },
    ],
    currentStatus: STATUS.SCHEDULED,
  };
};

/**
 * This mehtod adds a parcel to parcels array of shipment based on the parcel.shipmentId
 * @param {*} parcel the parcel that will include in the array of parcels of the shipment
 * @param {*} username author of the creation of the parcel
 */
async function addParcelToShipment(parcel, username) {
  const shipments = await findNotRemovedShipmentsBy({ _id: parcel.shipmentId });
  if (shipments.length > 0) {
    const shipment = shipments[0];
    shipment.parcels.push(parcel._id);
    /** save shipment */
    await Shipment.findOneAndUpdate(
      { _id: parcel.shipmentId },
      {
        parcels: shipment.parcels,
        modifiedBy: username,
      },
      { new: true }
    );
  } else {
    throw new Error(ERROR.NO_INVESTIGATION_FOUND.message);
  }
}

/**
 * Creates a new parcel
 * @param {*} sessionId
 * @param {*} username the user who has created the parcel
 * @param {*} investigationId
 * @param {*} myParcel The parcel object
 * @returns
 */
exports.createParcel = async function (sessionId, username, investigationId, myParcel) {
  /** Because a parcel is created from an investigation then it becomes automatically scheduled */
  myParcel.statuses = getInitialStatusByUsername(username).statuses;
  myParcel.currentStatus = getInitialStatusByUsername(username).currentStatus;

  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });
  if (investigations[0]) {
    myParcel.investigationName = investigations[0].name;
    myParcel.localContactNames = await getLocalContactNameListByInvestigationId(sessionId, investigationId);
    const parcel = await new Parcel(myParcel).save();
    global.gLogger.debug("Parcel created", { id: parcel._id });
    await addParcelToShipment(parcel, username);

    const newParcel = await findNotRemovedParcels({
      investigationId,
      _id: parcel._id,
    });
    return newParcel[0];
  }
};
exports.updateParcel = async function (parcel) {
  const {
    name,
    type,
    description,
    comments,
    storageConditions,
    shippingAddress,
    returnAddress,
    containsDangerousGoods,
    content,
    isReimbursed,
    reimbursedCustomsValue,
    returnInstructions,
    returnParcelInformation,
    shippingTrackingInformation,
    returnTrackingInformation,
  } = parcel;
  parcel = await Parcel.findOneAndUpdate(
    { _id: parcel._id },
    {
      name,
      type,
      description,
      comments,
      storageConditions,
      shippingAddress,
      returnAddress,
      containsDangerousGoods,
      content,
      isReimbursed,
      reimbursedCustomsValue,
      returnInstructions,
      returnParcelInformation,
      shippingTrackingInformation,
      returnTrackingInformation,
    },
    { new: true }
  );
  return parcel;
};

exports.createOrUpdateParcel = async (sessionId, investigationId, shipmentId, username, parcel) => {
  if (parcel.investigationId) {
    if (Number(investigationId) !== Number(parcel.investigationId)) {
      global.gLogger.error(`[createOrUpdateParcel] ${investigationId} does not match with investigationId of parcel ${parcel.investigationId}`);
      throw new Error(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message);
    }
  } else {
    parcel.investigationId = investigationId;
  }

  if (shipmentId !== parcel.shipmentId) throw new Error(ERROR.SHIPMENTID_DOES_NOT_MATCH.message);

  /** Check experiment and processing plan parameters */
  await checkParcelParameters(sessionId, parcel);

  if (parcel._id) {
    global.gLogger.debug("Updating parcel", { _id: parcel._id });
    parcel = await this.updateParcel(parcel);
    return parcel;
  }
  parcel = await this.createParcel(sessionId, username, investigationId, parcel);
  return parcel;
};

exports.deleteParcel = async (req, res) => {
  const { sessionId } = req.params;
  const { investigationId, shipmentId } = req.query;
  global.gLogger.debug("deleteParcel", { sessionId, investigationId, shipmentId, body: req.body });

  try {
    const parcel = await setStatus(sessionId, investigationId, req.body._id, "REMOVED", {}, req.session.name);
    res.send(parcel);
  } catch (error) {
    sendError(ERROR.PARCEL_FAILED_TO_DELETE.code, ERROR.PARCEL_FAILED_TO_DELETE.message, error, res);
  }
};

/**
 * Given a sessionId this method will return the investigationIds wich the user is participant or instrumentScientists
 * @param {*} sessionId
 * @returns
 */
async function getInvestigationIds(sessionId, name) {
  const [myInvestigations, localContactInvestigations] = await Promise.all([
    InvestigationAdapter.getInvestigationsBy(sessionId, { filter: "PARTICIPANT", username: name }),
    InvestigationAdapter.getInvestigationsBy(sessionId, { filter: "INSTRUMENTSCIENTIST", username: name }),
  ]);
  const investigations = myInvestigations.concat(localContactInvestigations);
  return investigations.map((inv) => inv.id);
}

/**
 * Builds the query to get the list of parcels.
 * @param {*} session
 * @param {*} investigationId
 * @param {*} shipmentId
 * @param {*} parcelId
 * @param {*} status
 * @param {*} search
 * @returns
 */
async function buildParcelQueryBy(session, investigationId, shipmentId, parcelId, status, search) {
  const $and = [];
  const { sessionId, name, isAdministrator } = session;
  /** Search by sessionId */
  if (!investigationId) {
    if (!isAdministrator) {
      const investigationIds = await getInvestigationIds(sessionId, name);
      $and.push({
        $or: [{ investigationId: { $in: investigationIds } }],
      });
    }
  } else {
    if (investigationId) {
      $and.push({
        $or: [{ investigationId: parseFloat(investigationId) }],
      });
    }
  }
  if (shipmentId) {
    $and.push({
      $or: [{ shipmentId }],
    });
  }
  if (parcelId) {
    $and.push({
      $or: [{ _id: parcelId }],
    });
  }

  if (search) {
    const searchCondition = { $regex: search, $options: "i" };
    const searchParameters = [
      "shippingAddress.name",
      "shippingAddress.surname",
      "shippingAddress.companyName",
      "shippingAddress.address",
      "shippingAddress.city",
      "shippingAddress.postalCode",
      "shippingAddress.country",
    ].map((param) => {
      const obj = {};
      obj[param] = searchCondition;
      return obj;
    });
    searchParameters.push({
      $or: [
        { name: searchCondition },
        { description: searchCondition },
        { investigationName: searchCondition },
        { currentStatus: searchCondition },
        { localContactNames: searchCondition },
      ],
    });
    $and.push({ $or: searchParameters });
  }

  if (status) {
    $and.push({
      $or: [{ currentStatus: status }],
    });
  }

  return $and.length === 0 ? {} : { $and };
}

/**
 * Gets parcels by query params
 * @param {*} req
 * @param {*} res
 * @param {*} nextp
 */
exports.getParcelsBy = async (req, res) => {
  const { investigationId, shipmentId, parcelId } = req.query;
  let { skip, limit, status, search } = req.query;

  global.gLogger.debug("getParcelsBy", { investigationId, shipmentId, skip, limit, parcelId, status, search });
  status = status ? status.toUpperCase() : undefined;
  search = getParamAsEscapedLowerString(search);

  const query = await buildParcelQueryBy(req.session, investigationId, shipmentId, parcelId, status, search);
  skip = isUndefined(skip) || skip === "undefined" ? 0 : skip;
  limit = isUndefined(limit) || limit === "undefined" ? global.gServerConfig.icat.maxQueryLimit : limit;

  const [parcels, total] = await Promise.all([findNotRemovedParcels(query, skip, limit), Parcel.countDocuments(query)]);

  global.gLogger.debug("Retrieved getParcelsBy", { skip, limit, total });
  setMetaDataTo(parcels, total, limit, skip);

  return res.send(parcels);
};

/**
 * Returns the configuration parameters needed for doing parcel declaration
 * @param {*} investigationId
 */
exports.getSetup = async (sessionId, investigationId) => {
  return getSetupConfigurationBy(sessionId, investigationId);
};
