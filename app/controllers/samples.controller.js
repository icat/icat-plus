const icat = require("../api/icat.js");
const { ERROR } = require("../errors.js");
const { sendError } = require("./helpers/helper.tracking.controller.js");
const { SampleAdapter } = require("../api/icat.js");
const { getParamAsEscapedLowerString, isUndefined, getSortOrder, getParamAsDateString } = require("./helpers/helper.controller.js");
const { populateOutputDatasets } = require("./datasets.controller.js");
const SampleInformation = require("../models/sampleinformation.model.js");
const ResourceInformation = require("../models/resourceinformation.model.js");
const { createModel } = require("mongoose-gridfs");
const multer = require("multer");

/**
 * Get all samples for a given investigationId.
 */
exports.getSamplesByInvestigationId = async (req, res) => {
  try {
    const { sessionId, investigationId } = req.params;
    const data = await SampleAdapter.getSamplesBy(sessionId, { investigationId });
    res.send(data);
  } catch (error) {
    sendError(500, "Failed to getSamplesByInvestigationId", error, res);
  }
};

function getParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "NAME":
        sortFilter = "sample.name";
        break;
      case "DATE":
        sortFilter = "sample.modTime";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[DATE|NAME]`);
    }
  }
  return sortFilter;
}

/**
 * Get all samples for a given investigationId.
 */
exports.getSamplesBy = async (req, res) => {
  try {
    const { sortBy, sortOrder, investigationId, instrumentName, datasetType, sampleIds, includeDatasets, nested, parameters } = req.query;
    let { startDate, endDate } = req.query;
    let { limit, skip, search, havingAcquisitionDatasets } = req.query;
    const { sessionId } = req.params;
    const maxQueryLimit = 10000; // override the global limit to ensure we will not retrieve too much samples if no filters
    limit = parseInt(limit) || maxQueryLimit;
    skip = parseInt(skip) || 0;
    search = getParamAsEscapedLowerString(search);

    startDate = getParamAsDateString(startDate, true);
    endDate = getParamAsDateString(endDate, true);

    global.gLogger.debug("getSamplesBy. ", {
      sessionId,
      investigationId,
      sampleIds,
      limit,
      skip,
      search,
      sortBy,
      sortOrder,
      datasetType,
      startDate,
      endDate,
      includeDatasets,
      nested,
      parameters,
      havingAcquisitionDatasets,
      instrumentName,
    });

    const formattedSortBy = getParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);
    const withDatasets = (includeDatasets && includeDatasets === "true") || parameters !== undefined || datasetType !== undefined || nested !== undefined;
    havingAcquisitionDatasets = !!havingAcquisitionDatasets && havingAcquisitionDatasets === "true";

    const samples = await SampleAdapter.getSamplesBy(sessionId, {
      investigationId,
      sampleIds,
      limit,
      skip,
      search,
      sortBy: formattedSortBy,
      sortOrder: formattedSortOrder,
      startDate,
      endDate,
      includeDatasets: withDatasets,
      parameters,
      havingAcquisitionDatasets,
      instrumentName,
    });

    if (datasetType) {
      for (let i = 0; i < samples.length; i++) {
        samples[i].datasets = samples[i].datasets.filter((ds) => ds.type === datasetType);
      }
    }
    for (let i = 0; i < samples.length; i++) {
      if (nested === "true") {
        samples[i].datasets = await populateOutputDatasets(sessionId, samples[i].datasets);
      }
    }
    return res.send(samples);
  } catch (e) {
    res.status(500).send(e.message);
  }
};

/**
 * Update sample parameter: only for editable samples and the description field or the sample comment
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} parameterId
 * @param {*} value
 * @param {*} res
 * @returns
 */
exports.updateSampleParameter = async (sessionId, investigationId, parameterId, value, res) => {
  global.gLogger.debug("updateSampleParameter", { investigationId, parameterId, value });

  if (!parameterId || !value) {
    return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
  }

  try {
    parameterId = parseInt(parameterId);
    const parameters = await icat.getSampleParameterById(sessionId, parameterId);
    if (parameters.length === 0) {
      return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
    }
    const sampleParameter = parameters[0].SampleParameter;
    const samples = await SampleAdapter.getSamplesBy(sessionId, { sampleIds: sampleParameter.sample.id });
    if (samples.length === 0) {
      return res.status(ERROR.NO_SAMPLE_FOUND.code).send({ message: ERROR.NO_SAMPLE_FOUND.message });
    }
    const sample = samples[0];

    if (sample.investigation.id !== parseInt(investigationId)) {
      return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
    }

    const parameterTypeName = sampleParameter.type.name;
    if (!canUpdateSampleParameter(sample, parameterTypeName)) {
      return res.status(ERROR.PARAMETER_NOT_EDITABLE.code).send({ message: ERROR.PARAMETER_NOT_EDITABLE.message });
    }
    await icat.updateSampleParameter(sessionId, parameterId, value);
    res.send({ id: parameterId, value, name: parameterTypeName });
  } catch (error) {
    sendError(500, "Failed to update Sample Parameter", error, res);
  }
};

/**
 * Create sample parameter: only for editable samples and the description field ro only for sample comment
 * @param {*} sessionId
 * @param {*} sampleId
 * @param {*} name
 * @param {*} value
 * @param {*} res
 * @returns
 */
exports.createSampleParameter = async (sessionId, sampleId, name, value, res) => {
  global.gLogger.debug("createSampleParameter", { sampleId, name, value });

  if (!sampleId || !value || !name) {
    return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
  }

  try {
    sampleId = parseInt(sampleId);
    const samples = await SampleAdapter.getSamplesBy(sessionId, { sampleIds: sampleId });
    if (samples.length === 0) {
      return res.status(ERROR.NO_SAMPLE_FOUND.code).send({ message: ERROR.NO_SAMPLE_FOUND.message });
    }
    const sample = samples[0];
    if (!canUpdateSampleParameter(sample, name)) {
      return res.status(ERROR.PARAMETER_NOT_EDITABLE.code).send({ message: ERROR.PARAMETER_NOT_EDITABLE.message });
    }
    const parameterTypes = await icat.getParameterTypeByName(sessionId, name);
    if (parameterTypes && parameterTypes.length > 0) {
      const sampleParameterType = parameterTypes[0].ParameterType;
      const parameter = await icat.createSampleParameter(sessionId, { id: sample.id }, { id: sampleParameterType.id }, value);
      res.send({ id: parameter[0], value, name });
    } else {
      return res.status(ERROR.NO_PARAMETER_BAD_PARAMS.code).send({ message: ERROR.NO_PARAMETER_BAD_PARAMS.message });
    }
  } catch (error) {
    sendError(500, "Failed to create Sample Parameter", error, res);
  }
};

function canUpdateSampleParameter(sample, parameterTypeName) {
  return (isEditableSample(sample) && isSampleParameterDescription(parameterTypeName)) || isSampleParameterComment(parameterTypeName);
}

function isSampleParameterDescription(sampleParameterType) {
  return sampleParameterType === global.gInvestigationConfig.sampleParameter.description;
}

function isSampleParameterComment(sampleParameterType) {
  return sampleParameterType === global.gInvestigationConfig.sampleParameter.comment;
}

function isEditableSample(sample) {
  const nonEditable = global.gInvestigationConfig.sampleParameter.nonEditable;
  return !sample.parameters.find((p) => p.name === nonEditable);
}

/** only used in the tests to remove created sample parameters */
exports.deleteSampleParameters = async (sampleParametersIds) => {
  global.gLogger.info("deleteSampleParameters", { sampleParametersIds });
  const sampleParameters = sampleParametersIds.map((id) => {
    return {
      SampleParameter: {
        id,
      },
    };
  });

  try {
    const session = await icat.asyncGetSession(global.gServerConfig.icat.authorizations.ingester.user);
    return await icat.deleteSampleParameters(session.sessionId, sampleParameters);
  } catch (error) {
    global.gLogger.error(`Error while removing sampleParameters ${error}`);
  }
};

/**
 * Get sample page
 */
exports.getSamplePage = async (req, res) => {
  try {
    const { sortBy, sortOrder, investigationId, instrumentName, sampleIds, parameters } = req.query;
    let { startDate, endDate } = req.query;
    let { limit, skip, search, havingAcquisitionDatasets } = req.query;
    const { sampleId, sessionId } = req.params;
    limit = parseInt(limit) || global.gServerConfig.icat.maxQueryLimit;
    skip = parseInt(skip) || 0;
    search = getParamAsEscapedLowerString(search);

    startDate = getParamAsDateString(startDate, true);
    endDate = getParamAsDateString(endDate, true);

    global.gLogger.debug("getSamplePage. ", {
      sessionId,
      investigationId,
      sampleIds,
      limit,
      skip,
      search,
      sortBy,
      sortOrder,
      startDate,
      endDate,
      parameters,
      havingAcquisitionDatasets,
      instrumentName,
    });

    const formattedSortBy = getParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);
    const withDatasets = parameters !== undefined;
    havingAcquisitionDatasets = !!havingAcquisitionDatasets && havingAcquisitionDatasets === "true";

    const params = {
      investigationId,
      sampleIds,
      search,
      sortBy: formattedSortBy,
      sortOrder: formattedSortOrder,
      startDate,
      endDate,
      includeDatasets: withDatasets,
      parameters,
      havingAcquisitionDatasets,
      instrumentName,
    };
    const samples = await SampleAdapter.getSamplesBy(sessionId, { ...params, sampleIds: sampleId });
    if (samples && samples.length === 1) {
      const [total, position] = await Promise.all([
        SampleAdapter.countSamplesBy(sessionId, params),
        SampleAdapter.countSamplesBy(sessionId, { ...params, sampleReferenceId: samples[0].id }),
      ]);

      global.gLogger.debug("Response", { total, position, limit, samples, index: Math.floor(position / limit) });

      const meta = {
        page: {
          total,
          totalPages: Math.ceil(total / limit),
          currentPage: Math.floor(position / limit),
        },
      };
      return res.send(meta);
    }

    res.status(ERROR.NO_SAMPLE_FOUND.code).send(ERROR.NO_SAMPLE_FOUND.message);
  } catch (e) {
    res.status(500).send(e.message);
  }
};

exports.uploadFile = async (req, res) => {
  // Create a storage object with a given configuration
  const storage = require("multer-gridfs-storage")({
    url: global.gServerConfig.database.uri,
    file: (req, file) => {
      return {
        filename: file.originalname,
      };
    },
  });

  // Set multer storage engine to the newly created object
  const upload = multer({ storage }).single("file");
  upload(req, res, async (err) => {
    const { fileType, multiplicity, groupName } = req.body;
    const { sessionId } = req.params;
    const { sampleId } = req.query;
    global.gLogger.info("upload sample file", { sampleId, fileType, multiplicity, groupName });

    if (err) {
      return res.json({ error_code: 1, err_desc: err });
    }

    if (isUndefined(sampleId)) {
      return res.status(ERROR.NO_SAMPLE_BAD_PARAMS.code).send({ message: ERROR.NO_SAMPLE_BAD_PARAMS.message });
    }

    const samples = await SampleAdapter.getSamplesBy(sessionId, { sampleIds: sampleId });
    if (!samples || samples.length === 0) {
      return res.status(ERROR.NO_SAMPLE_BAD_PARAMS.code).send({ message: ERROR.NO_SAMPLE_BAD_PARAMS.message });
    }
    const sample = samples[0];
    if (isEditableSample(sample)) {
      return res.status(ERROR.SAMPLE_NOT_FILE_ASSOCIATED.code).send({ message: ERROR.SAMPLE_NOT_FILE_ASSOCIATED.message });
    }

    const filename = req.file.filename;
    const sampleInformations = await SampleInformation.find({ investigationId: sample.investigation.id, sampleId });
    let resources = [];
    if (sampleInformations && sampleInformations.length > 0) {
      resources = sampleInformations[0].resources;
      if (resources) {
        const resource = resources.find((r) => r.filename === filename);
        if (resource) {
          return res.status(ERROR.SAMPLE_FILE_ALREADY_EXIST.code).send({ message: ERROR.SAMPLE_FILE_ALREADY_EXIST.message });
        }
      } else {
        resources = [];
      }
    }
    new ResourceInformation({
      filename,
      file: req.file.id,
      fileType,
      groupName,
      multiplicity,
    })
      .save()
      .then(async (resourceInformation) => {
        global.gLogger.info("Upload file done successfully");
        resources.push(resourceInformation);
        const options = { upsert: true, omitUndefined: true };
        const conditions = { sampleId, investigationId: sample.investigation.id };
        await SampleInformation.updateOne(conditions, { resources }, options);
        const sampleInformation = await SampleInformation.find(conditions);
        res.send(sampleInformation && sampleInformation.length > 0 ? sampleInformation[0] : undefined);
      })
      .catch((e) => {
        global.gLogger.error("Error uploading file as resource", { e: e.message });
        res.status(500).send({
          message: e.message || "Some error occurred while creating the Sample information.",
        });
      });
  });
};

exports.getSampleFiles = async (req, res) => {
  try {
    const { groupName, sampleId } = req.query;
    global.gLogger.info("get samples files", { sampleId, groupName });
    if (isUndefined(sampleId)) {
      return res.status(ERROR.NO_SAMPLE_BAD_PARAMS.code).send({ message: ERROR.NO_SAMPLE_BAD_PARAMS.message });
    }
    const sampleInformations = await SampleInformation.find({ sampleId });
    global.gLogger.info(`find sampleInformations ${sampleInformations.length}`);
    let sampleInformation = sampleInformations && sampleInformations.length > 0 ? sampleInformations[0] : undefined;
    if (sampleInformation && groupName) {
      const filteredResources = sampleInformation.resources.filter((r) => r.groupName.toLowerCase() === groupName.toLowerCase());
      if (filteredResources.length > 0) {
        sampleInformation.resources = filteredResources;
      } else {
        sampleInformation = undefined;
      }
    }
    res.send(sampleInformation);
  } catch (e) {
    res.status(500).send(e.message);
  }
};

exports.downloadFile = async (req, res) => {
  try {
    const { sampleId, resourceId } = req.query;
    global.gLogger.info("download sample files", { sampleId, resourceId });
    if (isUndefined(sampleId) || isUndefined(resourceId)) {
      return res.status(ERROR.BAD_PARAMS.code).send({ message: ERROR.BAD_PARAMS.message });
    }
    const Attachment = createModel({
      modelName: "fs",
    });

    const file = await Attachment.findById(resourceId);

    const length = file.length;
    res.header("Content-Length", length);

    const readStream = file.read();
    readStream.pipe(res);
    readStream.on("error", (error) => {
      global.gLogger.error(error);
    });
  } catch (e) {
    global.gLogger.error("Download", { error: e.message });
    res.status(500).send(e.message);
  }
};
