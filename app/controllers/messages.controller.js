const Message = require("../models/message.model.js");

exports.getMessages = async (type) => {
  global.gLogger.debug(`getMessages`);
  const query = type ? { type: type.toUpperCase() } : {};
  const messages = await Message.find(query);
  return messages;
};

exports.updateInformationMessage = async (body) => {
  global.gLogger.debug(`updateInformationMessage body=${JSON.stringify(body)}`);
  const informationType = "INFORMATION";
  const set = {};

  set.message = body.message;
  const update = {
    $set: set,
  };
  const conditions = { type: informationType };
  const options = { upsert: true, omitUndefined: true };
  await Message.updateOne(conditions, update, options);
  const messages = await this.getMessages(informationType);
  if (messages && messages.length > 0) {
    return messages[0];
  }
  return undefined;
};
