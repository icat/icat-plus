const icat = require("../api/icat.js");
const { sendError } = require("../errors.js");
const { getParamAsEscapedLowerString, getSortOrder } = require("./helpers/helper.controller.js");
const investigationHelper = require("../helpers/investigation.js");

function getDataCollectionParamAsSortBy(sortBy) {
  let sortFilter;
  if (sortBy) {
    switch (sortBy.toUpperCase()) {
      case "DATE":
        sortFilter = "dataCollection.createTime";
        break;
      case "DOI":
        sortFilter = "dataCollection.doi";
        break;
      default:
        throw new Error(`Parameter sortBy is invalid. sortBy=${sortBy}. Valid values:[DOI|DATE]`);
    }
  }
  return sortFilter;
}

exports.getDataCollectionsBy = async (req, res) => {
  const { datasetId, type, limit, skip, sortBy, sortOrder } = req.query;
  let { search } = req.query;
  global.gLogger.debug("getDataCollectionsBy", { datasetId, type, skip, limit, search, sortBy, sortOrder });
  try {
    search = getParamAsEscapedLowerString(search);
    let searchAsNormalizedInvestigation;
    try {
      searchAsNormalizedInvestigation = investigationHelper.normalize(search).toLowerCase();
      // eslint-disable-next-line no-unused-vars
    } catch (e) {}
    const formattedSortBy = getDataCollectionParamAsSortBy(sortBy);
    const formattedSortOrder = getSortOrder(formattedSortBy, sortOrder);
    const data = await icat.DataCollectionAdapter.getDataCollectionsBy(req.params.sessionId, {
      datasetId,
      type,
      limit,
      skip,
      search,
      sortBy: formattedSortBy,
      sortOrder: formattedSortOrder,
      searchAsNormalizedInvestigation,
    });
    return res.send(data);
  } catch (e) {
    sendError(500, "Failed to getDataCollectionsBy", e, res);
  }
};
