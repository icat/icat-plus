const Shipment = require("../models/shipment.model.js");
const helper = require("./helpers/helper.tracking.controller.js");
const { ERROR, sendError } = require("../errors.js");

/**
 * This method is called before the authorization middleware and will find the shipment Id and will add it to the request then the authorization layer can authoriza the sessionId with investigation
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function addInvestigationIdToRequest(req, res, next) {
  global.gLogger.info("addInvestigationIdToRequest", {
    sessionId: req.params.sessionId,
    shipmentId: req.params.shipmentId,
  });
  try {
    const shipment = await Shipment.findById(req.params.shipmentId);
    if (shipment) {
      global.gLogger.info("addInvestigationIdToRequest", { investigationId: shipment.investigationId });
      req.params.investigationId = shipment.investigationId;
      return next();
    }
    throw ERROR.NO_SHIPMENT_FOUND;
  } catch (e) {
    helper.sendError(500, "addInvestigationIdToRequest", e, res);
  }
}

async function getShipmentsBy(req, res) {
  const { sessionId } = req.params;
  const { investigationId, shipmentId } = req.query;
  global.gLogger.info("getShipmentsBy", { sessionId, shipmentId, investigationId });
  try {
    let shipments = [];
    if (!shipmentId) {
      shipments = await helper.findNotRemovedShipmentsBy({ investigationId });
    }
    if (shipmentId) {
      shipments = await helper.findNotRemovedShipmentsBy({ investigationId, _id: shipmentId });
    }
    res.send(shipments);
  } catch (e) {
    helper.sendError(500, "getShipmentsBy", e, res);
  }
}

/**
 * It creates a shipment. It receives the shipment document in the body.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function createShipment(req, res) {
  const { sessionId } = req.params;
  const { investigationId } = req.query;
  global.gLogger.info("createShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    let shipment = new Shipment(req.body);
    shipment = await helper.createShipment(shipment, investigationId, sessionId);
    res.send(shipment);
  } catch (error) {
    helper.sendError(500, "Failted to createShipment", error, res);
  }
}

/**
 * It updates the updatable parameters of a shipment
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function updateShipment(req, res) {
  const { sessionId } = req.params;
  const { investigationId } = req.query;
  global.gLogger.debug("updateShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    const shipment = await Shipment.findOneAndUpdate(
      { _id: req.body._id },
      {
        name: req.body.name,
        comments: req.body.comments,
        defaultReturnAddress: req.body.defaultReturnAddress,
        defaultShippingAddress: req.body.defaultShippingAddress,
        modifiedBy: req.session.name,
      },
      { new: true }
    )
      .populate("defaultShippingAddress")
      .populate("defaultReturnAddress");
    res.send(shipment);
  } catch (e) {
    helper.sendError(500, "updateShipment", e, res);
  }
}

async function deleteShipment(req, res) {
  const { sessionId } = req.params;
  const { investigationId } = req.query;
  global.gLogger.debug("deleteShipment", { sessionId, investigationId, shipment: JSON.stringify(req.body) });

  if (Number(investigationId) !== Number(req.body.investigationId)) {
    return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  }

  try {
    const shipment = await Shipment.findOneAndUpdate(
      { _id: req.body._id },
      {
        status: "REMOVED",
        deletedBy: req.session.name,
      },
      { new: true }
    );
    res.send(shipment);
  } catch (e) {
    helper.sendError(500, "delete", e, res);
  }
}

module.exports = {
  addInvestigationIdToRequest,
  createShipment,
  updateShipment,
  deleteShipment,
  getShipmentsBy,
};
