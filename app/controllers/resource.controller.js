const Event = require("../models/event.model.js");
const constants = require("../constants.js");
const { createModel } = require("mongoose-gridfs");
const { ERROR } = require("../errors.js");
const path = require("path");
// Create a storage object with a given configuration
const storage = require("multer-gridfs-storage")({
  url: global.gServerConfig.database.uri,
  file: (req, file) => {
    return {
      filename: file.originalname,
    };
  },
});

const multer = require("multer");
const { getInstrumentByName } = require("../cache/cache.js");
// Set multer storage engine to the newly created object
const upload = multer({ storage }).single("file");

// Create and Save a new event
exports.upload = (req, res) => {
  upload(req, res, async (err) => {
    global.gLogger.info("upload", req.query);
    let { instrumentName } = req.query;
    const { investigationId } = req.query;
    const { datasetId, machine, software, creationDate, text } = req.body;
    const { name, fullName } = req.session;

    if (err) {
      return res.json({ error_code: 1, err_desc: err });
    }

    if (isNaN(investigationId) && !instrumentName) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send({ message: ERROR.NO_INVESTIGATION_BAD_PARAMS.message });
    }

    if (instrumentName) {
      instrumentName = instrumentName.toUpperCase();
      if (!getInstrumentByName(instrumentName)) {
        return res.status(ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.code).send({ message: ERROR.WRONG_INSTRUMENTNAME_BAD_PARAMS.message });
      }
    }

    global.gLogger.info("Upload file", { investigationId, instrumentName });

    /**
     * Creating the event
     */
    const event = new Event({
      investigationId,
      datasetId,
      type: constants.EVENT_TYPE_ATTACHMENT,
      category: constants.EVENT_CATEGORY_FILE,
      text,
      filename: req.file.filename,
      username: name,
      software,
      machine,
      instrumentName,
      creationDate,
      file: req.file.id,
      contentType: req.file.contentType,
    });
    event
      .save()
      .then(async (event) => {
        global.gLogger.info("Upload file done successfully");
        event.username = fullName;
        res.send(event);
      })
      .catch((e) => {
        global.gLogger.error("Error uploading file as resource", { e: e.message });
        res.status(500).send({
          message: e.message || "Some error occurred while creating the Note.",
        });
      });
  });
};

/**
 * Download a file given a eventId.
 *
 **/
exports.download = async (eventId, res) => {
  const Attachment = createModel({
    modelName: "fs",
  });

  try {
    const event = await Event.findById(eventId);
    const { filename } = event;
    if (filename) {
      const extension = path.parse(filename).ext;
      switch (extension.toLocaleLowerCase()) {
        case ".svg":
          res.header("Content-Type", "image/svg+xml");
          break;
        case ".gif":
          res.header("Content-Type", "image/gif");
          break;

        default:
          break;
      }
    }

    const fileId = event.file[0];
    const file = await Attachment.findById(fileId);

    const length = file.length;
    res.header("Content-Length", length);

    const readStream = file.read();
    readStream.pipe(res);
    readStream.on("error", (error) => {
      global.gLogger.error(error);
    });
  } catch (err) {
    global.gLogger.error("Download", { error: err.message });
    res.status(500).send({
      message: "Event not found",
    });
  }
};
