const Address = require("../models/address.model.js");
const { InvestigationAdapter } = require("../api/icat.js");

const helper = require("./helpers/helper.tracking.controller.js");
const { sendError } = require("./helpers/helper.tracking.controller.js");
/**
 * It marks an existing address as REMOVED
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function deleteAddress(req, res) {
  const { sessionId } = req.params;
  const { username } = req.session;
  global.gLogger.debug("deleteAddress", { sessionId, username, address: JSON.stringify(req.body) });

  try {
    const address = await Address.findOneAndUpdate(
      { _id: req.body._id },
      {
        status: "REMOVED",
        deletedBy: username,
      },
      { new: true }
    );
    res.send(address);
  } catch (error) {
    sendError(500, "Failed to deleteAddress", error, res);
  }
}

/**
 * It updates an existing address
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function updateAddress(req, res) {
  const { sessionId } = req.params;
  global.gLogger.debug("updateAddress", { sessionId, address: JSON.stringify(req.body) });

  //if (Number(req.params.investigationId) !== Number(req.body.investigationId)) {
  //  return sendError(ERROR.INVESTIGATIONID_DOES_NOT_MATCH.code, ERROR.INVESTIGATIONID_DOES_NOT_MATCH.message, ERROR.INVESTIGATIONID_DOES_NOT_MATCH, res);
  //}

  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId: req.body.investigationId });
    const address = await Address.findOneAndUpdate(
      { _id: req.body._id },
      {
        investigationId: investigations[0].id,
        investigationName: investigations[0].name,
        name: req.body.name,
        surname: req.body.surname,
        address: req.body.address,
        region: req.body.region,
        companyName: req.body.companyName,
        city: req.body.city,
        postalCode: req.body.postalCode,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        country: req.body.country,
        defaultCourierAccount: req.body.defaultCourierAccount,
        defaultCourierCompany: req.body.defaultCourierCompany,
        modifiedBy: req.session.username,
      },
      { new: true }
    );
    res.send(address);
  } catch (e) {
    helper.sendError(500, "updateAddress", e, res);
  }
}

/**
 * It creates a new address
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function createAddress(req, res) {
  const { sessionId, investigationId } = req.params;
  const { username } = req.session;
  global.gLogger.debug("createAddress", { sessionId, address: JSON.stringify(req.body) });
  let address = req.body;
  try {
    address = await helper.createAddress(address, investigationId, sessionId, username);
    res.send(address);
  } catch (e) {
    helper.sendError(500, "Failed to createAddress", e, res);
  }
}

function findAddressNotRemoved(query) {
  return Address.find({ ...query, status: { $ne: "REMOVED" } });
}

/**
 * Returns addresses associated to an investigation
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getAddressesByInvestigationId(req, res) {
  const { sessionId, investigationId } = req.params;
  global.gLogger.debug("getAddresses", { sessionId, investigationId });
  try {
    const addresses = await findAddressNotRemoved({ investigationId });
    res.send(addresses);
  } catch (error) {
    helper.sendError(500, "Failed to getAddressesByInvestigationId", error, res);
  }
}

const getUserRegExp = (username) => {
  const userInfo = username.split("/");
  const searchUsername = userInfo[1] ? `.*/${userInfo[1]}` : userInfo[0];
  return new RegExp(`${searchUsername}`);
};
/**
 * Returns addresses for associated to all investigations of an user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function getAddressesByUsername(req, res) {
  const { sessionId } = req.params;
  const { username } = req.session;
  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { filter: "PARTICIPANT", username });
    const myInvestigationIds = investigations.map((inv) => {
      return inv.id;
    });
    const addresses = await findAddressNotRemoved({
      $or: [{ createdBy: getUserRegExp(username) }, { investigationId: { $in: myInvestigationIds } }],
    });

    res.send(addresses);
  } catch (error) {
    helper.sendError(500, "Failed to getAddressesByUsername", error, res);
  }
}

module.exports = {
  deleteAddress,
  updateAddress,
  createAddress,
  getAddressesByInvestigationId,
  getAddressesByUsername,
};
