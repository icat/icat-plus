const { DatasetSQL, TimeLineDatasetSQL } = require("../sql/dataset.sql.js");
const { buildlimit, getSessionQuery, buildFiltersConditions } = require("../query.js");
const ICAT = global.gServerConfig.icat;
const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

/**
 *
 * @param {*} sessionId
 * @param {*} startDate
 * @param {*} endDate
 * @param {*} limit
 * @param {*} skip
 * @param {*} search
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} instrument
 * @param {*} filter
 * @param {*} investigationName
 * @param {*} username
 * @param {*} query
 * @param {*} includeClause
 * @returns
 */
const doDatasetQuery = (sessionId, params) => {
  const {
    query,
    includeClause,
    sampleId,
    instrumentName,
    datasetId,
    datasetType,
    startDate,
    endDate,
    limit,
    skip,
    search,
    sortBy,
    sortOrder,
    investigationIds,
    doi,
    noBuildLimit,
    parameters,
  } = params;

  const orderStatement = noBuildLimit ? "" : DatasetSQL.sort.by(sortBy, sortOrder);
  const sqlFilters = [
    DatasetSQL.where.betweenDate(startDate, endDate),
    DatasetSQL.where.search(search),
    DatasetSQL.where.equalInvestigationId(investigationIds),
    DatasetSQL.where.equalInstrumentName(instrumentName),
    DatasetSQL.where.equalDOI(doi),
    DatasetSQL.where.equalDatasetType(datasetType),
    DatasetSQL.where.equalSampleId(sampleId),
    DatasetSQL.where.equalDatasetId(datasetId),
    DatasetSQL.where.filterOnParameters(investigationIds, sampleId, datasetId, parameters),
  ];

  const limitStatement = !noBuildLimit ? buildlimit(limit, skip) : "";

  const join = DatasetSQL.join.byDOI(doi);
  const joinParameters = DatasetSQL.join.byDatasetParameters(search, parameters);

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const fullQuery = `${prefix}query=${query} ${joinParameters} ${join}  ${buildFiltersConditions(sqlFilters)} ${orderStatement} ${includeClause} ${limitStatement}`;
  return encodeURI(fullQuery);
};

exports.getCountDatasetBy = (sessionId, params) => {
  /** this makes that the query will not have the limit and skip that prevents the count to work */
  params.noBuildLimit = true;
  return doDatasetQuery(sessionId, { ...{ includeClause: "" }, ...{ query: DatasetSQL.countQuery }, ...params });
};

exports.getDatasetsBy = (sessionId, params, include) => {
  params.noBuildLimit = false;
  return doDatasetQuery(sessionId, { ...{ includeClause: DatasetSQL.include.get(include) }, ...{ query: DatasetSQL.query }, ...params });
};

exports.getTimeLineBy = (sessionId, params) => {
  params.noBuildLimit = false;

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const { investigationId, sampleId } = params;
  const sqlFilters = [
    TimeLineDatasetSQL.where.equalInvestigationId(investigationId),
    TimeLineDatasetSQL.where.equalSampleId(sampleId),
    "(parameterType.name in ('definition', '__fileCount' ,'__volume', '__elapsedTime'))",
  ];

  const fullQuery = `${prefix}query=${TimeLineDatasetSQL.query} ${buildFiltersConditions(sqlFilters)} ${TimeLineDatasetSQL.include} `;
  return encodeURI(fullQuery);
};

exports.getTimeLineForAdminBy = (sessionId, params) => {
  params.noBuildLimit = false;

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const { investigationId, sampleId } = params;
  const sqlFilters = [
    TimeLineDatasetSQL.where.equalInvestigationId(investigationId),
    TimeLineDatasetSQL.where.equalSampleId(sampleId),
    "(parameterType.name in ('definition', '__fileCount' ,'__volume', '__elapsedTime'))",
  ];

  const fullQuery = `${prefix}query=${TimeLineDatasetSQL.adminQuery} ${buildFiltersConditions(sqlFilters)} `;
  return encodeURI(fullQuery);
};
