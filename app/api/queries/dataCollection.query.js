const { DataCollectionSQL } = require("../sql/dataCollection.sql.js");
const { buildlimit, getSessionQuery, buildFiltersConditions } = require("../query.js");
const ICAT = global.gServerConfig.icat;
const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

/**
 *
 * @param {*} sessionId
 * @param {*} limit
 * @param {*} skip
 * @param {*} search
 * @param {*} searchAsNormalizedInvestigation
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} type
 * @param {*} query
 * @param {*} includeClause
 * @param {*} noBuildLimit
 * @returns
 */
const doQuery = (sessionId, params) => {
  const {
    query,
    includeClause,
    datasetId,
    limit,
    skip,
    search,
    searchAsNormalizedInvestigation,
    sortBy,
    sortOrder,
    type,
    noBuildLimit,
    hasNoDOIs,
    dataCollectionId,
    usernameParticipant,
  } = params;

  const joinInvestigationUsers = DataCollectionSQL.join.joinInvestigationUsers(usernameParticipant);
  const limitStatement = !noBuildLimit ? buildlimit(limit, skip) : "";
  const orderStatement = DataCollectionSQL.sort.by(sortBy, sortOrder);
  const sqlFilters = [
    DataCollectionSQL.where.equalDatasetId(datasetId),
    DataCollectionSQL.where.equalDataCollectionId(dataCollectionId),
    DataCollectionSQL.where.search(search, searchAsNormalizedInvestigation),
    DataCollectionSQL.where.typePublisher(type),
    DataCollectionSQL.where.participantIs(usernameParticipant),
  ];
  if (hasNoDOIs) {
    sqlFilters.push(DataCollectionSQL.where.doiIsNull());
  } else {
    sqlFilters.push(DataCollectionSQL.where.doiIsNotNull());
  }

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const fullQuery = `${prefix}query=${query} ${joinInvestigationUsers} ${buildFiltersConditions(sqlFilters)} ${orderStatement} ${includeClause} ${limitStatement}`;
  global.gLogger.debug(fullQuery);
  return encodeURI(fullQuery);
};

exports.getCountDataCollectionsBy = (sessionId, params) => {
  /** this makes that the query will not have the limit and skip that prevents the count to work */
  params.noBuildLimit = true;
  return doQuery(sessionId, { ...{ includeClause: "" }, ...{ query: DataCollectionSQL.countQuery }, ...params });
};

exports.getDataCollectionsBy = (sessionId, params, include) => {
  params.noBuildLimit = false;
  return doQuery(sessionId, { ...{ includeClause: DataCollectionSQL.include.get(include) }, ...{ query: DataCollectionSQL.query }, ...params });
};
