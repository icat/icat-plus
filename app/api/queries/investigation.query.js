const { InvestigationSQL, TimeLineInvestigationSQL } = require("../sql/investigation.sql.js");
const { buildlimit, getSessionQuery, buildFiltersConditions } = require("../query.js");
const ICAT = global.gServerConfig.icat;
const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

/**
 *
 * @param {*} sessionId
 * @param {*} startDate
 * @param {*} endDate
 * @param {*} limit
 * @param {*} skip
 * @param {*} search
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} instrument
 * @param {*} filter
 * @param {*} investigationName
 * @param {*} username
 * @param {*} query
 * @param {*} includeClause
 * @returns
 */
const doInvestigationQuery = (sessionId, params) => {
  const {
    query,
    includeClause,
    startDate,
    endDate,
    limit,
    skip,
    search,
    sortBy,
    sortOrder,
    instrumentName,
    filter,
    investigationName,
    username,
    investigationId,
    doi,
    time,
    investigationUsername,
    noBuildLimit,
    searchAsNormalizedInvestigation,
    parameters,
  } = params;
  const orderStatement = InvestigationSQL.sort.by(sortBy, sortOrder);
  const sqlFilters = [
    InvestigationSQL.where.betweenDate(startDate, endDate),
    InvestigationSQL.where.fitTime(time),
    InvestigationSQL.where.search(search, searchAsNormalizedInvestigation),
    InvestigationSQL.where.equalInstrumentName(instrumentName),
    InvestigationSQL.where.equalInvestigationId(investigationId),
    InvestigationSQL.where.equalDOI(doi),
    InvestigationSQL.where.equalFilter(filter, username),
    InvestigationSQL.where.equalVisitIdAndLikeInvestigationName(investigationName),
    InvestigationSQL.where.investigationHasUser(investigationUsername),
    InvestigationSQL.where.filterOnParameters(investigationId, parameters),
  ];
  const limitStatement = !noBuildLimit ? buildlimit(limit, skip) : "";
  const join = InvestigationSQL.join.byFilter(filter, investigationUsername);
  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const fullQuery = `${prefix}query=${query} ${join}  ${buildFiltersConditions(sqlFilters)} ${orderStatement} ${includeClause} ${limitStatement}`;
  global.gLogger.debug(fullQuery);
  return encodeURI(fullQuery);
};

exports.getCountInvestigationBy = (sessionId, params) => {
  params.noBuildLimit = true;
  return doInvestigationQuery(sessionId, { ...{ includeClause: "" }, ...{ query: InvestigationSQL.countQuery }, ...params });
};

exports.getInvestigationsBy = (sessionId, params, include) => {
  params.noBuildLimit = false;
  return doInvestigationQuery(sessionId, { ...{ includeClause: InvestigationSQL.include.get(include) }, ...{ query: InvestigationSQL.query }, ...params });
};

exports.getTimeLineBy = (sessionId, params) => {
  params.noBuildLimit = false;

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const { instrument } = params;
  const sqlFilters = [
    TimeLineInvestigationSQL.where.equalInstrument(instrument),

    "(parameterType.name = 'definition' or parameterType.name = '__fileCount' or parameterType.name = '__volume' or parameterType.name = '__elapsedTime')",
  ];

  const fullQuery = `${prefix}query=${TimeLineInvestigationSQL.query} ${buildFiltersConditions(sqlFilters)} `;
  global.gLogger.debug(fullQuery);
  return encodeURI(fullQuery);
};
