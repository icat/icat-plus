const { SampleSQL } = require("../sql/sample.sql.js");
const { buildlimit, getSessionQuery, buildFiltersConditions } = require("../query.js");
const ICAT = global.gServerConfig.icat;
const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

/**
 *
 * @param {*} sessionId
 * @param {*} startDate
 * @param {*} endDate
 * @param {*} limit
 * @param {*} skip
 * @param {*} search
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} filter
 * @param {*} username
 * @param {*} query
 * @param {*} includeClause
 * @returns
 */
const doQuery = (sessionId, params) => {
  const {
    query,
    sampleIds,
    startDate,
    endDate,
    limit,
    skip,
    search,
    sortBy,
    sortOrder,
    investigationId,
    includeDatasets,
    noBuildLimit,
    parameters,
    havingAcquisitionDatasets,
    instrumentName,
    sampleReferenceId,
  } = params;
  let { includeClause } = params;

  const orderStatement = SampleSQL.sort.by(sortBy, sortOrder);
  const sqlFilters = [
    SampleSQL.where.betweenDate(startDate, endDate),
    SampleSQL.where.search(search),
    SampleSQL.where.equalInvestigationId(investigationId),
    SampleSQL.where.equalSampleIds(sampleIds),
    SampleSQL.where.filterOnParameters(investigationId, sampleIds, parameters),
    SampleSQL.where.filterOnHasAcquisitionDatasets(havingAcquisitionDatasets),
    SampleSQL.where.equalInstrumentName(instrumentName),
    SampleSQL.where.filterBySampleReference(sortBy, sortOrder, sampleReferenceId),
  ];
  // to fix the issue on sort by dataset date, we sort on parameter time
  // if we don't have the filter on acquisitionDatasets, we set by default datasetCount
  if (!havingAcquisitionDatasets && sortBy && sortBy.indexOf("parameters.modTime") > -1) {
    sqlFilters.push(SampleSQL.where.filterOnDatasetsCount());
  }

  if (includeDatasets) {
    const includeClauseDatasets = ", sample.datasets datasets, datasets.type datasetType, datasets.parameters datasetParameters, datasetParameters.type datasetParametersType";
    if (includeClause !== "" && includeClause.indexOf("sample.datasets") < 0) {
      includeClause += includeClauseDatasets;
    }
  }

  const limitStatement = !noBuildLimit ? buildlimit(limit, skip) : "";

  const prefix = `${ICAT_ENTITY_MANAGER + getSessionQuery(sessionId)}&`;
  const fullQuery = `${prefix}query=${query}  ${buildFiltersConditions(sqlFilters)} ${orderStatement} ${includeClause} ${limitStatement}`;
  global.gLogger.debug(fullQuery);
  return encodeURI(fullQuery);
};

exports.getCountSamplesBy = (sessionId, params) => {
  /** this makes that the query will not have the limit and skip that prevents the count to work */
  params.noBuildLimit = true;
  const query = SampleSQL.getCountQuery(params);

  return doQuery(sessionId, { ...{ includeClause: "" }, ...{ query }, ...params });
};

exports.getSamplesBy = (sessionId, params, include) => {
  params.noBuildLimit = false;
  return doQuery(sessionId, { ...{ includeClause: SampleSQL.include.get(include) }, ...{ query: SampleSQL.getQuery(params) }, ...params });
};
