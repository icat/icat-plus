const { filterOnParameters } = require("./helper.sql");

exports.DatasetSQL = {
  query: `select distinct(dataset) from Dataset dataset JOIN dataset.investigation investigation JOIN dataset.type datasetType LEFT JOIN dataset.sample sample JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument`,
  countQuery: `select count(distinct(dataset)) from Dataset dataset JOIN dataset.investigation investigation JOIN dataset.type datasetType LEFT JOIN dataset.sample sample  JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument`,

  include: {
    get(extra) {
      const defaultInclude = `include dataset.type type, dataset.investigation investigation, investigation.type investigationType, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, dataset.parameters parameters, parameters.type, dataset.sample, dataset.datasetTechniques datasetTechniques, datasetTechniques.technique technique`;
      if (extra) {
        return ` ${defaultInclude}, ${extra}`;
      }
      return defaultInclude;
    },
  },
  join: {
    byDOI(doi) {
      if (doi) {
        return " LEFT JOIN dataset.dataCollectionDatasets dcds LEFT JOIN dcds.dataCollection dataCollection ";
      }
      return "";
    },
    byDatasetParameters(search, parameters) {
      if (parameters || search) {
        return " JOIN dataset.parameters parameters JOIN parameters.type parameterType ";
      }
      return "";
    },
  },
  sort: {
    by(sortBy, sortOrder) {
      const order = sortOrder === 1 ? "ASC" : "DESC";
      const clause = sortBy ? `${sortBy} ${order}` : "dataset.startDate DESC ";
      return `order by ${clause}`;
    },
  },
  where: {
    equalDatasetType(datasetType) {
      return datasetType ? ` datasetType.name = '${datasetType}' ` : "";
    },

    equalSampleId(sampleId) {
      return sampleId ? `sample.id=${sampleId}` : "";
    },
    equalDatasetId(datasetId) {
      return datasetId ? `dataset.id IN (${datasetId})` : "";
    },

    /**
     * It returns the JPQL where clause
     * @param {*} investigationIds id or list of comma separated id list
     * @returns
     */
    equalInvestigationId(investigationIds) {
      if (investigationIds) {
        const ids = investigationIds.toString().split(",");
        if (ids.length === 1) {
          return ` investigation.id = '${investigationIds}' `;
        }
        return ` investigation.id IN (${investigationIds}) `;
      }
      return "";
    },
    equalDOI(doi) {
      return doi ? ` ( dataCollection.doi = '${doi}' OR investigation.doi = '${doi}' OR dataset.doi = '${doi}' ) ` : "";
    },

    equalInstrumentName(instrumentName) {
      return instrumentName ? `LOWER(instrument.name)='${instrumentName.toLowerCase()}'` : "";
    },

    /**
     * builds a sql clause to filter investigations by  name and visitId
     * Example of returned clause:
     * ""investigation.visitId = 'PROPOSAL' AND (investigation.name like 'IX%' OR investigation.name like 'FX%' OR investigation.name like 'IN%' OR investigation.name like 'IM%') "
     * @param {*} investigationName array of investigation name
     * @param {*} visitId filter on the visitId
     * @returns the clause
     */
    equalVisitIdAndLikeInvestigationName(investigationName, visitId) {
      const clauseVisitId = visitId && visitId.length > 0 ? `investigation.visitId = '${visitId}'` : "";

      let clauseName = investigationName ? `(investigation.name LIKE '${investigationName}%')` : "";
      if (Array.isArray(investigationName)) {
        clauseName = investigationName && investigationName.length > 0 ? `(${investigationName.map((n) => `investigation.name LIKE '${n}%'`).join(" OR ")})` : "";
      }

      const allDefinedFilters = [clauseVisitId, clauseName].filter((f) => f !== "");
      const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
      return filters;
    },

    betweenDate(startDate, endDate) {
      const filter = startDate && endDate ? ` (dataset.startDate >= '${startDate}' and dataset.endDate <= '${endDate}') ` : "";
      return filter;
    },

    /**
     * builds a sql clause to search a word in different fields. The search is based on lower case.
     * The fields are dataset name, sample name, definition, volume, fileCount
     * Example of returned clause:
     * "(LOWER(dataset.name) LIKE '%search%' OR  LOWER(sample.name) LIKE '%search%'
     *  OR (LOWER(parameter.stringValue) LIKE '%search%' AND parameterType.name is 'definition'))"
     * @param {*} search the word to search
     * @returns the clause
     */
    search(search) {
      const likeSearch = search ? `LIKE '%${search}%'` : "";
      const searchFields = ["dataset.name", "sample.name"];
      if (search) {
        const sqlSearch = `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")}`;
        const parameterSearch = ` OR (LOWER(parameters.stringValue) ${likeSearch} AND parameterType.name = 'definition' ))`;
        return sqlSearch + parameterSearch;
      }
      return "";
    },

    filterOnParameters(investigationIds, sampleId, datasetIds, parametersString) {
      const datasetQuery = ` (dataset.id IN (SELECT parameter.dataset.id FROM DatasetParameter parameter JOIN parameter.type parameterType  WHERE `;
      return filterOnParameters(investigationIds, sampleId, datasetIds, parametersString, datasetQuery);
    },
  },
};

exports.TimeLineDatasetSQL = {
  query: `select parameter from DatasetParameter parameter JOIN parameter.dataset dataset JOIN dataset.type datasetType JOIN dataset.investigation investigation, dataset.sample sample JOIN parameter.type parameterType`,
  adminQuery: `select dataset.startDate, dataset.endDate, investigation.id, investigation.name, dataset.name, dataset.id, sample.name, sample.id, parameterType.name, parameter.stringValue, datasetType.name from DatasetParameter parameter JOIN parameter.dataset dataset JOIN dataset.type datasetType JOIN dataset.investigation investigation, dataset.sample sample JOIN parameter.type parameterType`,
  where: {
    equalSampleId(sampleId) {
      return sampleId ? `sample.id=${sampleId}` : "";
    },

    equalInvestigationId(investigationId) {
      if (investigationId) {
        const ids = investigationId.toString().split(",");
        if (ids.length === 1) {
          return ` investigation.id = '${investigationId}' `;
        }
        return ` investigation.id IN (${investigationId}) `;
      }
      return "";
    },
  },
  include: "INCLUDE parameter.dataset dataset, dataset.investigation investigation, dataset.sample sample, parameter.type parameterType, dataset.type datasetType",
};
