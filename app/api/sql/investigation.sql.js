const EMBARGOED_FILTER = "investigation.releaseDate > CURRENT_TIMESTAMP ";
const RELEASED_FILTER = "( investigation.releaseDate < CURRENT_TIMESTAMP and investigation.doi <> null )";
const PARTICIPANT_FILTER = "investigationUser.name = ':username' ";
const USERNAME_FILTER = "user.name = ':username'";
const { MARGIN_HOURS } = require("../../constants");
const investigationHelper = require("../../helpers/investigation");

const { getFormattedTimeOrDefault } = require("../../helpers/date.js");
const { equalInstrumentName, filterOnInvestigationParameters } = require("./helper.sql");
const { dateFormat } = require("../../controllers/helpers/helper.controller.js");
const { subHours, addHours } = require("date-fns");

exports.InvestigationSQL = {
  query: `select distinct investigation from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument`,
  countQuery: `select count (distinct investigation) from Investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument`,
  include: {
    get(extra) {
      const defaultInclude = `include investigation.type type, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type`;
      if (extra) {
        return ` ${defaultInclude}, ${extra}`;
      }
      return defaultInclude;
    },
  },
  join: {
    byFilter(filter, investigationUsername) {
      let join = "";
      if (investigationUsername && (filter === undefined || filter.toUpperCase() !== "PARTICIPANT")) {
        join = " JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser ";
      }
      if (filter) {
        switch (filter.toUpperCase()) {
          case "INSTRUMENTSCIENTIST": {
            join += " JOIN instrument.instrumentScientists as instrumentScientists JOIN instrumentScientists.user as user ";
            return join;
          }
          case "PARTICIPANT":
            join = " JOIN investigation.investigationUsers as investigationUserPivot JOIN investigationUserPivot.user as investigationUser ";
            return join;
        }
      }

      return join;
    },
  },
  sort: {
    by(sortBy, sortOrder) {
      const order = sortOrder === 1 ? "ASC" : "DESC";
      const clause = sortBy ? `${sortBy} ${order}` : "investigation.startDate DESC ";
      return `order by ${clause}`;
    },
  },
  where: {
    equalFilter(filter, username) {
      if (filter) {
        switch (filter.toUpperCase()) {
          case "EMBARGOED": {
            return EMBARGOED_FILTER;
          }
          case "RELEASED": {
            return RELEASED_FILTER;
          }
          case "PARTICIPANT": {
            return username ? PARTICIPANT_FILTER.replace(":username", username) : "";
          }
          case "INSTRUMENTSCIENTIST": {
            return username ? USERNAME_FILTER.replace(":username", username) : "";
          }
          case "INDUSTRY": {
            return PARTICIPANT_FILTER.replace(":username", username);
          }
        }
      }
      return "";
    },
    /**
     * It returns the JPQL where clause
     * @param {*} investigationId id or list of comma separated id list
     * @returns
     */
    equalInvestigationId(investigationId) {
      if (investigationId) {
        const ids = investigationId.toString().split(",");
        if (ids.length === 1) {
          return ` investigation.id = '${investigationId}' `;
        }
        return ` investigation.id IN (${investigationId}) `;
      }
      return "";
    },

    equalDOI(doi) {
      return doi ? ` investigation.doi = '${doi}' ` : "";
    },

    equalInstrumentName(instrumentName) {
      return equalInstrumentName(instrumentName);
    },

    /**
     * builds a sql clause to filter investigations by  name and visitId
     * Example of returned clause:
     * ""investigation.visitId = 'PROPOSAL' AND (investigation.name like 'IX%' OR investigation.name like 'FX%' OR investigation.name like 'IN%' OR investigation.name like 'IM%') "
     * @param {*} investigationName array of investigation name
     * @param {*} visitId filter on the visitId
     * @returns the clause
     */
    equalVisitIdAndLikeInvestigationName(investigationName, visitId) {
      const clauseVisitId = visitId && visitId.length > 0 ? `investigation.visitId = '${visitId}'` : "";

      let clauseName = investigationName ? `(investigation.name LIKE '${investigationHelper.normalize(investigationName)}')` : "";
      if (Array.isArray(investigationName)) {
        clauseName =
          investigationName && investigationName.length > 0
            ? `(${investigationName.map((n) => `investigation.name LIKE '${investigationHelper.normalize(n)}'`).join(" OR ")})`
            : "";
      }

      const allDefinedFilters = [clauseVisitId, clauseName].filter((f) => f !== "");
      const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
      return filters;
    },

    betweenDate(startDate, endDate) {
      const filterStartDate = startDate ? ` (investigation.startDate >= '${startDate}' or investigation.endDate >= '${startDate}') ` : "";
      const filterEndDate = endDate ? `investigation.startDate <= '${endDate}'` : "";
      const allDefinedFilters = [filterStartDate, filterEndDate].filter((f) => f !== "");
      const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
      return filters;
    },

    fitTime(time) {
      let filters = "";
      if (time) {
        const timeAsDate = new Date(time);
        const formattedTime = dateFormat(time, "yyyy-MM-dd HH:mm:ss");
        const timeLow = subHours(timeAsDate, MARGIN_HOURS);
        const timeHigh = addHours(timeAsDate, MARGIN_HOURS);
        const times = [getFormattedTimeOrDefault(timeLow, formattedTime), formattedTime, getFormattedTimeOrDefault(timeHigh, formattedTime)];
        filters = `(${times.map((t) => `(investigation.startDate <= '${t}' AND investigation.endDate >= '${t}') `).join(" OR ")}) `;
      }
      return filters;
    },

    investigationHasUser(username) {
      return username ? ` investigationUser.name like '${username}%' ` : "";
    },

    /**
     * builds a sql clause to search a word in different fields. The search is based on lower case.
     * The fields are investigation title, summary, doi, name and instrument's name.
     * Example of returned clause:
     * "(LOWER(investigation.title) LIKE '%search%' OR  LOWER(investigation.summary) LIKE '%search%' OR  LOWER(investigation.doi) LIKE '%search%' OR  LOWER(investigation.name) LIKE '%search%' OR  LOWER(intrument.name) LIKE '%search%' )"
     * If the search can fit with an investigation name, the normalized name is given in searchAsNormalizedInvestigation and a condition will be added on the investigation.name
     * @param {*} search the word to search
     * @param {*} searchAsNormalizedInvestigation the search which has been normalized
     * @returns the clause
     */
    search(search, searchAsNormalizedInvestigation) {
      const likeSearch = search ? `LIKE '%${search}%'` : "";
      const searchFields = ["investigation.title", "investigation.summary", "investigation.doi", "investigation.name", "instrument.name"];
      let clause = `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")}`;
      if (searchAsNormalizedInvestigation) {
        clause += ` OR LOWER(investigation.name) LIKE '%${searchAsNormalizedInvestigation}%'`;
      }
      clause += ")";
      return search ? clause : "";
    },

    filterOnParameters(investigationId, parametersString) {
      const investigationQuery = ` (investigation.id IN (SELECT parameter.investigation.id FROM InvestigationParameter parameter JOIN parameter.type parameterType  WHERE `;
      return filterOnInvestigationParameters(investigationId, parametersString, investigationQuery);
    },
  },
};

exports.TimeLineInvestigationSQL = {
  query: `select investigation.startDate, investigation.endDate, investigation.id, investigation.name, instrument.name,  parameterType.name, parameter.stringValue from InvestigationParameter parameter JOIN parameter.investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument JOIN parameter.type parameterType`,
  where: {
    equalInstrument(instrument) {
      return instrument ? `instrument.name = '${instrument}' ` : "";
    },
  },
};
