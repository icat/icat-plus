exports.getOperator = (operator) => {
  switch (operator.toLowerCase()) {
    case "eq":
      return "=";
    case "like":
      return "LIKE";
    case "gt":
      return ">";
    case "gteq":
      return ">=";
    case "lt":
      return "<";
    case "lteq":
      return "<=";
    case "in":
      return "in";
  }
  return undefined;
};

exports.getValue = (operator, param) => {
  const value = param.toLowerCase();
  switch (operator.toLowerCase()) {
    case "=":
      return `'${value}'`;
    case "like":
      return `'%${value.replace(/[_]/gi, "\\_")}%'`;
    case "in":
      return `(${value
        .split(";")
        .map((v) => `'${v}'`)
        .join(",")})`;
    default:
      return value;
  }
};

exports.buildFieldName = (operator, fieldName, numericFieldName, includeNumericValues) => {
  if (includeNumericValues && ["<", "<=", ">", ">="].includes(operator.toLowerCase())) {
    return numericFieldName;
  }
  return `LOWER(${fieldName})`;
};

exports.getEscape = (operator) => {
  return operator.toLowerCase() === "like" ? "ESCAPE '\\'" : "";
};

exports.getSQLClause = (operator, fieldName, numericFieldName, param, includeNumericValues) => {
  if (!operator || !param) {
    return undefined;
  }
  const value = this.getValue(operator, param);
  const field = this.buildFieldName(operator, fieldName, numericFieldName, includeNumericValues);
  return `${field} ${operator} ${value} ${this.getEscape(operator)}`;
};

/**
 *
 * @param {*} instrumentName this is a comma separated list of instruments
 * @returns
 */
exports.equalInstrumentName = (instrumentName) => {
  /**
   *
   * @param {*} instrumentName Example: "ID23-1,ID30A-1,ID29"
   * @returns (instrument.name ='ID23-1' OR instrument.name ='ID30A-1' OR instrument.name ='ID29' )
   */
  function doOrByInstruments(instrumentName) {
    const instruments = instrumentName.toUpperCase().split(",");
    return `(${instruments.map((f) => `instrument.name ='${f}' `).join("OR ")}) `;
  }
  return instrumentName ? doOrByInstruments(instrumentName) : "";
};

/**
 * builds the query to filter on dataset parameters
 * @param {*} investigationIds
 * @param {*} sampleId
 * @param {*} datasetIds
 * @param {*} parametersString
 * @param {*} datasetQuery
 * @returns
 */
exports.filterOnParameters = (investigationIds, sampleIds, datasetIds, parametersString, datasetQuery) => {
  if (parametersString) {
    const datasetFilters = [];
    if (investigationIds) {
      datasetFilters.push(` parameter.dataset.investigation.id IN (${investigationIds})`);
    }
    if (sampleIds) {
      datasetFilters.push(` parameter.dataset.sample.id = ${sampleIds} `);
    }
    if (datasetIds) {
      datasetFilters.push(` parameter.dataset.id IN (${datasetIds})`);
    }
    return this.buildFilterQuery(datasetQuery, datasetFilters, parametersString, true);
  }
  return "";
};

exports.buildParameterFilterQuery = (parameterName, parameterFilterDescription, includeNumericValues) => {
  let paramFilter = `(LOWER(parameterType.name) = '${parameterName.toLowerCase()}' `;
  if (parameterFilterDescription.length > 2) {
    const operator = this.getOperator(parameterFilterDescription[1]);
    const sqlField = this.getSQLClause(operator, "parameter.stringValue", "parameter.numericValue", parameterFilterDescription[2], includeNumericValues);
    if (sqlField) {
      paramFilter += ` AND ${sqlField}`;
    }
  }
  paramFilter += " )";
  return paramFilter;
};

exports.buildFilterQuery = (query, elementFilters, parametersString, includeNumericValues) => {
  query += elementFilters.join(" AND ");
  const parameters = parametersString.split(",");
  let filter = ` (`;
  const filters = [];
  parameters.forEach((parameter) => {
    let filterForParameter = ` ${query} ${elementFilters.length === 0 ? "" : "AND"} (`;
    const values = parameter.split("~");
    if (values.length > 0) {
      const parameterNames = values[0];
      const names = parameterNames.split(";");
      const parametersFilters = names.map((name) => this.buildParameterFilterQuery(name, values, includeNumericValues)).join(" OR ");
      filterForParameter += parametersFilters;
      filterForParameter += ")))";
      filters.push(filterForParameter);
    }
  });
  filter += filters.join(" AND ");
  filter += ")";
  return `${filter}`;
};

/**
 * builds the query to filter on investigation parameters
 * @param {*} investigationId
 * @param {*} parametersString
 * @param {*} datasetQuery
 * @returns
 */
exports.filterOnInvestigationParameters = (investigationId, parametersString, investigationQuery) => {
  if (parametersString) {
    const investigationFilters = [];
    if (investigationId) {
      investigationFilters.push(` parameter.investigation.id IN (${investigationId})`);
    }
    return this.buildFilterQuery(investigationQuery, investigationFilters, parametersString, false);
  }
  return "";
};

exports.filterOnParametersForInstrument = (instrumentName, parametersString, datasetQuery) => {
  if (parametersString) {
    const datasetFilters = [];
    if (instrumentName) {
      datasetFilters.push(` instrument.name ='${instrumentName}'`);
    }
    return this.buildFilterQuery(datasetQuery, datasetFilters, parametersString, true);
  }
  return "";
};
