const { filterOnParameters, equalInstrumentName } = require("./helper.sql");

const fromClause = `from Sample sample JOIN sample.investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument`;

exports.SampleSQL = {
  buildQuery(params) {
    let query = "";
    const { includeDatasets, parameters, search, havingAcquisitionDatasets } = params;
    if (includeDatasets) {
      query += " LEFT JOIN sample.datasets datasets JOIN datasets.type datasetType JOIN datasets.parameters datasetParameters JOIN datasetParameters.type datasetParameterType";
    }
    if (havingAcquisitionDatasets || parameters !== undefined || (search !== undefined && search.trim().length > 0)) {
      query += " LEFT JOIN sample.parameters parameters LEFT JOIN parameters.type parameterType ";
    }
    return query;
  },
  getQuery(params) {
    let query = `select distinct(sample) ${fromClause} `;
    query += this.buildQuery(params);
    return query;
  },
  getCountQuery(params) {
    let query = `select count(distinct(sample)) ${fromClause} `;
    query += this.buildQuery(params);
    return query;
  },
  include: {
    get(extra) {
      const defaultInclude = ` INCLUDE sample.parameters parameters, parameters.type, sample.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument `;
      if (extra) {
        return ` ${defaultInclude}, ${extra}`;
      }
      return defaultInclude;
    },
  },
  sort: {
    by(sortBy, sortOrder) {
      const order = sortOrder === 1 ? "ASC" : "DESC";
      let clause = "sample.modTime DESC ";
      if (sortBy) {
        const sorts = sortBy.split(",");
        if (sorts.length === 1) {
          clause = `${sortBy} ${order} `;
        } else {
          clause = sorts.map((sort) => `${sort} ${order}  NULLS LAST`).join(",");
        }
      }
      return `order by ${clause} `;
    },
  },
  where: {
    equalSampleIds(sampleIds) {
      if (sampleIds) {
        const ids = sampleIds.toString().split(",");
        if (ids.length === 1) {
          return ` sample.id = '${sampleIds}' `;
        }
        return ` sample.id IN (${sampleIds}) `;
      }
      return "";
    },

    equalInstrumentName(instrumentName) {
      return equalInstrumentName(instrumentName);
    },

    /**
     * It returns the JPQL where clause
     * @param {*} investigationId id or list of comma separated id list
     * @returns
     */
    equalInvestigationId(investigationId) {
      if (investigationId) {
        const ids = investigationId.toString().split(",");
        if (ids.length === 1) {
          return ` investigation.id = '${investigationId}' `;
        }
        return ` investigation.id IN (${investigationId}) `;
      }
      return "";
    },

    betweenDate(startDate, endDate) {
      const filterStartDate = startDate ? ` sample.modTime >= '${startDate}'  ` : "";
      const filterEndDate = endDate ? `sample.modTime <= '${endDate}'` : "";
      const allDefinedFilters = [filterStartDate, filterEndDate].filter((f) => f !== "");
      const filters = allDefinedFilters.length > 0 ? `${allDefinedFilters.join("AND ")}` : "";
      return filters;
    },

    /**
     * builds a sql clause to search a word in different fields. The search is based on lower case.
     * The fields are dataset name, sample name, definition, volume, fileCount
     * Example of returned clause:
     * "(LOWER(dataset.name) LIKE '%search%' OR  LOWER(sample.name) LIKE '%search%'
     *  OR (LOWER(parameter.stringValue) LIKE '%search%' AND parameterType.name is 'definition'))"
     * @param {*} search the word to search
     * @returns the clause
     */

    search(search) {
      const likeSearch = search ? `LIKE '%${search}%'` : "";
      const searchFields = ["sample.name"];
      if (search) {
        const sqlSearch = `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")}`;
        const parameterSearch = ` OR (LOWER(parameters.stringValue) ${likeSearch} AND parameterType.name = 'sample_name' ))`;
        return sqlSearch + parameterSearch;
      }
      return "";
    },

    filterOnParameters(investigationId, sampleIds, parametersString) {
      const datasetQuery = ` (sample.id IN (SELECT parameter.dataset.sample.id FROM DatasetParameter parameter JOIN parameter.type parameterType  WHERE `;
      return filterOnParameters(investigationId, sampleIds, undefined, parametersString, datasetQuery);
    },

    filterOnHasAcquisitionDatasets(hasDatasets) {
      if (hasDatasets) {
        const nbDatasetQuery = " (parameterType.name = '__acquisitionDatasetCount' AND parameters.stringValue != '0')";
        return nbDatasetQuery;
      }
      return "";
    },

    filterOnDatasetsCount() {
      const nbDatasetQuery = ` ((parameterType.name = '__datasetCount' AND parameters.stringValue != '0')  OR (parameters.id IS NULL) )`;
      return nbDatasetQuery;
    },

    filterBySampleReference(sortBy, sortOrder, sampleId) {
      const order = sortOrder === 1 ? "<=" : ">=";
      return sampleId && sortBy ? `(${sortBy} ${order} (SELECT ${sortBy} FROM Sample sample WHERE sample.id = ${sampleId} ) AND sample.id != ${sampleId} )` : "";
    },
  },
};
