const fromClause = `from DataCollection dataCollection  JOIN dataCollection.dataCollectionDatasets dcds  JOIN dcds.dataset ds JOIN ds.investigation investigation LEFT JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument LEFT JOIN dataCollection.parameters parameters LEFT JOIN parameters.type parameterType `;

exports.DataCollectionSQL = {
  query: `select distinct(dataCollection) ${fromClause} `,
  countQuery: `select count(distinct(dataCollection)) ${fromClause} `,

  join: {
    joinInvestigationUsers(usernameParticipant) {
      return usernameParticipant ? ` JOIN  investigation.investigationUsers investigationUsers JOIN investigationUsers.user  user  ` : "";
    },
  },
  include: {
    get(extra) {
      const defaultInclude = ` INCLUDE dataCollection.parameters p, p.type, dataCollection.dataCollectionDatasets dcds, dcds.dataset `;
      if (extra) {
        return ` ${defaultInclude}, ${extra}`;
      }
      return defaultInclude;
    },
  },
  sort: {
    by(sortBy, sortOrder) {
      const order = sortOrder === 1 ? "ASC" : "DESC";
      const clause = sortBy ? `${sortBy} ${order}` : "dataCollection.modTime DESC ";
      return `order by ${clause}`;
    },
  },
  where: {
    doiIsNotNull() {
      return " dataCollection.doi IS NOT null ";
    },

    doiIsNull() {
      return " dataCollection.doi IS null ";
    },

    equalDatasetId(datasetId) {
      return datasetId ? `ds.id = ${datasetId}` : "";
    },

    equalDataCollectionId(dataCollectionId) {
      return dataCollectionId ? ` dataCollection.id = ${dataCollectionId} ` : "";
    },

    participantIs(usernameParticipant) {
      return usernameParticipant ? ` user.name = '${usernameParticipant}'  ` : "";
    },

    /**
     * builds a sql clause to search a word in different fields. The search is based on lower case.
     * The fields are title, abstract, investigationNames, instrumentNames.
     * Example of returned clause:
     * "((LOWER(parameters.stringValue) LIKE '%search%' AND parameterType.name in ('title', 'abstract', 'investigationNames', 'instrumentNames')) OR (LOWER(dataCollection.doi) LIKE '%ch5833%')  OR (LOWER(parameters.stringValue) LIKE '%search%' AND parameterType.name = 'investigationNames'))"
     * If the search can fit with an investigation name, the normalized name is given in searchAsNormalizedInvestigation and a condition will be added on the investigationNames
     * @param {*} search the word to search
     * @param {*} searchAsNormalizedInvestigation the search which has been normalized
     * @returns the clause
     */
    search(search, searchAsNormalizedInvestigation) {
      let clause;
      if (search) {
        clause = `((LOWER(parameters.stringValue) LIKE '%${search}%' AND parameterType.name in ('title', 'abstract', 'investigationNames', 'instrumentNames')) `;
        clause += ` OR (LOWER(dataCollection.doi) LIKE '%${search}%') `;
        if (searchAsNormalizedInvestigation) {
          clause += ` OR (LOWER(parameters.stringValue) LIKE '%${searchAsNormalizedInvestigation}%' AND parameterType.name = 'investigationNames')`;
        }
        clause += ") ";
      }
      return search ? clause : "";
    },

    typePublisher(type) {
      if (type) {
        const operator = type.toLowerCase() === "document" ? "=" : "!=";
        return ` LOWER(investigation.visitId) ${operator} 'publisher' `;
      }
      return "";
    },
  },
};
