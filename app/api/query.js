"use strict";

const { filterOnParameters, filterOnParametersForInstrument } = require("./sql/helper.sql");

const ICAT = global.gServerConfig.icat;
const IDS = global.gServerConfig.ids;

const GET_SESSION = "sessionId=:sessionId";

const ICAT_ENTITY_MANAGER = `${ICAT.server}/icat/entityManager?`;

const SERVER = `&server=${ICAT.server}`;

const GET_INVESTIGATIONS_ID = "SELECT distinct(Inv.id) from Investigation Inv";

const GET_DATASETS_IDS_BY_INVESTIGATION_ID = "SELECT dataset.id FROM Dataset dataset JOIN dataset.investigation investigation where investigation.id =:investigationId";

const GET_DATASETS_DATASET_IDS =
  "SELECT dataset FROM Dataset dataset JOIN dataset.investigation investigation where dataset.id IN (:datasetIDs) INCLUDE dataset.type type, dataset.investigation investigation, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument, dataset.parameters parameters, parameters.type, dataset.sample";

const GET_INVESTIGATION_BY_DATASETS_ID =
  "SELECT Inv from Investigation Inv JOIN Inv.datasets ds where ds.id in (:datasetIds) include Inv.investigationInstruments ii, ii.instrument";

const GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID =
  "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id, user.email, investigationUser.id, user.orcidId, user.familyName, user.givenName, user.affiliation FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where inv.id in (:investigationIds) ";

const GET_INVESTIGATIONUSER_BY_USERNAME =
  "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id, user.email, investigationUser.id, user.orcidId, user.familyName, user.givenName, user.affiliation FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where user.name = ':username'";

const GET_INSTRUMENTS_FOR_USER =
  "SELECT DISTINCT instrument FROM Instrument instrument JOIN instrument.investigationInstruments investigationInstruments JOIN investigationInstruments.investigation  investigation JOIN investigation.investigationUsers  investigationUsers JOIN investigationUsers.user  user WHERE user.name = ':username' ORDER BY instrument.name ";

const GET_INSTRUMENTS_FOR_INSTRUMENT_SCIENTIST =
  "SELECT DISTINCT instrument FROM Instrument instrument JOIN instrument.instrumentScientists instrumentScientists JOIN instrumentScientists.user user WHERE user.name = ':username' ORDER BY instrument.name ";

const IDS_GET_DATA = "/ids/getData?sessionId=:sessionId&";

exports.getDatasetParameterByInstrumentName = (sessionId, instrumentName, startDate, endDate, name, includeDatasets, parameters) => {
  let whereClause = `instrument.name  = '${instrumentName}' and dataset.startDate > '${startDate}' and dataset.endDate < '${endDate}' and parameterType.name= '${name}' `;
  if (parameters) {
    const datasetQuery = ` (dataset.id IN (SELECT parameter.dataset.id FROM DatasetParameter parameter JOIN parameter.type parameterType  JOIN parameter.dataset dataset JOIN dataset.investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument WHERE `;
    const filterClause = filterOnParametersForInstrument(instrumentName, parameters, datasetQuery);
    whereClause = `${whereClause} and  ${filterClause} `;
  }

  const includeClause = includeDatasets ? ", parameter.dataset" : "";
  const query =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT parameter from DatasetParameter parameter JOIN parameter.type parameterType JOIN parameter.dataset dataset JOIN dataset.investigation investigation JOIN investigation.investigationInstruments investigationInstruments JOIN investigationInstruments.instrument instrument where ${whereClause} INCLUDE parameter.type ${includeClause}`;
  return query;
};
/**
 * Gets the dataset parameter by investigation
 * @param {*} sessionId
 * @param {*} investigationId It is mandatory
 * @param {*} filters might contain name, sampleId, datasetType,  includeDatasets
 * @returns
 */
exports.getDatasetParameterByInvestigationId = (sessionId, investigationId, name, filters) => {
  let whereClause = `investigation.id  IN (${investigationId}) `;
  const { sampleId, datasetType, parameters } = filters;
  if (name) {
    whereClause = `${whereClause} and parameterType.name= '${name}'`;
  }
  if (sampleId) {
    whereClause = `${whereClause} and sample.id= '${sampleId}'`;
  }
  if (datasetType) {
    whereClause = `${whereClause} and datasetType.name= '${datasetType}'`;
  }

  if (parameters) {
    const datasetQuery = ` (dataset.id IN (SELECT parameter.dataset.id FROM DatasetParameter parameter JOIN parameter.type parameterType  WHERE `;
    const filterClause = filterOnParameters(investigationId, sampleId, undefined, parameters, datasetQuery);
    whereClause = `${whereClause} and  ${filterClause} `;
  }

  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT parameter from DatasetParameter parameter JOIN parameter.dataset dataset JOIN parameter.type parameterType JOIN dataset.type datasetType JOIN dataset.investigation investigation JOIN dataset.sample sample where  ${whereClause} INCLUDE parameter.type, parameter.dataset dataset, dataset.sample sample`
  );

  /** For the records
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT dataset.id, dataset.name, dataset.startDate, dataset.endDate, investigation.name, parameterType.name, parameter.stringValue, parameterType.units, parameter.id, sample.id, sample.name FROM DatasetParameter as parameter  JOIN parameter.dataset dataset JOIN dataset.sample sample JOIN parameter.type parameterType  JOIN dataset.investigation investigation where investigation.id  = '${investigationId}' ${whereClauseByName}`
  );
  */
};

exports.getPropertiesQuery = () => {
  return `${ICAT.server}/icat/properties`;
};

exports.getSessionQuery = (sessionId) => {
  return GET_SESSION.replace(":sessionId", sessionId);
};

exports.getSession = () => {
  return `${ICAT.server}/icat/session`;
};

exports.getSessionInformationURL = (sessionId) => {
  return `${this.getSession()}/${sessionId}`;
};

exports.logout = (sessionId) => {
  return `${this.getSession()}/${sessionId}`;
};

exports.getInvestigationUserByInvestigationIdURL = (sessionId, investigationIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID.replace(":investigationIds", investigationIds)}${SERVER}`;
};

exports.getInvestigationUserByUserNameURL = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONUSER_BY_USERNAME.replace(":username", username)}${SERVER}`;
};

exports.getInvestigations = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATIONS_ID}${SERVER}`;
};

exports.getInvestigationsByDatasetListIdsURL = (sessionId, datasetIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INVESTIGATION_BY_DATASETS_ID.replace(":datasetIds", datasetIds)}${SERVER}`;
};

exports.getDeleteQuery = (sessionId, entities) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&entities=${entities}`;
};

exports.getCreateQuery = (sessionId) => {
  return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId);
};

exports.getCreateDataCollectionQuery = (sessionId) => {
  return this.getCreateQuery(sessionId);
};

exports.getParameterTypeList = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=Select p from ParameterType p${SERVER}`;
};

exports.getInvestigationTypes = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=Select p from InvestigationType p${SERVER}`;
};

exports.getInvestigationById = (sessionId, investigationId) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT Inv from Investigation Inv where inv.id in (${investigationId}) INCLUDE Inv.investigationInstruments i, i.instrument, Inv.parameters p, p.type${SERVER}`
  );
};

/**
 * builds the WHERE clause form an array of filters, separated by a AND clause
 * Example of returned clause:
 * for filters = ["instrument.name = 'ID00'", "(LOWER(investigation.name) LIKE '%test%' OR LOWER(investigation.title) LIKE '%test%')", "investigation.startDate <= '2020-01-01'", ""]
 * "WHERE instrument.name = 'ID00' AND (LOWER(investigation.name) LIKE '%test%' OR LOWER(investigation.title) LIKE '%test%') AND investigation.startDate <= '2020-01-01'"
 * @param {*} filters  array of filters
 * @returns the clause
 */
exports.buildFiltersConditions = (filters) => {
  const allDefinedFilters = filters.filter((f) => f !== "");
  return allDefinedFilters.length > 0 ? `WHERE ${allDefinedFilters.join("AND ")}` : "";
};

exports.getDatasetIdsByInvestigationId = (sessionId, investigationId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_DATASETS_IDS_BY_INVESTIGATION_ID.replace(":investigationId", investigationId)}${SERVER}`;
};

exports.getInvestigationIdByDOI = (sessionId, doi) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT inv.id from Investigation inv where inv.doi='${doi}'${SERVER}`;
};

/**
 * Get the url to retrieve datasets from a dataset list
 * @param {string} datasetIds a comma separated string containing different datasetId
 * @param {string} sessionId
 */
exports.getDatasetsByIdList = (sessionId, datasetIds) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_DATASETS_DATASET_IDS.replace(":datasetIDs", datasetIds)}${SERVER}`;
};

exports.getUsersByGroupName = (sessionId, groupNames) => {
  const query = "SELECT user from User user JOIN user.userGroups userGroup JOIN userGroup.grouping grouping where  grouping.name in (':groupNames')";
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query.replace(":groupNames", groupNames)}${SERVER}`;
};

exports.getUsers = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT user from User user ORDER BY user.familyName ASC ${SERVER} `;
};

exports.getInstruments = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT instrument from Instrument instrument ORDER BY instrument.name ${SERVER}`;
};

exports.getInstrumentScientistsByUserName = (sessionId, userName) => {
  return (
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT instrumentScientist from InstrumentScientist instrumentScientist where instrumentScientist.user.name='${userName}' INCLUDE instrumentScientist.instrument instrument, instrumentScientist.user user${SERVER}`
  );
};

exports.getInstrumentScientists = (sessionId, instrumentName) => {
  const instrumentClause = instrumentName ? `WHERE LOWER(instrumentScientist.instrument.name) = '${instrumentName.toLowerCase()}' ` : "";
  const query = `SELECT instrumentScientist from InstrumentScientist instrumentScientist ${instrumentClause} INCLUDE instrumentScientist.instrument instrument, instrumentScientist.user user`;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

exports.getDatafilesByDatasetId = (sessionId, datasetIds, limit, skip, search) => {
  const searchCond = this.buildSearchDatafile(search);
  const filters = this.buildFiltersConditions([`dataset.id IN (${datasetIds})`, searchCond]);
  // include datafile.dataset might cause performance issues. it is currently needed for the UI
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select distinct datafile from Datafile datafile JOIN datafile.dataset dataset  ${filters} include datafile.dataset dataset ${this.buildlimit(limit, skip)} ${SERVER}`;
  return encodeURI(url);
};

exports.getDatafilesByIds = (sessionId, datafileIds) => {
  const query =
    "select df from Datafile df where df.id IN (:datafileIds) INCLUDE df.dataset dataset, dataset.investigation investigation, dataset.sample sample, investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument ";
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query.replace(":datafileIds", datafileIds)}${SERVER}`;
};

exports.getStatusByDatasetIds = (sessionId, datasetIds) => {
  return `${IDS.server}/ids/getStatus?sessionId=${sessionId}&datasetIds=${datasetIds}`;
};

/**
 * Returns the path where a datafile/dataset might be located
 * @param {*} sessionId
 * @param {*} datafileIds
 * @param {*} isOnline if true if will return only the path if it exists
 * @returns
 */
exports.getPathBy = (sessionId, datafileIds, datasetIds, isOnline) => {
  let url = `${IDS.server}/ids/path?sessionId=${sessionId}&isonline=${isOnline}`;
  url = datafileIds ? `${url}&datafileIds=${datafileIds}` : url;
  url = datasetIds ? `${url}&datasetIds=${datasetIds}` : url;
  return url;
};

/**
 * Get JPQL query which is used to get instrument scientists for a given instrumentName
 * @param {string} sessionId session identifier
 * @param {string } instrumentName instrument name
 * @returns { string} JPQL query
 */
exports.getInstrumentScientistsByInstrumentName = (sessionId, instrumentName) => {
  const query = `SELECT u FROM InstrumentScientist insts JOIN insts.user u WHERE insts.instrument.name='${instrumentName}`;
  return `${ICAT_ENTITY_MANAGER}${this.getSessionQuery(sessionId)}&query=${query}${SERVER}`;
};

/**
 * It returns the query that retrieves the instruments associated to an investigation user
 * @param {*} sessionId
 * @param {*} username
 * @returns
 */
exports.getInstrumentsForUser = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INSTRUMENTS_FOR_USER.replace(":username", username)}${SERVER}`;
};

exports.getInstrumentsForInstrumentScientist = (sessionId, username) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${GET_INSTRUMENTS_FOR_INSTRUMENT_SCIENTIST.replace(":username", username)}${SERVER}`;
};

exports.buildlimit = (limit, skip) => {
  skip = skip === undefined || skip === null ? 0 : skip;
  limit = limit === undefined || limit === null ? global.gServerConfig.icat.maxQueryLimit : limit;
  return limit !== undefined && skip !== undefined ? ` LIMIT ${skip}, ${limit} ` : "";
};

/**
 * builds a sql clause to search a word in different fields. The search is based on lower case.
 * The fields are dataset name, sample name, definition, volume, fileCount
 * Example of returned clause:
 * "(LOWER(dataset.name) LIKE '%search%' OR  LOWER(sample.name) LIKE '%search%'
 *  OR (LOWER(parameter.stringValue) LIKE '%search%' AND parameterType.name is 'definition'))"
 * @param {*} search the word to search
 * @returns the clause
 */
exports.buildSearchDataset = (search) => {
  const likeSearch = search ? `LIKE '%${search}%'` : "";
  const searchFields = ["dataset.name", "sample.name"];
  if (search) {
    const sqlSearch = `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")}`;
    const parameterSearch = ` OR (LOWER(parameters.stringValue) ${likeSearch} AND parameterType.name = 'definition' ))`;
    return sqlSearch + parameterSearch;
  }
  return "";
};

exports.buildSortDatasets = (sortBy, sortOrder) => {
  const order = sortOrder === 1 ? "ASC" : "DESC";
  return sortBy ? `${sortBy} ${order}` : "dataset.startDate DESC ";
};

/**
 * Returns a dataset parameter for a specified parameterId
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getDatasetParameterById = (sessionId, parameterId) => {
  const url = `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT parameter from DatasetParameter parameter  WHERE parameter.id = ${parameterId} ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.downloadData = (sessionId, datasetIds, datafileIds, outname) => {
  let params = datasetIds ? "datasetIds=:datasetIds".replace(":datasetIds", datasetIds) : "datafileIds=:datafileIds".replace(":datafileIds", datafileIds);
  if (outname !== null && outname !== undefined) {
    params = `${params}&outname=${outname}`;
  }
  return IDS.server + IDS_GET_DATA.replace(":sessionId", sessionId) + params;
};

/**
 * Returns other user's starting with the prefix expect the given usernameWithPrefix (username/smisPk)
 * @param {*} sessionId
 * @param {*} prefix
 * @param {*} userNameWithSmisPk
 * @returns a list of users corresponding to the prefix, could be empty
 */
exports.getUserByUsernamePrefix = (sessionId, prefix, userNameWithPrefix) => {
  const query = `SELECT user FROM User user WHERE user.name like '${prefix}/%' AND user.name <> '${userNameWithPrefix}' ORDER BY user.createTime DESC`;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

/**
 * Returns the users corresponding to a given name
 * @param {*} sessionId
 * @param {*} name
 * @returns a list of users, could be empty
 */
exports.getUserByName = (sessionId, name) => {
  const query = `SELECT user FROM User user WHERE user.name = '${name}' `;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

/**
 * builds a sql clause to search a word in different fields of a datafile. The search is based on lower case.
 * The fields are name, location, fileSize.
 * Example of returned clause:
 * "(LOWER(datafile.name) LIKE '%search%' OR  LOWER(datafile.location) LIKE '%search%'  )"
 * @param {*} search the word to search
 * @returns the clause
 */
exports.buildSearchDatafile = (search) => {
  const likeSearch = search ? `LIKE '%${search}%'` : "";
  const searchFields = ["datafile.name", "datafile.location"];
  return search ? `(${searchFields.map((f) => `LOWER(${f}) ${likeSearch} `).join(" OR ")})` : "";
};

/**
 * count the number of datafiles for a given datasetId. A search condition on datafile location/name could be added
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} search
 * @returns the number of datafiles
 */
exports.countEntityDatafilesByDatasetId = (sessionId, datasetsId, search) => {
  const searchCond = this.buildSearchDatafile(search);
  const filters = this.buildFiltersConditions([`dataset.id IN (${datasetsId}) `, searchCond]);
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=select count (distinct datafile) from Datafile datafile JOIN datafile.dataset dataset ${filters} ${SERVER} `;
  return encodeURI(url);
};

exports.getDataCollectionsByInvestigationId = (sessionId, investigationId) => {
  const query = `SELECT distinct(dc) from DataCollection dc JOIN dc.dataCollectionDatasets dcds JOIN dcds.dataset ds JOIN ds.investigation inv where dc.doi is not null and inv.id=${investigationId} order by dc.doi`;
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query}${SERVER}`;
};

/**
 * Returns a sample parameter for a specified parameterId
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getSampleParameterById = (sessionId, parameterId) => {
  const url =
    `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` +
    `query=SELECT parameter from SampleParameter parameter  WHERE parameter.id = ${parameterId} INCLUDE parameter.sample sample, parameter.type type ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.getParameterTypeByName = (sessionId, name) => {
  const url = `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT p from ParameterType p WHERE p.name = '${name}' ${SERVER}`;
  global.gLogger.debug(url);
  return encodeURI(url);
};

exports.findDatafileParameters = (sessionId, datasetId, filepath) => {
  const query = `SELECT p from DatafileParameter p JOIN p.datafile datafile WHERE datafile.dataset.id = ${datasetId} AND datafile.location = '${filepath}' INCLUDE p.type type`;
  const url = `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=${query} ${SERVER}`;
  return encodeURI(url);
};

exports.getTechniques = (sessionId) => {
  return `${ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)}&` + `query=SELECT technique from Technique technique ORDER BY technique.pid ${SERVER}`;
};

exports.getDatasetTechniques = (sessionId, datasetIds) => {
  const url = `${
    ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId)
  }&query=SELECT datasetTechnique from DatasetTechnique datasetTechnique WHERE datasetTechnique.dataset.id in (${datasetIds}) INCLUDE datasetTechnique.technique ${SERVER}`;
  return encodeURI(url);
};
