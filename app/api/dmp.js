const axios = require("axios-https-proxy-fix");

/**
 * Create a DSW token from the credentials
 * @param {*} url
 * @param {*} credentials
 * @returns
 */
exports.createToken = async (url, credentials) => {
  const response = await axios.post(`${url}/tokens`, credentials, {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  });
  return response.data;
};

/**
 * Search DMP questionnaires by the investigation name, returns the result from DSW
 * @param {*} url
 * @param {*} token
 * @param {*} investigationName
 * @returns
 */
exports.getQuestionnaires = async (url, token, investigationName) => {
  const response = await axios.get(`${url}/questionnaires?size=1&q=${investigationName}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return response.data;
};
