exports.parse = (users) => {
  const parsed = users.map((element) => {
    // Commented out because it is too slow
    const modUser = null; //cache.getUserByName(element.InstrumentScientist.modId); //_.find(users, function(u){ return u.id === parseInt(element.InstrumentScientist.modId);});

    const { InstrumentScientist } = element;
    return {
      name: InstrumentScientist.instrument.name,
      id: InstrumentScientist.id,
      modId: InstrumentScientist.modId,
      mod: {
        time: InstrumentScientist.modTime,
        modId: InstrumentScientist.modId,
        user: modUser
          ? {
              fullName: modUser ? modUser.fullName : "",
              name: InstrumentScientist.modId,
            }
          : null,
      },
      user: {
        fullName: InstrumentScientist.user.fullName,
        name: InstrumentScientist.user.name,
      },
      instrument: {
        name: InstrumentScientist.instrument.name,
        description: InstrumentScientist.instrument.description,
      },
    };
  });
  return parsed;
};
