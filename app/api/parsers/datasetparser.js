const datasetParameter = require("./datasetParameter.js");
const datasetTechnique = require("./datasetTechnique.js");

function getParameters(dataset) {
  const params = [];
  for (let i = 0; i < dataset.Dataset.parameters.length; i++) {
    const parameter = dataset.Dataset.parameters[i];
    params.push(datasetParameter.parse(parameter.type.name, parameter.stringValue, parameter.id, parameter.type.units, dataset.Dataset.id));
  }
  return params;
}

function getTechniques(dataset) {
  const techniques = [];
  for (let i = 0; i < dataset.Dataset.datasetTechniques.length; i++) {
    const technique = dataset.Dataset.datasetTechniques[i];
    techniques.push(datasetTechnique.parse(dataset.Dataset.id, technique.technique.id, technique.technique.pid, technique.technique.name, technique.technique.description));
  }
  return techniques;
}

function getInstrumentName(dataset) {
  if (dataset.investigation && dataset.investigation.investigationInstruments && dataset.investigation.investigationInstruments[0]) {
    if (dataset.investigation.investigationInstruments[0].instrument) return dataset.investigation.investigationInstruments[0].instrument.name;
  }
  return "";
}

exports.parse = (datasets) => {
  const parsed = [];

  for (let i = 0; i < datasets.length; i++) {
    const element = datasets[i];

    parsed.push({
      id: element.Dataset.id,
      name: element.Dataset.name,
      startDate: element.Dataset.startDate,
      endDate: element.Dataset.endDate,
      location: element.Dataset.location,
      investigation: element.Dataset.investigation,
      type: element.Dataset.type ? element.Dataset.type.name : null,
      sampleName: element.Dataset.sample ? element.Dataset.sample.name : null,
      sampleId: element.Dataset.sample ? element.Dataset.sample.id : null,
      parameters: element.Dataset.parameters ? getParameters(element) : [],
      instrumentName: getInstrumentName(element.Dataset),
      techniques: element.Dataset.datasetTechniques ? getTechniques(element) : [],
    });
  }
  return parsed;
};

exports.buidlTimeLineData = (listRecords) => {
  /** This groups the records by dataset id */
  const groupedByDatasetIds = listRecords.reduce((accumulator, currentObject) => {
    const datasetId = currentObject.datasetId;
    if (!accumulator[datasetId]) {
      accumulator[datasetId] = [];
    }
    accumulator[datasetId].push(currentObject);
    return accumulator;
  }, {});

  const datasets = [];
  for (const datasetId in groupedByDatasetIds) {
    const dataset = groupedByDatasetIds[datasetId][0];
    dataset.parameters = [];
    const parameters = groupedByDatasetIds[datasetId].reduce((accumulator, currentObject) => {
      const datasetId = currentObject.parameterName;
      if (!accumulator[datasetId]) {
        accumulator[datasetId] = [];
      }
      accumulator[datasetId].push(currentObject);
      return accumulator;
    }, {});
    for (const parameterName in parameters) {
      dataset[parameterName] = parameters[parameterName][0].parameterValue;
    }
    delete dataset.parameterName;
    delete dataset.parameterValue;
    /** Checking values are set as they might not exist as dataset parameter*/
    ["__fileCount", "__elapsedTime", "__volume"].forEach((p) => {
      if (!dataset[p]) {
        dataset[p] = 0;
      }
    });
    datasets.push(dataset);
  }
  return datasets;
};

/**
 * Parses the timelineRecords in a object by reducing the number of lines to dataset
 * @param {*} timeLineRecord [dataset.startDate, dataset.endDate, investigation.id, investigation.name, dataset.name, dataset.id, sample.name, sample.id, parameterType.name, parameter.stringValue]
 */
exports.parseTimeLineFromRecords = (timeLineRecords) => {
  const listRecords = timeLineRecords.map((record) => {
    return {
      startDate: record[0],
      endDate: record[1],
      investigationId: record[2],
      investigationName: record[3],
      datasetName: record[4],
      datasetId: record[5],
      sampleName: record[6],
      sampleId: record[7],
      parameterName: record[8],
      parameterValue: record[9],
      datasetType: record[10],
    };
  });
  return this.buidlTimeLineData(listRecords);
};

/**
 * Parses the datasetParameters in a object by reducing the number of lines to dataset
 * @param {*} parameters
 */
exports.parseTimeLine = (parameters) => {
  const listRecords = parameters.map((parameter) => {
    const datasetParameter = parameter.DatasetParameter;
    const dataset = datasetParameter.dataset;
    return {
      startDate: dataset.startDate,
      endDate: dataset.endDate,
      investigationId: dataset.investigation.id,
      investigationName: dataset.investigation.name,
      datasetName: dataset.name,
      datasetId: dataset.id,
      sampleName: dataset.sample.name,
      sampleId: dataset.sample.id,
      parameterName: datasetParameter.type.name,
      parameterValue: datasetParameter.stringValue,
      datasetType: dataset.type.name,
    };
  });
  return this.buidlTimeLineData(listRecords);
};
