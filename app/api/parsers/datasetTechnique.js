exports.parse = (datasetId, techniqueId, pid, name, description) => {
  return {
    id: techniqueId,
    datasetId,
    pid,
    name,
    description,
  };
};
