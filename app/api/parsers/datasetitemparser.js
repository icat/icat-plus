/**
 * This function convert a dataset into a item https://github.com/panosc-eu/panosc-search-scoring/blob/master/docs/md/PaNOSC_Federated_Search_Results_Scoring_API.md#items
 */
exports.parse = (datasets) => {
  return datasets.map((dataset) => {
    try {
      return {
        id: dataset.id,
        group: "datasets",
        fields: {
          search: `${dataset.investigation.visitId}  ${["name", "sampleName"].map((field) => dataset[field]).toString()} ${["summary", "title", "doi"]
            .map((field) => `${dataset.investigation[field]} `)
            .toString()}`.replace(/,g/, ""),
        },
      };
    } catch (e) {
      global.gLogger.error(e);
    }
    return null;
  });
};
