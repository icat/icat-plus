/**
 * This functions unflatter a parameter name and value
 **/
function mergeParameterNameAndValue(parameterName, name, value) {
  function parseToFloat(value) {
    try {
      return parseFloat(value);
    } catch {
      return NaN;
    }
  }

  if (parameterName) {
    if (name) {
      if (value) {
        const names = name.trim().split(" ");
        const values = value.trim().split(" ");
        if (names.length === values.length) {
          const merge = [];
          for (let i = 0; i < names.length; i++) {
            merge.push({
              name: names[i],
              numericValue: parseToFloat(values[i]),
              stringValue: values[i],
            });
          }
          return {
            name: parameterName.replace("_name", ""),
            value: merge,
          };
        }
      }
    }
  }
  return null;
}

/**
 * This function convert a dataset into a dataset_document
 **/
exports.parse = (datasets) => {
  return datasets.map((dataset) => {
    try {
      const datasetStartDate = dataset.startDate;
      const datasetEndDate = dataset.endDate;
      const parametersMerged = [];
      const parametersDictionary = {};
      const hasDefinition = dataset.parameters.find((parameter) => parameter.name === "definition");
      for (let i = 0; i < dataset.parameters.length; i++) {
        parametersDictionary[dataset.parameters[i].name] = dataset.parameters[i].value;
        /** parameters starting by __ are not allowed on elastic search */
        dataset[dataset.parameters[i].name.replace("__", "")] = dataset.parameters[i].value;

        /** Converting string to number */
        if (dataset.parameters[i].name === "InstrumentMonochromator_energy") {
          dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
        }
        if (dataset.parameters[i].name === "InstrumentMonochromator_wavelength") {
          dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
        }
        if (dataset.parameters[i].name === "InstrumentSource_current") {
          dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
        }

        //** it is not guaranteed that the startDate and endDate have date format, in case it can not be parsed then we use the original dataset values */
        if (dataset.parameters[i].name === "endDate" || dataset.parameters[i].name === "startDate") {
          try {
            dataset[dataset.parameters[i].name] = new Date(dataset.parameters[i].value);
          } catch {
            dataset[dataset.parameters[i].name] = dataset.parameters[i].name === "startDate" ? dataset.startDate : dataset.endDate;
          }
        }

        if (!hasDefinition && dataset.parameters[i].name === "scanType") {
          dataset.definition = dataset.parameters[i].value;
        }
      }

      dataset.parametersCount = dataset.parameters.length;
      /** parameters ending by _name */
      const composedParameters = dataset.parameters.filter((parameter) => {
        return parameter.name.endsWith("_name");
      });

      for (let i = 0; i < composedParameters.length; i++) {
        const parameterMerged = mergeParameterNameAndValue(
          composedParameters[i].name,
          parametersDictionary[composedParameters[i].name],
          parametersDictionary[composedParameters[i].name.replace("_name", "_value")]
        );
        if (parameterMerged) {
          parametersMerged.push(parameterMerged);
        }
      }

      for (let i = 0; i < parametersMerged.length; i++) {
        dataset[parametersMerged[i].name] = parametersMerged[i].value;
      }

      /** Investigation parameters */

      dataset.investigationId = dataset.investigation.id;
      dataset.investigationName = dataset.investigation.name;
      dataset.investigationSummary = dataset.investigation.summary;
      dataset.investigationTitle = dataset.investigation.title;
      dataset.investigationVisitId = dataset.investigation.visitId;
      dataset.releaseDate = dataset.investigation.releaseDate;
      dataset.startDate = datasetStartDate;
      dataset.endDate = datasetEndDate;
      dataset.investigationDOI = dataset.investigation.doi;
      if (!dataset.definition) {
        dataset.definition = "undefined";
      }

      if (dataset.investigation) {
        if (dataset.investigation.investigationInstruments) {
          if (dataset.investigation.investigationInstruments.length > 0) {
            dataset.instrumentId = dataset.investigation.investigationInstruments[0].instrument.id;
            /** removing dash because the es analyzer will break into pieces */
            if (dataset.investigation.investigationInstruments[0].instrument.name)
              dataset.instrumentName = dataset.investigation.investigationInstruments[0].instrument.name.replace(/-/g, "");
          }
        }
      }
      const notNull = (value) => {
        if (!value || value === "") {
          return "";
        }
        return value;
      };

      dataset.escompactsearch = `${notNull(dataset.investigation.visitId)} ${notNull(dataset.investigation.summary)} ${notNull(dataset.investigation.name)} ${notNull(
        dataset.InstrumentMonochromatorCrystal_reflection
      )} ${notNull(dataset.InstrumentMonochromatorCrystal_type)} ${notNull(dataset.InstrumentMonochromatorCrystal_usage)} ${notNull(dataset.definition)} `;
      `${notNull(dataset.name)} `;
      notNull(dataset.Sample_name);

      dataset.estype = "dataset";

      delete dataset.parameters;
      delete dataset.investigation;
      return dataset;
    } catch (e) {
      global.gLogger.error(e);
    }
    return null;
  });
};
