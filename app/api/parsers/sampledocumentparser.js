const datasetParameter = require("./datasetParameter.js");
const InvestigationParser = require("./investigationparser.js");

function getParameters(parameters) {
  const params = [];
  for (let i = 0; i < parameters.length; i++) {
    const parameter = parameters[i];
    params.push(datasetParameter.parse(parameter.type.name, parameter.stringValue, parameter.id, parameter.type.units));
  }
  return params;
}

function getDatasets(datasets) {
  const parsedDatasets = [];
  for (let i = 0; i < datasets.length; i++) {
    const dataset = datasets[i];
    parsedDatasets.push({
      id: dataset.id,
      name: dataset.name,
      startDate: dataset.startDate,
      endDate: dataset.endDate,
      location: dataset.location,
      investigation: dataset.investigation,
      type: dataset.type ? dataset.type.name : null,
      sampleName: dataset.sample ? dataset.sample.name : null,
      sampleId: dataset.sample ? dataset.sample.id : null,
      parameters: dataset.parameters ? getParameters(dataset.parameters) : [],
    });
  }
  return parsedDatasets;
}

exports.parse = (samples) => {
  const parsed = [];
  for (let i = 0; i < samples.length; i++) {
    try {
      const { id, name, investigation, modTime, parameters, datasets } = samples[i].Sample;
      parsed.push({
        id,
        name,
        investigation: investigation ? InvestigationParser.lightParse([{ Investigation: investigation }])[0] : {},
        modTime,
        parameters: parameters ? getParameters(parameters) : [],
        datasets: datasets ? getDatasets(datasets) : [],
      });
    } catch (e) {
      global.gLogger.error(e);
    }
  }
  return parsed;
};
