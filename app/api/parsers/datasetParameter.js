exports.parse = (typeName, value, parameterId, units, datasetId, sampleId, sampleName, createTime) => {
  return {
    name: typeName,
    value,
    id: parameterId,
    units,
    datasetId,
    sampleId,
    sampleName,
    createTime,
  };
};
