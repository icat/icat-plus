exports.parse = (techniques) => {
  return techniques.map((element) => {
    return element.Technique;
  });
};
