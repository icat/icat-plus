// Next line does not work as getParameterTypeDictionary becomes undefined
//const { getParameterTypeDictionary } = require("../../cache/cache.js");
const cache = require("../../cache/cache.js");

exports.parse = (investigations) => {
  try {
    const parameterTypes = cache.getParameterTypeDictionary();
    const result = [];
    for (const i in investigations) {
      const investigation = investigations[i];
      const parameters = [];
      if (investigation) {
        if (investigation.Investigation) {
          if (investigation.Investigation.parameters) {
            for (let j = 0; j < investigation.Investigation.parameters.length; j++) {
              const parameterTypeId = investigation.Investigation.parameters[j].type.id;
              const parameterTypeName = parameterTypes[parameterTypeId];
              parameters.push({
                name: parameterTypeName,
                value: investigation.Investigation.parameters[j].stringValue,
              });
            }
          }
        }
      }
      investigation.Investigation.parameters = parameters;
      result.push(investigation);
    }
    return result;
  } catch (e) {
    global.gLogger.error(e);
  }
};

exports.lightParse = (investigations) => {
  try {
    const parameterTypes = cache.getParameterTypeDictionary();
    return investigations.map((investigation) => {
      if (investigation.Investigation) {
        if (investigation.Investigation.parameters) {
          const parameters = {};
          investigation.Investigation.parameters.forEach((parameter) => {
            if (parameter.type) {
              parameters[parameterTypes[parameter.type.id]] = parameter.stringValue;
            }
          });

          let instrument = {};
          if (investigation.Investigation.investigationInstruments && investigation.Investigation.investigationInstruments.length > 0) {
            if (investigation.Investigation.investigationInstruments[0].instrument) {
              const instr = investigation.Investigation.investigationInstruments[0].instrument;
              const instrumentScientists = [];
              if (instr.instrumentScientists && instr.instrumentScientists.length > 0) {
                instr.instrumentScientists.forEach((instrumentScientist) => {
                  const { name } = instrumentScientist.user;
                  instrumentScientists.push({ name });
                });
              }

              instrument = {
                name: instr.name.toUpperCase(),
                id: instr.id,
                instrumentScientists,
              };
            }
          }

          const investigationUsers = [];
          if (investigation.Investigation.investigationUsers) {
            if (investigation.Investigation.investigationUsers.length > 0) {
              investigation.Investigation.investigationUsers.forEach((investigationUser) => {
                const { email, fullName, familyName, name, orcidId, givenName, affiliation } = investigationUser.user;
                investigationUsers.push({ role: investigationUser.role, user: { email, fullName, familyName, name, orcidId, givenName, affiliation } });
              });
            }
          }

          const { name, startDate, endDate, id, doi, title, visitId, releaseDate, summary, meta, type } = investigation.Investigation;
          return { name, startDate, endDate, id, doi, title, visitId, releaseDate, summary, parameters, instrument, investigationUsers, meta, type };
        }
      }
      return null;
    });
  } catch (e) {
    global.gLogger.error(e);
  }
};

/**
 * Parses the timelineRecords in a object by reducing the number of lines to investigation
 * @param {*} timeLineRecord [dataset.startDate, dataset.endDate, investigation.id, investigation.name, parameterType.name, parameter.stringValue]
 */
exports.parseTimeLine = (timeLineRecords) => {
  const listRecords = timeLineRecords.map((record) => {
    return {
      startDate: record[0],
      endDate: record[1],
      investigationId: record[2],
      investigationName: record[3],
      instrumentName: record[4],
      parameterName: record[5],
      parameterValue: record[6],
    };
  });
  /** This groups the records by dataset id */
  const groupedByInvestigationIds = listRecords.reduce((accumulator, currentObject) => {
    const investigationId = currentObject.investigationId;
    if (!accumulator[investigationId]) {
      accumulator[investigationId] = [];
    }
    accumulator[investigationId].push(currentObject);
    return accumulator;
  }, {});

  const investigations = [];
  for (const investigationId in groupedByInvestigationIds) {
    const investigation = groupedByInvestigationIds[investigationId][0];
    investigation.parameters = [];
    const parameters = groupedByInvestigationIds[investigationId].reduce((accumulator, currentObject) => {
      const datasetId = currentObject.parameterName;
      if (!accumulator[datasetId]) {
        accumulator[datasetId] = [];
      }
      accumulator[datasetId].push(currentObject);
      return accumulator;
    }, {});
    for (const parameterName in parameters) {
      investigation[parameterName] = parameters[parameterName][0].parameterValue;
    }
    delete investigation.parameterName;
    delete investigation.parameterValue;
    /** Checking values are set as they might not exist as dataset parameter*/
    ["__fileCount", "__elapsedTime", "__volume"].forEach((p) => {
      if (!investigation[p]) {
        investigation[p] = 0;
      }
    });
    investigations.push(investigation);
  }
  return investigations;
};
