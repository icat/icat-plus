/**
 * This method parses a list of parameters
 * @param {*} parameters
 * @returns
 */
exports.parse = (parameters) => {
  return parameters.map((parameter) => {
    const { name, id, units, description } = parameter.ParameterType;
    return { name, id, units, description };
  });
};
