exports.parse = (instruments) => {
  return instruments.map((element) => {
    return element.Instrument;
  });
};
