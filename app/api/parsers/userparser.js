exports.parse = (users) => {
  return users.map((element) => {
    return {
      name: element.User.name,
      fullName: element.User.fullName,
      email: global.gServerConfig.user.showEmail ? element.User.email : "",
      id: element.User.id,
      orcidId: element.User.orcidId,
      givenName: element.User.givenName,
      familyName: element.User.familyName,
      affiliation: element.User.affiliation,
    };
  });
};

exports.fullParse = (users) => {
  return users.map((element) => {
    return element.User;
  });
};
