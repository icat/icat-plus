const axios = require("axios-https-proxy-fix");
const datasetParser = require("./parsers/datasetparser.js");
const datacollectionParser = require("./parsers/datacollectionparser.js");
const sampleParser = require("./parsers/sampledocumentparser.js");
const instrumentScientistParser = require("./parsers/instrumentscientistparser.js");
const userParser = require("./parsers/userparser.js");
const qs = require("qs");
const icatQuery = require("./query.js");
const investigationQuery = require("./queries/investigation.query.js");
const datasetQuery = require("./queries/dataset.query.js");
const sampleQuery = require("./queries/sample.query.js");
const dataCollectionQuery = require("./queries/dataCollection.query.js");
const investigationParser = require("./parsers/investigationparser.js");
const proxy = false;
const { isUndefined } = require("../controllers/helpers/helper.controller");

exports.getDatasetParameter = async (sessionId, filters) => {
  const { investigationId, name, sampleId, datasetType, instrumentName, startDate, endDate, includeDatasets, parameters } = filters;
  global.gLogger.debug("getDatasetParameter", { investigationId, name, sampleId, datasetType, instrumentName, startDate, endDate, includeDatasets });
  let response = {};
  if (investigationId) {
    response = await doGet(icatQuery.getDatasetParameterByInvestigationId(sessionId, investigationId, name, { name, sampleId, datasetType, includeDatasets, parameters }));
  }
  if (instrumentName && startDate && endDate) {
    response = await doGet(icatQuery.getDatasetParameterByInstrumentName(sessionId, instrumentName, startDate, endDate, name, includeDatasets, parameters));
  }
  return response.data;
};

exports.getInvestigationsByDatasetListIdsURL = async (sessionId, datasetIds) => {
  const response = await doGet(icatQuery.getInvestigationsByDatasetListIdsURL(sessionId, datasetIds));
  return response.data;
};

exports.getPropertiesQuery = async () => {
  const response = await axios.get(icatQuery.getPropertiesQuery());
  return response.data;
};

exports.getInvestigationTypes = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const investigationTypes = await axios.get(icatQuery.getInvestigationTypes(data.sessionId));
  return investigationTypes.data;
};

exports.getParameterTypeList = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const parameterTypeList = await axios.get(icatQuery.getParameterTypeList(data.sessionId));
  return parameterTypeList.data;
};

function parseInvestigationUserToObject(investigationUserRecord) {
  return {
    name: investigationUserRecord[0],
    fullName: investigationUserRecord[1],
    role: investigationUserRecord[2],
    investigationName: investigationUserRecord[3],
    investigationId: investigationUserRecord[4],
    email: investigationUserRecord[5],
    id: investigationUserRecord[6],
    orcidId: investigationUserRecord[7],
    familyName: investigationUserRecord[8],
    givenName: investigationUserRecord[9],
    affiliation: investigationUserRecord[10],
  };
}
/**
 * This method will retrieve an array of objects with the information concerning the investigation user
 *
 * Example : [{"name":"reader","role":"Principal investigator","investigationName":"IH-CH-1336","investigationId":77570462}]
 *
 */
exports.getInvestigationUserByInvestigationId = async (sessionId, investigationIds) => {
  const response = await doGet(icatQuery.getInvestigationUserByInvestigationIdURL(sessionId, investigationIds));
  return response.data.map((o) => {
    return parseInvestigationUserToObject(o);
  });
};

/**
 * Retrieves all users linked to a specified investigation with a specified role
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} role
 * @returns Array of investigationUser
 */
exports.getInvestigationUserByInvestigationIdAndRole = async (sessionId, investigationId, role) => {
  const users = await this.getInvestigationUserByInvestigationId(sessionId, investigationId);
  return users.filter((u) => u.role === role);
};

exports.asyncGetInvestigationUserByUserName = async (sessionId, username) => {
  const response = await doGet(icatQuery.getInvestigationUserByUserNameURL(sessionId, username));
  return response.data.map((o) => {
    return parseInvestigationUserToObject(o);
  });
};

/**
 * Get the debugrmation of the sessionId or return 403 if session is not found. Documentation: https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html#669420320
 * @example
 * {"userName":"db/root","remainingMinutes":117. 87021666666666}
 * @param {string} sessionId
 * @return {json} JSON object with username and remainingMinutes fields
 */
exports.asyncGetSessionInformation = async (sessionId) => {
  const response = await doGet(icatQuery.getSessionInformationURL(sessionId));
  return response.data;
};

/**
 * Removes the session from the icat server. It means a log out: https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html#1024708709
 * @param {*} sessionId
 * @returns
 */
exports.logout = async (sessionId) => {
  const response = await axios.delete(icatQuery.logout(sessionId));
  return response;
};

exports.asyncGetInvestigationUserBySessionId = async (sessionId) => {
  const data = await this.asyncGetSessionInformation(sessionId);
  const responseInvestigationUsers = await this.asyncGetInvestigationUserByUserName(sessionId, data.userName);
  return responseInvestigationUsers;
};

exports.getProxy = () => {
  return false;
};

/**
 * THIS METHOD DUPLICATED FUNCTIONALITYOF GETSESSION SO WE CAN MIGRATE SMOOTLY ALL CLIENTS
 * Create an ICAT session given a credential
 * @param {*} credentials the crendential to create the new session with
 */
exports.asyncGetSession = async (credentials) => {
  const response = await axios.post(
    icatQuery.getSession(),
    qs.stringify({
      json: JSON.stringify(credentials),
    }),
    {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    }
  );
  return response.data;
};

exports.updateDataCollectionDOI = async (authentication, dataCollectionId, doi) => {
  const getSessionResponse = await this.asyncGetSession(authentication);
  const { sessionId } = getSessionResponse;

  const dataCollection = [
    {
      DataCollection: {
        id: dataCollectionId,
        doi,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(dataCollection),
  });

  const response = await doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data);
  return response.data;
};

/**
 * Refreshes an existing ICAT session
 * @param {string} sessionId
 * @returns
 */
exports.refreshSession = async (sessionId) => {
  const response = await axios.put(icatQuery.getSessionInformationURL(sessionId));
  return response;
};

function doDelete(endPoint, data) {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  };
  return axios.delete(endPoint, data, headers);
}

function createParameterType(parameterType, value) {
  return {
    type: { id: parameterType[0].ParameterType.id },
    stringValue: value.toString(),
  };
}
/**
 * This method will create the data collection composed by the datasetIdList
 */
exports.mint = async (authentication, datasetIdList, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) => {
  if (abstract && abstract.length > 4000) {
    global.gLogger.debug("[mint] Abstract is longer than 4000 chars", { abstract });
  }

  /** It gets a new session for the minter user */

  const getSessionResponse = await this.asyncGetSession(authentication);
  const { sessionId } = getSessionResponse;
  global.gLogger.debug("[mint ICAT SUCCESS] New session for the minter user received", { sessionId });
  const parameters = buildDataCollectionParameters(title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName);

  const dataCollection = [
    {
      DataCollection: {
        dataCollectionDatasets: datasetIdList.map((id) => {
          return { dataset: { id } };
        }),
        parameters,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(dataCollection),
  });

  // This creates a datacollection
  const response = await doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data);
  return response.data;
};

function buildDataCollectionParameters(title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) {
  if (abstract && abstract.length > 4000) {
    global.gLogger.debug("[mint] Abstract is longer than 4000 chars", { abstract });
  }
  const parameters = [];
  // Getting parameters: title, abstract
  const parameterTypes = global.appCache.get("parameterTypes");
  if (parameterTypes) {
    const getParameterByName = (name) => {
      return parameterTypes.filter((p) => {
        return p.ParameterType.name === name;
      });
    };

    const titleParameterType = getParameterByName("title");
    const abstractParameterType = getParameterByName("abstract");
    const mintedByNameParameterType = getParameterByName("mintedByName");
    const mintedByFullNameParameterType = getParameterByName("mintedByFullName");
    const investigationNamesParameterType = getParameterByName("investigationNames");
    const instrumentNamesParameterType = getParameterByName("instrumentNames");
    /** Setting dataCollection Parameters */
    if (title && titleParameterType) {
      parameters.push(createParameterType(titleParameterType, title));
    } else {
      global.gLogger.warn("ParameterType title has not been found");
    }
    if (abstract && abstractParameterType) {
      parameters.push(createParameterType(abstractParameterType, abstract));
    } else {
      global.gLogger.warn("ParameterType abstract has not been found");
    }

    if (investigationNamesParameterType) {
      parameters.push(createParameterType(investigationNamesParameterType, investigationNames));
    } else {
      global.gLogger.warn("ParameterType investigationNames has not been found");
    }

    if (instrumentNamesParameterType) {
      parameters.push(createParameterType(instrumentNamesParameterType, instrumentsNames));
    } else {
      global.gLogger.warn("ParameterType instrumentNames has not been found");
    }

    if (mintedByNameParameterType) {
      parameters.push(createParameterType(mintedByNameParameterType, mintedByName));
    } else {
      global.gLogger.warn("ParameterType mintedByName has not been found");
    }

    if (mintedByFullNameParameterType) {
      parameters.push(createParameterType(mintedByFullNameParameterType, mintedByFullName));
    } else {
      global.gLogger.warn("ParameterType mintedByFullName has not been found");
    }
  } else {
    global.gLogger.warn("There are no parameter types retrieved in cache");
  }
  return parameters;
}

/**
 *  Create the dataCollection parameters for a bespoke DOI
 * @param {*} dataCollection
 * @param {*} title
 * @param {*} abstract
 * @param {*} investigationNames
 * @param {*} instrumentsNames
 * @param {*} mintedByName
 * @param {*} mintedByFullName
 */
exports.createDataCollectionParameters = async (dataCollection, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) => {
  const getSessionResponse = await this.asyncGetSession(global.gServerConfig.icat.authorizations.minting.user);
  const { sessionId } = getSessionResponse;
  const parameters = buildDataCollectionParameters(title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName);
  for (const parameter in parameters) {
    createDataCollectionParameter(sessionId, dataCollection, parameter.type, parameter.stringValue);
  }
};

async function createDataCollectionParameter(sessionId, dataCollection, dataCollectionParameterType, stringValue) {
  const dataCollectionParameters = [
    {
      DataCollectionParameter: {
        dataCollection,
        stringValue,
        type: dataCollectionParameterType,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(dataCollectionParameters),
  });

  global.gLogger.debug("createDataCollectionParameter", { data });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
}

exports.addInvestigationUser = async (sessionId, users) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(users),
  });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

const setPaginationMetadata = (datasets, totalWithoutFilters, total, limit, skip) => {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;
  datasets.forEach((d) => {
    d.meta = {
      page: {
        totalWithoutFilters,
        total,
        totalPages,
        currentPage,
      },
    };
  });
  return datasets;
};

/**
 * InvestigationAdapter encapsulates all endpoints to ICAT that returns investigation-related entities
 */
exports.InvestigationAdapter = {
  countInvestigationsBy: async function countInvestigationsBy(sessionId, params) {
    const response = await doGet(investigationQuery.getCountInvestigationBy(sessionId, params));
    return response.data[0];
  },

  async getTimelineBy(sessionId, params) {
    const query = investigationQuery.getTimeLineBy(sessionId, params);
    const response = await doGet(query);
    return investigationParser.parseTimeLine(response.data);
  },
  getInvestigationsBy: async function getInvestigationsBy(sessionId, params, include, enforceLimit) {
    if (!params) {
      // enforceLimit is a Temporary solution until the cache can be removed
      params = { limit: enforceLimit ? enforceLimit : global.gServerConfig.icat.maxQueryLimit, skip: 0 };
    }
    let { limit, skip } = params;
    skip = isUndefined(skip) ? 0 : skip;
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : limit;

    // Temporary solution until the cache can be removed
    limit = enforceLimit ? enforceLimit : limit;

    if (params.pagination && (params.pagination === "false" || params.pagination === false)) {
      const response = await doGet(investigationQuery.getInvestigationsBy(sessionId, params, include));
      return investigationParser.lightParse(response.data);
    }

    // eslint-disable-next-line no-unused-vars
    const { search, ...rest } = params;
    const [totalWithoutFilters, total, response] = await Promise.all([
      this.countInvestigationsBy(sessionId, rest),
      this.countInvestigationsBy(sessionId, params),
      doGet(investigationQuery.getInvestigationsBy(sessionId, params, include)),
    ]);
    return setPaginationMetadata(investigationParser.lightParse(response.data), totalWithoutFilters, total, limit, skip);
  },
};

/**
 * DatasetAdapter encapsulates all endpoints to ICAT that returns dataset-related entities
 */
exports.DatasetAdapter = {
  async countDatasetsBy(sessionId, params) {
    const response = await doGet(datasetQuery.getCountDatasetBy(sessionId, params));
    return response.data[0];
  },

  async getTimelineBy(sessionId, params) {
    const query = datasetQuery.getTimeLineBy(sessionId, params);
    const response = await doGet(query);
    return datasetParser.parseTimeLine(response.data);
  },

  async getTimelineForAdminBy(sessionId, params) {
    const query = datasetQuery.getTimeLineForAdminBy(sessionId, params);
    const response = await doGet(query);
    return datasetParser.parseTimeLineFromRecords(response.data);
  },

  async getDatasetsBy(sessionId, params) {
    const query = datasetQuery.getDatasetsBy(sessionId, params);

    let { limit, skip } = params;
    skip = isUndefined(skip) ? 0 : skip;
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : limit;

    if (params.pagination && (params.pagination === "false" || params.pagination === false)) {
      const response = await doGet(query);
      return datasetParser.parse(response.data);
    }
    // eslint-disable-next-line no-unused-vars
    const { parameters, ...rest } = params;
    const [totalWithoutFilters, total, response] = await Promise.all([this.countDatasetsBy(sessionId, rest), this.countDatasetsBy(sessionId, params), doGet(query)]);
    return setPaginationMetadata(datasetParser.parse(response.data), totalWithoutFilters, total, limit, skip);
  },
};

/**
 * SampleAdapter encapsulates all endpoints to ICAT that returns sample-related entities
 */
exports.SampleAdapter = {
  async countSamplesBy(sessionId, params) {
    const response = await doGet(sampleQuery.getCountSamplesBy(sessionId, params));
    return response.data[0];
  },
  async getSamplesBy(sessionId, params) {
    const query = sampleQuery.getSamplesBy(sessionId, params);

    let { limit, skip } = params;
    skip = isUndefined(skip) ? 0 : skip;
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : limit;

    if (params.pagination && (params.pagination === "false" || params.pagination === false)) {
      const response = await doGet(query);
      return sampleParser.parse(response.data);
    }
    // eslint-disable-next-line no-unused-vars
    const { parameters, ...rest } = params;
    const [totalWithoutFilters, total, response] = await Promise.all([this.countSamplesBy(sessionId, rest), this.countSamplesBy(sessionId, params), doGet(query)]);
    return setPaginationMetadata(sampleParser.parse(response.data), totalWithoutFilters, total, limit, skip);
  },
};

/**
 * DataCollection encapsulates all endpoints to ICAT that returns dataset-related entities
 */
exports.DataCollectionAdapter = {
  async countDataCollectionBy(sessionId, params) {
    const response = await doGet(dataCollectionQuery.getCountDataCollectionsBy(sessionId, params));
    return response.data[0];
  },

  async getDataCollectionsBy(sessionId, params) {
    const query = dataCollectionQuery.getDataCollectionsBy(sessionId, params);

    let { limit, skip } = params;
    skip = isUndefined(skip) ? 0 : skip;
    limit = isUndefined(limit) ? global.gServerConfig.icat.maxQueryLimit : limit;

    if (params.pagination && (params.pagination === "false" || params.pagination === false)) {
      const response = await doGet(query);
      return datacollectionParser.parse(response.data);
    }
    // eslint-disable-next-line no-unused-vars
    const { parameters, ...rest } = params;
    const [totalWithoutFilters, total, response] = await Promise.all([this.countDataCollectionBy(sessionId, rest), this.countDataCollectionBy(sessionId, params), doGet(query)]);
    return setPaginationMetadata(datacollectionParser.parse(response.data), totalWithoutFilters, total, limit, skip);
  },
};

function doPost(endPoint, data, onSuccess, onError) {
  const headers = {
    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
  };
  if (onSuccess) {
    axios
      .post(endPoint, data, headers)
      .then((response) => {
        onSuccess(response.data);
      })
      .catch((error) => {
        onError(error);
      });
  } else {
    return axios.post(endPoint, data, headers);
  }
}

function doGet(url, onSuccess, onError, postProcessing) {
  if (!onSuccess) {
    if (postProcessing) {
      return new Promise((resolve, reject) => {
        axios
          .get(url, { proxy })
          .then((response) => {
            resolve(postProcessing(response.data));
          })
          .catch((error) => {
            reject(error);
          });
      });
    }
    return axios.get(url, { proxy });
  }
  axios
    .get(url, { proxy })
    .then((response) => {
      if (postProcessing) {
        onSuccess(postProcessing(response.data));
      } else {
        onSuccess(response.data);
      }
    })
    .catch((error) => {
      try {
        onError(error);
      } catch (error) {
        global.gLogger.error(error);
      }
    });
}

/** Return all users by group name */
exports.getUsersByGroupName = async (authentication, groupName) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getUsersByGroupName(data.sessionId, groupName));
  return response.data;
};

/** Returns all users by group name */
exports.getUsers = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getUsers(data.sessionId));
  return response.data;
};

exports.getInstruments = async (authentication) => {
  const data = await this.asyncGetSession(authentication);
  const response = await doGet(icatQuery.getInstruments(data.sessionId));
  return response.data;
};

exports.getInstrumentsBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getInstruments(sessionId));
  return response.data;
};

/** Returns all users by sessionId */
exports.getUsersBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getUsers(sessionId));
  global.gLogger.debug(icatQuery.getUsers(sessionId), { users: userParser.parse(response.data).length });
  return userParser.parse(response.data);
};

/** Returns instrumentScientists by user's name' */
exports.getInstrumentScientistsByUserName = async (sessionId, userName) => {
  const response = await doGet(icatQuery.getInstrumentScientistsByUserName(sessionId, userName));
  return instrumentScientistParser.parse(response.data);
};

/** Returns instrumentScientists, filtered by instrumentName */
exports.getInstrumentScientists = async (sessionId, instrumentName) => {
  const response = await doGet(icatQuery.getInstrumentScientists(sessionId, instrumentName));
  return instrumentScientistParser.parse(response.data);
};

/**
 * Get a list of data files by dataset id
 * @param {string} sessionId
 * @param {string} datasetId
 * @param {string} limit
 * @param {string} skip
 * @param {string} search
 */
exports.getDatafilesByDatasetId = async (sessionId, datasetId, limit, skip, search) => {
  const response = await doGet(icatQuery.getDatafilesByDatasetId(sessionId, datasetId, limit, skip, search));
  return response.data;
};

/**
 * count the number of datafiles of a given datasetId, could be filtered with a search text on datafile location/name
 * @param {*} sessionId
 * @param {*} datasetId
 * @param {*} search
 * @returns
 */
exports.countDatafileByDatasetId = async (sessionId, datasetsId, search) => {
  const response = await doGet(icatQuery.countEntityDatafilesByDatasetId(sessionId, datasetsId, search));
  return response.data;
};

/**
 * Get a list of data files by ids
 * @param {string} sessionId
 * @param {string} datasetIds A comma separated list of datafileIds ids. Example: "2342,1323,234234"
 */
exports.getDatafilesByIds = async (sessionId, datafileIds) => {
  const response = await doGet(icatQuery.getDatafilesByIds(sessionId, datafileIds));
  return response.data;
};

/**
 * Return the archive status of the data files specified by the  datasetIds  along with a sessionId.
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} datasetIds A comma separated list of dataset ids. Example: "2342,1323,234234"
 */
exports.getStatusByDatasetIds = async (sessionId, datasetIds) => {
  const response = await Promise.all(datasetIds.split(",").map((id) => doGet(icatQuery.getStatusByDatasetIds(sessionId, id))));
  return response.map((res) => res.data);
};

/**
 * Return the status of the data files
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} datafileIds A comma separated list of datafile ids. Example: "2342,1323,234234"
 * @param {string} datasetIds A comma separated list of dataset ids. Example: "2342,1323,234234"
 * @param {string} isOnline A boolean that checks if the file is available on disk
 */
exports.getPathBy = async (sessionId, datafileIds, datasetIds, isOnline) => {
  const response = await doGet(icatQuery.getPathBy(sessionId, datafileIds, datasetIds, isOnline));
  return response.data;
};

/**
 * Get InstrumentScientists by instrument name.
 * @param {string} sessionId session identifier identifying the user.
 * @param {string} instrumentName instrument name identifying the instrument (=beamline)
 */
exports.getInstrumentScientistsByInstrumentName = async (sessionId, instrumentName) => {
  global.gLogger.debug("getInstrumentScientistsByInstrumentName()", { sessionId, instrumentName });
  const response = await doGet(icatQuery.getInstrumentScientistsByInstrumentName(sessionId, instrumentName));
  return response.data;
};

exports.createInstrumentScientists = async (sessionId, instrumentScientists) => {
  global.gLogger.debug("createInstrumentScientists", { sessionId });
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(instrumentScientists),
  });
  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

exports.deleteInstrumentScientists = async (sessionId, instrumentScientists) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(instrumentScientists),
  });

  global.gLogger.debug("deleteInstrumentScientists", { data, sessionId, instrumentScientists });
  const response = await doDelete(icatQuery.getDeleteQuery(sessionId, JSON.stringify(instrumentScientists)), data);
  return response.data;
};

exports.deleteInvestigationUser = async (sessionId, investigationUser) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(investigationUser),
  });

  global.gLogger.debug("deleteInvestigationUser", { data, sessionId, investigationUser: JSON.stringify(investigationUser) });
  const response = await doDelete(icatQuery.getDeleteQuery(sessionId, JSON.stringify(investigationUser)), data);
  return response.data;
};

exports.getInstrumentsForUser = async (sessionId, username) => {
  global.gLogger.debug("getInstrumentsForUser", { username, query: icatQuery.getInstrumentsForUser(sessionId, username) });
  const response = await doGet(icatQuery.getInstrumentsForUser(sessionId, username));
  return response.data;
};

exports.getInstrumentsForInstrumentScientist = async (sessionId, username) => {
  global.gLogger.debug("getInstrumentsForInstrumentScientist", { username, query: icatQuery.getInstrumentsForInstrumentScientist(sessionId, username) });
  const response = await doGet(icatQuery.getInstrumentsForInstrumentScientist(sessionId, username));
  return response.data;
};

/**
 * Returns a dataset parameter for a specified id
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getDatasetParameterById = async (sessionId, parameterId) => {
  global.gLogger.debug("getDatasetParameterById", { parameterId });
  const response = await doGet(icatQuery.getDatasetParameterById(sessionId, parameterId));
  return response.data;
};

/**
 * Updates a dataset parameter value
 * @param {*} sessionId
 * @param {*} parameterId
 * @param {*} value
 * @returns empty array
 */
exports.updateDatasetParameter = async (sessionId, parameterId, value) => {
  const datasetParameter = [
    {
      DatasetParameter: {
        id: parameterId,
        stringValue: value,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(datasetParameter),
  });

  global.gLogger.debug("updateDatasetParameter", { data });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

/**
 * Returns other user's starting with the prefix expect the given usernameWithPrefix (username/smisPk)
 * @param {*} sessionId the sessionId
 * @param {*} prefix  the username
 * @param {*} userNameWithPrefix  the userName concatenated with smisPk, separated by a slash
 * @returns a list of users corresponding to the prefix, could be empty
 */
exports.getUserByUsernamePrefix = async (sessionId, prefix, userNameWithPrefix) => {
  const response = await doGet(icatQuery.getUserByUsernamePrefix(sessionId, prefix, userNameWithPrefix));
  return userParser.fullParse(response.data);
};

/**
 * Returns the users corresponding to a given name
 * @param {*} sessionId
 * @param {*} name
 * @returns a list of users corresponding to the name (must be unique), empty otherwise
 */
exports.getUserByName = async (sessionId, name) => {
  const response = await doGet(icatQuery.getUserByName(sessionId, name));
  return userParser.fullParse(response.data);
};

/**
 * Returns all dataCollections linked to an investigation
 * dataCollections containing datasets linked to the specific investigation
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.getDataCollectionsByInvestigationId = async (sessionId, investigationId) => {
  const response = await doGet(icatQuery.getDataCollectionsByInvestigationId(sessionId, investigationId));
  return response && response.data ? datacollectionParser.parse(response.data) : [];
};

/**
 * Returns a sample parameter for a specified id
 * @param {*} sessionId
 * @param {*} parameterId
 * @returns
 */
exports.getSampleParameterById = async (sessionId, parameterId) => {
  global.gLogger.debug("getSampleParameterById", { parameterId });
  const response = await doGet(icatQuery.getSampleParameterById(sessionId, parameterId));
  return response.data;
};

/**
 * Updates a sample parameter value
 * @param {*} sessionId
 * @param {*} parameterId
 * @param {*} stringValue
 * @returns empty array
 */
exports.updateSampleParameter = async (sessionId, parameterId, stringValue) => {
  const sampleParameter = [
    {
      SampleParameter: {
        id: parameterId,
        stringValue,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(sampleParameter),
  });

  global.gLogger.debug("updateSampleParameter", { data });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

/**
 * Creates a sample parameter value
 * @param {*} sessionId
 * @param {*} name
 * @param {*} stringValue
 * @returns empty array
 */
exports.createSampleParameter = async (sessionId, sample, sampleParameterType, stringValue) => {
  const sampleParameters = [
    {
      SampleParameter: {
        sample,
        stringValue,
        type: sampleParameterType,
      },
    },
  ];
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(sampleParameters),
  });

  global.gLogger.debug("createSampleParameter", { data });

  const response = await doPost(icatQuery.getCreateQuery(sessionId), data);
  return response.data;
};

exports.getParameterTypeByName = async (sessionId, name) => {
  global.gLogger.debug("getParameterTypeByName", { name });
  const response = await doGet(icatQuery.getParameterTypeByName(sessionId, name));
  return response.data;
};

exports.deleteSampleParameters = async (sessionId, sampleParameters) => {
  const data = qs.stringify({
    sessionId,
    entities: JSON.stringify(sampleParameters),
  });

  global.gLogger.debug("deleteSampleParameters", { data, sessionId, sampleParameters });
  const response = await doDelete(icatQuery.getDeleteQuery(sessionId, JSON.stringify(sampleParameters)), data);
  return response.data;
};

exports.findDatafileParameters = async (sessionId, datasetsId, filepath) => {
  const response = await doGet(icatQuery.findDatafileParameters(sessionId, datasetsId, filepath));
  return response.data;
};

exports.getTechniquesBySessionId = async (sessionId) => {
  const response = await doGet(icatQuery.getTechniques(sessionId));
  return response.data;
};

exports.getDatasetTechniques = async (sessionId, datasetIds) => {
  const response = await doGet(icatQuery.getDatasetTechniques(sessionId, datasetIds));
  return response.data;
};
