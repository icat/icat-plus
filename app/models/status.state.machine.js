const Status = require("./status.js");

module.exports = Object.freeze({
  CREATED: [],
  SCHEDULED: [Status.CREATED],
  SENT: [Status.SCHEDULED],
  STORES: [Status.SENT],
  BEAMLINE: [Status.STORES],
  BACK_STORES: [Status.BEAMLINE],
  BACK_USER: [Status.BACK_STORES],
  ON_HOLD: [Status.BACK_STORES],
  DESTROYED: "DESTROYED",
  REMOVED: [],
});
