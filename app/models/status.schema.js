/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const STATUS = global.gTrackingConfig.STATUS;

const StatusSchema = new mongoose.Schema(
  {
    status: {
      type: String,
      default: STATUS.CREATED,
      set: (v) => v.toUpperCase(),
    },
    createdBy: {
      type: String,
      required: true,
    },
    comments: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

StatusSchema.virtual("createdByFullName").get(function () {
  return global.users[this.createdBy] ? global.users[this.createdBy].fullName : null;
});

StatusSchema.virtual("createdByEmail").get(function () {
  return global.users[this.createdBy] ? global.users[this.createdBy].email : null;
});

module.exports = { StatusSchema };
