const mongoose = require("mongoose");

const DatasetAccessSchema = mongoose.Schema(
  {
    datasetId: { type: Number, required: true },
    user: { type: String, required: true },
    email: { type: String },
    type: {
      type: String,
      required: true,
      enum: ["RESTORE"],
      default: "RESTORE",
    },
    status: {
      type: String,
      enum: ["ONGOING", "DONE"],
      default: "ONGOING",
    },
    creationDate: {
      type: Date,
      required: true,
      default: Date.now,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("DatasetAccess", DatasetAccessSchema);
