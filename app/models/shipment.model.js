/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const Schema = require("mongoose").Schema;

/** Shipment schema */
const ShipmentSchema = mongoose.Schema(
  {
    /** Investigation identifier indicating which investigation this event belongs to.*/
    investigationId: Number,
    /** Name of the investigation */
    investigationName: {
      type: String,
      required: true,
    },
    /** Courier company details */
    defaultCourierAccount: { type: String },
    defaultCourierCompany: { type: String },
    description: { type: String },
    comments: { type: String },
    name: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ["OPENED", "REMOVED"],
      default: "OPENED",
    },
    /** Reference to Address that will be used by default in parcels created within this shipment. This address defines where the sample should be returned */
    defaultReturnAddress: { type: Schema.Types.ObjectId, ref: "Address" },
    /** Reference to Address that will be used by default in parcels created within this shipment, This address defines where the sample should be sent */
    defaultShippingAddress: { type: Schema.Types.ObjectId, ref: "Address" },
    parcels: [{ type: Schema.Types.ObjectId, ref: "Parcel" }],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Shipment", ShipmentSchema);
