/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

/** Message schema */
const MessageSchema = mongoose.Schema(
  {
    type: { type: String, required: true, enum: ["INFORMATION"] },
    message: { type: String },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Message", MessageSchema);
