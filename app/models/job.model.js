/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

const InvestigationType = new mongoose.Schema({
  id: { type: Number, required: true },
  name: { type: String, required: true },
});

const JobStatusSchema = new mongoose.Schema(
  {
    status: {
      type: String,
      default: "CREATED",
      set: (v) => v.toUpperCase(),
    },
  },
  {
    timestamps: true,
  }
);

const StepSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      set: (v) => v.toUpperCase(),
    },
    status: {
      type: String,
      set: (v) => v.toUpperCase(),
    },
    message: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const LogSchema = new mongoose.Schema(
  {
    log: {
      type: Object,
    },
  },
  {
    timestamps: true,
  }
);

const JobSchema = mongoose.Schema(
  {
    investigations: [InvestigationType],
    /** this is the identifier of the workflow */
    identifier: {
      type: String,
      required: true,
    },
    datasetIds: [{ required: true, type: [Number] }],
    datasetNames: [{ required: true, type: [String] }],
    sampleIds: [{ required: false, type: [Number] }],
    sampleNames: [{ required: false, type: [String] }],
    userNames: {
      type: [String],
      required: true,
    },
    /** it corresponds to the ewoks job_id */
    jobId: {
      type: String,
    },
    /** this is the overall status of workflow */
    status: {
      type: String,
      default: "CREATED",
    },
    logs: [LogSchema],
    input: Object,
    workflow: Object,
    /** This is the execution step. Example: compression, reduction, phasing, integration, etc... but can be also the execution node*/
    steps: [StepSchema],
    statuses: [JobStatusSchema],
  },
  {
    timestamps: true,
    toJSON: { virtuals: true }, // So `res.json()` and other `JSON.stringify()` functions include virtuals
    toObject: { virtuals: true },
  }
);

/**
 * This virtual returns a list with logs, statuses and steps ordered by _id
 */
JobSchema.virtual("events").get(function () {
  const logs = this.logs.map((log) => {
    return {
      log: log.log,
      _id: log._id,
      createdAt: log.createdAt,
      type: "log",
    };
  });

  const steps = this.steps.map((step) => {
    return {
      name: step.name,
      status: step.status,
      createdAt: step.createdAt,
      _id: step._id,
      type: "step",
    };
  });

  let statuses = this.statuses.map((status) => {
    return {
      name: this.identifier,
      status: status.status,
      createdAt: status.createdAt,
      _id: status._id,
      type: "status",
    };
  });

  statuses = [
    ...statuses,
    {
      name: this.identifier,
      status: "CREATED",
      createdAt: this.createdAt,
      _id: this._id,
      type: "status",
    },
  ];

  return [...logs, ...steps, ...statuses].sort((b, a) => {
    if (a._id < b._id) {
      return -1;
    }
    return 1;
  });
});

module.exports = mongoose.model("Job", JobSchema);
