/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const Address = require("./address.model.js");
const Item = require("./item.model.js");
const TrackingInformation = require("./trackinginformation.model.js");
const { StatusSchema } = require("./status.schema.js");
const { getUserFullNameByName } = require("../cache/cache.js");
const Schema = require("mongoose").Schema;
const ReturnParcelInformation = require("./returnparcelinformation.model.js");

const ParcelSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    shipmentId: { type: Schema.Types.ObjectId },
    /** Investigation identifier indicating which investigation this event belongs to.*/
    investigationId: { type: Number, required: true },
    /** Name of the investigation */
    description: { type: String },
    investigationName: { type: String, required: true },
    containsDangerousGoods: { type: Boolean },
    type: {
      type: String,
      enum: ["DEFAULT"],
      default: "DEFAULT",
    },
    currentStatus: { type: String },
    statuses: [StatusSchema],
    returnAddress: Address.schema,
    shippingAddress: Address.schema,
    /* List of localContacts stored as User.name */
    localContactNames: [{ type: String }],
    items: [{ type: Schema.Types.ObjectId, ref: "Item" }],
    /* content is similar to items but objects are embeded */
    content: [{ type: Item.schema }],
    comments: { type: String },
    storageConditions: { type: String },
    isReimbursed: { type: Boolean, default: false },
    reimbursedCustomsValue: {
      type: Number,
      default: 50,
      required() {
        return this.isReimbursed === true;
      },
    },
    returnParcelInformation: { type: ReturnParcelInformation.schema, default: () => ({ returnType: "USER_MANAGE_PAPERS" }) },
    returnInstructions: { type: String },
    shippingTrackingInformation: TrackingInformation.schema,
    returnTrackingInformation: TrackingInformation.schema,
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
  }
);

ParcelSchema.virtual("status").get(function () {
  const statusSortedByCreationTime = this.statuses.sort((a, b) => a._id - b._id);
  if (statusSortedByCreationTime.length > 0) {
    return statusSortedByCreationTime[statusSortedByCreationTime.length - 1].status;
  }
  return "Not available";
});

ParcelSchema.virtual("investigation").get(function () {
  if (global.investigationsKeys) return global.investigationsKeys[this.investigationId];
});

ParcelSchema.virtual("localContactFullnames").get(function () {
  return this.localContactNames ? this.localContactNames.map((l) => getUserFullNameByName(l)) : [];
});

ParcelSchema.methods.getContainers = function () {
  return this.content.filter((item) => item.type === "CONTAINER");
};

/**
 * Returns the sample sheets that are in a parcel. It includes within any container and at the root level.
 * @param {*} parcel
 */
ParcelSchema.methods.getSampleSheets = function () {
  let samples = this.content.filter((item) => item.type === "SAMPLESHEET");
  const containers = this.getContainers();
  containers.forEach((container) => {
    samples = [...samples, ...container.content.filter((item) => item.type === "SAMPLESHEET")];
  });
  return samples;
};

/**
 * Returns the sampleIds of all items contained in the parcels
 * @param {*} parcel
 */
ParcelSchema.methods.getSampleIds = function () {
  return this.getSampleSheets().map((item) => item.sampleId);
};

ParcelSchema.methods.getItemParameterKeys = function (plans) {
  if (plans && plans.length && plans[0] !== undefined) {
    const keys = [];
    plans.forEach((plan) => {
      keys.push(plan.map((p) => p.key));
    });
    return keys.flat();
  }
  return [];
};
/**
 * Returns all the keys for any defined experimentPlan parameter
 * @param {*} parcel
 */
ParcelSchema.methods.getExperimentPlanKeys = function () {
  return this.getItemParameterKeys(this.getSampleSheets().map((item) => item.experimentPlan));
};

/**
 * Returns all the keys for any defined processingPlan parameter
 * @param {*} parcel
 */
ParcelSchema.methods.getProcessingPlanKeys = function () {
  return this.getItemParameterKeys(this.getSampleSheets().map((item) => item.processingPlan));
};

module.exports = mongoose.model("Parcel", ParcelSchema);
