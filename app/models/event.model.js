/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const Schema = require("mongoose").Schema;

/** Event.content schema */
const Content = new Schema({
  /** Format of the content. Possible values: [plainText, html] */
  format: String,
  /** Text of the content. */
  text: String,
});

/** Event schema */
const EventSchema = mongoose.Schema(
  {
    /** Investigation identifier indicating which investigation this event belongs to.*/
    investigationId: Number,
    /** Name of the investigation */
    investigationName: String,
    /** Dataset identifier this event belongs to. Only relevant for logbook associated to a dataset  */
    datasetId: Number,
    /** Name of the dataset */
    datasetName: String,
    /** Type of the event. Possible values: ['annotation', notification', 'attachment', 'broadcast'] */
    type: {
      type: String,
      required: true,
      lowercase: true,
      enum: ["annotation", "notification", "attachment", "broadcast"],
    },
    /** List of TagIds associated to this event */
    tag: [{ type: Schema.Types.ObjectId, ref: "Tag" }],
    /** Title of this event */
    title: String,
    /** Category of this event. Possible values: ['commandLine', 'comment', 'debug', 'error', 'info', 'file'] */
    category: {
      type: String,
      required: true,
      lowercase: true,
      enum: ["commandline", "comment", "debug", "error", "info", "file"],
    },
    /** NOT USED */
    filepath: String,
    /** Name of the file when the event contains a file */
    filename: String,
    /** Size of the file when the event contains a file */
    fileSize: Number,
    /** User name who created or updated this event */
    username: { type: String },
    /** Textual content of this event */
    content: [Content],
    /** Beamline where this event was generated */
    instrumentName: String, //{ type: String, uppercase: true },
    /** Program from where this event comes from */
    software: { type: String, uppercase: true },
    /** Machine running the program from where this event comes from */
    machine: String,
    /** Source from where this event comes from */
    source: String,
    /** Previous event version. This entry constitutes the event history. */
    previousVersionEvent: Object,
    /** Event marked as removed and will not be retrieved */
    removed: {
      type: Boolean,
      default: false,
    },
    /** TimeStamp when the event was submitted to ICAT+ */
    creationDate: {
      type: Date,
      required: true,
      default: Date.now,
    },
    /** Updated file when the event contains a file */
    file: [{ type: Schema.Types.ObjectId }],
    /** File content type when the event contains a file */
    contentType: String,
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

/**
 * This hooks normalizes each event and it runs before the save is executed
 * It basically sets a creationDate
 */
EventSchema.pre("save", function (next) {
  try {
    if (!this.creationDate) {
      this.creationDate = new Date();
    }
  } catch (e) {
    global.gLogger.error("Error during pre-save event", { e });
  }
  next();
});

/**
 * For the records, this could eventually be done however it will cause that removed events can not be restored as will never be retrieved from DB

EventSchema.pre("find", function () {
  if (this.getFilter() && this.getFilter().$and) {
    this.getFilter().$and.push({ $or: [{ removed: { $exists: false } }, { removed: false }] });
  }
});
*/

module.exports = mongoose.model("Event", EventSchema);
