/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const Resourceinformation = require("./resourceinformation.model.js");

/** Sample information schema */
const SampleInformationSchema = mongoose.Schema(
  {
    investigationId: { type: Number, required: true },
    sampleId: { type: Number, required: true },
    resources: [{ type: Resourceinformation.schema }],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("SampleInformation", SampleInformationSchema);
