/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

/** Sample information schema */
const TrackingInformationSchema = mongoose.Schema(
  {
    trackingNumber: String,
    forwarder: String,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("TrackingInformation", TrackingInformationSchema);
