/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

const LogbookFilters = new mongoose.Schema({
  sortBy: { type: String },
  sortOrder: { type: Number },
  types: { type: String },
});

const Target = new mongoose.Schema({
  instrumentName: { type: String },
  investigationName: { type: String },
  investigationStartDate: { type: String },
  investigationEndDate: { type: String },
});

const RecentlyVisited = new mongoose.Schema({
  url: { type: String, required: true },
  label: { type: String, required: true },
  target: { type: Target, required: true },
});

const MXShell = new mongoose.Schema({
  inner: { type: Number },
  outer: { type: Number },
  overall: { type: Number },
});

const MXCutoff = new mongoose.Schema({
  completeness: { type: MXShell },
  resolution_limit_low: { type: MXShell },
  resolution_limit_high: { type: MXShell },
  r_meas_all_IPlus_IMinus: { type: MXShell },
  mean_I_over_sigI: { type: MXShell },
  cc_half: { type: MXShell },
  cc_ano: { type: MXShell },
});

const MXSettings = new mongoose.Schema({
  autoProcessingRankingShell: { type: String, required: true },
  autoProcessingRankingParameter: { type: String, required: true },
  autoProcessingRankingDisableSpaceGroup: { type: Boolean },
  scanTypeFilters: { type: [String], required: true },
  cutoffs: { type: MXCutoff },
});

const PageSize = new mongoose.Schema({
  key: { type: String, required: true },
  value: { type: Number, required: true },
});

/** User Settings schema */
const UserPreferencesSchema = mongoose.Schema(
  {
    username: { type: String, required: true },
    selection: {
      datasetIds: { type: [Number], default: [] },
      sampleIds: { type: [Number], default: [] },
    },
    logbookFilters: { type: LogbookFilters },
    recentlyVisited: { type: [RecentlyVisited], default: [] },
    mxsettings: { type: MXSettings },
    isGroupedBySample: { type: Boolean },
    pagination: [PageSize],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("UserPreferences", UserPreferencesSchema);
