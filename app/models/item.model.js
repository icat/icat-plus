const mongoose = require("mongoose");

const ContainerType = new mongoose.Schema(
  {
    name: { type: String, required: true },
    capacity: { type: Number, required: true },
  },
  {
    _id: false,
  }
);

const ParameterType = new mongoose.Schema({
  key: { type: String, required: true },
  value: { type: mongoose.Schema.Types.Mixed, required: true },
});

/**
 * Need to make the field containerType as required if the type of item is CONTAINER
 * @returns True if the type of Item is container
 */
function isContainer() {
  return ["CONTAINER"].indexOf(this.type) > -1;
}

const ItemSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    description: { type: String },
    comments: { type: String },
    sampleId: { type: Number },
    sampleName: { type: String }, // It is the name of the sample when type=SAMPLESHEET
    type: {
      type: String,
      required: true,
      enum: ["SAMPLESHEET", "TOOL", "OTHER", "CONTAINER"],
    },
    sampleChangerLocation: { type: Number },
    containerType: { type: ContainerType, required: isContainer },
    sampleContainerPosition: { type: String },
    content: [this],
    experimentPlan: [ParameterType],
    processingPlan: [ParameterType],
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Item", ItemSchema);
