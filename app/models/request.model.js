const mongoose = require("mongoose");

/** Request schema used for profiling the requests */
const RequestSchema = mongoose.Schema(
  {
    isAdministrator: Boolean,
    isInstrumentScientist: Boolean,
    username: String,
    method: String,
    investigationIds: String,
    sampleId: String,
    nested: String,
    datasetType: String,
    investigationId: String,
    duration: Number,
    url: String,
    endpoint: String,
    sessionId: String,
    hour: {
      type: Number,
      required: true,
      default: new Date().getHours(),
    },
    day: {
      type: Number,
      required: true,
      default: new Date().getDay(),
    },
    month: {
      type: Number,
      required: true,
      default: new Date().getMonth(),
    },
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

/**
 * This hooks extracts the endpoint
 */
RequestSchema.pre("save", function (next) {
  try {
    if (this.url) {
      this.endpoint = this.url.split("?")[0].replace(this.sessionId, "");
    }
  } catch (e) {
    global.gLogger.error("Error during pre-save event", { e });
  }
  next();
});

module.exports = mongoose.model("Request", RequestSchema);
