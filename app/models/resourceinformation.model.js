/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");
const Schema = require("mongoose").Schema;

/** Resource information schema */
const ResourceInformationSchema = mongoose.Schema(
  {
    filename: { type: String, required: true },
    file: { type: Schema.Types.ObjectId },
    fileType: { type: String },
    groupName: { type: String },
    multiplicity: { type: Number },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("ResourceInformation", ResourceInformationSchema);
