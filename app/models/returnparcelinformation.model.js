/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require("mongoose");

/** Resource information schema */
const ReturnParcelInformationSchema = mongoose.Schema(
  {
    returnType: {
      type: String,
      enum: ["USER_MANAGE_PAPERS", "USER_ORGANIZE_RETURN"],
      required: true,
      default: "USER_MANAGE_PAPERS",
    },
    forwarderName: {
      type: String,
      required() {
        return this.returnType === "USER_ORGANIZE_RETURN";
      },
    },
    forwarderAccount: {
      type: String,
    },
    plannedPickupDate: {
      type: Date,
      required() {
        return this.returnType === "USER_ORGANIZE_RETURN";
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("ReturnParcelInformation", ReturnParcelInformationSchema);
