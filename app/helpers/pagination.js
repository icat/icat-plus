/**
 * This function will fill the metadata information
 */
exports.setMetaDataTo = (entities, total, limit, skip) => {
  const totalPages = Math.ceil(total / limit);
  const currentPage = Math.ceil(skip / limit) + 1;

  entities.forEach((i) => {
    i.meta = {
      page: {
        total,
        totalPages,
        currentPage,
      },
    };
  });
};
