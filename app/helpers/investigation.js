exports.normalize = (investigationName) => {
  /** Get the proposal type */
  const investigationTypes = global.appCache.get("investigationTypes"); //No idea why it does not recognize this function getInvestigationTypes();
  let normalizedInvestigationType = null;
  let normalizedInvestigationNumber = null;

  for (const element of investigationTypes) {
    const investigationTypeName = element.InvestigationType.name;
    /** replacing - */

    const investigationNameClear = investigationTypeName.replace(new RegExp("-", "g"), "").toUpperCase();
    investigationName = investigationName.replace(new RegExp("-", "g"), "").toUpperCase();

    if (investigationName.toUpperCase().startsWith(investigationNameClear)) {
      normalizedInvestigationType = investigationTypeName;
      normalizedInvestigationNumber = investigationName.toUpperCase().replace(investigationNameClear, "").replace(new RegExp("-", "g"), "");
    }
  }
  if (normalizedInvestigationType == null || normalizedInvestigationNumber == null) {
    global.gLogger.error("Investigation name can not be normalized. ", {
      investigationName,
      normalizedInvestigationType,
      normalizedInvestigationNumber,
    });
    throw new Error(`Investigation ${investigationName} name can not be normalized`);
  }

  /** ID are testing investigations and does not follow same format **/
  if (normalizedInvestigationType.toUpperCase().startsWith("ID")) {
    return `${normalizedInvestigationType}${normalizedInvestigationNumber}`;
  }

  /** ID are testing investigations and does not follow same format **/
  if (normalizedInvestigationType.toUpperCase().startsWith("BM")) {
    return investigationName;
  }

  return `${normalizedInvestigationType}-${normalizedInvestigationNumber}`;
};

/**
 * Returns true if the given investigation is under embargo, false otherwise
 * It checks the release date of the investigation
 * @param {*} investigation
 * @returns
 */
exports.isInvestigationUnderEmbargo = (investigation) => {
  if (!investigation || !investigation.releaseDate) {
    return true;
  }
  if (new Date(investigation.releaseDate) < new Date()) {
    return false;
  }
  return true;
};
