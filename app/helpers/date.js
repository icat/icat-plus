const { isValid, format } = require("date-fns");

/**
 * Returns the time 'YYYY-MM-DDTHH:mm:ss' formatted if valid, defaultFormattedTime otherwise
 * @param {*} time
 * @param {*} defaultFormattedTime
 * @returns
 */
exports.getFormattedTimeOrDefault = (time, defaultFormattedTime) => {
  const outputFormat = "yyyy-MM-dd HH:mm:ss";
  return isValid(time) ? format(time, outputFormat) : defaultFormattedTime;
};
