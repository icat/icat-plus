const ERROR = {
  NO_SESSION_REFRESH_ALLOWED: {
    description: "The server does not support the functionality required to fulfill the request",
    code: 501,
    message: "This server does not allow to refresh the token",
  },
  NOT_AUTHENTICATED_BAD_PARAMS: {
    description: "Request is not authenticated.",
    code: 400,
    message: "Request is not authenticated. No sessionId found.",
  },
  NO_SESSION: {
    description: "No session found.",
    code: 400,
    message: "Session is not found for this sessionId.",
  },
  NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR: {
    description: "User is not participant or instrument scientist or administrator given one investigation.",
    code: 403,
    message: "No privileges to access to this resource",
  },
  NO_PARTICIPANT: {
    description: "User is not participant given one investigation",
    code: 403,
    message: "No privileges to access to this resource",
  },
  NO_GRANT_INVESTIGATION_MANAGER: {
    description: "User is not granted to manage investigation users.",
    code: 403,
    message: "No privileges to access to this resource",
  },
  NO_PRINCIPAL_INVESTIGATOR: {
    description: "User is not principal investigator.",
    code: 403,
    message: "No privileges to access to this resource",
  },
  NO_INVESTIGATION_BAD_PARAMS: {
    description: "No investigationId found in the request",
    code: 400,
    message: "Bad parameters. InvestigationId no found",
  },
  NO_INSTRUMENTNAME_BAD_PARAMS: {
    description: "No instrument name found in the request",
    code: 400,
    message: "Bad parameters. Instrument name no found",
  },
  WRONG_INSTRUMENTNAME_BAD_PARAMS: {
    description: "No instrument name found in the DATABASE",
    code: 400,
    message: "Bad parameters. Instrument name no found",
  },
  BAD_PARAMS: {
    description: "Bad parameters",
    code: 400,
    message: "Bad parameters. ",
  },
  NO_USERS_BAD_PARAMS: {
    description: "No users found in the request",
    code: 400,
    message: "Bad parameters. Users no found",
  },
  NO_BASE64_BAD_PARAMS: {
    description: "No base64 found in the request",
    code: 400,
    message: "Bad parameters. base64 no found",
  },
  NO_INVESTIGATION_FOUND: {
    description: "No investigation was found",
    code: 400,
    message: "No investigation found",
  },
  NO_PARAMETER_BAD_PARAMS: {
    description: "No parameter name or value found",
    code: 400,
    message: "Bad parameters. No parameter found or incorrect value",
  },
  NO_DATASETIDS_PARAM_FOUND: {
    description: "No datasetIds parameter found",
    code: 400,
    message: "Bad parameters. No datasetIds found or incorrect value",
  },
  NO_DATASETS_FOUND: {
    description: "No datasets found",
    code: 400,
    message: "No datasets found or permissions invalid",
  },
  NO_EWOKS_IDENTIFIER: {
    description: "No idenfitier parameter found",
    code: 400,
    message: "Bad parameters. No idenfitier parameter found",
  },
  NO_EWOKS_QUEUE: {
    description: "No queue found",
    code: 400,
    message: "Bad queue. No queue found",
  },
  NO_PERMISSIONS_INVESTIGATION: {
    description: "User is not allowed to access to the investigation",
    code: 403.25,
    message: "You are not allowed to access to the investigation",
  },
  INCORRECT_API_KEY: {
    description: "API key is incorrect",
    code: 403,
    message: "API key is not valid",
  },
  NO_ELASTIC_SEARCH: {
    description: "Elastic search is disabled in this server",
    code: 500,
    message: "Elastic search is disabled",
  },
  NO_EWOKS_ENABLED: {
    description: "Ewoks is disabled in this server",
    code: 500,
    message: "Ewoks is disabled",
  },
  NO_EWOKS_DATASETS: {
    description: "Bad parameters. No datasetIds",
    code: 500,
    message: "Bad parameters. No datasetIds",
  },
  NO_ADMINISTRATOR_PRIVILEGES: {
    description: "Administrator privileges are required",
    code: 403,
    message: "Administrator privileges required",
  },
  NO_ALLOWED_TAG_CREATION: {
    description: "Not allowed to create tags",
    code: 403,
    message: "Not allowed to create tags",
  },
  INVALID_TAG_ID: {
    description: "Tag ID parameter is invalid or empty",
    code: 400,
    message: "Tag ID parameter is invalid",
  },
  DUPLICATED_TAG_NAME: {
    description: "Tag name is duplicated within the investigation",
    code: 400,
    message: "Duplicated tag name",
  },
  NO_ADMINISTRATOR_OR_INSTRUMENTSCIENTIST: {
    description: "User is not administrator nor intrument scientist",
    code: 403,
    message: "User is not administrator nor intrument scientist",
  },
  FAILED_RETRIEVE_INSTRUMENT_SCIENTISTS_BY_USERNAME: {
    description: "Failed to retrieve the instruments scientists",
    code: 500,
    message: "There was an error retrieving the instrument scientists",
  },
  LOGIN_FAILED: {
    description: "Authentication failed",
    code: 403,
    message: "Authentication failed",
  },
  FAILED_TO_RETRIEVE_INVESTIGATIONUSERS: {
    description: "Failed to retrieve investigation users",
    code: 500,
    message: "Failed to retrieve investigation users",
  },
  FAILED_TO_RETRIEVE_INSTRUMENTS: {
    description: "Failed to retrieve instruments",
    code: 500,
    message: "Failed to retrieve instruments",
  },
  INVESTIGATIONID_DOES_NOT_MATCH: {
    description: "Expecting matching between investigation Id parameters not found",
    code: 400,
    message: "Expecting matching between investigation Id parameters not found",
  },
  SHIPMENTID_DOES_NOT_MATCH: {
    description: "Expecting matching between shipment Id parameters not found",
    code: 400,
    message: "Expecting matching between shipment Id parameters not found",
  },
  PARCEL_FAILED_TO_CREATE: {
    description: "Parcel could not be created",
    code: 500,
    message: "Parcel could not be created",
  },
  PARCEL_FAILED_TO_UPDATE: {
    description: "Parcel could not be updated",
    code: 500,
    message: "Parcel could not be updated",
  },
  PARCEL_FAILED_TO_DELETE: {
    description: "Parcel could not be deleted",
    code: 500,
    message: "Parcel could not be deleted",
  },
  PARCEL_FAILED_TO_SET_STATUS: {
    description: "Parcel could not be changed the status",
    code: 500,
    message: "Parcel could not be changed the status",
  },
  PARCEL_FAILED_TO_BE_TRANSFERED: {
    description: "Parcel could not be transferred to other investigation",
    code: 500,
    message: "Parcel could not be transferred to other investigation",
  },
  NO_SHIPMENT_FOUND: {
    description: "Shipment not found",
    code: 500,
    message: "Shipment not found",
  },
  MAX_ENTITIES_EXCEEDED: {
    description: "Max number of entitites to be updated are exceeded",
    code: 500,
    message: "Max number of entitites to be updated are exceeded",
  },
  NO_DATAFILEID: {
    description: "No datafileId parameter found in query",
    code: 400,
    message: "No datafileId parameter in query",
  },
  NO_DATAFILE: {
    description: "No datafile found or no permissions",
    code: 404,
    message: "No datafile found or no permissions",
  },
  INVALID_EVENT_TYPE: {
    description: "Event type is invalid",
    code: 400,
    message: "Event type is invalid",
  },
  DMP_DISABLED: {
    description: "DMP is disabled",
    code: 500,
    message: "DMP is disabled",
  },
  FAILED_TO_RETRIEVE_DMP: {
    description: "Failed to retrieve dmp questionnaires",
    code: 500,
    message: "Failed to retrieve dmp questionnaires",
  },
  PARAMETER_NOT_EDITABLE: {
    description: "The parameter cannot be editable",
    code: 400,
    message: "The parameter cannot be editable",
  },
  NO_SAMPLE_FOUND: {
    description: "No sample was found",
    code: 400,
    message: "No sample found",
  },
  NO_ALLOWED_EDIT_EVENT: {
    description: "The event cannot be edited",
    code: 403,
    message: "Event cannot be edited",
  },
  FILE_NOT_FOUND: {
    description: "File not found",
    code: 404,
    message: "File not found ",
  },
  NO_ALLOWED_EDIT_EMBARGOED: {
    description: "The entity cannot be edited (embargoed investigation)",
    code: 403,
    message: "Entity cannot be edited",
  },
  FAILED_TO_RETRIEVE_TECHNIQUES: {
    description: "Failed to retrieve techniques",
    code: 500,
    message: "Failed to retrieve techniques",
  },
  NO_SAMPLE_BAD_PARAMS: {
    description: "No sampleId found in the request",
    code: 400,
    message: "Bad parameters. SampleId no found",
  },
  SAMPLE_NOT_FILE_ASSOCIATED: {
    description: "No file can be associated to this sample",
    code: 400,
    message: "No file can be associated to this sample",
  },
  SAMPLE_FILE_ALREADY_EXIST: {
    description: "A file already exists for this sample with the same name",
    code: 400,
    message: "A file already exists for this sample with the same name",
  },
};

/** Error can be a error from the dictionary or a native error */
function CustomError(error) {
  if (error.code) {
    this.code = error.code;
  }
  if (error.description) {
    this.description = error.description;
  }
  if (error.message) {
    this.message = error.message;
  }

  if (!this.code) {
    // Native error that is an string
    this.code = 500;
    this.message = error.message;
  }
}

function sendError(code, message, e, res) {
  global.gLogger.error(e);
  global.gLogger.error(message, {
    error: e.message,
  });
  res.status(code).send({
    message: message || e.message,
  });
}

module.exports.sendError = sendError;
module.exports.ERROR = ERROR;
module.exports.CustomError = CustomError;
