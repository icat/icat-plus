module.exports = {
  xml: `<?xml version="1.0" encoding="UTF-8"?>
<resource xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://datacite.org/schema/kernel-4" xsi:schemaLocation="http://schema.datacite.org/meta/kernel-4/metadata.xsd">
	<identifier identifierType="DOI">%DOI%</identifier>
	<creators>
		%AUTHORS%
	</creators>
	<titles>
		<title>%TITLE%</title>
	</titles>
	<publisher>European Synchrotron Radiation Facility</publisher>
	<publicationYear>%PUBLICATION_YEAR%</publicationYear>
	<resourceType resourceTypeGeneral="Dataset">Datacollection</resourceType>
	<subjects>
    	<subject subjectScheme="Proposal">%INVESTIGATION_NAME%</subject>
    	<subject subjectScheme="Instrument">%INSTRUMENT_NAME%</subject>
      %KEYWORDS%
      %TECHNIQUES%
	</subjects>
  <dates>
    <date dateType="Issued">%RELEASE_YEAR%</date>
  </dates>
	<language>en</language>
	<version>1</version>
	<descriptions>
		<description descriptionType="Abstract">%ABSTRACT%</description>
	</descriptions>
  %RELATEDIDENTIFIERS%
</resource>`,
};
