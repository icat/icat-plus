const { InvestigationAdapter, asyncGetInvestigationUserBySessionId, getInstrumentScientistsByUserName, DatasetAdapter, SampleAdapter } = require("../api/icat.js");

const { ERROR } = require("../errors.js");
const {
  hasPermissionOnInvestigation,
  getInvestigationIdFromRequest,
  getSampleIdFromRequest,
  getSessionIdFromRequest,
  getApiKeyFromRequest,
  isInvestigationUnderEmbargo,
  getSession,
  getAuthenticator,
  setAuthenticator,
  allowsAdministratorOrInstrumentScientistOrByRoles,
} = require("./helper.authentication.js");

const CONSTANTS = require("../constants.js");

/**
 * Returns true if the user is participant of the investigation
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
const isParticipant = async (sessionId, investigationId) => {
  const investigationUsers = await asyncGetInvestigationUserBySessionId(sessionId);
  return investigationUsers.filter((investigationUser) => investigationUser.investigationId === Number(investigationId)).length > 0;
};
/**
 *
/**
 * It allows if  elastic search is enabled
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.isElasticSearchEnabled = (req, res, next) => {
  if (global.gServerConfig.elasticsearch.enabled) {
    return next();
  }
  return res.status(ERROR.NO_ELASTIC_SEARCH.code).send(ERROR.NO_ELASTIC_SEARCH.message);
};

/**
 * It allows if  ewoks is enabled
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.isEwoksEnabled = (req, res, next) => {
  if (global.gServerConfig.ewoks.enabled) {
    return next();
  }
  return res.status(ERROR.NO_EWOKS_ENABLED.code).send(ERROR.NO_EWOKS_ENABLED.message);
};

/**
 * Specif auth middleware to allow or deny launching ewoks jobs
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowLaunchJob = async (req, res, next) => {
  const { datasetIds } = req.query;
  const session = await getSession(req);

  const { sessionId, isAdministrator, username, isInstrumentScientist } = session;
  global.gLogger.debug("allowLaunchJob", { datasetIds, username });
  if (!datasetIds) {
    global.gLogger.error("No datasets in input job for ewoks", { datasetIds });
    return res.status(ERROR.NO_EWOKS_DATASETS.code).send(ERROR.NO_EWOKS_DATASETS.message);
  }

  /** Administrator and can always launch jobs */
  if (isAdministrator || isInstrumentScientist) {
    return next();
  }
  global.gLogger.debug("getDatasets", { datasetIds });
  const datasets = await DatasetAdapter.getDatasetsBy(sessionId, { datasetId: datasetIds });

  if (datasets.length === 0) {
    global.gLogger.error("No datasets found in database for ewoks", { datasetIds });
    return res.status(ERROR.NO_PERMISSIONS_INVESTIGATION.code).send(ERROR.NO_PERMISSIONS_INVESTIGATION.message);
  }
  const investigationIds = datasets.map((dataset) => dataset.investigation.id);

  const isAllowed = await Promise.all(investigationIds.map((investigationId) => isParticipant(sessionId, investigationId)));
  global.gLogger.debug("isAllowed", { isAllowed, investigationIds, datasetIds });
  /** if allowed for all investigation then is ok */
  if (isAllowed.filter((permitted) => permitted === false).length === 0) {
    return next();
  }
  return res.status(ERROR.NO_PERMISSIONS_INVESTIGATION.code).send(ERROR.NO_PERMISSIONS_INVESTIGATION.message);
};

/**
 * It allows only to administrators
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowAdministrators = async (req, res, next) => {
  const session = await getSession(req);
  if (session.isAdministrator) {
    return next();
  }
  return res.status(ERROR.NO_ADMINISTRATOR_PRIVILEGES.code).send(ERROR.NO_ADMINISTRATOR_PRIVILEGES.message);
};

exports.allowsGrantInvestigationAccessManager = async (req, res, next) => {
  return allowsAdministratorOrInstrumentScientistOrByRoles(req, res, next, [CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR, CONSTANTS.ROLE_LOCAL_CONTACT]);
};

/**
 * allows if there is an investigation which user is participant or investigation is not under embargo anymore
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser = async (req, res, next) => {
  const authenticator = "allowsInvestigationAfterEmbargoOrAllowsInvestigationUser";
  const sessionId = getSessionIdFromRequest(req);
  const investigationId = getInvestigationIdFromRequest(req);
  global.gLogger.warn("allowsInvestigationAfterEmbargoOrAllowsInvestigationUser", { investigationId, sessionId });

  const session = await getSession(req);

  if (getAuthenticator(sessionId, authenticator, investigationId)) {
    return next();
  }
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  global.gLogger.warn("No investigation allowsInvestigationAfterEmbargoOrAllowsInvestigationUser");
  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  if (session.isAdministrator) {
    setAuthenticator(sessionId, authenticator, investigationId);
    return next();
  }

  try {
    const isEmbargoed = await isInvestigationUnderEmbargo(sessionId, investigationId);
    global.gLogger.debug("Is under embargo", { isEmbargoed, investigationId });
    if (isEmbargoed) {
      /** This happens when investigation is under embargo then it allows the access if and only if user is a participant (InvestigationUser)   */
      return this.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
    }
    setAuthenticator(sessionId, authenticator, investigationId);
    return next();
  } catch (error) {
    res.status(error.code).send(error.message);
  }
};

exports.allowsEmbargoedInvestigationUserOrAdministratorOrInstrumentScientists = async (req, res, next) => {
  try {
    global.gLogger.debug("allowsEmbargoedInvestigationUserOrAdministratorOrInstrumentScientists");

    const sessionId = getSessionIdFromRequest(req);
    const investigationId = getInvestigationIdFromRequest(req);

    if (!sessionId || !investigationId) {
      return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
    }
    const isEmbargoed = await isInvestigationUnderEmbargo(sessionId, investigationId);

    if (!isEmbargoed) {
      return res.status(ERROR.NO_ALLOWED_EDIT_EMBARGOED.code).send({ message: ERROR.NO_ALLOWED_EDIT_EMBARGOED.message });
    }
    return this.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
  } catch (e) {
    global.gLogger.error(e);
    return res.status(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.code).send(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.message);
  }
};

const canUserAccessInvestigation = async (sessionId, investigationId, username) => {
  const investigationUsers = await asyncGetInvestigationUserBySessionId(sessionId);
  const isInvestigationUser = investigationUsers.find((user) => user.investigationId === Number(investigationId));

  /** Check if user is participant of the investigations **/
  if (isInvestigationUser) {
    global.gLogger.debug("User is a investigationUser of the investigation", {
      investigationId,
      username,
    });
    return true;
  }

  /** If not participant it could be instrumentScientist */
  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { username, investigationId, filter: "INSTRUMENTSCIENTIST" });
  if (investigations && investigations.length && investigations.length > 0) {
    global.gLogger.debug("User is a instrumentScientist of the investigation", {
      investigations,
      investigationId,
      username,
    });
    return true;
  }
  return false;
};

exports.allowsInvestigationUserOrAdministratorsOrInstrumentScientistsForSample = async (req, res, next) => {
  try {
    const authenticator = "allowsInvestigationUserOrAdministratorsOrInstrumentScientistsForSample";
    const sessionId = getSessionIdFromRequest(req);
    const sampleId = getSampleIdFromRequest(req);

    if (!sessionId) {
      return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
    }

    if (!sampleId) {
      return res.status(ERROR.NO_SAMPLE_BAD_PARAMS.code).send(ERROR.NO_SAMPLE_BAD_PARAMS.message);
    }

    const { isAdministrator, username } = await getSession(req);

    if (getAuthenticator(sessionId, authenticator, sampleId)) {
      return next();
    }

    if (isAdministrator) {
      setAuthenticator(sessionId, authenticator, sampleId);
      return next();
    }

    const samples = await SampleAdapter.getSamplesBy(sessionId, { sampleIds: sampleId });
    if (samples && samples.length > 0) {
      const investigationId = samples[0].investigation.id;
      const canUserAccessInv = await canUserAccessInvestigation(sessionId, investigationId, username);
      if (canUserAccessInv) {
        setAuthenticator(sessionId, authenticator, investigationId);
        return next();
      }
    }
  } catch (e) {
    global.gLogger.error(e);
  }

  return res.status(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.code).send(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.message);
};

/*
 * Permission validator by investigationId: it allows the following 'person' (identified by its sessionId) to access the resource associated to a given investigationId:
 * - administrators
 * - investigationUsers , ie participants of the investigation indentified by investigationId
 * - instrumentScientist
 * @param {Request} req http request. It should contain sessionId and investigationId
 * @param {Response} res http response
 * @param {callback} next the next function
 */
exports.allowsInvestigationUserOrAdministratorsOrInstrumentScientists = async (req, res, next) => {
  try {
    const authenticator = "allowsInvestigationUserOrAdministratorsOrInstrumentScientists";
    const sessionId = getSessionIdFromRequest(req);
    const investigationId = getInvestigationIdFromRequest(req);

    if (!sessionId) {
      return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
    }

    if (!investigationId) {
      return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
    }

    const { isAdministrator, username } = await getSession(req);

    if (getAuthenticator(sessionId, authenticator, investigationId)) {
      return next();
    }

    if (isAdministrator) {
      setAuthenticator(sessionId, authenticator, investigationId);
      return next();
    }

    const canUserAccessInv = await canUserAccessInvestigation(sessionId, investigationId, username);
    if (canUserAccessInv) {
      setAuthenticator(sessionId, authenticator, investigationId);
      return next();
    }
  } catch (e) {
    global.gLogger.error(e);
  }

  return res.status(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.code).send(ERROR.NO_PARTICIPANT_SCIENTIST_OR_ADMINISTRATOR.message);
};

/**
 * It allows to instrumentScientist and administrator
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowsInstrumentScientistsOrAdministrators = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  const { isAdministrator, username } = await getSession(req);

  if (isAdministrator) {
    return next();
  }

  const instrumentScientists = await getInstrumentScientistsByUserName(sessionId, username);
  if (instrumentScientists && instrumentScientists.length && instrumentScientists.length > 0) {
    return next();
  }
  return res.status(ERROR.NO_ADMINISTRATOR_OR_INSTRUMENTSCIENTIST.code).send(ERROR.NO_ADMINISTRATOR_OR_INSTRUMENTSCIENTIST.message);
};

/**
 *  It checks if the sessionId matches with the API key
 *  In the future it would be recommended to filter by network's IP
 **/
exports.requiresApiKey = (req, res, next) => {
  const apiKey = getApiKeyFromRequest(req);
  if (!apiKey) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }
  if (apiKey === global.gServerConfig.server.API_KEY) {
    return next();
  }
  return res.status(ERROR.INCORRECT_API_KEY.code).send(ERROR.INCORRECT_API_KEY.message);
};

/**
 * It requires to be signed in and a non-expired session found
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.requiresSession = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  const session = await getSession(req);
  if (!session) {
    global.gLogger.error(ERROR.NO_SESSION);
    return res.status(ERROR.NO_SESSION.code).send(ERROR.NO_SESSION.message);
  }
  return next();
};

/**
 * It allows to the participants of the investigation and the administrator
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.requiresInvestigation = async (req, res, next) => {
  const sessionId = getSessionIdFromRequest(req);
  const investigationId = getInvestigationIdFromRequest(req);

  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }

  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  const { isAdministrator } = await getSession(req);

  if (isAdministrator) {
    return next();
  }

  if (hasPermissionOnInvestigation(sessionId, investigationId)) {
    return next();
  }
  return res.status(ERROR.NO_PERMISSIONS_INVESTIGATION.code).send(ERROR.NO_PERMISSIONS_INVESTIGATION.message);
};

/**
 * Allows access to instrumentScientist and Administrator if the instrumentName is filled, or it allows access to user/InstrumentScientist/Admin for a given investigationId
 * This allows to implement allowing the access to beamline scientist to the logbook of the beamline
 * @param {*} req query contains either investigationId or instrumentName
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.allowAccessToInstrumentScientistOrUsers = async (req, res, next) => {
  if (!req.query.investigationId && req.query.instrumentName) {
    return this.allowsInstrumentScientistsOrAdministrators(req, res, next);
  }
  return this.allowsInvestigationUserOrAdministratorsOrInstrumentScientists(req, res, next);
};

/**
 *  It checks if the sessionId matches with the API key
 **/
exports.isValidApiKey = (req) => {
  const sessionId = getSessionIdFromRequest(req);
  return sessionId && sessionId === global.gServerConfig.server.API_KEY;
};
