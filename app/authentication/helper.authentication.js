const { ERROR, CustomError } = require("../errors.js");
const { InvestigationAdapter, getInstrumentScientistsByInstrumentName } = require("../api/icat.js");
const { initSession } = require("../cache/cache.js");
const { isInvestigationUnderEmbargo } = require("../helpers/investigation.js");

/** This property should be optional */
const IN_MEMORY_AUTHENTICATION = true;

/**
 * Gets the basic information needed from the user
 * @param {*} req
 */
exports.getSession = async (req) => {
  req.session = await initSession(this.getSessionIdFromRequest(req));
  if (!req.session) {
    global.gLogger.warn("No session retrieved");
  }
  return req.session;
};

/** Checks in the memory if the method is authorized (true) */
exports.getAuthenticator = (sessionId, method, entity) => {
  if (global.authenticators && global.authenticators[sessionId] && global.authenticators[sessionId][`${method}_${entity}`] === true) {
    global.gLogger.debug("Authorized by using fast authenticator", { sessionId, method, entity });
    return true;
  }
  global.gLogger.debug("No authorized by using fast authenticator", { sessionId, method, entity });
  return false;
};

/** Set to true the authorization for this method and session */
exports.setAuthenticator = (sessionId, method, entity) => {
  global.gLogger.debug("setAuthenticator", { sessionId, method, entity });
  if (IN_MEMORY_AUTHENTICATION) {
    if (!global.authenticators[sessionId]) {
      global.authenticators[sessionId] = {};
      global.authenticators[sessionId][method] = false;
      global.authenticators[sessionId][`${method}_${entity}`] = false;
    }
    global.authenticators[sessionId][`${method}_${entity}`] = true;
  }
};

/**
 * It allows administrator, instrumentScientist or by roles
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @param {*} roles [CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR, CONSTANTS.ROLE_LOCAL_CONTACT]
 * @returns
 */
exports.allowsAdministratorOrInstrumentScientistOrByRoles = async (req, res, next, roles) => {
  const investigationId = this.getInvestigationIdFromRequest(req);
  const sessionId = this.getSessionIdFromRequest(req);
  const { username, isAdministrator } = req.session;
  global.gLogger.debug("allowsAdministratorOrInstrumentScientistOrByRoles", { sessionId, investigationId, roles: JSON.stringify(roles) });
  if (!sessionId) {
    return res.status(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.code).send(ERROR.NOT_AUTHENTICATED_BAD_PARAMS.message);
  }
  if (!investigationId) {
    return res.status(ERROR.NO_INVESTIGATION_BAD_PARAMS.code).send(ERROR.NO_INVESTIGATION_BAD_PARAMS.message);
  }

  // Next if user is administrator
  if (isAdministrator) {
    global.gLogger.debug("User is administrator");
    return next();
  }

  const include =
    "investigation.investigationUsers investigationUsers, investigationUsers.user user, instrument.instrumentScientists as instrumentScientists, instrumentScientists.user scuser";
  const investigations = await InvestigationAdapter.getInvestigationsBy(
    sessionId,
    {
      investigationId: [investigationId],
    },
    include
  );
  if (investigations.length === 0) {
    return res.status(ERROR.NO_INVESTIGATION_FOUND.code).send(ERROR.NO_INVESTIGATION_FOUND.message);
  }
  const investigation = investigations[0];

  // check if user has role
  const { investigationUsers } = investigation;

  const hasRole = investigationUsers.find((investigationUser) => investigationUser.user.name === username && roles.find((role) => role === investigationUser.role));
  if (hasRole) {
    global.gLogger.debug("User has role to manage investigationUsers");
    return next();
  }

  // check instrumentScientists
  const isInstrumentScientist = investigation.instrument.instrumentScientists.map((u) => u.name).filter((u) => u === username).length > 0;
  if (isInstrumentScientist) {
    global.gLogger.debug("User is instrumentScientist of the investigation instrument");
    return next();
  }

  return res.status(ERROR.NO_GRANT_INVESTIGATION_MANAGER.code).send(ERROR.NO_GRANT_INVESTIGATION_MANAGER.message);
};

const getParameterByName = (req, parameterName) => {
  if (req.params) {
    if (req.params[parameterName]) {
      return req.params[parameterName];
    }
  }
  if (req.header) {
    if (req.header[parameterName]) {
      return req.header[parameterName];
    }
  }
  if (req.body) {
    if (req.body[parameterName]) {
      return req.body[parameterName];
    }
  }
  if (req.query) {
    if (req.query[parameterName]) {
      return req.query[parameterName];
    }
  }
  global.gLogger.error(`No ${parameterName} @ header, body or query in ${req.originalUrl}`);
  return null;
};

exports.getInvestigationIdFromRequest = (req) => {
  const param = getParameterByName(req, "investigationId");
  return param ? Number(param) : param;
};

exports.getSampleIdFromRequest = (req) => {
  const param = getParameterByName(req, "sampleId");
  return param ? Number(param) : param;
};

exports.getInstrumentName = (req) => {
  return getParameterByName(req, "instrumentName");
};

exports.getSessionIdFromRequest = (req) => {
  return getParameterByName(req, "sessionId");
};

exports.getApiKeyFromRequest = (req) => {
  return getParameterByName(req, "apiKey");
};

/**
 * Check that the user identified by sessionID has permission to read investigationId
 * @param {string} sessionId sessionId identifying the user
 * @param {string} investigationId investigation identifier
 * @param {function} next express callback function
 */
exports.hasPermissionOnInvestigation = async (sessionId, investigationId) => {
  const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId);
  return investigations.find((element) => {
    return Number(element.id) === Number(investigationId);
  });
};

/** It affects to tags, to be checked later */
exports.allowsInstrumentScientistsOrAdministratorsByInstrumentName = async (req, res, next, username) => {
  const sessionId = this.getSessionIdFromRequest(req);
  const instrumentName = this.getInstrumentName(req).toUpperCase();

  if (sessionId) {
    if (instrumentName) {
      const users = await getInstrumentScientistsByInstrumentName(sessionId, instrumentName);
      global.gLogger.debug("users count who are instrumentUsers or administrator : ", { userCount: users.length, users: JSON.stringify(users) });

      // check whether the current user identified by its sessionId is listed in the response
      const foundUser = users.find((user) => user.User.name === username);
      if (foundUser) {
        return next();
      }
      return res.status(403).send("You are not allowed to create a bemline tag.");
    }
    return res.status(400).send("No Instrument");
  }
  return res.status(403).send("No sessionId");
};

/**
 *  This method checks if the release date is < now
 * @param {*} sessionId
 * @param {*} investigationId
 * @returns
 */
exports.isInvestigationUnderEmbargo = async (sessionId, investigationId) => {
  try {
    const investigations = await InvestigationAdapter.getInvestigationsBy(sessionId, { investigationId });

    if (investigations && investigations.length > 0 && investigations[0]) {
      const investigation = investigations[0];
      return isInvestigationUnderEmbargo(investigation);
    }
    throw ERROR.NO_INVESTIGATION_FOUND;
  } catch (e) {
    global.gLogger.error(e);
    throw new CustomError(e);
  }
};
