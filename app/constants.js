module.exports = Object.freeze({
  ROLE_PRINCIPAL_INVESTIGATOR: "Principal investigator",
  ROLE_LOCAL_CONTACT: "Local contact",
  ROLE_COLLABORATOR: "Collaborator",

  //logbook related constants
  EVENT_TYPE_ANNOTATION: "annotation",
  EVENT_TYPE_NOTIFICATION: "notification",
  EVENT_TYPE_ATTACHMENT: "attachment",
  EVENT_TYPE_BROADCAST: "broadcast",

  EVENT_CONTENT_HTML_FORMAT: "html", //the format which denotes that the associated text is HTML text
  EVENT_CONTENT_PLAINTEXT_FORMAT: "plainText", //the format which denotes that the associated text is plain text

  EVENT_CATEGORY_COMMANDLINE: "commandLine",
  EVENT_CATEGORY_COMMENT: "comment",
  EVENT_CATEGORY_DEBUG: "debug",
  EVENT_CATEGORY_ERROR: "error",
  EVENT_CATEGORY_INFO: "info",
  EVENT_CATEGORY_FILE: "file",

  MARGIN_HOURS: 48,
});
