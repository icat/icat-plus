require("it-each")({ testPerIteration: true });

const datafileResource = require("../resources/datafile.resource.js");
const datafileHelper = require("../helper/datafile.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");
const mock = require("mock-fs");

describe("Datafile", () => {
  afterEach(async () => {
    mock.restore();
  });

  describe("[DEPRECATED] /catalogue/:sessionId/dataset/id/:datasetId/datafile", () => {
    it.each(datafileResource.byDatasets, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, datasetId, limit, skip, search, expected } = element;
        const response = await datafileHelper.getDatafilesByDatasetId(user, datasetId, limit, skip, search);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.an("array").of.length(expected.fileCount);
          if (limit) {
            expect(response.body.length).to.be.at.most(limit);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/datafile", () => {
    it.each(datafileResource.byDatasets, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, datasetId, limit, skip, search, expected } = element;
        const response = await datafileHelper.getDatafilesByDatasetId(user, datasetId, limit, skip, search);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.an("array").of.length(expected.fileCount);
          if (limit) {
            expect(response.body.length).to.be.at.most(limit);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/datafile/download", () => {
    it.each(datafileResource.download, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, datasetId, investigationId, filepath, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        mock({
          "fakeDir/test.png": Buffer.from([8, 6, 7, 5, 3, 0, 9]),
        });
        const response = await datafileHelper.download(sessionId, investigationId, datasetId, filepath);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          expect(response.text).to.be.a("string");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
