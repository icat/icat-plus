require("it-each")({ testPerIteration: true });
const _ = require("lodash");
const eventResource = require("../resources/logbook.resource");
const sessionHelper = require("../helper/session.js");
const logbookHelper = require("../helper/logbook.js");
const { expect } = require("chai");
const { testEvent, doFindEventsDates } = require("../helper/logbook");
const logbookControllerHelper = require("../../app/controllers/helpers/helper.logbook.controller");
const { prepopulateTags } = require("../helper/tag");
const Event = require("../../app/models/event.model.js");
const { dateFormat } = require("../../app/controllers/helpers/helper.controller.js");
const { format, parse, subDays, addDays, isAfter, isBefore } = require("date-fns");

describe("Logbook", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("/logbook/:sessionId/stats/investigation", () => {
    it.each(eventResource.stats, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, startDate, endDate, expected, logbook } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;

        /** Populating events */
        await this.prepopulateEvents(logbook, sessionId);

        const params = new URLSearchParams();
        if (startDate) params.set("startDate", startDate);
        if (endDate) params.set("endDate", endDate);

        const response = await global.gRequester.get(`/logbook/${sessionId}/stats/investigation?${params.toString()}`).send();
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.length).equal(new Set(logbook.map((log) => log.investigationId)).size);
          /** This test could be completed by checking the number of annotations and notifications retrieved for each investigation */
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/logbook/:sessionId/stats/count", () => {
    it.each(eventResource.count, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, startDate, endDate, expected, logbook } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;

        /** Populating events */
        await this.prepopulateEvents(logbook, sessionId);

        const params = new URLSearchParams();
        if (startDate) params.set("startDate", startDate);
        if (endDate) params.set("endDate", endDate);

        const response = await global.gRequester.get(`/logbook/${sessionId}/stats/count?${params.toString()}`).send();
        expect(response.status).to.equal(expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe.skip("/logbook/:sessionId/event/createfrombase64", () => {
    it.each(eventResource.createFromBase64, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, investigationId, instrumentName, expected } = element;
        const response = await logbookHelper.createEventFrombase64(user, element.body, investigationId, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (response.status === 200) {
          testEvent(response.body, expected.event);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/logbook/:sessionId/event/page", () => {
    it.each(eventResource.page, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, event, types, limit, sortOrder, instrumentName, testEventIndices, creator } = element;
        const getCreatorSessionResponse = await sessionHelper.doGetSession(creator.credential);
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;

        /** Populating events by the creator */
        const events = await this.prepopulateEvents(
          [...new Array(element.eventCount)].map(() => event),
          getCreatorSessionResponse.body.sessionId
        );

        const responses = await Promise.all(testEventIndices.map((index) => doPageEvent(sessionId, events[index]._id, investigationId, types, limit, sortOrder, instrumentName)));
        const pages = responses.map((response) => response.body.page);
        const status = new Set(responses.map((response) => response.status));

        expect(status.has(element.expected.status)).equal(true);

        if (element.expected.status < 400) {
          for (let i = 0; i < pages.length; i++) {
            expect(pages[i].index).equal(element.expected.index[i]);
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event/count", () => {
    it.each(eventResource.countEvents, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, events, types, instrumentName, search, useDate, filterInvestigation, prepopulatedTags, tags, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;
        /** Populating events */
        if (tags && prepopulatedTags) {
          await this.prepopulatesEventsWithTags(sessionId, prepopulatedTags, events);
        } else {
          await this.prepopulateLogbookEvents(events, instrumentName);
        }
        /** Do query */
        const date = useDate ? dateFormat(new Date(), "yyyy-MM-dd") : undefined;
        global.gLogger.debug(JSON.stringify(user.credential));
        const queryResponse = await doCountEvent(sessionId, { investigationId, types, search, instrumentName, date, filterInvestigation, tags });
        const { body } = queryResponse;
        expect(queryResponse.status).equal(expected.status);
        if (queryResponse.status < 300) {
          expect(body[0].totalNumber).equal(expected.occurrences);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event/remove", () => {
    it.each(eventResource.remove, "[%s]", ["description"], async (element, next) => {
      try {
        const { removeBy, remover, events, administrator, restorer, expected } = element;

        const getAdministratorResponse = await sessionHelper.doGetSession(administrator.credential);

        /** Do populate */
        await this.prepopulateLogbookEvents(events);

        /** Do search */
        const findResponse = await logbookHelper.doFindEvent(getAdministratorResponse.body.sessionId, removeBy);
        const eventsFound = findResponse.body.length;

        /** Do remove */
        const getRemoverSessionResponse = await sessionHelper.doGetSession(remover.credential);
        const removeResponse = await logbookHelper.doRemoveEvent(getRemoverSessionResponse.body.sessionId, removeBy);

        expect(removeResponse.status).equal(expected.status.remove);
        if (expected.status.remove < 400) {
          const removedEvents = removeResponse.body;
          expect(eventsFound).equal(removedEvents.length);

          /** Checks that the expected number of events removed */
          if (expected.removeCount) {
            expect(expected.removeCount).equal(removedEvents.length);
          }

          removedEvents.forEach((event) => {
            expect(event.removed).equal(true);
          });

          /** Do restore */
          const getRestorerSessionResponse = await sessionHelper.doGetSession(restorer.credential);
          const restoreResponse = await logbookHelper.doRestoreEvent(getRestorerSessionResponse.body.sessionId, removeBy);

          expect(restoreResponse.status).equal(expected.status.restore);
          if (expected.status.restore < 400) {
            const restoredEvents = restoreResponse.body;
            expect(removedEvents.length).equal(restoredEvents.length);
            restoredEvents.forEach((event) => {
              expect(event.removed).equal(false);
            });
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("SEARCH GET /logbook/:sessionId/event", () => {
    it.each(eventResource.search, "[%s]", ["description"], async (element, next) => {
      try {
        const { expected, events, investigationId, user, search, limit, skip, types, sortBy, sortOrder, tags, doExtraSearch, instrumentName, useDate, searchTags } = element;
        const { matches, status, searchs } = expected;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;

        await this.prepopulatesEventsWithTags(sessionId, tags, events);

        /** Do query */
        const date = useDate ? dateFormat(new Date(), "yyyy-MM-dd") : undefined;
        const queryResponse = await logbookHelper.doFindEvent(sessionId, {
          investigationId,
          types,
          limit,
          skip,
          sortBy,
          sortOrder,
          search,
          instrumentName,
          date,
          tags: searchTags,
        });
        const { body } = queryResponse;
        expect(queryResponse.status).equal(status);

        if (status < 300) {
          expect(body.length).equal(matches);
          if (matches > 0) {
            if (searchs) {
              for (let i = 0; i < body.length; i++) {
                const event = body[i];
                expect(event.meta.search.page).equal(expected.occurrences[i].page);
                expect(event.meta.search.occurrences).equal(expected.occurrences[i].occurrences);
              }
            }
            checkEventDate(date, body);
            checkTag(searchTags, body);
          }
        }

        if (doExtraSearch) {
          const doExtraSearchResponse = await logbookHelper.doFindEvent(sessionId, {
            investigationId: doExtraSearch.investigationId,
            types,
            limit,
            skip,
            sortBy,
            sortOrder,
            search,
          });
          expect(doExtraSearchResponse.status).equal(doExtraSearch.expected.status);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /logbook/:sessionId/event", () => {
    it.each(eventResource.createEvent, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, event, expected, instrumentName } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);
        const { sessionId } = getSessionResponse.body;

        const url = logbookHelper.getEventEndPointURL(sessionId, { investigationId, instrumentName });
        const response = await global.gRequester.post(url).send(event);
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          event.content = expected.content;
          testEvent(response.body, event, ["title", "investigationId", "type", "category", "creationDate"]);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /logbook/:sessionId/event", () => {
    it.each(eventResource.updateEvent, "%s", ["description"], async (element, next) => {
      try {
        const { user, createEventTags, updateEventTags, updateEventStep, investigationId, expected, instrumentName } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user.credential);

        let { sessionId } = getSessionResponse.body;
        /** Tag Creation */
        let tags = [];
        let tagsCreation = [];
        if (createEventTags) {
          tagsCreation = [...tagsCreation, ...createEventTags];
        }
        if (updateEventTags) {
          tagsCreation = [...tagsCreation, ...updateEventTags];
        }

        if (tagsCreation.length > 0) {
          tags = await prepopulateTags(tagsCreation, sessionId);
        }

        // Assing the recently created tag to the event
        if (createEventTags) {
          createEventTags.forEach((createEventTag) => {
            element.event.tag.push(_.find(tags, (createdTag) => createdTag.name === createEventTag.name)._id);
          });
        }

        // Creating event
        const events = await this.prepopulateLogbookEvents([element.event], instrumentName);

        // Updating the event
        if (updateEventStep) {
          updateEventStep.event._id = events[0]._id;

          if (updateEventStep.user) {
            const updaterSessionResponse = await sessionHelper.doGetSession(updateEventStep.user.credential);
            sessionId = updaterSessionResponse.body.sessionId;
          }
          if (updateEventTags) {
            updateEventTags.forEach((updateEventTag) => {
              updateEventStep.event.tag.push(_.find(tags, (createdTag) => createdTag.name === updateEventTag.name)._id);
            });
          }

          const url = logbookHelper.getEventEndPointURL(sessionId, { investigationId, instrumentName });

          const eventUpdatedResponse = await global.gRequester.put(url).send(updateEventStep.event);
          expect(eventUpdatedResponse.status).to.equal(expected.status);
          if (element.expected.status === 200) {
            testEvent(eventUpdatedResponse.body, expected.event); // test the latest version of the event
            if (expected.event.previousVersionEvent) {
              testEvent(eventUpdatedResponse.body.previousVersionEvent, expected.event.previousVersionEvent);
            }
            if (instrumentName) {
              /**Instrument are upper case in ICAT */
              expect(eventUpdatedResponse.body.instrumentName).equal(instrumentName.toUpperCase());
            }
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event", () => {
    it.each(eventResource.getEvents, "%s", ["description"], async (element, next) => {
      try {
        const {
          user,
          events,
          investigationId,
          instrumentName,
          expected,
          types,
          limit,
          skip,
          sortBy,
          sortOrder,
          search,
          useDate,
          filterInvestigation,
          prepopulatedTags,
          tags,
          startTimeCheck,
          endTimeCheck,
        } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;

        const prepopulatedEvents = await this.prepopulatesEventsWithTags(sessionId, prepopulatedTags, events);

        /** Doing the find request */
        const date = useDate ? dateFormat(new Date(), "yyyy-MM-dd") : undefined;
        const startTime = this.getTime(prepopulatedEvents, startTimeCheck);
        const endTime = this.getTime(prepopulatedEvents, endTimeCheck);
        const response = await logbookHelper.doFindEvent(sessionId, {
          investigationId,
          types,
          limit,
          skip,
          sortBy,
          sortOrder,
          search,
          instrumentName,
          date,
          filterInvestigation,
          tags,
          startTime,
          endTime,
        });
        const eventsResponse = response.body;
        expect(response.status).to.equal(expected.status);

        if (expected.status < 300) {
          /** if investigationId/instrumentName then it checks that events belong to that investigation/beamline */
          eventsResponse.forEach((event) => {
            if (investigationId) {
              expect(event.investigationId).to.equal(investigationId);
            }
            if (instrumentName) {
              expect(event.instrumentName.toUpperCase()).to.equal(instrumentName.toUpperCase());
            }
          });

          if (types) {
            /** if types then it checks that events are filtered corerctly by type and category */
            const categories = logbookControllerHelper.parseTypesToTypeCategory(types);
            eventsResponse.forEach((event) => {
              const found = categories.find((e) => e.type === event.type && e.category === event.category) !== null;
              expect(found).to.equal(true);
            });
          }
          checkTag(tags, eventsResponse);
          checkEventDate(date, eventsResponse);
          checkEventTime(startTime, endTime, eventsResponse);

          /** Check number of occurences */
          if (expected.occurrences) {
            expect(eventsResponse.length).to.equal(expected.occurrences);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event for broadcast", () => {
    it.each(eventResource.getBroadcastEvents, "%s", ["description"], async (element, next) => {
      try {
        const { user, events, investigationId, instrumentName, types, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        if (events) {
          await this.prepopulatesBroadcastEvents(events);
        }
        /** Doing the find request */
        const response = await logbookHelper.doFindEvent(sessionId, { investigationId, types, instrumentName });
        const eventsResponse = response.body;
        expect(response.status).to.equal(expected.status);

        if (expected.status < 300) {
          /** Check number of occurences */
          if (expected.occurrences) {
            expect(eventsResponse.length).to.equal(expected.occurrences);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event for txt export", () => {
    it.each(eventResource.toTXT, "%s", ["description"], async (element, next) => {
      try {
        const { user, events, investigationId, instrumentName, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        if (events) {
          await this.prepopulatesBroadcastEvents(events);
        }
        /** Doing the find request */
        const response = await logbookHelper.doFindEvent(sessionId, { investigationId, instrumentName, format: "txt" });

        expect(response.status).to.equal(expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /logbook/:sessionId/event/dates", () => {
    it.each(eventResource.eventsDates, "[%s]", ["description"], async (element, next) => {
      try {
        const { investigationId, user, populateUser, search, events, types, instrumentName, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        /** Populating events */
        await this.userPrepopulateEvents(events, populateUser);
        /** Do query */
        const response = await doFindEventsDates(sessionId, investigationId, types, search, instrumentName);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const { body } = response;
          expect(body.length).equal(expected.result.length);
          body.forEach((date, i) => {
            expect(date._id).equal(expected.result[i]._id);
            expect(date.event_count).equal(expected.result[i].event_count);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

exports.prepopulatesEventsWithTags = async (sessionId, tags, events) => {
  let tagList = [];
  if (tags) {
    /** Populating tags **/
    tagList = await prepopulateTags(tags, sessionId);
    /** This is needed to work with the tags otherwise it is constant and its value remains unchanged */
    events = JSON.parse(JSON.stringify(events));
    /** Event.tag is an array of ObjectId then we need to replace to simulate that the event has the right reference to the tag **/

    events.forEach((event) => {
      const tagIds = [];
      if (event.tag) {
        event.tag.forEach((evenTag) => {
          const foundTag = tagList.find((tag) => evenTag.toLowerCase() === tag.name.toLowerCase());
          if (foundTag) {
            tagIds.push(foundTag._id);
          }
        });
      }

      event.tag = tagIds;
    });
  }

  /** Populating events */
  if (events) {
    const prepopulatedEvents = await this.prepopulateEvents(events, sessionId);
    return prepopulatedEvents;
  }
};

exports.getEventDate = (event) => {
  return parse(event.creationDate.substr(0, 19), "yyyy-MM-dd'T'HH:mm:ss", new Date());
};

exports.getTime = (events, timeCheck) => {
  const formatTime = "yyyy-MM-dd'T'HH:mm:ss";
  if (events && events.length > 0 && timeCheck !== undefined) {
    if (timeCheck === "before") {
      const eventDate = this.getEventDate(events[0]);
      return format(subDays(eventDate, 1), formatTime);
    } else if (timeCheck === "after") {
      const eventDate = this.getEventDate(events[events.length - 1]);
      return format(addDays(eventDate, 1), formatTime);
    }
  }
  return undefined;
};

exports.prepopulatesBroadcastEvents = async (events) => {
  const responses = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationName, instrumentName } = event;
    const params = new URLSearchParams();
    if (investigationName) params.set("investigationName", investigationName);
    if (instrumentName) params.set("instrumentName", instrumentName);
    const url = `/dataacquisition/${global.gServerConfig.server.API_KEY}/notification?${params.toString()}`;
    const response = await global.gRequester.post(url).send(event);
    responses.push(response.body);
  }
  return responses;
};

/** We can not use Promise.all because of the order of the events could not match the order of the creation */
exports.prepopulateEvents = async (events, sessionId) => {
  const responses = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationId, instrumentName } = event;
    const url = logbookHelper.getEventEndPointURL(sessionId, { investigationId, instrumentName });
    const response = await global.gRequester.post(url).send(event);
    responses.push(response.body);
  }
  return responses;
};

exports.userPrepopulateEvents = async (events, user) => {
  const sessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = sessionResponse.body;
  await this.prepopulateEvents(events, sessionId);
};

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event/meta endpoint
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} types
 * @param {*} limit
 * @param {*} sortOrder
 * @param {*} instrumentName
 * @returns
 */
function getEventPageEndPointURL(sessionId, _id, investigationId, types, sortOrder, instrumentName, limit) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  if (_id) params.set("_id", _id);
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (limit) params.set("limit", limit);
  return `/logbook/${sessionId}/event/page?${params.toString()}`;
}

async function doPageEvent(sessionId, _id, investigationId, types, limit, sortOrder, instrumentName) {
  const url = getEventPageEndPointURL(sessionId, _id, investigationId, types, sortOrder, instrumentName, limit);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
}

function checkEventDate(date, events) {
  if (date) {
    events.forEach((event) => {
      expect(event.creationDate).is.not.null;
      expect(event.creationDate.substr(0, 10)).equal(date);
    });
  }
}

function checkEventTime(startTime, endTime, events) {
  const formatTime = "yyyy-MM-dd'T'HH:mm:ss";
  if (startTime || endTime) {
    events.forEach((event) => {
      expect(event.creationDate).is.not.null;
      const eventTime = parse(event.creationDate.substr(0, 19), formatTime, new Date());
      if (startTime) {
        expect(isAfter(eventTime, parse(startTime, formatTime, new Date())));
      }
      if (endTime) {
        expect(isBefore(eventTime, parse(endTime, formatTime, new Date())));
      }
    });
  }
}

function checkTag(tags, events) {
  if (tags) {
    tags = tags.split(",").map((t) => t.toUpperCase());
    events.forEach((event) => {
      let isInTagList = false;
      event.tag.forEach((t) => {
        const tagName = t.name.toUpperCase();
        isInTagList = isInTagList || tags.includes(tagName, 0);
      });

      expect(isInTagList).to.be.true;
    });
  }
}

async function doCountEvent(sessionId, queryParams) {
  const url = getCountEventEndPointURL(sessionId, queryParams);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
}

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event/count endpoint
 * @param {*} sessionId
 * @param {*} queryParams
 * @returns
 */
function getCountEventEndPointURL(sessionId, queryParams) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return `/logbook/${sessionId}/event/count?${params.toString()}`;
}

// used to prefill a released logbook, as we cannot edit it, save event direclty in the database
exports.prepopulateLogbookEvents = async (events, instrumentName) => {
  const response = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationId, type, category, content, creationDate, username, tag, previousVersionEvent } = event;
    const eventToBeCreated = new Event({
      investigationId,
      type,
      category,
      instrumentName: instrumentName ? instrumentName.toUpperCase() : undefined,
      creationDate,
      username,
      tag,
      content: logbookControllerHelper.translateEventContentToHtml(content),
      previousVersionEvent,
    });
    const eventCreated = await logbookControllerHelper.saveEvent(eventToBeCreated);
    response.push(eventCreated);
  }
  return response;
};
