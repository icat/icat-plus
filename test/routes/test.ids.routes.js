const idsResource = require("../resources/ids.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");
const nock = require("nock");
const { getOutname } = require("../../app/controllers/ids.controller.js");
const datasetAccessController = require("../../app/controllers/datasetaccess.controller");
const STATUS = require("../../app/models/datasetaccess.status.js");
const TYPE = require("../../app/models/datasetaccess.type.js");

require("it-each")({
  testPerIteration: true,
});

function checkDatasetAccess(datasetAccess, expected) {
  expect(datasetAccess.datasetId).equal(expected.datasetId);
  expect(datasetAccess.user).equal(expected.user);
  expect(datasetAccess.email).equal(expected.email);
  expect(datasetAccess.type).equal(expected.type);
  expect(datasetAccess.status).equal(expected.status);
}

const restoreData = async (sessionId, datasetIds, name, email) => {
  const params = new URLSearchParams();
  if (datasetIds) params.set("datasetIds", datasetIds);
  const request = `/ids/${sessionId}/datasets/restore?${params.toString()}`;
  return global.gRequester.post(request).set("Content-Type", "application/json").send({ name, email });
};

const downloadData = async (user, datasetIds, datafileIds) => {
  const sessionId = await sessionHelper.getSessionId(user);
  const params = new URLSearchParams();
  if (datasetIds) params.set("datasetIds", datasetIds);
  if (datafileIds) params.set("datafileIds", datafileIds);
  const url = `/ids/${sessionId}/data/download?${params.toString()}`;
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};

const getPath = async (user, datasetIds, datafileIds, investigationId, isonline) => {
  const sessionId = await sessionHelper.getSessionId(user);

  const params = new URLSearchParams();
  if (datasetIds) params.set("datasetIds", datasetIds);
  if (datafileIds) params.set("datafileIds", datafileIds);
  if (isonline) params.set("isonline", isonline);
  if (investigationId) params.set("investigationId", investigationId);
  const url = `/ids/${sessionId}/path?${params.toString()}`;

  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};

describe("Datasets", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
    if (!nock.isActive()) nock.activate();
    nock.cleanAll();
  });

  afterEach(async () => {
    nock.cleanAll();
  });

  describe("get outname", () => {
    it.each(idsResource.outname, "%s", ["description"], async (element, next) => {
      const { name, expected } = element;
      expect(getOutname(name)).to.equal(expected.name);
      try {
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/ids/:sessionId/data/download", () => {
    it.each(idsResource.getDataDownload, "%s", ["description"], async (element, next) => {
      const { user, datasetIds, datafileIds } = element;
      nock(global.gServerConfig.ids.server).get(/.*/).reply(200, {});
      try {
        const response = await downloadData(user, datasetIds, datafileIds);
        expect(response.status).to.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/ids/:sessionId/path", () => {
    it.each(idsResource.getPaths, "%s", ["description"], async (element, next) => {
      const { user, datasetIds, datafileIds, investigationId } = element;
      nock(global.gServerConfig.ids.server).get(/.*/).reply(200, ["/nock/file.txt"]);
      try {
        const response = await getPath(user, datasetIds, datafileIds, investigationId);
        expect(response.status).to.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/ids/:sessionId/datasets/restore", () => {
    it.each(idsResource.getDataRestore, "%s", ["description"], async (element, next) => {
      const { user, datasetId, name, email, prepopulate } = element;
      const sessionId = await sessionHelper.getSessionId(user);
      nock(global.gServerConfig.ids.server).get(`/ids/getData?sessionId=${sessionId}&datasetIds=${datasetId}`).reply(200, {});
      await this.prepopulateDataAccesses(prepopulate);
      const response = await restoreData(sessionId, datasetId, name, email);
      expect(response.status).to.equal(element.expected.status);
      if (element.expected.status < 300) {
        const datasetAccesses = await datasetAccessController.findDatasetAccesses(datasetId, TYPE.RESTORE, STATUS.ONGOING, name);
        expect(datasetAccesses).to.not.be.null;
        expect(datasetAccesses.length).to.equal(1);
        checkDatasetAccess(datasetAccesses[0], element.expected.datasetAccess);
      }
      return next();
    });
  });
});

exports.prepopulateDataAccesses = async (dataAccesses) => {
  if (!dataAccesses) {
    return;
  }
  for (let i = 0; i < dataAccesses.length; i++) {
    await dataAccesses[i].save();
  }
};
