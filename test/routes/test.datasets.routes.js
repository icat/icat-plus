const datasetResource = require("../resources/dataset.resource.js");
const sessionHelper = require("../helper/session.js");
const datasetHelper = require("../helper/dataset.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

const checkDocument = (document) => {
  expect(document).to.have.property("type");
  expect(document).to.have.property("id");
  expect(document).to.have.property("parametersCount");
  expect(document).to.have.property("instrumentName");
  expect(document).to.have.property("investigationName");
  expect(document).to.have.property("investigationId");
  // why the beamline name can not contain - ?? --> Because Elastic search does not like it!!
  expect(document.instrumentName).to.be.a("string").to.not.contain("-");
};

describe("Datasets", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  function checkLimitParameterInResponse(limit, response) {
    if (limit) {
      expect(response.body.length).to.be.at.most(limit);
    }
  }

  describe("/catalogue/:sessionId/dataset/id/:datasetId/datacollection", () => {
    it.each(datasetResource.getDataCollectionByDataSetId, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        try {
          global.gRequester
            .get(`/catalogue/${getSessionResponse.body.sessionId}/dataset/id/${element.datasetId}/datacollection`)
            .set("Content-Type", "application/json")
            .send()
            .end((_err, response) => {
              expect(response.status).to.equal(element.expected.status);
              expect(response.body).to.be.an("array");
              next();
            });
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("GET /catalogue/:sessionId/dataset", () => {
    it.each(datasetResource.getDatasetBy, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationIds, sampleId, instrumentName, datasetIds, limit, sortBy, sortOrder, search, datasetType, nested, skip, parameters } = element;
        const response = await datasetHelper.getDatasetsBy(user, {
          investigationIds,
          datasetIds,
          sampleId,
          instrumentName,
          limit,
          sortBy,
          sortOrder,
          search,
          datasetType,
          nested,
          skip,
          parameters,
        });
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          checkLimitParameterInResponse(limit, response);

          response.body.forEach((dataset) => {
            expect(dataset).to.have.property("meta");
            expect(dataset.meta).to.have.property("page");
            expect(dataset.meta.page).to.have.property("totalPages").is.not.null;
            expect(dataset.meta.page).to.have.property("currentPage").is.not.null;
            expect(dataset.meta.page).to.have.property("totalWithoutFilters").is.not.null;
            expect(dataset.meta.page).to.have.property("total").is.not.null;
          });

          if (datasetType) {
            response.body.forEach((dataset) => {
              expect(dataset.type).equals(datasetType);
            });
          }
          if (nested) {
            response.body.forEach((dataset) => {
              const outputDatasetParameter = dataset.parameters.find((param) => param.name === "__full_output_datasetIds");
              if (outputDatasetParameter != null) {
                const outputDatasetIds = outputDatasetParameter.value.split(" ");
                expect(dataset.outputDatasets.length).equal(outputDatasetIds.length);
              }
            });
          }
          if (sampleId) {
            response.body.forEach((dataset) => {
              expect(dataset.sampleId).equals(sampleId);
            });
          }
          if (instrumentName) {
            response.body.forEach((dataset) => {
              expect(dataset.instrumentName).equals(instrumentName.toUpperCase());
            });
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("[DEPRECATED] /catalogue/:sessionId/dataset/id/:datasetIds/dataset", () => {
    it.each(datasetResource.getDatasetByDatasetIds, "%s", ["description"], (element, next) => {
      try {
        global.gRequester
          .post("/session")
          .set("Content-Type", "application/json")
          .send(element.user)
          .end((_err, res) => {
            global.gRequester
              .get(`/catalogue/${res.body.sessionId}/dataset/id/${element.datasetIds}/dataset`)
              .set("Content-Type", "application/json")
              .send()
              .end((_err, response) => {
                expect(response.status).to.equal(element.expected.status);
                expect(response.body).to.be.an("array");
                expect(response.body).to.be.an("array").to.have.lengthOf.above(0);
                next();
              });
          });
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/dataset/id/:datasetIds/dataset_document", () => {
    it.each(datasetResource.getDatasetByDatasetIds, "%s", ["description"], async (element, next) => {
      try {
        const response = await datasetHelper.getDatasetDocumentByDatasetIds(element.user, element.datasetIds);
        expect(response.status).to.equal(element.expected.status);
        response.body.forEach((document) => {
          checkDocument(document);
        });
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/dataset/startdate/:startDate/enddate/:endDate/dataset_document", () => {
    it.each(datasetResource.getDatasetDocumentByDateRange, "%s", ["description"], async (element, next) => {
      const response = await datasetHelper.getDatasetDocumentByDates(element.user, element.startDate, element.endDate);
      try {
        expect(response.status).to.equal(element.expected.status);
        response.body.forEach((document) => {
          checkDocument(document);
        });
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/dataset/timeline", () => {
    it.each(datasetResource.getTimeline, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationId, sampleId } = element;
        const response = await datasetHelper.getTimelineBy(user, { investigationId, sampleId });
        expect(response.status).to.equal(element.expected.status);

        if (response.body.length > 0) {
          response.body.forEach((timelineElement) => {
            [
              "startDate",
              "endDate",
              "investigationId",
              "investigationName",
              "datasetName",
              "datasetId",
              "sampleName",
              "sampleId",
              "datasetType",
              "definition",
              "__fileCount",
              "__volume",
              "__elapsedTime",
            ].forEach((e) => expect(timelineElement).to.have.property(e));

            if (investigationId) {
              expect(timelineElement.investigationId).equal(investigationId);
            }
            if (sampleId) {
              expect(timelineElement.sampleId).equal(sampleId);
            }
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/doi/prefix/suffix/datasets?sessionId", () => {
    it.each(datasetResource.getDatasetByDOI, "%s", ["description"], async (element, next) => {
      try {
        const { user, doi, limit, sortBy, sortOrder, search } = element;
        const response = await datasetHelper.getDatasetsByDOI(user, doi.prefix, doi.suffix, limit, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.status).to.equal(element.expected.status);
          expect(response.body).to.be.an("array").to.have.length.above(0);
          checkLimitParameterInResponse(limit, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

exports.prepopulateDataAccesses = async (dataAccesses) => {
  if (dataAccesses) {
    for (let i = 0; i < dataAccesses.length; i++) {
      await dataAccesses[i].save();
    }
  }
};
