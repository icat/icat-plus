require("it-each")({ testPerIteration: true });

const resource = require("../resources/parcel.resource.js");
const shipmentHelper = require("../helper/shipment.js");
const parcelHelper = require("../helper/parcel.js");
const { expect } = require("chai");
const sessionHelper = require("../helper/session");

const testParcel = (parcel) => {
  expect(parcel._id).not.to.be.null;
  expect(parcel.shipmentId).not.to.be.null;
  expect(parcel.investigatonId).not.to.be.null;
  expect(parcel.investigation).not.to.be.null;
};

const getParcelById = async (user, investigationId, parcelId) => {
  const getParcelByIdResponse = await parcelHelper.getParcelById(user, investigationId, parcelId);
  if (getParcelByIdResponse.status < 300) {
    testParcel(getParcelByIdResponse.body);
  }
  return getParcelByIdResponse;
};

const getParcelBy = async (user, investigationId, parcelId) => {
  const getParcelByIdResponse = await parcelHelper.getParcelBy(user, investigationId, parcelId);
  if (getParcelByIdResponse.status < 300) {
    testParcel(getParcelByIdResponse.body);
  }
  return getParcelByIdResponse;
};

const createParcel = async (user, parcel, investigationId, shipmentId, status) => {
  const createParcelResponse = await parcelHelper.createParcel(user, parcel, investigationId, shipmentId);
  expect(createParcelResponse.status).to.equal(status);

  if (status < 300) {
    expect(createParcelResponse.body).to.not.be.null;
    const parcel = getParcelById(user, investigationId, createParcelResponse.body._id);
    return parcel;
  }
};

describe("Tracking Parcel", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });
  describe("E2E Shipment and parcel creation and set status", async () => {
    it.each(resource.setStatus, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);

        for (let i = 0; i < element.statuses.length; i++) {
          const { statusName, body, user, expected } = element.statuses[i];

          const setParcelStatusResponse = await parcelHelper.setParcelStatus(user, element.investigationId, createParcelResponse.body._id, statusName, body);
          expect(setParcelStatusResponse.status).to.equal(expected.status);

          const getParcelByIdResponse = await getParcelById(element.user, createParcelResponse.body.investigationId, createParcelResponse.body._id);
          expect(getParcelByIdResponse.body.length).equal(1);
          expect(getParcelByIdResponse.body[0].status).to.equal(expected.statusName || statusName);
          testParcel(getParcelByIdResponse.body);
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /tracking/{sessionId}/parcel", () => {
    it.each(resource.createParcel, "%s", ["description"], async (element, next) => {
      try {
        const { shipmentCreator, shipment, investigationId, parcelCreator, parcel, expected } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        const createdParcel = await createParcel(parcelCreator, parcel, investigationId, createShipmentResponse.body._id, expected.status);
        if (createdParcel && expected.isReimbursed) {
          expect(createdParcel.isReimbursed).to.equal(expected.isReimbursed);
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelByStatus, "%s %s", ["description", "status"], async (element, next) => {
      try {
        const getParcelByStatusResponse = await parcelHelper.getParcelBy(element.user, { status: element.status });

        expect(getParcelByStatusResponse.status).to.be.equal(element.expected.status);
        const parcels = getParcelByStatusResponse.body;
        if (element.expected.status < 300) {
          expect(parcels).to.be.an("array");
          parcels.forEach((parcel) => {
            expect(parcel.status).to.equal(element.status);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/:sessionId/setup", () => {
    it.each(resource.getSetup, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationId, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const setupResponse = await global.gRequester.get(`/tracking/${sessionId}/setup?investigationId=${investigationId}`).set("Content-Type", "application/json").send();
        expect(setupResponse.status).to.equal(expected.status);
        if (expected.status < 400) {
          const setup = setupResponse.body;
          const { containerTypes, experimentPlan, sampleDescription, processingPlan, instrumentName, sampleChangerType } = setup;
          expect(containerTypes).to.be.an("array");
          expect(experimentPlan).to.be.an("array");
          expect(sampleDescription).to.be.an("array");
          expect(processingPlan).to.be.an("array");
          expect(instrumentName).to.be.an("string");
          expect(sampleChangerType).to.be.an("array");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelsByInvestigationId, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);

        const getParcelByShipmentIdResponse = await parcelHelper.getParcelBy(element.user, {
          investigationId: element.investigationId,
          shipmentId: createShipmentResponse.body._id,
        });
        expect(getParcelByShipmentIdResponse.status).to.be.equal(element.expected.status);

        if (element.expected.status < 300) {
          expect(getParcelByShipmentIdResponse.body).to.be.an("array").to.have.lengthOf(1);
          testParcel(getParcelByShipmentIdResponse.body[0]);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelBySessionId, "%s", ["description"], async (element, next) => {
      try {
        const { shipmentCreator, shipment, investigationId, parcelCreator, parcel, status, search, expected, user } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await parcelHelper.createParcel(parcelCreator, parcel, investigationId, createShipmentResponse.body._id);
        const _id = createParcelResponse.body._id;

        /** Check that the parcel is visible */
        const getParcelBySessionIdResponse = await parcelHelper.getParcelBySessionId(user, { status, search });

        expect(getParcelBySessionIdResponse.body).to.be.an("array");
        expect(getParcelBySessionIdResponse.body.length).to.be.equal(expected.size);

        if (expected.size > 0) {
          const parcelFound = getParcelBySessionIdResponse.body.find((parcel) => {
            return parcel._id === _id;
          });

          expect(parcelFound._id).to.equal(_id);
          expect(parcelFound.investigation).to.not.be.undefined;
          expect(parcelFound.investigation.id).equal(investigationId);
          expect(parcelFound.status).to.not.be.undefined;
          expect(parcelFound.localContactFullnames).to.not.be.undefined;
          expect(parcelFound.status).to.equal(expected.parcelStatus);
          expect(parcelFound.isReimbursed).to.equal(expected.isReimbursed);
          checkTrackingInformation(parcelFound, expected);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelBy, "%s", ["description"], async (element, next) => {
      try {
        const { shipmentCreator, shipment, investigationId, parcelCreator, parcels, expected, user, limit, skip, status } = element;
        const { parcelCount } = element.expected;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);

        /** Creating the parcels */
        await Promise.all(
          parcels.map((parcel) => {
            parcel.shipmentId = createShipmentResponse.body._id;
            return createParcel(parcelCreator, parcel, investigationId, createShipmentResponse.body._id, 200);
          })
        );

        /** Doing the test */

        const getParcelBySessionIdResponse = await parcelHelper.getParcelBy(user, { limit, skip, status });

        expect(getParcelBySessionIdResponse.body).to.be.an("array");

        if ("parcelCount" in expected) {
          expect(getParcelBySessionIdResponse.body.length).to.be.equal(parcelCount);
        } else {
          expect(getParcelBySessionIdResponse.body.length).to.be.equal(limit ? parcels.length - limit : parcels.length);
        }

        for (const parcel of getParcelBySessionIdResponse.body) {
          expect(parcel.investigation).to.not.be.undefined;
          expect(parcel.investigation.id).equal(investigationId);
          expect(parcel.status).to.not.be.undefined;
          expect(parcel.localContactFullnames).to.not.be.undefined;
          expect(parcel.status).to.equal(expected.parcelStatus);
          /** checking pagination */
          expect(parcel.meta).to.not.be.undefined;
          expect(parcel.meta.page).to.not.be.undefined;
          expect(parcel.meta.page).to.have.property("total").that.is.a("number");
          expect(parcel.meta.page).to.have.property("totalPages").that.is.a("number");
          expect(parcel.meta.page).to.have.property("currentPage").that.is.a("number");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel with skip & limit", () => {
    it.each(resource.getParcelBySessionIdSkipLimit, "%s", ["description"], async (element, next) => {
      try {
        const { shipmentCreator, shipment, investigationId, parcelCreator, user, skip, limit, expected, deleteParcels } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);

        await Promise.all(
          element.parcels.map((parcel) => {
            parcel.shipmentId = createShipmentResponse.body._id;
            return createParcel(parcelCreator, parcel, investigationId, createShipmentResponse.body._id, 200);
          })
        );

        if (deleteParcels) {
          const allParcelBySessionIdResponse = await parcelHelper.getParcelBySessionId(user);
          const allParcels = allParcelBySessionIdResponse.body;

          /** delete Parcels */
          for (let i = 0; i < deleteParcels; i++) {
            const deleteResponse = await parcelHelper.deleteParcel(element.parcelCreator, { _id: allParcels[i] }, investigationId, createShipmentResponse.body._id);
            expect(deleteResponse.status).equal(200);
          }

          const deleteParcelsReponse = await parcelHelper.getParcelBySessionId(user);
          const remainingParcels = deleteParcelsReponse.body;
          /** checking that the parcels are actually deleted  */
          expect(remainingParcels.length + deleteParcels).equal(element.parcels.length);
        }

        const getParcelBySessionIdResponse = await parcelHelper.getParcelBySessionId(user, { skip, limit });
        const responseParcels = getParcelBySessionIdResponse.body;

        expect(getParcelBySessionIdResponse.status).equal(expected.status);
        expect(responseParcels.length).equal(expected.count);

        responseParcels.forEach((parcel) => {
          expect(parcel.meta.page.total).equal(expected.meta.page.total);
          expect(parcel.meta.page.totalPages).equal(expected.meta.page.totalPages);
          expect(parcel.meta.page.currentPage).equal(expected.meta.page.currentPage);
        });
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/parcel", () => {
    it.each(resource.getParcelById, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id, 200);
        const parcelId = createParcelResponse.body._id;

        const getParcelByShipmentIdResponse = await getParcelBy(element.user, { investigationId: element.investigationId, parcelId });
        expect(getParcelByShipmentIdResponse.status).to.be.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(getParcelByShipmentIdResponse.body).to.be.an("array");

          expect(getParcelByShipmentIdResponse.body.length).equal(1);
          expect(parcelId).to.be.equal(getParcelByShipmentIdResponse.body._id);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /tracking/{sessionId}/parcel", () => {
    it.each(resource.createParcelWithContent, " %s", ["description"], async (element, next) => {
      try {
        const { expected, parcel, parcelCreator, investigationId, shipmentCreator, shipment, modifyContent } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        parcel.shipmentId = createShipmentResponse.body._id;

        let createParcelResponse = await parcelHelper.createOrUpdateParcel(parcelCreator, parcel, investigationId, createShipmentResponse.body._id);
        expect(createParcelResponse.status).to.be.equal(expected.status);

        if (expected.status < 400) {
          const newParcel = createParcelResponse.body;
          expect(newParcel.investigation).to.not.be.undefined;
          expect(newParcel.investigation.id).equal(investigationId);
          expect(newParcel.status).to.not.be.undefined;
          expect(newParcel.localContactFullnames).to.not.be.undefined;

          if (parcel.content[0].containerType) {
            expect(newParcel.content[0].containerType).to.not.be.undefined;
            expect(newParcel.content[0].containerType.name).to.not.be.undefined;
          }
          /* If there is modification in the test */
          if (modifyContent) {
            newParcel.content[0].content = modifyContent;
            createParcelResponse = await parcelHelper.createOrUpdateParcel(parcelCreator, newParcel, investigationId, createShipmentResponse.body._id);
            const modifiedParcel = createParcelResponse.body;
            expect(modifiedParcel.content.length).equal(parcel.content.length);
            expect(modifiedParcel.content[0].content.length).equal(modifyContent.length);
            expect(modifiedParcel._id).equal(newParcel._id);
          }
        }

        /** in case of invalid parameters */
        if (expected.status === 400) {
          expect(createParcelResponse.body.experimentPlan.toString()).equal(expected.missingParameters.experimentPlan.toString());
          expect(createParcelResponse.body.processingPlan.toString()).equal(expected.missingParameters.processingPlan.toString());
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Creates a shipment, then a parcel, then removes the parcel
   */
  describe("DELETE /tracking/:sessionId/parcel", () => {
    it.each(resource.deleteParcel, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;

        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id);

        const deleteParcelResponse = await parcelHelper.deleteParcel(
          element.parcelCreator,
          { _id: createParcelResponse.body._id },
          element.investigationId,
          element.parcel.shipmentId
        );

        expect(deleteParcelResponse.status).to.equal(element.expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /tracking/{sessionId}/parcel", () => {
    it.each(resource.updateParcel, "%s", ["description"], async (element, next) => {
      try {
        const { user, shipment, investigationId, parcel, update, expected } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(user, shipment, investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        const shipmentId = createShipmentResponse.body._id;
        parcel.shipmentId = shipmentId;
        const createdParcelResponse = await createParcel(user, parcel, investigationId, createShipmentResponse.body._id, 200);
        expect(createdParcelResponse.status).to.equal(200);
        const parcelToUpdate = createdParcelResponse.body[0];
        parcelToUpdate.description = update.description;
        parcelToUpdate.comments = update.comment;
        parcelToUpdate.returnInstructions = update.returnInstructions;
        if (update.isReimbursed) {
          parcelToUpdate.isReimbursed = update.isReimbursed;
        }
        if (update.reimbursedCustomsValue) {
          parcelToUpdate.reimbursedCustomsValue = update.reimbursedCustomsValue;
        }
        parcelToUpdate.returnParcelInformation = update.returnParcelInformation;
        const updatedParcelResponse = await parcelHelper.createOrUpdateParcel(update.user, parcelToUpdate, investigationId, shipmentId);
        expect(updatedParcelResponse.status).to.equal(expected.status);
        if (expected.status < 300) {
          const updatedParcel = updatedParcelResponse.body;
          checkParcelUpdate(updatedParcel, update);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

function checkParcelUpdate(updatedParcel, expected) {
  expect(updatedParcel).not.to.be.null;
  if (expected.description) {
    expect(updatedParcel.description).to.be.equal(expected.description);
  }
  if (expected.comment) {
    expect(updatedParcel.comments).to.be.equal(expected.comment);
  }
  if (expected.returnInstructions) {
    expect(updatedParcel.returnInstructions).to.be.equal(expected.returnInstructions);
  }
  if (expected.isReimbursed) {
    expect(updatedParcel.isReimbursed).to.be.equal(expected.isReimbursed);
    if (expected.reimbursedCustomsValue) {
      expect(updatedParcel.reimbursedCustomsValue).to.be.equal(expected.reimbursedCustomsValue);
    } else {
      expect(updatedParcel.reimbursedCustomsValue).to.be.equal(50);
    }
  } else {
    expect(updatedParcel.isReimbursed).to.be.false;
  }
  if (expected.returnParcelInformation) {
    expect(updatedParcel.returnParcelInformation).not.to.be.null;
    expect(updatedParcel.returnParcelInformation.returnType).to.be.equal(expected.returnParcelInformation.returnType);
    expect(updatedParcel.returnParcelInformation.forwarderName).to.be.equal(expected.returnParcelInformation.forwarderName);
    if (expected.returnParcelInformation.plannedPickupDate) {
      expect(updatedParcel.returnParcelInformation.plannedPickupDate.split("T")[0]).to.be.equal(expected.returnParcelInformation.plannedPickupDate);
    }
    expect(updatedParcel.returnParcelInformation.courierAccount).to.be.equal(expected.returnParcelInformation.courierAccount);
    expect(updatedParcel.returnParcelInformation.courierCompany).to.be.equal(expected.returnParcelInformation.courierCompany);
    if (expected.returnParcelInformation.customsValue) {
      expect(updatedParcel.returnParcelInformation.customsValue).to.be.equal(expected.returnParcelInformation.customsValue);
    }
  }
}

function checkTrackingInformation(parcel, expected) {
  if (expected.shippingTrackingInformation) {
    expect(parcel.shippingTrackingInformation).not.to.be.null;
    expect(parcel.shippingTrackingInformation.trackingNumber).to.equal(expected.shippingTrackingInformation.trackingNumber);
    expect(parcel.shippingTrackingInformation.forwarder).to.equal(expected.shippingTrackingInformation.forwarder);
  }
  if (expected.returnTrackingInformation) {
    expect(parcel.returnTrackingInformation).not.to.be.null;
    expect(parcel.returnTrackingInformation.trackingNumber).to.equal(expected.returnTrackingInformation.trackingNumber);
    expect(parcel.returnTrackingInformation.forwarder).to.equal(expected.returnTrackingInformation.forwarder);
  }
}
