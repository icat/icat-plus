const parameterResource = require("../resources/datasetparameter.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("Parameters", () => {
  describe("PUT /catalogue/:sessionId/datasetParameter", () => {
    it.each(parameterResource.updateParameter, "%s", ["description"], async (element, next) => {
      try {
        const { user, parameterId, value } = element;
        const newValue = value ? value + new Date().toLocaleString() : undefined;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const sessionId = sessionResponse.body.sessionId;
        const parameter = { value: newValue };
        const response = await global.gRequester.put(`/catalogue/${sessionId}/datasetParameter?parameterId=${parameterId}`).set("Content-Type", "application/json").send(parameter);
        expect(response.status).to.be.equal(element.expected.status);
        if (element.expected.status === 200) {
          expect(response.body.value).to.be.equal(newValue);
          expect(response.body.parameterId).to.be.equal(parameterId);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
  describe("GET /catalogue/:sessionId/datasetparametercsv", () => {
    it.each(parameterResource.getCSVDatasetParameters, "%s", ["description"], async (element, next) => {
      try {
        const { expected, user, query } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const params = new URLSearchParams();

        for (const [key, value] of Object.entries(query)) {
          params.set(key, value);
        }

        const response = await global.gRequester.get(`/catalogue/${sessionId}/datasetparameter?${params.toString()}`).set("Content-Type", "application/json").send();
        expect(response.status).to.be.equal(expected.status);

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
  describe("GET /catalogue/:sessionId/datasetparameter/values", () => {
    it.each(parameterResource.getDatasetParametersValues, "%s", ["description"], async (element, next) => {
      try {
        const { expected, user, query } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const params = new URLSearchParams();

        for (const [key, value] of Object.entries(query)) {
          params.set(key, value);
        }

        const response = await global.gRequester.get(`/catalogue/${sessionId}/datasetparameter/values?${params.toString()}`).set("Content-Type", "application/json").send();
        expect(response.status).to.be.equal(expected.status);

        const result = response.body;
        expect(result.length).to.be.equal(query.name.split(",").length);
        for (let i = 0; i < result.length; i++) {
          const element = result[i];
          expect(element.name).to.be.equal(query.name.split(",")[i]);
          expect(element.values).to.be.an("array");
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
  describe("GET /catalogue/:sessionId/datasetparameter", () => {
    it.each(parameterResource.getDatasetParameters, "%s", ["description"], async (element, next) => {
      try {
        const { expected, user, query } = element;
        const { sampleId } = query;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const params = new URLSearchParams();

        for (const [key, value] of Object.entries(query)) {
          params.set(key, value);
        }

        let response = await global.gRequester.get(`/catalogue/${sessionId}/datasetparameter?${params.toString()}`).set("Content-Type", "application/json").send();

        expect(response.status).to.be.equal(expected.status);
        if (expected.status < 300) {
          const parameters = response.body;
          expect(parameters).to.be.an("array");
          if (!expected.isEmpty) {
            expect(parameters).not.to.be.empty;
            parameters.forEach((parameter) => {
              ["name", "value", "id", "units", "createTime"].map((property) => expect(parameter).to.have.nested.property(property));
            });
            if (sampleId) {
              parameters.forEach((parameter) => {
                expect(parameter.sampleId).equal(sampleId);
                expect(parameter.sampleName).to.not.be.null;
              });
            }
            params.set("name", parameters[0].name);
            /** Testing investigation + name */
            response = await global.gRequester.get(`/catalogue/${sessionId}/datasetparameter?${params.toString()}`).set("Content-Type", "application/json").send();
            const parameter = response.body[0];
            expect(parameter.name).equal(parameters[0].name);
          } else {
            expect(parameters).to.be.empty;
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
