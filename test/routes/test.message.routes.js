require("it-each")({ testPerIteration: true });

const { getMessages } = require("../helper/message.js");
const sessionHelper = require("../helper/session.js");
const messageResource = require("../resources/message.resource.js");

describe("Message", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("GET /messages", () => {
    it.each(messageResource.getMessages, "%s", ["description"], async (element, next) => {
      const { prepopulate, expected, type } = element;
      await this.prepopulateMessages(prepopulate);
      const response = await getMessages(type);
      const { body } = response;
      expect(response.status).to.be.equal(element.expected.status);
      if (response.status < 300) {
        expect(response.body).to.be.an("array");
        if (expected.message) {
          expect(response.body.length).equal(expected.message.length);
          response.body.forEach((message, id) => {
            expect(message.message).to.equal(expected.message[id].message);
            expect(message.type).to.equal(expected.message[id].type);
          });
        } else {
          expect(body).to.be.deep.equal({});
        }
      }
      next();
    });
  });

  describe("PUT /messages/:sessionId/information", () => {
    it.each(messageResource.updateInformationMessage, "%s", ["description"], async (element, next) => {
      try {
        const { prepopulate, user, message, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        await this.prepopulateMessages(prepopulate);
        const url = this.getMessageURL(sessionId);
        const response = await global.gRequester.put(url).send(message);
        const { body } = response;
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          if (expected.message) {
            expect(body.message).to.equal(expected.message.message);
            expect(body.type).to.equal(expected.message.type);
          } else {
            expect(body).to.be.deep.equal({});
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

exports.getMessageURL = (sessionId) => {
  return `/messages/${sessionId}/information`;
};

exports.prepopulateMessages = async (prepopulate) => {
  const responses = [];
  const messages = prepopulate.messages;
  if (messages) {
    const prepopulatedSessionResponse = await sessionHelper.doGetSession(prepopulate.user.credential);
    const getPrepopulatedSessionResponse = prepopulatedSessionResponse.body;
    const url = this.getMessageURL(getPrepopulatedSessionResponse.sessionId);
    for (let i = 0; i < prepopulate.messages.length; i++) {
      const response = await global.gRequester.put(url).send({ type: messages[i].type, message: messages[i].message });
      expect(response.status).to.equal(200);
      responses.push(response.body);
    }
  }
  return responses;
};
