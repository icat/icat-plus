require("it-each")({ testPerIteration: true });

const sampleInformationResource = require("../resources/sampleinformation.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");

describe("SampleInformation", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("POST /catalogue/:sessionId/files/upload", () => {
    it.each(sampleInformationResource.uploadFile, "%s", ["description"], async (element, next) => {
      try {
        const { user, sampleId, data, filename, fileInformation, prepopulate, expected } = element;
        await this.prepopulateSampleInformation(prepopulate);
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const file = Buffer.from(data, "utf-8");
        const response = await this.uploadSampleInformation(sessionId, sampleId, file, filename, fileInformation);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const sampleInformation = response.body;
          expect(sampleInformation.sampleId).to.equal(sampleId);
          expect(sampleInformation.resources).to.be.an("array");
          checkResourceInformation(sampleInformation.resources[sampleInformation.resources.length - 1], {
            filename,
            fileType: fileInformation.fileType,
            groupName: fileInformation.groupName,
            multiplicity: fileInformation.multiplicity,
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /catalogue/:sessionId/files", () => {
    it.each(sampleInformationResource.getFiles, "%s", ["description"], async (element, next) => {
      try {
        const { user, sampleId, groupName, prepopulate, expected } = element;
        await this.prepopulateSampleInformation(prepopulate);
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const response = await this.getSampleInformation(sessionId, sampleId, groupName);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const sampleInformation = response.body;
          expect(sampleInformation).to.not.be.null;
          if (expected.resources) {
            expect(sampleInformation.resources.length).to.be.equal(expected.resources.length);
            expect(sampleInformation.sampleId).to.equal(expected.sampleId);
            sampleInformation.resources.forEach((resource, i) => {
              const expectedResource = expected.resources[i];
              checkResourceInformation(resource, expectedResource);
            });
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /catalogue/:sessionId/files/download", () => {
    it.each(sampleInformationResource.downloadFile, "%s", ["description"], async (element, next) => {
      try {
        const { user, sampleId, prepopulate, expected } = element;
        await this.prepopulateSampleInformation(prepopulate);
        const getSessionResponse = await sessionHelper.doGetSession(prepopulate.user);
        const responseExistingSampleInformation = await this.getSampleInformation(getSessionResponse.body.sessionId, sampleId);
        const resourceId = responseExistingSampleInformation.body.resources[0].file;

        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const response = await this.downloadSampleInformation(sessionId, sampleId, resourceId);
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

function checkResourceInformation(resource, expected) {
  expect(resource.file).to.not.be.null;
  expect(resource.filename).to.equal(expected.filename);
  expect(resource.fileType).to.equal(expected.fileType);
  expect(resource.groupName).to.equal(expected.groupName);
  expect(resource.multiplicity).to.equal(expected.multiplicity);
}

exports.uploadSampleInformation = async (sessionId, sampleId, file, filename, body) => {
  const params = new URLSearchParams();
  if (sampleId) params.set("sampleId", sampleId);
  const url = `/catalogue/${sessionId}/files/upload?${params.toString()}`;
  const request = global.gRequester.post(url).set("Content-Type", "multipart/form-data");
  if (body.fileType) {
    request.field("fileType", body.fileType);
  }
  if (body.groupName) {
    request.field("groupName", body.groupName);
  }
  if (body.multiplicity) {
    request.field("multiplicity", body.multiplicity);
  }
  return request.attach("file", file, filename);
};

exports.prepopulateSampleInformation = async (prepopulate) => {
  if (prepopulate) {
    const getSessionResponse = await sessionHelper.doGetSession(prepopulate.user);
    const { sessionId } = getSessionResponse.body;
    for (let i = 0; i < prepopulate.sampleInformations.length; i++) {
      const sampleInformation = prepopulate.sampleInformations[i];
      const file = Buffer.from(sampleInformation.data, "utf-8");
      const filename = sampleInformation.filename;
      const response = await this.uploadSampleInformation(sessionId, sampleInformation.sampleId, file, filename, {
        fileType: sampleInformation.fileType,
        multiplicity: sampleInformation.multiplicity,
        groupName: sampleInformation.groupName,
      });
      expect(response.status).to.equal(200);
    }
  }
};

exports.getSampleInformation = async (sessionId, sampleId, groupName) => {
  const params = new URLSearchParams();
  if (sampleId) params.set("sampleId", sampleId);
  if (groupName) params.set("groupName", groupName);
  const url = `/catalogue/${sessionId}/files?${params.toString()}`;
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};

exports.downloadSampleInformation = async (sessionId, sampleId, resourceId) => {
  const params = new URLSearchParams();
  if (sampleId) params.set("sampleId", sampleId);
  if (resourceId) params.set("resourceId", resourceId);
  const url = `/catalogue/${sessionId}/files/download?${params.toString()}`;
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};
