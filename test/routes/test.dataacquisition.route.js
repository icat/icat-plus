require("it-each")({ testPerIteration: true });

const dataAcquisitionResource = require("../resources/dataacquisition.resource");
const investigationResource = require("../resources/investigations.resource");
const { getSessionId } = require("../helper/session.js");
const { expect } = require("chai");
const { prepopulateTags } = require("../helper/tag");
const { getTags } = require("../helper/tag");
const logbookHelper = require("../helper/logbook.js");
const DatasetAccess = require("../../app/models/datasetaccess.model.js");
const nock = require("nock");

const getInvestigationBy = async (apiKey, instrumentName, time, investigationName) => {
  const params = new URLSearchParams();
  if (instrumentName) params.set("instrumentName", instrumentName);
  if (time) params.set("time", time);
  if (investigationName) params.set("investigationName", investigationName);

  const request = `/dataacquisition/${apiKey}/investigation?${params.toString()}`;
  return global.gRequester.get(request).set("Content-Type", "application/json").send();
};

const getDatasetsByInvestigationId = async (apiKey, investigationId, limit, sortBy, sortOrder, search) => {
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  if (investigationId) params.set("investigationId", investigationId);
  return global.gRequester.get(`/dataacquisition/${apiKey}/dataset?${params.toString()}`).send();
};

describe("DataAcquisition", () => {
  // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("/dataacquisition/:sessionId/mint", () => {
    it.each(dataAcquisitionResource.mint, "%s", ["description"], async (element, next) => {
      const { datasetIdList, title, abstract, authors, keywords, citationPublicationDOI, referenceURL, expected } = element;
      nock(global.gServerConfig.datacite.mds).post("/metadata").reply(200, {});
      nock(global.gServerConfig.datacite.mds).post(`/doi`).query(true).reply(200, {});
      try {
        const response = await global.gRequester
          .post(`/dataacquisition/${global.gServerConfig.server.API_KEY}/mint`)
          .send({ datasetIdList, title, abstract, authors, keywords, citationPublicationDOI, referenceURL });
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/dataacquisition/:apiKey/notification", () => {
    describe("Create beamline notification", () => {
      it.each(dataAcquisitionResource.createBeamlineNotification, "%s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, event, adminUser, expected } = element;
          const { instrumentName } = event;
          const adminSessionId = await getSessionId(adminUser);
          await prepopulateTags(tagsToFillDB, adminSessionId);
          const response = await global.gRequester
            .post(`/dataacquisition/${global.gServerConfig.server.API_KEY}/notification?instrumentName=${instrumentName}`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            logbookHelper.testEvent(response.body, expected.event);
            if (expected.tags) {
              const responseTags = await getTags(adminSessionId, null, instrumentName);
              expect(responseTags.body.length).equal(expected.tags.length);
              expected.tags.forEach((expectedTag, index) => {
                logbookHelper.testTag(responseTags.body[index], expectedTag);
              });
            }
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("/dataacquisition/:apiKey/notification", () => {
    describe("Create investigation notification", () => {
      it.each(dataAcquisitionResource.createInvestigationNotification, "%s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, adminUser, expected, event, investigationId } = element;
          const { investigationName, instrumentName } = event;
          const adminSessionId = await getSessionId(adminUser);
          await prepopulateTags(tagsToFillDB, adminSessionId);
          const response = await global.gRequester
            .post(`/dataacquisition/${global.gServerConfig.server.API_KEY}/notification?investigationName=${investigationName}&instrumentName=${instrumentName}`)
            .set("Content-Type", "application/json")
            .send(element.event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            logbookHelper.testEvent(response.body, expected.event);
            if (expected.tags) {
              const responseTags = await getTags(adminSessionId, investigationId, instrumentName);
              expect(responseTags.body.length).equal(expected.tags.length);
              expected.tags.forEach((expectedTag, index) => {
                logbookHelper.testTag(responseTags.body[index], expectedTag);
              });
            }
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe("/dataacquisition/:apiKey/notification", () => {
    describe("Create broadcast event", () => {
      it.each(dataAcquisitionResource.createBroadcastEvent, "%s", ["description"], async (element, next) => {
        try {
          const { tagsToFillDB, sessionId, event, expected } = element;
          await prepopulateTags(tagsToFillDB, sessionId);
          const response = await global.gRequester.post(`/dataacquisition/${sessionId}/notification`).set("Content-Type", "application/json").send(event);

          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            logbookHelper.testEvent(response.body, expected.event);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      });
    });
  });

  describe.skip("/dataacquisition/:apiKey/base64", function () {
    this.retries(4);
    it.each(dataAcquisitionResource.createFromBase64byBeamlineAndInstrumentName, "[%s]", ["description"], async (element, next) => {
      try {
        const response = await global.gRequester
          .post(
            `/dataacquisition/${element.apiKey ? element.apiKey : global.gServerConfig.server.API_KEY}/base64?investigationName=${element.investigationName}&instrumentName=${
              element.instrumentName
            }`
          )
          .set("Content-Type", "application/json")
          .send(element.body);

        expect(response.status).to.equal(element.expected.status);

        /** check the tags creation */
        if (element.body.tag) {
          expect(response.body.tag).is.not.null;
          expect(response.body.tag.length).to.equal(element.expected.tagCount);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/dataacquisition/:apiKey/investigation", () => {
    it.each(investigationResource.allocateInvestigations, "%s", ["description"], async (element, next) => {
      try {
        const { instrumentName, investigationName, time, expected } = element;
        const response = await getInvestigationBy(global.gServerConfig.server.API_KEY, instrumentName, time, investigationName);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          if (expected.visitId) {
            expect(response.body[0].visitId).to.equal(expected.visitId);
          } else {
            expect(response.body.length).to.equal(0);
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/dataacquisition/:apiKey/dataset", () => {
    it.each(dataAcquisitionResource.getDatasetByInvestigationId, "%s", ["description"], async (element, next) => {
      try {
        const { investigationId, limit, sortBy, sortOrder, search } = element;
        const response = await getDatasetsByInvestigationId(global.gServerConfig.server.API_KEY, investigationId, limit, sortBy, sortOrder, search);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array").to.have.length.above(0);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/dataacquisition/:apiKey/dataset/restore", () => {
    it.each(dataAcquisitionResource.restore, "%s", ["description"], async (element, next) => {
      const { datasetId, level, message, dataAccessToFillDB, expected } = element;
      try {
        await this.prepopulateDataAccesses(dataAccessToFillDB);
        const response = await global.gRequester.post(`/dataacquisition/${global.gServerConfig.server.API_KEY}/dataset/restore?datasetId=${datasetId}`).send({ level, message });
        expect(response.status).to.equal(expected.status);
        if (element.expected.status < 300) {
          expect(response.body.length).to.equal(expected.nbUpdated);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /dataacquisition/:apiKey/events/move", () => {
    it.each(dataAcquisitionResource.moveEvents, "[%s]", ["description"], async (element, next) => {
      try {
        const { adminUser, events, sourceInvestigationId, destinationInvestigationId, expected } = element;
        const adminSessionId = await getSessionId(adminUser);
        /** Populating events */
        await this.prepopulateEvents(events, adminSessionId);
        /** Do query */
        const response = await logbookHelper.doMoveEvents(element.apiKey ? element.apiKey : global.gServerConfig.server.API_KEY, sourceInvestigationId, destinationInvestigationId);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          const { body } = response;
          expect(body.nModified).equal(expected.modified);
          if (expected.events) {
            const getResponse = await logbookHelper.doFindEvent(adminSessionId, { investigationId: destinationInvestigationId });
            const eventsResponse = getResponse.body;
            expect(eventsResponse.length).to.equal(expected.events.length);
            expected.events.forEach((expectedEvent, index) => {
              logbookHelper.testEvent(eventsResponse[index], expectedEvent);
            });
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

/** We can not use Promise.all because of the order of the events could not match the order of the creation */
exports.prepopulateEvents = async (events, sessionId) => {
  const responses = [];
  for (let i = 0; i < events.length; i++) {
    const event = events[i];
    const { investigationId, instrumentName } = event;
    const url = getEventEndPointURL(sessionId, investigationId, null, null, null, null, null, null, instrumentName);
    const response = await global.gRequester.post(url).send(event);
    responses.push(response.body);
  }
  return responses;
};

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event endpoint
 * @param {*} sessionId
 * @param {*} investigationId
 * @param {*} types
 * @param {*} limit
 * @param {*} skip
 * @param {*} sortBy
 * @param {*} sortOrder
 * @param {*} search
 * @param {*} instrumentName
 * @returns
 */
function getEventEndPointURL(sessionId, investigationId, types, limit, skip, sortBy, sortOrder, search, instrumentName) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", skip);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortBy) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  if (instrumentName) params.set("instrumentName", instrumentName);
  return `/logbook/${sessionId}/event?${params.toString()}`;
}

exports.prepopulateDataAccesses = async (dataAccesses) => {
  const dataAccessList = [];
  if (dataAccesses) {
    for (let i = 0; i < dataAccesses.length; i++) {
      const dataAccess = dataAccesses[i];
      let dataAccessToCreate = new DatasetAccess({
        datasetId: dataAccess.datasetId,
        user: dataAccess.user,
      });
      dataAccessToCreate = await dataAccessToCreate.save();
      dataAccessList.push(dataAccessToCreate);
    }
  }
  return dataAccessList;
};
