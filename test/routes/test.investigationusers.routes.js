require("it-each")({ testPerIteration: true });

const resource = require("../resources/investigationUsers.resource.js");
const sessionHelper = require("../helper/session.js");
const { expect } = require("chai");

describe("InvestigationsUsers", () => {
  describe("GET /catalogue/:sessionId/investigation/id/:investigationId/investigationusers", () => {
    it.each(resource.investigationusers, "[ %s]", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${element.investigationId}/investigationusers`)
          .set("Content-Type", "application/json")
          .send()
          .end((_err, response) => {
            expect(response.status).to.equal(element.expected.status);
            expect(response.body).to.be.an("array");
            expect(response.body).to.be.an("array").that.is.not.empty;
            next();
          });
      });
    });
  });

  describe("POST /catalogue/:sessionId/investigation/id/:investigationId/investigationusers", () => {
    it.each(resource.addInvestigationUser, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationId, collaborator, revoker, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const response = await global.gRequester.post(getInvestigationUsersURL(sessionId, investigationId)).set("Content-Type", "application/json").send(collaborator);
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          const getRevokerSessionResponse = await sessionHelper.doGetSession(revoker);
          const revokeResponse = await global.gRequester
            .delete(getInvestigationUsersURL(getRevokerSessionResponse.body.sessionId, investigationId))
            .set("Content-Type", "application/json")
            .send(collaborator);
          expect(revokeResponse.status).to.equal(200);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("DELETE /catalogue/:sessionId/investigation/id/:investigationId/investigationusers", () => {
    it.each(resource.deleteInvestigationUser, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationId, collaborator, granter, expected } = element;
        if (granter) {
          const granterSessionResponse = await sessionHelper.doGetSession(granter);
          const granterResponse = await global.gRequester
            .post(getInvestigationUsersURL(granterSessionResponse.body.sessionId, investigationId))
            .set("Content-Type", "application/json")
            .send(collaborator);
          expect(granterResponse.status).to.equal(200);
        }
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const response = await global.gRequester.delete(getInvestigationUsersURL(sessionId, investigationId)).set("Content-Type", "application/json").send(collaborator);
        expect(response.status).to.equal(expected.status);
        if (response.status >= 300 && granter) {
          const granterSessionResponse = await sessionHelper.doGetSession(granter);
          const revokeResponse = await global.gRequester
            .delete(getInvestigationUsersURL(granterSessionResponse.body.sessionId, investigationId))
            .set("Content-Type", "application/json")
            .send(collaborator);
          expect(revokeResponse.status).to.equal(200);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

function getInvestigationUsersURL(sessionId, investigationId) {
  return `/catalogue/${sessionId}/investigation/id/${investigationId}/investigationusers`;
}
