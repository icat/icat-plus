require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const cacheResource = require("../resources/cache.resource.js");
const { expect } = require("chai");

describe("Cache", () => {
  describe("post /cache", () => {
    it.each(cacheResource.reload, "%s", ["description"], async (element, next) => {
      const { user, expected } = element;
      try {
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const response = await global.gRequester.post(`/cache/${sessionId}`).send();
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
