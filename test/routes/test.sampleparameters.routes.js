require("it-each")({ testPerIteration: true });

const sampleParametersResource = require("../resources/sampleparameters.resource.js");
const sampleparametersHelper = require("../helper/sampleparameter.js");
const sessionHelper = require("../helper/session.js");
const { deleteSampleParameters } = require("../../app/controllers/samples.controller.js");

describe("SampleParameters", () => {
  describe("PUT /catalogue/{sessionId}/sampleParameters", () => {
    it.each(sampleParametersResource.updateSampleParameter, "%s", ["description"], async (element, next) => {
      try {
        const { investigationId, parameterId, value, user, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const newvalue = value + Date.now();
        const response = await sampleparametersHelper.updateSampleParameter(sessionId, investigationId, parameterId, newvalue);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.value).to.equal(newvalue);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
  describe("POST /catalogue/{sessionId}/sample/{sampleId}/sampleParameters", () => {
    it.each(sampleParametersResource.createSampleParameter, "%s", ["description"], async (element, next) => {
      try {
        const { investigationId, sampleId, name, value, user } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const response = await sampleparametersHelper.createSampleParameter(sessionId, investigationId, sampleId, name, value);
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status === 200) {
          // Remove from database the new sampleParameter
          const sampleParameters = JSON.parse(response.text);
          const sampleParameterIds = [sampleParameters.id];
          const deleteSampleParameter = await deleteSampleParameters(sampleParameterIds);
          expect(deleteSampleParameter).to.equal("");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
