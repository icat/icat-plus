require("it-each")({ testPerIteration: true });

const dataCollectionResource = require("../resources/dataCollection.resource.js");
const sessionHelper = require("../helper/session.js");

describe("Datacollections", () => {
  describe("/catalogue/:sessionId/datacollection", () => {
    it.each(dataCollectionResource.byDataCollections, "[%s]", ["description"], async (element, next) => {
      try {
        const { user, datasetId, limit, search, expected } = element;
        const response = await this.getDataCollectionBy(user, datasetId, limit, search);
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.an("array");
          if (limit) {
            expect(response.body.length).to.be.at.most(limit);
          }
        }
        return next();
      } catch {
        return next();
      }
    });
  });
});

exports.getDataCollectionBy = async (user, datasetId, limit, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  if (datasetId) params.set("datasetId", datasetId);
  if (limit) params.set("limit", limit);
  if (search) params.set("search", search);
  return global.gRequester.get(`/catalogue/${sessionId}/datacollection?${params.toString()}`).set("Content-Type", "application/json").send();
};
