require("it-each")({ testPerIteration: true });

const investigationsResource = require("../resources/investigations.resource.js");
const investigationHelper = require("../helper/investigation.js");
const { expect } = require("chai");

describe("Investigations", () => {
  function checkLimitParameterInResponse(limit, response) {
    if (limit) {
      expect(response.body.length).to.be.at.most(limit);
    }
  }

  function checkInvestigationDates(startDate, endDate, response) {
    if (startDate) {
      response.body.forEach((inv) => {
        expect(inv.endDate === undefined || new Date(inv.endDate) >= new Date(startDate)).to.be.true;
      });
    }
    if (endDate) {
      response.body.forEach((inv) => {
        expect(inv.startDate === undefined || new Date(inv.startDate) <= new Date(endDate)).to.be.true;
      });
    }
  }

  function checkNoIndustrials(investigation, industrials) {
    industrials.forEach((industrialType) => {
      expect(investigation.name.toLowerCase()).to.not.have.string(industrialType.toLowerCase());
    });
  }

  describe("/catalogue/:sessionId/investigation", () => {
    it.each(
      [...investigationsResource.getInvestigations, ...investigationsResource.getInvestigationByInstrument, ...investigationsResource.getInvestigationsByPeriod],
      "%s",
      ["description"],
      async (element, next) => {
        try {
          const { user, instrument, limit, startDate, endDate, search, withHasAccess, parameters } = element;
          const response = await investigationHelper.getInvestigationBy(user, { instrumentName: instrument, limit, startDate, endDate, search, withHasAccess, parameters });

          response.body.forEach((investigation) => {
            expect(investigation).to.have.property("meta");
            expect(investigation.meta).to.have.property("page");
            expect(investigation.meta.page).to.have.property("totalPages").is.not.null;
            expect(investigation.meta.page).to.have.property("currentPage").is.not.null;
            expect(investigation.meta.page).to.have.property("totalWithoutFilters").is.not.null;
            expect(investigation.meta.page).to.have.property("total").is.not.null;
          });

          expect(response.status).to.equal(element.expected.status);
          if (element.expected.status < 300) {
            expect(response.body).to.be.an("array");
            expect(response.body.length).to.be.above(0);
          }
          checkLimitParameterInResponse(limit, response);
          checkInvestigationDates(startDate, endDate, response);
          if (instrument) {
            const instrumentNameUpperCase = instrument.toUpperCase();
            response.body.forEach((inv) => {
              expect(inv.instrument).not.to.be.null;
              expect(instrumentNameUpperCase).includes(inv.instrument.name.toUpperCase());
            });
          }
          if (withHasAccess) {
            response.body.forEach((inv) => {
              expect(inv.canAccessDatasets).not.to.be.null;
            });
          }
          return next();
        } catch (e) {
          return next(e);
        }
      }
    );
  });

  describe("/catalogue/:sessionId/investigation", () => {
    it.each(investigationsResource.allocateInvestigations, "%s", ["description"], async (element, next) => {
      try {
        const { instrumentName, investigationName, time, user, expected } = element;
        const response = await investigationHelper.getInvestigationBy(user, { instrumentName, time, investigationName });
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          if (expected.visitId) {
            expect(response.body[0].visitId).to.equal(expected.visitId);
          } else {
            expect(response.body.length).to.equal(0);
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation?ids=[...]", () => {
    it.each(investigationsResource.getMultipleInvestigations, "%s", ["description"], async (element, next) => {
      try {
        const response = await investigationHelper.getInvestigationBy(element.user, { ids: element.ids });
        expect(response.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(response.body).to.be.an("array");
          expect(response.body.length).to.equal(element.expected.numberInvestigations);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation/filter=", () => {
    it.each(
      [
        ...investigationsResource.getInvestigationByReleasedStatus,
        ...investigationsResource.getInvestigationByEmbargoedStatus,
        ...investigationsResource.getInvestigationByParticipantStatus,
      ],
      "[%s]: %s",
      ["status", "description"],
      async (element, next) => {
        try {
          const { user, status, limit, startDate, endDate, sortBy, sortOrder, search, instrumentName } = element;
          const response = await investigationHelper.getInvestigationBy(user, { instrumentName, filter: status, limit, startDate, endDate, sortBy, sortOrder, search });

          expect(response.status).to.equal(element.expected.status);
          if (element.expected.status < 300) {
            expect(response.body).to.be.an("array");
            response.body.forEach((inv) => {
              expect(inv.releaseDate).not.to.be.null;
              if (status === "released") {
                expect(inv.releaseDate).not.to.be.null;
                expect(new Date(inv.releaseDate) <= new Date()).to.be.true;
                expect(inv.doi).not.to.be.null;
                checkNoIndustrials(inv, element.expected.industrials);
              }
              if (status === "embargoed") {
                expect(new Date(inv.releaseDate) > new Date()).to.be.true;
                checkNoIndustrials(inv, element.expected.industrials);
              }

              if (instrumentName) {
                expect(inv.instrument.name.toUpperCase()).equal(instrumentName.toUpperCase());
              }
            });
            checkLimitParameterInResponse(limit, response);
            checkInvestigationDates(startDate, endDate, response);
          }
          return next();
        } catch (e) {
          return next(e);
        }
      }
    );
  });

  describe("/catalogue/:sessionId/investigation/username=", () => {
    it.each(investigationsResource.getInvestigationsByUsername, "%s", ["description"], async (element, next) => {
      try {
        const { user, expected, username } = element;
        const response = await investigationHelper.getInvestigationBy(user, { username });
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.an("array");
          expect(response.body.length).greaterThan(0);
          response.body.forEach((investigation) => {
            expect(investigation.investigationUsers.length).greaterThan(0);
            const investigationUsers = investigation.investigationUsers.map((u) => u.user.name.split("/")[0]);
            expect(investigationUsers).to.include(username);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/catalogue/:sessionId/investigation/timeline", () => {
    it.each(investigationsResource.getTimeline, "%s", ["description"], async (element, next) => {
      try {
        const { user, instrument } = element;
        const response = await investigationHelper.getTimelineBy(user, { instrument });
        expect(response.status).to.equal(element.expected.status);

        if (response.body.length > 0) {
          response.body.forEach((timelineElement) => {
            ["startDate", "endDate", "investigationId", "investigationName", "instrumentName", "__fileCount", "__volume", "__elapsedTime"].forEach((e) =>
              expect(timelineElement).to.have.property(e)
            );

            if (instrument) {
              expect(timelineElement.instrument).equal(instrument);
            }
          });
        }
      } catch (e) {
        return next(e);
      }

      return next();
    });
  });

  describe("/catalogue/investigation/name/:investigationName/normalize", () => {
    it.each(investigationsResource.normalize, "[%s -> %s]", ["input", "expected"], (element, next) => {
      const { input, expected } = element;
      global.gRequester
        .get(`/catalogue/investigation/name/${input}/normalize`)
        .set("Content-Type", "application/json")
        .send()
        .end((_err, response) => {
          expect(response.status).to.equal(expected.status);
          if (expected.status < 300) {
            expect(response.text).to.equal(expected.name);
          }
          next();
        });
    });
  });
});
