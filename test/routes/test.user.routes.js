require("it-each")({ testPerIteration: true });

const userResource = require("../resources/user.resource.js");
const sessionHelper = require("../helper/session.js");

describe("Users", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
  });

  describe("GET /catalogue/:sessionId/user", () => {
    it.each(userResource.getUsers, "%s", ["description"], (element, next) => {
      sessionHelper.doGetSession(element.user).then((getSessionResponse) => {
        expect(getSessionResponse.status).equal(200);
        global.gRequester
          .get(`/catalogue/${getSessionResponse.body.sessionId}/user`)
          .set("Content-Type", "application/json")
          .send()
          .end((err, response) => {
            if (!err) {
              expect(response.status).to.equal(element.expected.status);
              return next();
            }
            return next(err);
          });
      });
    });
  });

  describe("GET /catalogue/{sessionId}/investigation/{:investigationId}/user", () => {
    it.each(userResource.getUsersForInvestigation, "%s", ["description"], async (element, next) => {
      try {
        const { user, investigationId, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        const response = await global.gRequester.get(`/catalogue/${sessionId}/investigation/${investigationId}/user`).send();
        const { body } = response;
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          expect(body).to.be.an("array");
          expect(body).to.be.an("array").that.is.not.empty;
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /users/:sessionId/preferences", () => {
    it.each(userResource.getUserPreferences, "%s", ["description"], async (element, next) => {
      try {
        const { user, prepopulatedPreferences, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        await this.prepopulatePreferences(prepopulatedPreferences);
        const url = getUserPreferencesURL(sessionId);
        const response = await global.gRequester.get(url).send();
        const { body } = response;
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          if (expected.preferences) {
            checkUserPreferences(body, expected.preferences);
          } else {
            expect(body).to.be.deep.equal({});
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("PUT /users/:sessionId/preferences", () => {
    it.each(userResource.updateUserPreferences, "%s", ["description"], async (element, next) => {
      try {
        const { user, prepopulatedPreferences, userpreferences, expected } = element;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        await this.prepopulatePreferences(prepopulatedPreferences);
        const url = getUserPreferencesURL(sessionId);
        const response = await global.gRequester.put(url).send(userpreferences);
        const { body } = response;
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          if (expected.preferences) {
            checkUserPreferences(body, expected.preferences);
          } else {
            expect(body).to.be.deep.equal({});
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

describe("PUT simultaneously /users/:sessionId/preferences", () => {
  it.each(userResource.updateSimultaneouslyUserPreferences, "%s", ["description"], async (element, next) => {
    try {
      const { user, prepopulatedPreferences, userpreferences, expected } = element;
      const sessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = sessionResponse.body;
      await this.prepopulatePreferences(prepopulatedPreferences);
      const url = getUserPreferencesURL(sessionId);
      const [response1, response2] = await Promise.all([global.gRequester.put(url).send(userpreferences), global.gRequester.put(url).send(userpreferences)]);
      expect(response1.status).to.equal(expected.status);
      expect(response2.status).to.equal(expected.status);
      if (expected.status < 300) {
        checkUserPreferences(response1.body, expected.preferences);
        checkUserPreferences(response2.body, expected.preferences);
      }
      return next();
    } catch (e) {
      return next(e);
    }
  });
});

function checkSelection(userpreferences, expected) {
  const selection = userpreferences.selection;
  const expectedSelection = expected.selection;
  if (expectedSelection) {
    if (expectedSelection.datasetIds) {
      expect(selection.datasetIds.length).to.equal(expectedSelection.datasetIds.length);
      expectedSelection.datasetIds.forEach((datasetId, index) => {
        expect(selection.datasetIds[index]).to.equal(datasetId);
      });
    }
    if (expectedSelection.sampleIds) {
      expect(selection.sampleIds.length).to.equal(expectedSelection.sampleIds.length);
      expectedSelection.sampleIds.forEach((sampleId, index) => {
        expect(selection.sampleIds[index]).to.equal(sampleId);
      });
    }
  } else {
    expect(selection).to.be.deep.equal(expectedSelection);
  }
}

function checkLogbookFilter(userpreferences, expected) {
  const logbookFilters = userpreferences.logbookFilters;
  const expectedLogbookFilters = expected.logbookFilters;
  if (expectedLogbookFilters) {
    expect(logbookFilters.sortBy).to.be.equal(expectedLogbookFilters.sortBy);
    expect(logbookFilters.sortOrder).to.be.equal(expectedLogbookFilters.sortOrder);
    expect(logbookFilters.types).to.be.equal(expectedLogbookFilters.types);
  } else {
    expect(typeof logbookFilters).to.eq("undefined");
  }
}

function checkRecentlyVisited(userpreferences, expected) {
  if (expected.recentlyVisited) {
    expected.recentlyVisited.forEach((visit, index) => {
      const element = userpreferences.recentlyVisited[index];
      expect(element.url).to.be.equal(visit.url);
      expect(element.label).to.be.equal(visit.label);
      expect(element.target.instrumentName).to.be.equal(visit.target.instrumentName);
      expect(element.target.investigationName).to.be.equal(visit.target.investigationName);
      expect(element.target.investigationStartDate).to.be.equal(visit.target.investigationStartDate);
      expect(element.target.investigationEndDate).to.be.equal(visit.target.investigationEndDate);
    });
  } else {
    expect(userpreferences.recentlyVisited.length).to.equal(0);
  }
}

function checkMXShell(shell, expectedShell) {
  if (expectedShell) {
    checkMXShell(shell.inner, expectedShell.inner);
    checkMXShell(shell.outer, expectedShell.outer);
    checkMXShell(shell.overall, expectedShell.overall);
  } else {
    expect(typeof shell).to.eq("undefined");
  }
}

function checkMXCutoffs(mxsettings, expectedMxSettings) {
  const cutoffs = mxsettings.cutoffs;
  const expectedCutoffs = expectedMxSettings.cutoffs;
  if (expectedCutoffs) {
    checkMXShell(cutoffs.completeness, expectedCutoffs.completeness);
    checkMXShell(cutoffs.resolution_limit_low, expectedCutoffs.resolution_limit_low);
    checkMXShell(cutoffs.resolution_limit_high, expectedCutoffs.resolution_limit_high);
    checkMXShell(cutoffs.r_meas_all_IPlus_IMinus, expectedCutoffs.r_meas_all_IPlus_IMinus);
    checkMXShell(cutoffs.mean_I_over_sigI, expectedCutoffs.mean_I_over_sigI);
    checkMXShell(cutoffs.cc_half, expectedCutoffs.cc_half);
    checkMXShell(cutoffs.cc_ano, expectedCutoffs.cc_ano);
  } else {
    expect(typeof cutoffs).to.eq("undefined");
  }
}

function checkMxSettings(userpreferences, expected) {
  const mxsettings = userpreferences.mxsettings;
  const expectedMxSettings = expected.mxsettings;
  if (expectedMxSettings) {
    expect(mxsettings.autoProcessingRankingShell).to.be.equal(expectedMxSettings.autoProcessingRankingShell);
    expect(mxsettings.autoProcessingRankingParameter).to.be.equal(expectedMxSettings.autoProcessingRankingParameter);
    expect(mxsettings.autoProcessingRankingDisableSpaceGroup).to.be.equal(expectedMxSettings.autoProcessingRankingDisableSpaceGroup);
    checkMXCutoffs(mxsettings, expectedMxSettings);
  } else {
    expect(typeof mxsettings).to.eq("undefined");
  }
}

function checkPagination(userpreferences, expected) {
  global.gLogger.info(`userpreferences ${JSON.stringify(userpreferences)}`);
  global.gLogger.info(`expected ${JSON.stringify(expected)}`);
  const pagination = userpreferences.pagination;
  const expectedPagination = expected.pagination;
  if (expectedPagination) {
    expectedPagination.forEach((paginationInformation, index) => {
      const element = pagination[index];
      global.gLogger.info(`expectedPagination ${JSON.stringify(expectedPagination)}`);
      global.gLogger.info(`element ${JSON.stringify(element)}`);
      expect(element.key).to.be.equal(paginationInformation.key);
      expect(element.value).to.be.equal(paginationInformation.value);
    });
  } else {
    expect(pagination.length).to.equal(0);
  }
}

function checkUserPreferences(userpreferences, expected) {
  expect(userpreferences.user).to.equal(expected.user);
  checkSelection(userpreferences, expected);
  checkLogbookFilter(userpreferences, expected);
  checkRecentlyVisited(userpreferences, expected);
  checkMxSettings(userpreferences, expected);
  expect(userpreferences.isGroupedBySample).to.equal(expected.isGroupedBySample);
  checkPagination(userpreferences, expected);
}

exports.prepopulatePreferences = async (preferences) => {
  const responses = [];
  if (preferences) {
    for (let i = 0; i < preferences.length; i++) {
      const { user, ...rest } = preferences[i];
      const sessionResponse = await sessionHelper.doGetSession(user.credential);
      const { sessionId } = sessionResponse.body;
      const url = getUserPreferencesURL(sessionId);
      rest.user = user.name;
      const response = await global.gRequester.put(url).send(rest);
      expect(response.status).to.equal(200);
      responses.push(response.body);
    }
  }
  return responses;
};

function getUserPreferencesURL(sessionId) {
  return `/users/${sessionId}/preferences`;
}
