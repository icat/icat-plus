const oaipmhResource = require("../resources/oaipmh.resource.js");
const XmlReader = require("xml-reader");
const XmlQuery = require("xml-query");
require("it-each")({ testPerIteration: true });

describe("OAI-PMH", () => {
  describe.skip("/oai-pmh request", () => {
    it.each(oaipmhResource.listIdentifiers, "[%s]", ["description"], (element, next) => {
      // Request the list of identifiers
      global.gRequester
        .get(`/oaipmh/${element.requestListIdentifiers}`)
        .send()
        .end((_err, getOAIResponse) => {
          expect(getOAIResponse.status).to.equal(200);
          const identifiers = XmlQuery(XmlReader.parseSync(getOAIResponse.text)).find("identifier");
          let counter = 1;
          identifiers.each((identifier) => {
            // Request by investigation identifier
            global.gRequester
              .get(`/oaipmh/${element.requestByIdentifier}${identifier.children[0].value}`)
              .send()
              .end((_err, getInvestigationResponse) => {
                expect(getInvestigationResponse.status).to.equal(200);
                counter = counter + 1;
                const title = XmlQuery(XmlReader.parseSync(getInvestigationResponse.text)).find("dc:title").text();
                // check that there are no forbidden proposal types
                element.expected.forbiddenProposalTypes.forEach((forbiddenProposalType) => {
                  expect(title.toLowerCase().startsWith(forbiddenProposalType)).to.be.false;
                });
                // end of tests condition
                if (counter === identifiers.ast.length) {
                  return next();
                }
              });
          });
        });
    });

    it.each(oaipmhResource.listRecords, "[%s]", ["description"], (element, next) => {
      global.gRequester
        .get(`/oaipmh/${element.requestListRecord}`)
        .send()
        .end((_err, getResponse) => {
          expect(getResponse.status).to.equal(200);
          const identifiers = XmlReader.parseSync(getResponse.text);
          element.expected.forbiddenProposalTypes.forEach((forbiddenProposalType) => {
            const concatTitles = XmlQuery(identifiers).find("dc:title").text().toString();
            expect(concatTitles.includes(forbiddenProposalType)).to.be.false;
          });
          next();
        });
    });
  });
});
