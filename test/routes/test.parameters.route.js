const parameterResource = require("../resources/parameter.resource.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("Parameters", () => {
  describe("/catalogue/parameters", () => {
    it.each(parameterResource.getParameters, "%s", ["description"], async (element, next) => {
      try {
        const { expected, parameterTypeId, name } = element;
        let response = null;

        const params = new URLSearchParams();
        if (parameterTypeId) params.set("parameterTypeId", parameterTypeId);
        if (name) params.set("name", name);

        if (parameterTypeId || name) {
          response = await global.gRequester.get(`/catalogue/parameters?${params.toString()}`).set("Content-Type", "application/json").send();
        } else {
          response = await global.gRequester.get(`/catalogue/parameters`).set("Content-Type", "application/json").send();
        }
        expect(response.status).to.be.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.a("array");
          if (parameterTypeId || name) {
            expect(response.body.length).to.be.equal(1);
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
