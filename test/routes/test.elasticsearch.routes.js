const elasticResource = require("../resources/elasticsearch.resource.js");
const sessionHelper = require("../helper/session.js");

require("it-each")({
  testPerIteration: true,
});

describe.skip("/elasticsearch/:sessionId/stats/instrument/metrics", () => {
  it.each(elasticResource.instrumentMetrics, "[%s]", ["description"], async (element, next) => {
    const getSessionResponse = await sessionHelper.doGetSession(element.user);
    try {
      const response = await global.gRequester
        .post(`/elasticsearch/${getSessionResponse.body.sessionId}/stats/instrument/metrics?instrumentName=${element.instrumentName}`)
        .set("Content-Type", "application/json")
        .send({ ranges: element.ranges });

      expect(response.status).to.equal(element.expected.status);
      return next();
    } catch (e) {
      return next(e);
    }
  });
});
