const resource = require("../resources/panosc.resource.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

describe("Panosc", () => {
  describe("/api/db", () => {
    it.each(resource.getDBByInvestigationId, "%s", ["description"], async (element, next) => {
      const { investigationId } = element;
      try {
        const params = new URLSearchParams();
        if (investigationId) params.set("investigationId", investigationId);

        const response = await global.gRequester.get(`/api/db?${params.toString()}`).set("Content-Type", "application/json").send();
        expect(response.status).to.equal(element.expected.status);
      } catch (e) {
        return next(e);
      }
      return next();
    });
  });

  describe.skip("/api/db", () => {
    it.each(resource.getItemsByDateRange, "%s", ["description"], async (element, next) => {
      const { startDate, endDate } = element;
      const params = new URLSearchParams();
      if (startDate) params.set("startDate", startDate);
      if (endDate) params.set("endDate", endDate);

      const response = await global.gRequester.get(`/api/scoring/items?${params.toString()}`).set("Content-Type", "application/json").send();
      expect(response.status).to.equal(element.expected.status);
      return next();
    });
  });
});
