require("it-each")({ testPerIteration: true });

const instrumentResource = require("../resources/instrument.resource.js");
const instrumentHelper = require("../helper/instrument.js");

describe("Instrument", () => {
  describe("GET /catalogue/instrument", () => {
    it.each(instrumentResource.getInstruments, "%s", ["description"], async (element, next) => {
      const getInstrumentResponse = await instrumentHelper.getInstruments();
      expect(getInstrumentResponse.status).to.be.equal(element.expected.status);
      next();
    });
  });

  describe("GET /catalogue/{sessionId}/instruments", () => {
    it.each(instrumentResource.getInstrumentsBySessionId, "%s", ["description"], async (element, next) => {
      try {
        const getInstrumentsBySessionIdResponse = await instrumentHelper.getInstrumentsBySessionId(element.user, element.filter);
        expect(getInstrumentsBySessionIdResponse.status).to.be.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
