require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const datasetHelper = require("../helper/dataset.js");
const resource = require("../resources/ewoks.resource.js");
const { expect } = require("chai");
const nock = require("nock");
const { getInputsLocation } = require("../../app/controllers/ewoks.controller.js");

async function updateJobStatus(sessionId, job, update) {
  const params = new URLSearchParams();
  params.set("id", job._id);
  const response = await global.gRequester.put(`/ewoks/${sessionId}/jobs?${params.toString()}`).set("Content-Type", "application/json").send(update);
  return response.body;
}

describe("Ewoks", () => {
  beforeEach(() => {
    try {
      if (global.gServerConfig.database.isMongoUnitEnabled) {
        global.mongoUnit.drop();
      }
    } catch (e) {
      global.gLogger.error(e);
    }
    if (!nock.isActive()) nock.activate();
    nock.cleanAll();
  });

  afterEach(() => {
    nock.cleanAll();
  });

  const checkLimitParameterInResponse = (limit, response) => {
    if (limit) {
      expect(response.body.length).to.be.at.most(limit);
    }
  };

  const checkPagination = (job) => {
    expect(job.meta).to.not.be.undefined;
    expect(job.meta.page).to.not.be.undefined;
    expect(job.meta.page).to.have.property("total").that.is.a("number");
    expect(job.meta.page).to.have.property("totalPages").that.is.a("number");
    expect(job.meta.page).to.have.property("currentPage").that.is.a("number");
  };

  describe("getInputsLocation", () => {
    it.each(resource.getInputsLocation, "%s", ["description"], async (element, next) => {
      try {
        const { user, datasetIds, expected } = element;
        const getSessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = getSessionResponse.body;
        const datasetResponse = await datasetHelper.getDatasetsBy(user, { datasetIds });
        const dataset = datasetResponse.body[0];
        const location = await getInputsLocation(sessionId, dataset);

        expect(location).to.equal(expected.location);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/ewoks/:sessionId/workflows", () => {
    it.each(resource.getWorkflows, "%s", ["description"], async (element, next) => {
      const { submitter, expected, items, identifier, queue, datasetIds, reader, updates, investigationId, all } = element;
      nock(`${global.gServerConfig.ewoks.server}`).get("/workflows/descriptions").reply(200, {
        items,
      });

      const getSessionResponse = await sessionHelper.doGetSession(submitter);
      const { sessionId } = getSessionResponse.body;
      try {
        const url = `/ewoks/${sessionId}/workflows`;
        const response = await global.gRequester.get(url).send();
        expect(200).to.equal(response.status);

        /** Submit a job */
        const params = new URLSearchParams();
        if (identifier) {
          params.set("identifier", identifier);
          nock(`${global.gServerConfig.ewoks.server}/execute/`).post(`/${identifier}`).reply(200, { job_id: "jobId" });
        }

        if (queue) params.set("queue", queue);
        if (datasetIds) params.set("datasetIds", datasetIds);

        const urlRun = `/ewoks/${sessionId}/jobs?${params.toString()}`;
        const responseRun = await global.gRequester.post(urlRun).set("Content-Type", "application/json").send();

        expect(responseRun.status).to.equal(expected.submitStatus);

        if (expected.submitStatus < 400) {
          /** Get list of jobs */
          const getSessionReaderResponse = await sessionHelper.doGetSession(reader);
          const readerSessionId = getSessionReaderResponse.body.sessionId;

          const getQueryParams = new URLSearchParams();
          if (investigationId) getQueryParams.set("investigationId", investigationId);
          if (all) getQueryParams.set("all", all);

          const responseGet = await global.gRequester.get(`/ewoks/${readerSessionId}/jobs?${getQueryParams.toString()}`);
          expect(responseGet.status).to.equal(expected.readerStatus);

          if (expected.readerStatus < 400) {
            if (expected.jobCount) expect(responseGet.body.length).to.equal(expected.jobCount);

            /** Changing the status of a job */
            if (expected.jobCount > 0 && updates) {
              const jobs = responseGet.body;

              for (const job of jobs) {
                for (const index in updates) {
                  const { step, status, logs } = updates[index];
                  const updatedJob = await updateJobStatus(readerSessionId, job, { step, status, logs });
                  if (status) {
                    expect(updatedJob.status).equal(updates[index].status);
                  }
                  if (step) {
                    const latestStep = updatedJob.steps[updatedJob.steps.length - 1];
                    expect(latestStep.name).equal(updates[index].step.name);
                    expect(latestStep.status).equal(updates[index].step.status.toUpperCase());
                    expect(latestStep.message).equal(updates[index].step.message);
                  }
                }
              }
            }
          }
        }

        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/ewoks/:sessionId/jobs", () => {
    it.each(resource.getJobs, "%s", ["description"], async (element, next) => {
      try {
        const {
          prepopulateUser,
          user,
          prepopulatedJobs,
          investigationId,
          sampleIds,
          datasetIds,
          jobId,
          skip,
          limit,
          sortBy,
          sortOrder,
          all,
          status,
          endDate,
          search,
          expected,
        } = element;
        nock(`${global.gServerConfig.ewoks.server}/execute/`).post(`/identifier1`).reply(200, { job_id: "jobId1" });
        nock(`${global.gServerConfig.ewoks.server}/execute/`).post(`/identifier2`).reply(200, { job_id: "jobId2" });
        nock(`${global.gServerConfig.ewoks.server}/execute/`).post(`/identifier3`).reply(200, { job_id: "jobId3" });
        const prepopulatedSessionResponse = await sessionHelper.doGetSession(prepopulateUser);
        const getPrepopulatedSessionResponse = prepopulatedSessionResponse.body;
        const sessionResponse = await sessionHelper.doGetSession(user);
        const { sessionId } = sessionResponse.body;
        await this.prepopulateJobs(getPrepopulatedSessionResponse.sessionId, prepopulatedJobs);
        const url = getJobEndPointURL(sessionId, { investigationId, sampleIds, datasetIds, jobId, skip, limit, sortBy, sortOrder, all, status, endDate, search });
        const response = await global.gRequester.get(url).send();
        const { body } = response;
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          expect(body.length).equal(expected.jobs.length);
          for (let i = 0; i < body.length; i++) {
            const job = body[i];
            expect(job.identifier).equal(expected.jobs[i].identifier);
            checkPagination(job);
          }
          checkLimitParameterInResponse(limit, response);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});

exports.prepopulateJobs = async (sessionId, jobs) => {
  const responses = [];
  for (let i = 0; i < jobs.length; i++) {
    const job = jobs[i];
    const url = getJobEndPointURL(sessionId, job);
    const response = await global.gRequester.post(url).send(job);
    expect(response.status).to.equal(200);
    let body = response.body;
    if (job.status) {
      // update the job status, since it is set as "CREATED" when a job is created.
      const responseUpdate = await global.gRequester.put(`${url}?&id=${body.id}`).send(job);
      expect(responseUpdate.status).to.equal(200);
      body = responseUpdate.body;
    }
    responses.push(body);
  }
  return responses;
};

function getJobEndPointURL(sessionId, queryParams) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return `/ewoks/${sessionId}/jobs?${params.toString()}`;
}
