require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const h5groveResource = require("../resources/h5grove.resource.js");
const { expect } = require("chai");

describe("H5Grove", () => {
  describe("/h5grove", () => {
    it.each(h5groveResource.request, "%s", ["description"], async (element, next) => {
      const { user, datafileId, endPoint, expected } = element;
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      try {
        const params = new URLSearchParams();

        params.set("sessionId", sessionId);
        params.set("datafileId", datafileId);

        const url = `${endPoint}?${params.toString()}`;
        const response = await global.gRequester.get(url).send();
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
