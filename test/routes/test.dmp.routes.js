require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const dmpResource = require("../resources/dmp.resource.js");
const { expect } = require("chai");
const nock = require("nock");

describe("DMP", () => {
  describe("/dmp/:sessionId/questionnaires", () => {
    it.each(dmpResource.questionnaires, "%s", ["description"], async (element, next) => {
      const { user, investigationId, investigationName, expected } = element;
      nock(global.gServerConfig.dmp.server).post(`/tokens`).reply(200, { token: "tokenTest" });
      nock(global.gServerConfig.dmp.server)
        .get(`/questionnaires?size=1&q=exitingInvestigationName`)
        .reply(200, { _embedded: { questionnaires: [{ uuid: "a-uuid" }] } });
      nock(global.gServerConfig.dmp.server)
        .get(`/questionnaires?size=1&q=nonExitingInvestigationName`)
        .reply(200, { _embedded: { questionnaires: [] } });
      nock(global.gServerConfig.dmp.server).get(`/questionnaires?size=1&q=failed`).reply(500);
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      try {
        const url = `/dmp/${sessionId}/questionnaires?investigationId=${investigationId}&investigationName=${investigationName}`;
        const response = await global.gRequester.get(url).set("Content-Type", "application/json").send();
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body.length, expected.questionnaires.length);
          response.body.forEach((q, id) => {
            expect(q.uuid).to.equal(expected.questionnaires[id].uuid);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
