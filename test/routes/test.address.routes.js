require("it-each")({ testPerIteration: true });

const addressResource = require("../resources/address.resource.js");
const addressHelper = require("../helper/address.js");
const { expect } = require("chai");

describe("Tracking Address", () => {
  /**
   * Address creation
   */
  describe("POST /tracking/{sessionId}/investigation/id/{investigationId}/address", () => {
    it.each(addressResource.createAddress, "%s", ["description"], async (element, next) => {
      try {
        const response = await addressHelper.createAddress(element.user, element.address, element.investigationId);
        expect(response.status).to.equal(element.expected.status);
        if (response.status < 300) {
          /** Check that the addresses can be retrieved */
          const getAddressResponse = await addressHelper.getAddress(element.user);
          /** There is at least one address */
          expect(getAddressResponse.body).to.have.lengthOf.above(0);
          /** It contains the address just created */
          expect(getAddressResponse.body.find((element) => element._id === response.body._id)).to.not.be.null;
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Get Address by investigationId
   */
  describe("GET /tracking/{sessionId}/investigation/id/{investigationId}/address", () => {
    it.each(addressResource.getAddressByInvestigationId, "%s", ["description"], async (element, next) => {
      try {
        const getAddressByInvestigationIdResponse = await addressHelper.getAddressByInvestigationId(element.user, element.investigationId);
        expect(getAddressByInvestigationIdResponse.status).to.equal(element.expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Get Address by sessionId
   */
  describe("GET /tracking/{sessionId}/address", () => {
    it.each(addressResource.getAddressByUsername, "%s", ["description"], async (element, next) => {
      try {
        const getAddressResponse = await addressHelper.getAddress(element.user);
        expect(getAddressResponse.status).equal(200);
        expect(getAddressResponse.status).to.equal(element.expected.status);
        return next();
      } catch (err) {
        return next(err);
      }
    });
  });

  /**
   * This test
   * 1) creates an address
   * 2) Update the address
   */
  describe("PUT /tracking/{sessionId}/investigation/id/{investigationId}/address", () => {
    it.each(addressResource.modifyAddress, "%s", ["description"], async (element, next) => {
      try {
        const createAddressResponse = await addressHelper.createAddress(element.addressCreator, element.address, element.investigationId);
        expect(createAddressResponse.status).to.equal(200);
        const updatedAddress = createAddressResponse.body;
        Object.assign(updatedAddress, element.modified);
        const updateAddressResponse = await addressHelper.updateAddress(element.addressUpdator, updatedAddress, element.investigationId);
        expect(updateAddressResponse.status).to.equal(element.expected.status);
        if (updateAddressResponse.status < 300) {
          expect(updateAddressResponse.body.investigationId).to.equal(element.expected.address.investigationId);
          expect(updateAddressResponse.body).to.include(element.expected.address);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * This test
   * 1) creates an address
   * 2) deletes the address
   */
  describe("DELETE /tracking/{sessionId}/investigation/id/{investigationId}/address", () => {
    it.each(addressResource.deleteAddress, "%s", ["description"], async (element, next) => {
      try {
        /** Create address */
        const createAddressResponse = await addressHelper.createAddress(element.addressCreator, element.address, element.investigationId);
        expect(createAddressResponse.status).to.equal(200);

        /** Remove address */
        const deleteAddressResponse = await addressHelper.deleteAddress(element.addressRemover, createAddressResponse.body, element.investigationId);
        expect(deleteAddressResponse.status).to.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(deleteAddressResponse.body.status).to.be.equal("REMOVED");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
