require("it-each")({ testPerIteration: true });

const trackingResource = require("../resources/shipment.resource.js");
const shipmentHelper = require("../helper/shipment.js");
const addressHelper = require("../helper/address.js");
const { expect } = require("chai");

const testShipment = (shipment) => {
  expect(shipment._id).to.not.equal(null);
  expect(shipment.investigationId).to.not.equal(null);
  expect(shipment.investigationName).to.not.equal(null);
};

describe("Tracking Shipment", () => {
  describe("GET /tracking/{sessionId}/shipment getShipmentsByInvestigationId", () => {
    it.each(trackingResource.getShipmentsByInvestigationId, "%s", ["description"], async (element, next) => {
      try {
        const { investigationId, user, expected } = element;
        const responseGetShiomentById = await shipmentHelper.getShipmentBy(user, { investigationId });
        expect(responseGetShiomentById.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("GET /tracking/{sessionId}/shipment getShipmentById", () => {
    it.each(trackingResource.getShipmentById, "%s", ["description"], async (element, next) => {
      const { shipment, investigationId, shipmentCreator, shipmentReader, expected } = element;
      const responseShipmentCreation = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
      const responseGetShiomentById = await shipmentHelper.getShipmentBy(shipmentReader, { investigationId, shipmentId: responseShipmentCreation.body._id });
      try {
        expect(responseGetShiomentById.status).to.equal(expected.status);
        if (shipment.status < 300) {
          testShipment(responseGetShiomentById.body);
          expect(responseGetShiomentById.body._id).to.equal(responseShipmentCreation.body._id);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("DELETE /tracking/{sessionId}/shipment", () => {
    it.each(trackingResource.deleteShipment, "%s", ["description"], async (element, next) => {
      try {
        const { shipmentCreator, shipmentRemover, investigationId, shipment, expected } = element;
        const createShipmentResponse = await shipmentHelper.createShipment(shipmentCreator, shipment, investigationId);
        const deleteShipmentResponse = await shipmentHelper.deleteShipment(shipmentRemover, createShipmentResponse.body, investigationId);

        expect(deleteShipmentResponse.status).to.equal(expected.status);
        if (deleteShipmentResponse.status < 300) {
          const getShipmentByIdResponse = await shipmentHelper.getShipmentBy(shipmentCreator, { investigationId, shipmentId: createShipmentResponse.body._id });
          expect(getShipmentByIdResponse.body).to.be.empty;
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("POST /tracking/{sessionId}/shipment", () => {
    it.each(trackingResource.createShipment, "%s", ["description"], async (element, next) => {
      try {
        if (element.shippingAddress) {
          const shippingAddressCreationResponse = await addressHelper.createAddress(element.user, element.shippingAddress, element.investigationId);
          element.shipment.defaultShippingAddress = shippingAddressCreationResponse.body._id;
        }

        if (element.returnAddress) {
          const addressCreationResponse = await addressHelper.createAddress(element.user, element.returnAddress, element.investigationId);
          element.shipment.defaultReturnAddress = addressCreationResponse.body;
        }

        const shipmentCreationResponse = await shipmentHelper.createShipment(element.user, element.shipment, element.investigationId);
        expect(shipmentCreationResponse.status).to.equal(element.expected.status);
        testShipment(shipmentCreationResponse.body);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
