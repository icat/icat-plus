const sampleResource = require("../resources/sample.resource.js");
const { getSamplesBy, getSamplesByInvestigationId, samplePage } = require("../helper/sample.js");
const { expect } = require("chai");

require("it-each")({
  testPerIteration: true,
});

/**
 * This checks the format of the sample
 * @param {*} sample
 */
const checkSampleFormat = (sample) => {
  expect(sample).to.have.property("id");
  expect(sample).to.have.property("name");
  expect(sample).to.have.property("parameters");
  expect(sample.parameters).to.be.an("array");
  if (sample.parameters.length > 0) {
    for (let i = 0; i < sample.parameters.length; i++) {
      const parameter = sample.parameters[i];
      expect(parameter).to.have.property("id");
      expect(parameter).to.have.property("name");
      //expect(parameter).to.have.property("value");
      expect(parameter).to.have.property("units");
    }
  }
  expect(sample).to.have.property("meta");
  expect(sample.meta).to.have.property("page");
  expect(sample.meta.page).to.have.property("totalPages").is.not.null;
  expect(sample.meta.page).to.have.property("currentPage").is.not.null;
  expect(sample.meta.page).to.have.property("totalWithoutFilters").is.not.null;
  expect(sample.meta.page).to.have.property("total").is.not.null;
};

const checkLimitParameterInResponse = (limit, response) => {
  if (limit) {
    expect(response.body.length).to.be.at.most(limit);
  }
};

describe("[DEPRECATED] /catalogue/:sessionId/investigation/id/:investigationId/sample", () => {
  it.each(sampleResource.getSamplesByInvestigationId, "%s, %s", ["description", "investigationId"], async (element, next) => {
    const response = await getSamplesByInvestigationId(element.user, element.investigationId);
    expect(response.status).to.equal(element.expected.status);
    const samples = response.body;
    expect(samples).to.be.an("array");
    return next();
  });
});

describe("/catalogue/:sessionId/samples", () => {
  it.each(sampleResource.getSamplesBy, "%s", ["description"], async (element, next) => {
    const { user, investigationId, sampleIds, search, expected, datasetType, nested, limit, parameters, havingAcquisitionDatasets, instrumentName } = element;
    const { count, status } = expected;
    // if havingAcquisitionDatasets we force the loading of the datasets, so we can check that we only load samples with datasets.
    const includeDatasets = havingAcquisitionDatasets;
    const response = await getSamplesBy(user, {
      search,
      investigationId,
      sampleIds,
      datasetType,
      nested,
      limit,
      parameters,
      havingAcquisitionDatasets,
      includeDatasets,
      instrumentName,
    });
    expect(response.status).to.equal(status);

    if (element.expected.status < 400) {
      const samples = response.body;
      expect(samples).to.be.an("array");
      checkLimitParameterInResponse(limit, response);

      if (samples.length > 0) {
        for (let i = 0; i < samples.length; i++) {
          checkSampleFormat(samples[i]);
        }
      }
      /** Check samples */
      if (sampleIds) {
        for (let i = 0; i < samples.length; i++) {
          expect(sampleIds.toString().indexOf(samples[i].id) !== -1).to.be.true;
        }
      }
      if (count) {
        expect(samples.length).equal(count);
      }

      if (datasetType) {
        samples.forEach((sample) => {
          sample.datasets.forEach((dataset) => {
            expect(dataset.type).equals(datasetType);
          });
        });
      }
      if (instrumentName) {
        samples.forEach((sample) => {
          expect(instrumentName.toUpperCase()).includes(sample.investigation.instrument.name.toUpperCase());
        });
      }
      if (nested) {
        samples.forEach((sample) => {
          sample.datasets.forEach((dataset) => {
            const outputDatasetParameter = dataset.parameters.find((param) => param.name === "__full_output_datasetIds");
            if (outputDatasetParameter != null) {
              const outputDatasetIds = outputDatasetParameter.value.split(" ");
              expect(dataset.outputDatasets.length).equal(outputDatasetIds.length);
            }
          });
        });
      }
      if (limit) {
        samples.forEach((sample) => {
          expect(sample).to.have.property("meta");
          expect(sample.meta).to.have.property("page");
          expect(sample.meta.page).to.have.property("totalPages").is.not.null;
          expect(sample.meta.page).to.have.property("currentPage").is.not.null;
          expect(sample.meta.page).to.have.property("totalWithoutFilters").is.not.null;
          expect(sample.meta.page).to.have.property("total").is.not.null;
        });
      }
      if (havingAcquisitionDatasets) {
        samples.forEach((sample) => {
          expect(sample.datasets.length).greaterThan(0);
        });
      }
    }
    return next();
  });

  describe("/catalogue/{sessionId}/samples/{sampleId}/page", () => {
    it.each(sampleResource.page, "[%s]", ["description"], async (element, next) => {
      try {
        const { expected, user, sampleId, investigationId, sampleIds, search, datasetType, limit, parameters, instrumentName } = element;
        const response = await samplePage(user, sampleId, {
          search,
          investigationId,
          sampleIds,
          datasetType,
          limit,
          parameters,
          instrumentName,
        });
        expect(response.status).to.equal(expected.status);
        const { body } = response;
        if (response.status < 300) {
          expect(body.page).to.not.be.undefined;
          expect(body.page).to.have.property("total").that.is.a("number");
          expect(body.page).to.have.property("totalPages").that.is.a("number");
          expect(body.page).to.have.property("currentPage").that.is.a("number");
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
