require("it-each")({ testPerIteration: true });

const techniqueResource = require("../resources/technique.resource.js");
const techniqueHelper = require("../helper/technique.js");

describe("Technique", () => {
  describe("GET /catalogue/techniques", () => {
    it.each(techniqueResource.getTechniques, "%s", ["description"], async (element, next) => {
      const response = await techniqueHelper.getTechniques();
      expect(response.status).to.be.equal(element.expected.status);
      if (response.status < 300) {
        const { body } = response;
        expect(body).to.be.an("array");
        body.forEach((technique) => {
          expect(technique).to.have.property("pid");
          expect(technique).to.have.property("name");
        });
      }
      next();
    });
  });
});
