require("it-each")({ testPerIteration: true });
const sessionHelper = require("../helper/session.js");
const doiResource = require("../resources/doi.resource.js");
const { expect } = require("chai");
const nock = require("nock");

describe("DOI", () => {
  before(async () => {
    if (!nock.isActive()) nock.activate();
    nock.cleanAll();
  });

  afterEach(async () => {
    nock.cleanAll();
  });

  describe("/doi/:sessionId/mint", () => {
    it.each(doiResource.mint, "%s", ["description"], async (element, next) => {
      const { user, datasetIdList, title, abstract, authors, reserveDataCollection, reservedDatasetIdList, keywords, citationPublicationDOI, referenceURL, expected } = element;
      nock(global.gServerConfig.datacite.mds).post("/metadata").reply(200, {});
      nock(global.gServerConfig.datacite.mds).post(`/doi`).query(true).reply(200, {});
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      let dataCollectionId;
      try {
        if (reserveDataCollection) {
          const response = await global.gRequester.post(`/doi/${sessionId}/reserve`).send({ datasetIdList: reservedDatasetIdList });
          expect(response.status).to.equal(expected.status);
          const reservedDoi = response.body.message;
          const parts = reservedDoi.split("-");
          dataCollectionId = parts[parts.length - 1];
        }
        const response = await global.gRequester
          .post(`/doi/${sessionId}/mint`)
          .send({ datasetIdList, title, abstract, authors, dataCollectionId, keywords, citationPublicationDOI, referenceURL });
        expect(response.status).to.equal(expected.status);
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  function checkDOIUser(expected, user) {
    expect(user.name).equal(expected.name);
    if (expected.nameIdentifiers) {
      expect(user.nameIdentifiers).not.to.be.undefined;
      expect(user.nameIdentifiers.length).to.equal(1);
      expect(user.nameIdentifiers[0].nameIdentifier).equal(expected.nameIdentifiers[0].nameIdentifier);
    } else {
      expect(user.nameIdentifiers).to.be.undefined;
    }
    if (expected.contributorType) {
      expect(user.contributorType).equal(expected.contributorType);
    } else {
      expect(user.contributorType).to.be.undefined;
    }
  }

  describe("GET /doi/:prefix/:suffix/json-datacite", async () => {
    it.each(doiResource.datacite, "[%s]", ["description"], async (element, next) => {
      try {
        const { prefix, suffix, expected } = element;
        /** Do query */
        const queryResponse = await global.gRequester.get(`/doi/${prefix}/${suffix}/json-datacite`).send();
        expect(queryResponse.status).to.equal(expected.status);
        if (queryResponse.status < 300) {
          const { subjects, creators, contributors, relatedIdentifiers } = queryResponse.body;
          expect(subjects.length).equal(3);
          expect(subjects[0].subjectScheme).equal("Proposal Type Description");
          expect(subjects[0].subject).equal(expected.proposalTypeDescription);
          expect(subjects[1].subjectScheme).equal("Proposal");
          expect(subjects[1].subject).equal(expected.name);
          expect(subjects[2].subjectScheme).equal("Instrument");
          expect(subjects[2].subject).equal(expected.instrument);
          expect(creators.length).equal(expected.creators.length);
          creators.forEach((creator, id) => {
            checkDOIUser(creator, expected.creators[id]);
          });
          expect(contributors.length).equal(expected.contributors.length);
          contributors.forEach((contributor, id) => {
            checkDOIUser(contributor, expected.contributors[id]);
          });
          if (expected.relatedIdentifiers) {
            expect(relatedIdentifiers).not.to.be.null;
            expect(relatedIdentifiers.length).equal(expected.relatedIdentifiers.length);
            relatedIdentifiers.forEach((relatedIdentifier, id) => {
              expect(relatedIdentifier.relatedIdentifier).equal(expected.relatedIdentifiers[id].relatedIdentifier);
            });
          }
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/doi/:prefix/:suffix/reports", () => {
    it.each(doiResource.expReports, "%s", ["description"], async (element, next) => {
      const { prefix, suffix, expected } = element;
      const reportName = "report.pdf";
      nock(global.gServerConfig.experimentalReports.server).get(/.*/).reply(200, [reportName]);
      try {
        const url = `/doi/${prefix}/${suffix}/reports`;
        const response = await global.gRequester.get(url).set("Content-Type", "application/json").send();
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          const reports = response.body;
          expect(reports.length).equal(expected.reports.length);
          reports.forEach((report, id) => {
            expect(report).to.have.property("proposal").that.is.a("string");
            expect(report).to.have.property("underEmbargo").that.is.a("boolean");
            expect(report.nbReports).equal(expected.reports[id].nbReports);
            expect(report.reports.length).equal(1);
            expect(report.reports[0]).equal(reportName);
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/doi/:sessionId/reserve", () => {
    it.each(doiResource.reserveDOI, "%s", ["description"], async (element, next) => {
      const { user, datasetIdList, expected } = element;
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      try {
        const response = await global.gRequester.post(`/doi/${sessionId}/reserve`).send({ datasetIdList });
        expect(response.status).to.equal(expected.status);
        if (response.status < 300) {
          expect(response.body.message).to.not.be.null;
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  describe("/doi/:sessionId/reserved-dois", () => {
    it.each(doiResource.getReservedDOIs, "%s", ["description"], async (element, next) => {
      const { user, expected } = element;
      const getSessionResponse = await sessionHelper.doGetSession(user);
      const { sessionId } = getSessionResponse.body;
      try {
        const response = await global.gRequester.get(`/doi/${sessionId}/reserved-dois`).set("Content-Type", "application/json").send();
        expect(response.status).to.equal(expected.status);
        if (expected.status < 300) {
          expect(response.body).to.be.an("array");
          if (expected.length) {
            expect(response.body.length).to.be.equal(expected.length);
          }
          response.body.forEach((dc) => {
            expect(dc.doi).to.be.undefined;
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
