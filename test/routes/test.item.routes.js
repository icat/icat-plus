require("it-each")({ testPerIteration: true });

const resource = require("../resources/item.resource.js");
const shipmentHelper = require("../helper/shipment.js");
const parcelHelper = require("../helper/parcel.js");
const itemHelper = require("../helper/item.js");

describe("Tracking Item", () => {
  /**
   * Creates a shipment, then a parcel, then an item
   */
  describe("POST /tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item", () => {
    it.each(resource.addItems, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id);

        expect(createParcelResponse.status).to.equal(200);

        const addItemResponse = await itemHelper.addItem(element.user, element.investigationId, createParcelResponse.body._id, element.item);
        expect(addItemResponse.status).to.be.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(addItemResponse.body.items).to.be.an("array").to.have.lengthOf(1);
          addItemResponse.body.items.forEach((item) => {
            if (item.sampleId) {
              expect(item.sampleName).to.not.be.empty;
            }
          });
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Creates a shipment, then a parcel, then an item and then it edits it.
   * Fetches then the parcel with populated items to check if the edition worked/failed.
   */
  describe("PUT /tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item/:itemId", () => {
    it.each(resource.editItems, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id);
        expect(createParcelResponse.status).to.equal(200);
        const parcelId = createParcelResponse.body._id;
        const addItemResponse = await itemHelper.addItem(element.itemCreator, element.investigationId, parcelId, element.initialItem);
        expect(addItemResponse.status).to.be.equal(200);
        expect(addItemResponse.body.items).to.be.an("array").to.have.lengthOf(1);

        const editItemResponse = await itemHelper.editItem(element.user, element.investigationId, parcelId, addItemResponse.body.items[0]._id, element.newItem);
        expect(editItemResponse.status).to.be.equal(element.expected.status);

        const getParcelResponse = await parcelHelper.getParcelById(element.parcelCreator, element.investigationId, parcelId);
        expect(getParcelResponse.status).to.be.equal(200);
        if (element.expected.status < 300) {
          // Edition worked
          expect(editItemResponse.body).to.include(element.newItem);
          // Are the values not in the right order?
          //expect(getParcelResponse.body.items[0]).to.deep.equal(editItemResponse.body);
        } else {
          // Edition failed
          expect(getParcelResponse.body[0].items[0]).to.include(element.initialItem);
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });

  /**
   * Creates a shipment, then a parcel, then an item and then it removes it
   */
  describe("DELETE /tracking/:sessionId/investigation/id/:investigationId/parcel/id/:parcelId/item/:itemId", () => {
    it.each(resource.removeItems, "%s", ["description"], async (element, next) => {
      try {
        const createShipmentResponse = await shipmentHelper.createShipment(element.shipmentCreator, element.shipment, element.investigationId);
        expect(createShipmentResponse.status).to.equal(200);
        element.parcel.shipmentId = createShipmentResponse.body._id;
        const createParcelResponse = await parcelHelper.createParcel(element.parcelCreator, element.parcel, element.investigationId, createShipmentResponse.body._id);
        expect(createParcelResponse.status).to.equal(200);
        const addItemResponse = await itemHelper.addItem(element.itemCreator, element.investigationId, createParcelResponse.body._id, element.item);
        expect(addItemResponse.status).to.be.equal(200);

        const removeItemResponse = await itemHelper.removeItem(element.user, element.investigationId, createParcelResponse.body._id, addItemResponse.body.items[0]._id);
        expect(removeItemResponse.status).to.be.equal(element.expected.status);
        if (element.expected.status < 300) {
          expect(removeItemResponse.body.items).to.be.undefined;
        }
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
