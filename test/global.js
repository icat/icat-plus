//require("dotenv").config({ path: `.env.${process.env.NODE_ENV}`.toLowerCase() });
global.gServerConfig = require("../config/server.config.js");
global.gRestoreConfig = require("./../app/restore.config.js");
global.gTrackingConfig = require("./tracking.config.js");

global.chaiHttp = require("chai-http");
global.chai = require("chai");

global.gResource = require("./env/global.resource.js");
global.gResource.investigations = require("./resources/investigations.resource.js");
global.expect = global.chai.expect;
global.chai.use(global.chaiHttp);

global.mongoUnit = require("mongo-unit");

before((done) => {
  if (global.gServerConfig.database.isMongoUnitEnabled) {
    // starts a mongodb instance which will stay in memory
    try {
      global.mongoUnit
        .start({ verbose: false })
        .then((mongoUri) => {
          global.gServerConfig.database.uri = mongoUri;
          global.gServer = require("../server.js");
          global.gLogger.debug(`Started mongoDB Unit for all tests. URI : ${mongoUri}`);
          global.gServer.onCacheInitialized = function () {
            global.gRequester = global.chai.request(global.gServer).keepOpen();
            global.gLogger.debug("Server is listening...");
            setTimeout(() => {
              done();
            }, 5000);
          };
        })
        .catch((e) => {
          global.gLogger.debug("Could not start the mongo instance for tests.");
          global.gLogger.debug(e);
          throw e;
        });
    } catch (e) {
      global.gLogger.debug("Could not start the mongo instance for tests.");
      global.gLogger.debug(e);
      throw e;
    }
  } else {
    global.gServer = require("../server.js");
    global.gServer.onCacheInitialized = function () {
      global.gRequester = global.chai.request(global.gServer).keepOpen();
      global.gLogger.debug("Server is listening...");
      setTimeout(() => {
        done();
      }, 5000);
    };
  }
});

after(() => {
  if (global.gServerConfig.database.isMongoUnitEnabled) {
    global.gRequester.close();
    global.gLogger.debug("Server is closed");
    global.mongoUnit.stop();
    global.gLogger.debug("MongoDB is stopped.");
  }
});

beforeEach(() => {});

describe("Global", () => {
  describe("Users", () => {
    it("[Allowed Users are defined on global.resources.js]", () => {
      expect(global.gResource).to.have.property("users");
      expect(global.gResource.users).to.have.property("allowed");
      expect(global.gResource.users.allowed).to.be.an("array");
      expect(global.gResource.users.allowed.length).to.be.above(0);
    });
    it("[Unallowed Users are defined on global.resources.js]", () => {
      expect(global.gResource).to.have.property("users");
      expect(global.gResource.users).to.have.property("denied");
      expect(global.gResource.users.denied).to.be.an("array");
      expect(global.gResource.users.denied.length).to.be.above(0);
    });
  });
});
