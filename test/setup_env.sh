#!/bin/bash
declare -A props

while IFS='=' read -r key value; do
   echo $key
   echo $value
   if [ -n "${key}" ]; then
      echo "$key=\$$key" >> .env.test
   fi
done < "${1:-/dev/stdin}"
