const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  outname: [
    {
      description: "File name with no /",
      name: "file.txt",
      expected: {
        name: "file.txt",
      },
    },
    {
      description: "File name with /",
      name: "/file.txt",
      expected: {
        name: "file.txt",
      },
    },
    {
      description: "File name with subfolder",
      name: "/subfolder/file.txt",
      expected: {
        name: "file.txt",
      },
    },
  ],
  getPaths: [
    {
      description: "paths should fail if no investigationId",
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "paths should fail if no datasetId or datafiles are passed as parameter",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "paths should fail if the datafileId does not exist",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigationuser can retrieve a path from a dataset linked to an investigation",
      user: investigationUser.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve a path from a dataset linked to an investigation",
      user: administrator.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot retrieve a path from a dataset linked to an investigation under embargo",
      user: anonymous.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "InstrumentScientist can retrieve a path from a dataset linked to an investigation",
      user: instrumentScientist.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  getDataDownload: [
    {
      description: "Data download failed if no datasetIds and no datafileIds",
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Data download should redirect for downloading dataset",
      user: investigationUser.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        status: 200,
      },
    },
    {
      description: "Data download should redirect for downloading datafile",
      user: investigationUser.credential,
      datafileIds: "123456789",
      expected: {
        status: 200,
      },
    },
  ],
  getDataRestore: [
    {
      description: "Restore data failed if no datasetId ",
      user: investigationUser.credential,
      name: investigationUser.name,
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data failed if no name ",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data should record a dataset query",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a dataset query with email",
      user: investigationUser.credential,
      name: anonymous.name,
      email: anonymous.email,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: anonymous.name,
          email: anonymous.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should not record a query if a query already exists for the same user, dataset, ongoing",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists but done",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: investigationUser.name,
        type: "RESTORE",
        status: "DONE",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another user",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: instrumentScientist.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another dataset",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: 123,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
  ],
};
