const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;

const addresses = globalResource.addresses;

const modifiedAddress = {
  investigationId: investigationUser.investigations.participates[0].investigationId,
  name: "New Jonh",
  surname: "New Doe",
  companyName: "New ESRF",
  address: "New RUE DE LA POSTE",
  city: "New Grenoble",
  region: "New Rhone-Alpes/Isere",
  postalCode: "New 38000",
  email: "New esrf@esrf.fr",
  phoneNumber: "New 123123123",
  country: "New France",
  defaultCourierCompany: "FEDEX",
  defaultCourierAccount: "X123456N",
};

const otherModifiedAddress = {
  investigationId: investigationUser.investigations.participates[1].investigationId,
  name: "New Jonh",
  surname: "New Doe",
  companyName: "New ESRF",
  address: "New RUE DE LA POSTE",
  city: "New Grenoble",
  region: "New Rhone-Alpes/Isere",
  postalCode: "New 38000",
  email: "New esrf@esrf.fr",
  phoneNumber: "New 123123123",
  country: "New France",
};

const noAccessModifiedAddress = {
  investigationId: investigationUser.investigations.noAccess.investigationId,
  name: "New Jonh",
  surname: "New Doe",
  companyName: "New ESRF",
  address: "New RUE DE LA POSTE",
  city: "New Grenoble",
  region: "New Rhone-Alpes/Isere",
  postalCode: "New 38000",
  email: "New esrf@esrf.fr",
  phoneNumber: "New 123123123",
  country: "New France",
};

module.exports = {
  createAddress: [
    {
      description: "Investigation user creates a new address",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user is not allowed to create an address in other's investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      address: addresses.valid.complete,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator  creates a new address",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScients creates a new address",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator creates a new address in an investigation that he does not participate",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user creates a new address with optional courier fields",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.courier,
      expected: {
        status: 200,
      },
    },
  ],
  modifyAddress: [
    {
      description: "User modifies an address",
      addressCreator: investigationUser.credential,
      addressUpdator: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: modifiedAddress,
      expected: {
        status: 200,
        address: modifiedAddress,
      },
    },
    {
      description: "User modifies an address by changing the proposal",
      addressCreator: investigationUser.credential,
      addressUpdator: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: otherModifiedAddress,
      expected: {
        status: 200,
        address: otherModifiedAddress,
      },
    },
    /** {
      description: "User modifies an address by changing the proposal and leaving investigationName empty",
      addressCreator: investigationUser.credential,
      addressUpdator: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: {
        investigationId: investigationUser.investigations.participates[1].investigationId,
        name: "New Jonh",
        surname: "New Doe",
        investigationName: null,
        companyName: "New ESRF",
        address: "New RUE DE LA POSTE",
        city: "New Grenoble",
        region: "New Rhone-Alpes/Isere",
        postalCode: "New 38000",
        email: "New esrf@esrf.fr",
        phoneNumber: "New 123123123",
        country: "New France",
      },
      expected: {
        status: 500,
        address: modifiedAddress,
      },
    },**/
    {
      description: "User can not change the address to other investigation with no permission",
      addressCreator: investigationUser.credential,
      addressUpdator: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: noAccessModifiedAddress,
      expected: {
        status: 500, //TO FIX
        address: modifiedAddress,
      },
    },
    {
      description: "User is not allowed to modify other's investigation address",
      addressCreator: administrator.credential,
      addressUpdator: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      address: addresses.valid.complete,
      modified: modifiedAddress,
      expected: {
        status: 403,
        address: modifiedAddress,
      },
    },
    {
      description: "Anonymous is not allowed to modify addresses",
      addressCreator: investigationUser.credential,
      addressUpdator: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: modifiedAddress,
      expected: {
        status: 403,
        address: modifiedAddress,
      },
    },
  ],
  deleteAddress: [
    {
      description: "Investigation user creates a new address and then deletes it",
      addressCreator: investigationUser.credential,
      addressRemover: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: modifiedAddress,
      expected: {
        status: 200,
        address: modifiedAddress,
      },
    },
    {
      description: "Anonymous is not allowed to delete an address",
      addressCreator: investigationUser.credential,
      addressRemover: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      address: addresses.valid.complete,
      modified: modifiedAddress,
      expected: {
        status: 403,
        address: modifiedAddress,
      },
    },
  ],
  getAddressByInvestigationId: [
    {
      description: "Investigation user is allowed to get addresses",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user is not allowed to get other's addresses",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous is not allowed to get addresses",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator is allowed to gets addresses",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist is allowed to get addresses",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  getAddressByUsername: [
    {
      description: "User retrieves his addresses",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous is ALLOWED to retrieve addresses",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
};
