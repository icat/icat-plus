const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  byDatasets: [
    {
      description: "Investigation user gets files by dataset id",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: investigationUser.investigations.participates[0].datasets.datasetWithSample.fileCount,
      },
    },
    {
      description: "Anonymous gets files by dataset id, there is not middleware but it should retrieve an empty list from ICAT",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSampleNoDataCollection.id,
      user: anonymous.credential,
      expected: {
        status: 200,
        fileCount: 0,
      },
    },
    {
      description: "Administrator gets files by dataset id",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      user: administrator.credential,
      expected: {
        status: 200,
        fileCount: investigationUser.investigations.participates[0].datasets.datasetWithSample.fileCount,
      },
    },
    {
      description: "Investigation user gets files by dataset id with limit",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      limit: 3,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 3,
      },
    },
    {
      description: "Investigation user gets files by dataset id with limit and skip",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      limit: 3,
      skip: 2,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 3,
      },
    },
    {
      description: "Investigation user gets files by dataset id with search",
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      limit: 20,
      search: "file",
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount: 6,
      },
    },
    {
      description: "Investigation user gets files by multiple dataset id",
      datasetId: `${investigationUser.investigations.participates[0].datasets.datasetWithSample.id}, ${investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id}`,
      user: investigationUser.credential,
      expected: {
        status: 200,
        fileCount:
          investigationUser.investigations.participates[0].datasets.datasetWithSample.fileCount +
          investigationUser.investigations.participates[0].datasets.datasetWithNoSample.fileCount,
      },
    },
  ],
  download: [
    {
      description: "Undefined datasetId should return an error",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      filepath: "/test.json",
      expected: {
        status: 400,
      },
    },
    {
      description: "Undefined filepath should return an error",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      expected: {
        status: 400,
      },
    },
    {
      description: "File not found should return an error",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithSample.id,
      filepath: "/unknowpath/file.png",
      expected: {
        status: 404,
      },
    },
    {
      description: "InvestigationUser should download file",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      filepath: "/data/datafile.edf",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator should download file",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      filepath: "/data/datafile.edf",
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientists should download file",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      filepath: "/data/datafile.edf",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot download file",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      filepath: "/data/datafile.edf",
      expected: {
        status: 403,
      },
    },
  ],
};
