const globalResource = require("../env/global.resource.js");
const { createEventByAttributes } = require("./../helper/logbook.js");
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const instruments = globalResource.instruments;
const instrumentName = instruments[0].name;
const CONSTANTS = require("../../app/constants");
const { parseTypesToTypeCategory } = require("../../app/controllers/helpers/helper.logbook.controller");
const { EVENT_TYPE_ANNOTATION } = require("../../app/constants");

const comentAnnotationEvents = [
  {
    instrumentName,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    creationDate: "2020-01-01",
    content: [
      {
        format: "plainText",
        text: "this is my first",
      },
    ],
  },
];
/**
 * Helper that will calculate the occurences of the events based on their type and category
 * @param {*} logbook Array of events
 * @param {*} types Example: 'notification-info,annotication-error'
 * @returns
 */
const getOccurencesByTypeCategory = (logbook, types) => {
  return logbook
    .filter((e) => e.removed !== true)
    .filter((e) => {
      return (
        parseTypesToTypeCategory(types).filter((categoryType) =>
          categoryType.category ? categoryType.type === e.type && categoryType.category === e.category : categoryType.type === e.type
        ).length > 0
      );
    }).length;
};

const populatedInvestigationLogbookWithTags = [
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "first tag",
      },
    ],
    tag: ["SYSTEM"],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "different tag",
      },
    ],
    tag: ["CALIBRATION"],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_ERROR,
    content: [
      {
        format: "plainText",
        text: "error with first tag",
      },
    ],
    tag: ["SYSTEM"],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "two tags",
      },
    ],
    tag: ["SYSTEM", "CALIBRATION"],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "no tag",
      },
    ],
  },
];
const removedEvent = {
  investigationId: investigationUser.investigations.participates[0].investigationId,
  type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
  category: CONSTANTS.EVENT_CATEGORY_COMMENT,
  removed: true,
  content: [
    {
      format: "plainText",
      text: "This element is removed and should not alter the tests",
    },
  ],
};
const populatedInvestigationLogbook = [
  removedEvent,
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "this is my first",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_ANNOTATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "this is my TARGET second",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_ERROR,
    content: [
      {
        format: "plainText",
        text: "this is my error 1",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_ERROR,
    content: [
      {
        format: "plainText",
        text: "this is my error 2 target",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_DEBUG,
    content: [
      {
        format: "plainText",
        text: "this is my debug",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "this is my notification with comment",
      },
    ],
  },
];
const populatedNoAccessLogbook = [
  {
    investigationId: investigationUser.investigations.noAccess.investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "this is my notification with comment",
      },
    ],
  },
];

const populatedReleasedLogbook = [
  {
    investigationId: anonymous.investigations.released.investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "this is my notification with comment in a released investigation",
      },
    ],
  },
];

const populatedlogbookTest1 = [...populatedInvestigationLogbook, ...populatedNoAccessLogbook];

const populatedlogbookTestSearch1 = [
  removedEvent,
  {
    investigationId: investigationUser.investigations.noAccess.investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    tag: ["SYSTEM"],
    content: [
      {
        format: "plainText",
        text: "First comment",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    tag: ["SYSTEM"],
    content: [
      {
        format: "plainText",
        text: "First comment",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_COMMENT,
    content: [
      {
        format: "plainText",
        text: "Second comment",
      },
    ],
  },
  {
    investigationId: investigationUser.investigations.participates[0].investigationId,
    type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
    category: CONSTANTS.EVENT_CATEGORY_INFO,
    tag: ["SYSTEM", "CALIBRATION"],
    content: [
      {
        format: "plainText",
        text: "Third comment",
      },
    ],
  },
];

const tags = [
  {
    name: "SYSTEM",
    investigationId: investigationUser.investigations.participates[0].investigationId,
  },
  {
    name: "CALIBRATION",
    investigationId: investigationUser.investigations.participates[0].investigationId,
  },
];

const dummyEvent = {
  investigationId: investigationUser.investigations.participates[0].investigationId,
  type: "notification",
  title: "notification title",
  category: "error",
  content: [
    {
      format: "plainText",
      text: "beam lost. Line one\nLine two",
    },
  ],
  creationDate: "2018-09-01T00:00:01.000Z",
};

const dummyBeamlineEvent = {
  instrumentName,
  type: "notification",
  title: "notification title",
  category: "error",
  content: [
    {
      format: "plainText",
      text: "beam lost. Line one\nLine two",
    },
  ],
  creationDate: "2018-09-01T00:00:01.000Z",
};

/**
 * Returns the length of the events filtering by removed
 * @param {*} events
 * @returns
 */
const getLogbookLength = (events) => {
  return events.length - events.filter((e) => e.removed === true).length;
};
module.exports = {
  count: [
    {
      description: "InvestigationUser can not access to the count stats",
      user: investigationUser.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not access to the count stats",
      user: anonymous.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist can not access to the count stats",
      user: instrumentScientist.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can access to the count stats",
      user: administrator.credential,
      logbook: populatedlogbookTest1,
      startDate: "2019-01-01",
      endDate: "2030-01-01",
      expected: {
        status: 200,
      },
    },
  ],
  stats: [
    {
      description: "InvestigationUser can not access to the stats",
      user: investigationUser.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not access to the stats",
      user: anonymous.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist can not access to the stats",
      user: instrumentScientist.credential,
      logbook: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can access to the stats",
      user: administrator.credential,
      logbook: populatedlogbookTest1,
      startDate: "2019-01-01",
      endDate: "2030-01-01",
      expected: {
        status: 200,
        annotations: 2,
        notifications: 4,
      },
    },
    {
      description: "Administrator can not get the stats because missing endDate",
      user: administrator.credential,
      logbook: populatedlogbookTest1,
      startDate: "2019-01-01",
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can not get the stats because missing startDate",
      user: administrator.credential,
      logbook: populatedlogbookTest1,
      endDate: "2019-01-01",
      expected: {
        status: 400,
      },
    },
  ],
  createFromBase64byBeamlineAndInstrumentName: [
    {
      description: "InvestigationUser creates a event with base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid instrument",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: "Invalid instrument",
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid investigation name",
      user: investigationUser.credential,
      investigationName: "TEST-XXXX",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with wrong API_KEY",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      API_KEY: "wrong",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser creates a event with no base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no investigationName",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no instrumentName",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no API_KEY",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser cannot create an event in a released logbook",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.released.investigationName,
      instrumentName: investigationUser.investigations.released.instrumentName,
      API_KEY: global.gServerConfig.server.API_KEY,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
  ],
  createFromBase64: [
    {
      description: "A participant creates an event with base64 in the investigation logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
        event: {
          type: "annotation",
          category: "comment",
          investigationId: investigationUser.investigations.participates[0].investigationId,
          username: investigationUser.fullName,
        },
      },
    },
    {
      description: "InstrumentScientist creates a base64 event in the investigation logbook",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
        event: {
          type: "annotation",
          category: "comment",
          investigationId: investigationUser.investigations.participates[0].investigationId,
          username: instrumentScientist.fullName,
        },
      },
    },
    {
      description: "Administrator creates a base64 event in the investigation logbook",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
        event: {
          type: "annotation",
          category: "comment",
          investigationId: investigationUser.investigations.participates[0].investigationId,
          username: administrator.fullName,
        },
      },
    },
    {
      description: "A participant creates a base64 event without base64 parameter in the investigation logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "A participant creates a base64 event without investigationId parameter in the investigation logbook",
      user: investigationUser.credential,
      investigationId: null,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous creates a base64 event in the investigation logbook (forbidden)",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "A participant cannot create an event with base64 in the beamline logbook",
      user: investigationUser.credential,
      instrumentName: dummyBeamlineEvent.instrumentName.toUpperCase(),
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator creates a base64 event in the beamline logbook",
      user: administrator.credential,
      instrumentName: dummyBeamlineEvent.instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
        event: {
          type: "annotation",
          category: "comment",
          instrumentName,
          username: administrator.fullName,
        },
      },
    },
    {
      description: "InstrumentScientist creates a base64 event in the beamline logbook",
      user: instrumentScientist.credential,
      instrumentName: dummyBeamlineEvent.instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
        event: {
          type: "annotation",
          category: "comment",
          investigationId: investigationUser.investigations.participates[0].investigationId,
          username: instrumentScientist.fullName,
        },
      },
    },
    {
      description: "Anonymous cannot create a base64 event in the beamline logbook",
      user: anonymous.credential,
      instrumentName: dummyBeamlineEvent.instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot create a base64 event without base64 parameter in the beamline logbook",
      user: instrumentScientist.credential,
      instrumentName: dummyBeamlineEvent.instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot create a base64 event in the beamline logbook with a wrong instrumentName",
      user: administrator.credential,
      instrumentName: "unknown",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot create a base64 event without investigationId or instrumentName",
      user: administrator.credential,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "A participant cannot create an event with base64 in a released logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.released.investigationId,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
  ],
  page: [
    {
      description: "Gets the metadata of the page size=1, eventCount=10",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], // Event to be tested
      limit: 1, // size of page
      sortOrder: 1,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], // Page index where the events[testEventIndex] should be found
      },
    },
    {
      description: "Gets the metadata of the page size=5, eventCount=10",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], // Event to be tested
      limit: 5, // size of page
      sortOrder: 1,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [1, 1, 1, 1, 1, 2, 2, 2, 2, 2], // Page index where the events[testEventIndex] should be found
      },
    },
    {
      description: "Gets the metadata of the page size=3, eventCount=10",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], // Event to be tested
      limit: 3, // size of page
      sortOrder: 1,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [1, 1, 1, 2, 2, 2, 3, 3, 3, 4], // Page index where the events[testEventIndex] should be found
      },
    },
    {
      description: "Gets the metadata of the page size=3, eventCount=10 sortOrder=-1",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      sortOrder: -1,
      limit: 3, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [4, 3, 3, 3, 2, 2, 2, 1, 1, 1], // Page index where the events[testEventIndex] should be found
      },
    },

    {
      description: "Gets the metadata of the page size=1, eventCount=3 sortOrder=1 type=notification",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 3, // Number of event to be created
      testEventIndices: [0, 1, 2],
      types: "notification",
      sortOrder: 1,
      limit: 1, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [0, 0, 0], // Page index where the events[testEventIndex] should be found
      },
    },
    {
      description: "Gets the metadata of the page size=1, eventCount=3 sortOrder=-1 type=annotation",
      user: investigationUser,
      creator: investigationUser,
      eventCount: 3, // Number of event to be created
      testEventIndices: [0, 1, 2],
      types: "annotation",
      sortOrder: -1,
      limit: 1, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [3, 2, 1], // Page index where the events[testEventIndex] should be found
      },
    },
    {
      description: "Anonymous has not access to the metadata of the page size=3, eventCount=10 sortOrder=1",
      user: anonymous,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      sortOrder: 1,
      limit: 3, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator has not access to the metadata of the page size=3, eventCount=10 sortOrder=-1",
      user: administrator,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      sortOrder: -1,
      limit: 3, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [4, 3, 3, 3, 2, 2, 2, 1, 1, 1],
      },
    },
    {
      description: "InstrumentScientist has not access to the metadata of the page size=3, eventCount=10 sortOrder=-1",
      user: instrumentScientist,
      creator: investigationUser,
      eventCount: 10, // Number of event to be created
      testEventIndices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
      sortOrder: -1,
      limit: 3, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [4, 3, 3, 3, 2, 2, 2, 1, 1, 1],
      },
    },
    {
      description: "Administrator has access to the metadata without specified types",
      user: administrator,
      creator: investigationUser,
      eventCount: 3, // Number of event to be created
      testEventIndices: [0],
      sortOrder: -1,
      limit: 1, // size of page
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [3],
      },
    },
    {
      description: "Administrator has access to the metadata without investigationId and instrumentName",
      user: administrator,
      creator: investigationUser,
      eventCount: 3, // Number of event to be created
      testEventIndices: [0],
      sortOrder: -1,
      limit: 1, // size of page
      types: "annotation",
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
      },
      expected: {
        status: 200,
        index: [3],
      },
    },
  ],
  search: [
    {
      description: "Administrator can not search events by without instrumentName and investigationId",
      user: administrator,
      tags,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "First comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 0,
      expected: {
        matches: 1, // Number of events actually retrieved
        status: 200,
        occurrences: [
          {
            occurences: 2, // Number the results
            page: 1, // Page number where the original event should be fund
          },
        ],
      },
    },
    {
      description: "Search events by types with extra search that refuses the access using the authenticator in memory",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 0,
      expected: {
        status: 200,
        matches: 1, // Number of events actually retrieved
        occurrences: [
          {
            occurences: 2, // Number the results
            page: 1, // Page number where the original event should be fund
          },
        ],
      },
      doExtraSearch: {
        investigationId: investigationUser.investigations.underEmbargo.investigationId,
        expected: {
          status: 400,
        },
      },
    },
    {
      description: "Search events by types with extra search that allows the access using the authenticator in memory",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 0,
      expected: {
        matches: 1, // Number of events actually retrieved
        occurrences: [
          {
            occurences: 2, // Number the results
            page: 1, // Page number where the original event should be fund
          },
        ],
        status: 200,
      },
      doExtraSearch: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        expected: {
          status: 200,
        },
      },
    },
    {
      description: "Search two events by types with skip ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 2,
            page: 2,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Investigation user search by types with skip",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 2,
            page: 2,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search two events by types with skip and instrumentName that does not exist",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      instrumentName: "idXX",
      expected: {
        matches: 0,
        status: 200,
      },
    },
    {
      description: "Search two events by type and category with skip=1 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 2,
            page: 2,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search two events by type and category with limit=2 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 2,
      skip: 0,
      expected: {
        matches: 2,
        occurrences: [
          {
            occurences: 2,
            page: 1,
          },
          {
            occurences: 2,
            page: 1,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search three events by type with limit=1 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 0,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 3,
            page: 1,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search three events by type with limit=1 and skip=1 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 3,
            page: 2,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search three events with no case sensitive by type with limit=1 and skip=1 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "ComMENT",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 3,
            page: 2,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search no events by type with limit=1 and skip=5 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "comment",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 5,
      expected: {
        matches: 0,
        occurrences: [],
        status: 200,
      },
    },
    {
      description: "Search by type and tag with limit=3 ",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "System",
      sortBy: "_id",
      sortOrder: -1,
      limit: 1,
      skip: 0,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 2,
            page: 1,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Search with date",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification",
      search: "System",
      sortBy: "_id",
      sortOrder: -1,
      limit: 6,
      skip: 0,
      useDate: true,
      expected: {
        matches: 2,
        occurrences: [
          {
            occurences: 1,
            page: 1,
          },
        ],
        status: 200,
      },
    },
    {
      description: "Investigation user search with tags",
      user: investigationUser,
      tags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTestSearch1,
      types: "notification-comment",
      search: "comment",
      sortBy: "_id",
      searchTags: "SYSTEM",
      sortOrder: -1,
      expected: {
        matches: 1,
        occurrences: [
          {
            occurences: 1,
            page: 1,
          },
        ],
        status: 200,
      },
    },
  ],
  createEvent: [
    {
      description: "Creation annotation/comment with plainText and HTML",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        title: "a title",
        category: "comment",
        content: [
          {
            format: "plainText",
            text: "this is my comment",
          },
          {
            format: "html",
            text: "<p>this is my comment</p>",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 200,
        content: [
          {
            format: "plainText",
            text: "this is my comment",
          },
          {
            format: "html",
            text: "<p>this is my comment</p>",
          },
        ],
      },
    },
    {
      description: "Creation notification/error with plainText",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. No line break",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 200,
        content: [
          {
            format: "plainText",
            text: "beam lost. No line break",
          },
        ],
      },
    },
    {
      description: "Creation notification/error with plainText with \\n",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 200,
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
          {
            format: "html",
            text: "beam lost. Line one<br>Line two",
          },
        ],
      },
    },
    {
      description: "Anonymous can not create a event",
      user: anonymous,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can create a event",
      user: administrator,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument scientist can create a event in a investigation",
      user: administrator,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument scientist can not create an event with no investigationId and no instrumentName",
      user: instrumentScientist,
      event: dummyEvent,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can not create an event with no investigationId and no instrumentName",
      user: administrator,
      event: dummyEvent,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous can not create an event with no investigationId and no instrumentName",
      user: anonymous,
      event: dummyEvent,
      expected: {
        status: 403,
      },
    },
    {
      description: "Instrument scientist can create an logbook event",
      user: instrumentScientist,
      instrumentName,
      event: dummyEvent,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anynmous can not create an logbook event",
      user: anonymous,
      instrumentName,
      event: dummyEvent,
      expected: {
        status: 403,
      },
    },
    {
      description: "Instrument scientist can not create an logbook event when instrumentName is wrong",
      user: instrumentScientist,
      instrumentName: "id5060",
      event: dummyEvent,
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation cannot create an event in a released logbook",
      user: investigationUser,
      investigationId: investigationUser.investigations.released.investigationId,
      event: {
        investigationId: investigationUser.investigations.released.investigationId,
        type: "notification",
        title: "notification title",
        category: "error",
        content: [
          {
            format: "plainText",
            text: "beam lost. Line one\nLine two",
          },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 403,
      },
    },
  ],
  updateEvent: [
    {
      description: "A participant updates an event by changing its content",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
        username: investigationUser.name,
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
          previousVersionEvent: createEventByAttributes({
            investigationId: investigationUser.investigations.participates[0].investigationId,
            creationDate: "2018-09-01T00:00:01.000Z",
            username: investigationUser.name,
          }),
          username: investigationUser.name,
        }),
      },
    },
    {
      description: "A participant updates an event by adding a tag",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
        username: investigationUser.name,
      }),
      updateEventTags: [
        {
          name: "tag1",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          creationDate: "2018-09-01T00:00:02.000Z",
          tag: [
            {
              name: "tag1",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
          previousVersionEvent: createEventByAttributes({
            investigationId: investigationUser.investigations.participates[0].investigationId,
            creationDate: "2018-09-01T00:00:01.000Z",
            username: investigationUser.name,
          }),
          username: investigationUser.name,
        }),
      },
    },
    {
      description: "A participant updates an event by replacing the tag",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
        username: investigationUser.name,
      }),
      createEventTags: [
        {
          name: "tag1",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      updateEventTags: [
        {
          name: "tag2",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          creationDate: "2018-09-01T00:00:02.000Z",
          tag: [
            {
              name: "tag2",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
          username: investigationUser.name,
          previousVersionEvent: createEventByAttributes({
            investigationId: investigationUser.investigations.participates[0].investigationId,
            creationDate: "2018-09-01T00:00:01.000Z",
            tag: [
              {
                name: "tag1",
                investigationId: investigationUser.investigations.participates[0].investigationId,
              },
            ],
            username: investigationUser.name,
          }),
        }),
      },
    },
    {
      description: "An administrator updates an event by changing its content",
      user: administrator,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
        username: administrator.name,
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
          previousVersionEvent: createEventByAttributes({
            investigationId: investigationUser.investigations.participates[0].investigationId,
            creationDate: "2018-09-01T00:00:01.000Z",
            username: administrator.name,
          }),
          username: administrator.name,
        }),
      },
    },
    {
      description: "A participant can not update an event by changing its type",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
        creationDate: "2018-09-01T00:00:01.000Z",
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: "notification",
          category: "comment",
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 500,
      },
    },
    {
      description: "A participant can not update an event by changing its category",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
        creationDate: "2018-09-01T00:00:01.000Z",
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: "annotation",
          category: "info",
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 500,
      },
    },
    {
      description: "A participant can not update an event using an unrecognized category",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          category: "FAKE CATEGORY",
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 500,
      },
    },
    {
      description: "A participant can not update an event without providing a category",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        type: "annotation",
        category: "comment",
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      updateEventStep: {
        event: {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: "annotation",
          creationDate: "2018-09-01T00:00:02.000Z",
        },
      },
      expected: {
        status: 500,
      },
    },
    {
      description: "Anonymous user can not update an event",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.participates[0].investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
      }),
      updateEventStep: {
        user: anonymous,
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.participates[0].investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Instrument scientist updates an event in the beamline logbook in upper case",
      user: instrumentScientist,
      instrumentName: dummyBeamlineEvent.instrumentName.toUpperCase(),
      event: dummyEvent,
      updateEventStep: {
        user: instrumentScientist,
        event: dummyEvent,
      },
      expected: {
        event: dummyEvent,
        status: 200,
      },
    },
    {
      description: "Instrument scientist updates an event in the beamline logbook in lower case",
      user: instrumentScientist,
      instrumentName: dummyBeamlineEvent.instrumentName.toLowerCase(),
      event: dummyBeamlineEvent,
      updateEventStep: {
        user: instrumentScientist,
        event: dummyBeamlineEvent,
      },
      expected: {
        event: dummyEvent,
        status: 200,
      },
    },
    {
      description: "InvestigationUser cannot update an event in a released logbook",
      user: investigationUser,
      investigationId: investigationUser.investigations.released.investigationId,
      event: createEventByAttributes({
        investigationId: investigationUser.investigations.released.investigationId,
        creationDate: "2018-09-01T00:00:01.000Z",
      }),
      updateEventStep: {
        event: createEventByAttributes({
          investigationId: investigationUser.investigations.released.investigationId,
          content: [
            { format: "plainText", text: "this is my updated comment" },
            { format: "html", text: "<p>this is my updated comment</p>" },
          ],
          creationDate: "2018-09-01T00:00:02.000Z",
        }),
      },
      expected: {
        status: 403,
      },
    },
  ],
  createBeamlineNotification: [
    {
      description: "Creation of a beamline notification",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        instrumentName,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
      },
    },
  ],
  createInvestigationNotification: [
    {
      description: "Event investigation name in capital letters",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with no creationDate",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, not in summer time",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, a date in summer time",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-04-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-04-09T10:18:22.749+02:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, with offset",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749+01:00",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, UTC",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749Z",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749Z",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format)",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
        }),
      },
    },
    {
      description: "Event with creationDate: unknown format)",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
        }),
      },
    },
    {
      description: "Event with a tag with name",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "myTestTag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          tag: [
            {
              name: "mytesttag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag with ID",
      user: investigationUser.credential,
      tagsToFillDB: [
        {
          name: "My TAG",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        tag: [{ name: "My TAG" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "my tag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag that comes as a string with ID",
      user: investigationUser.credential,
      tagsToFillDB: [
        {
          name: "mytag",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        tag: '[{"name":"global"}]',
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "global",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },

    {
      description: "Event with investigation name that can not be normalized",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "XX-III-ProposalMathilde",
        instrumentName,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with wrong instrumentName",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: "not instrument",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with no instrumentName",
      user: investigationUser.credential,
      tagsToFillDB: [],
      event: {
        type: "notification",
        investigationName: investigationUser.investigations.participates[0].investigationName,
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },

    {
      description: "Event with no type",
      user: investigationUser.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
    {
      description: "XXEvent with no category",
      user: investigationUser.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
  ],

  getEvents: [
    {
      description: "InvestigationUser get events by investigationId with filterInvestigation=true",
      user: investigationUser.credential,
      filterInvestigation: true,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 200,
        /** removed events are filtered from the count */
        occurrences: getLogbookLength(populatedInvestigationLogbook),
      },
    },
    {
      description: "InvestigationUser get events by investigationId with filterInvestigation=false",
      user: investigationUser.credential,
      filterInvestigation: false,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 200,
        occurrences: getLogbookLength(populatedInvestigationLogbook),
      },
    },
    {
      description: "InvestigationUser get events by investigationId with sort",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      sortBy: "_id",
      expected: {
        status: 200,
        /** Number of events expected to be found */
        occurrences: getLogbookLength(populatedInvestigationLogbook),
      },
    },
    {
      description: "InvestigationUser get events by investigationId with limit",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      limit: 3,
      types: "",
      expected: {
        status: 200,
        /** Number of events expected to be found */
        occurrences: 3,
      },
    },
    {
      description: "InvestigationUser get events by investigationId with skip",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      skip: 2,
      types: "",
      expected: {
        status: 200,
        /** Number of events expected to be found */
        occurrences: getLogbookLength(populatedInvestigationLogbook) - 2,
      },
    },
    {
      description: "InvestigationUser get events by investigationId with skip and limit",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      skip: getLogbookLength(populatedInvestigationLogbook) - 2,
      limit: 1,
      types: "",
      expected: {
        status: 200,
        /** Number of events expected to be found */
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser get events by investigationId and a single types (type + category)",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "notification-error",
      expected: {
        status: 200,
        occurrences: getOccurencesByTypeCategory(
          populatedlogbookTest1.filter((e) => e.investigationId === investigationUser.investigations.participates[0].investigationId),
          "notification-error"
        ),
      },
    },
    {
      description: "InvestigationUser get events by investigationId and a single types (type)",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "annotation",
      expected: {
        status: 200,
        occurrences: getOccurencesByTypeCategory(populatedInvestigationLogbook, "annotation"),
      },
    },
    {
      description: "InvestigationUser get events by investigationId and a single types (type)",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "notification-error,notification-debug",
      expected: {
        status: 200,
        occurrences: getOccurencesByTypeCategory(populatedInvestigationLogbook, "notification-error,notification-debug"),
      },
    },
    {
      description: "InvestigationUser get events by investigationId and a single types (type) and limit",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      limit: 1,
      types: "notification-error,notification-debug",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser get events by investigationId and a two types (type)",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "annotation,notification",
      expected: {
        status: 200,
        occurrences: getOccurencesByTypeCategory(populatedInvestigationLogbook, "annotation,notification"),
      },
    },
    {
      description: "InvestigationUser searches an string that does not exist in the logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      search: "THIS STRING DOES NOT EXIST IN THE LOGBOOK",
      expected: {
        status: 200,
        occurrences: 0,
      },
    },
    {
      description: "InvestigationUser searches a string that exist in a event of the logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      search: "target",
      expected: {
        status: 200,
        occurrences: 2,
      },
    },
    {
      description: "InvestigationUser searches a string that exist in a event of the logbook different case",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      search: "TARgeT",
      expected: {
        status: 200,
        occurrences: 2,
      },
    },
    {
      description: "InvestigationUser get events by investigationId and date",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      useDate: true,
      expected: {
        status: 200,
        occurrences: getLogbookLength(populatedInvestigationLogbook),
      },
    },

    {
      description: "Anonymous can read the logbook of an investigation that is released",
      user: anonymous.credential,
      investigationId: anonymous.investigations.released.investigationId,
      events: populatedlogbookTest1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can not read the logbook of an investigation that is under embargo",
      user: anonymous.credential,
      investigationId: anonymous.investigations.underEmbargo.investigationId,
      events: populatedlogbookTest1,
      expected: {
        status: 400,
      },
    },

    {
      description: "Anonymous can not read the beamline logbook",
      user: anonymous.credential,
      instrumentName,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not read the global logbook",
      user: anonymous.credential,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist can read the beamline logbook",
      user: instrumentScientist.credential,
      instrumentName: instrumentScientist.instrument.name,
      events: populatedlogbookTest1,
      expected: {
        status: 200,
        occurrences: getLogbookLength(populatedInvestigationLogbook),
      },
    },
    {
      description: "InstrumentScientist can read the beamline logbook with filterInvestigation = true",
      user: instrumentScientist.credential,
      instrumentName: instrumentScientist.instrument.name,
      filterInvestigation: true,
      events: populatedlogbookTest1,
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.filter((e) => !e.investigationId).length,
      },
    },

    {
      description: "Administrator can read the global logbook with types",
      user: administrator.credential,
      events: populatedlogbookTest1,
      types: "broadcast,notification,annotation",
      expected: {
        status: 200,
        occurrences: getLogbookLength(populatedlogbookTest1),
      },
    },
    {
      description: "InstrumentScientist can not read the global logbook",
      user: instrumentScientist.credential,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser can not read the global logbook",
      user: investigationUser.credential,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser can not read the beamline logbook",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator get events without types",
      user: administrator.credential,
      events: populatedlogbookTest1,
      expected: {
        status: 200,
        occurrences: getLogbookLength(populatedlogbookTest1),
      },
    },
    {
      description: "InvestigationUser get events with case insensitive on type category",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "notification-deBUG",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser get events by tags",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbookWithTags,
      prepopulatedTags: tags,
      tags: "SYSTEM",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbookWithTags.filter((e) => e.tag && e.tag.indexOf("SYSTEM") > -1).length,
      },
    },
    {
      description: "InvestigationUser get events by tags when no events corresponding to the tag",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbookWithTags,
      prepopulatedTags: tags,
      tags: "NOTAG",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbookWithTags.filter((e) => e.tag && e.tag.indexOf("NOTAG") > -1).length,
      },
    },
    {
      description: "InvestigationUser get events by tags is case insensitive",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbookWithTags,
      prepopulatedTags: tags,
      tags: "calibration",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbookWithTags.filter((e) => e.tag && e.tag.indexOf("CALIBRATION") > -1).length,
      },
    },
    {
      description: "InvestigationUser get events with startTime before the events and endTime after",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbook,
      sortBy: "_id",
      sortOrder: 1,
      startTimeCheck: "before",
      endTimeCheck: "after",
      expected: {
        occurrences: getLogbookLength(populatedInvestigationLogbook),
        status: 200,
      },
    },
    {
      description: "InvestigationUser get events with startTime after the events",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbook,
      sortBy: "_id",
      sortOrder: 1,
      startTimeCheck: "after",
      expected: {
        occurrences: 0,
        status: 200,
      },
    },
  ],
  toTXT: [
    {
      description: "InvestigationUser exports a logbook in txt",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_CATEGORY_COMMENT,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "Investigation comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
      ],
      expected: {
        status: 200,
      },
    },
  ],
  getBroadcastEvents: [
    {
      description: "InvestigationUser cannot see broadcast events if after last event",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      types: "notification-info,broadcast-error",
      events: [
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "Investigation comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser can see broadcast events if before last event and after first event",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      types: "notification-info,broadcast-error",
      events: [
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "last comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 3,
      },
    },
    {
      description: "InvestigationUser cannot see broadcast events if before first event",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      types: "notification-info,broadcast-error",
      events: [
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser cannot see broadcast events if no event in investigation logbook",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      types: "notification-info,broadcast-error",
      events: [
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 0,
      },
    },
    {
      description: "InvestigationUser cannot see broadcast events with non expected type",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      types: "notification-info,broadcast-error",
      events: [
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast info",
            },
          ],
        },
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "last comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 2,
      },
    },
    {
      description: "InvestigationUser can see broadcast events if types is not defined",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "last comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 3,
      },
    },
    {
      description: "InstrumentScientist cannot see broadcast events if after last event",
      user: instrumentScientist.credential,
      instrumentName,
      types: "notification-info,broadcast-error",
      events: [
        {
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "Last beamline comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InstrumentScientist can see broadcast events if before last event and after first event",
      user: instrumentScientist.credential,
      instrumentName,
      types: "notification-info,broadcast-error",
      events: [
        {
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "last comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 3,
      },
    },
    {
      description: "InstrumentScientist cannot see broadcast events if before first event",
      user: instrumentScientist.credential,
      instrumentName,
      types: "notification-info,broadcast-error",
      events: [
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InstrumentScientist cannot see broadcast events if no event in beamline logbook",
      user: instrumentScientist.credential,
      instrumentName,
      types: "notification-info,broadcast-error",
      events: [
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 0,
      },
    },
    {
      description: "InstrumentScientist can see broadcast events if investigation events in beamline logbook",
      user: instrumentScientist.credential,
      instrumentName,
      types: "notification-info,broadcast-error",
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationName,
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "first comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          source: "ebs",
          tag: [{ name: "MACHINE" }, { name: "BEAM-LOSS" }],
          content: [
            {
              format: "plainText",
              text: "broadcast error",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationName,
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_INFO,
          content: [
            {
              format: "plainText",
              text: "last comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        occurrences: 3,
      },
    },
  ],
  eventsDates: [
    {
      description: "InvestigationUser get events dates by investigationId",
      user: investigationUser.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "Administrator get events dates by investigationId",
      user: administrator.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "InstrumentScientist get events dates by investigationId",
      user: instrumentScientist.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "Anonymous cannot get events dates by investigationId",
      user: anonymous.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser get events dates by investigationId with multiple dates",
      user: investigationUser.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-03-03",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          creationDate: "2020-02-02",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "second item",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        result: [
          { _id: "2020-03-03", event_count: 1 },
          { _id: "2020-02-02", event_count: 1 },
          { _id: "2020-01-01", event_count: 2 },
        ],
      },
    },
    {
      description: "InvestigationUser get events dates by investigationId with type criteria",
      user: investigationUser.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          creationDate: "2020-02-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "notification-comment",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "InvestigationUser get events dates by investigationId with search criteria",
      user: investigationUser.credential,
      populateUser: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "First Comment",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-02-01",
          content: [
            {
              format: "plainText",
              text: "Second Comment",
            },
          ],
        },
      ],
      types: "notification-comment",
      search: "First",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "InvestigationUser cannot get events dates by instrumentName",
      user: investigationUser.credential,
      populateUser: administrator.credential,
      instrumentName,
      events: comentAnnotationEvents,
      types: "",
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist get events dates by instrumentName",
      user: instrumentScientist.credential,
      populateUser: administrator.credential,
      instrumentName,
      events: comentAnnotationEvents,
      types: "",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "Administrator get events dates by instrumentName",
      user: administrator.credential,
      populateUser: administrator.credential,
      instrumentName,
      events: comentAnnotationEvents,
      types: "",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "Anonymous cannot get events dates by instrumentName",
      user: anonymous.credential,
      populateUser: administrator.credential,
      instrumentName,
      events: comentAnnotationEvents,
      types: "",
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist get events dates by instrumentName with multiple dates",
      user: instrumentScientist.credential,
      populateUser: administrator.credential,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-03-03",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my second",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        result: [
          { _id: "2020-03-03", event_count: 1 },
          { _id: "2020-01-01", event_count: 2 },
        ],
      },
    },
    {
      description: "InstrumentScientist get events dates by instrumentName with type criteria",
      user: instrumentScientist.credential,
      populateUser: administrator.credential,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_ERROR,
          creationDate: "2020-02-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "notification-comment",
      expected: {
        status: 200,
        result: [{ _id: "2020-01-01", event_count: 1 }],
      },
    },
    {
      description: "InstrumentScientist get events dates by instrumentName with search criteria",
      user: instrumentScientist.credential,
      populateUser: administrator.credential,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "First comment",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-02-01",
          content: [
            {
              format: "plainText",
              text: "Second comment",
            },
          ],
        },
      ],
      search: "second",
      expected: {
        status: 200,
        result: [{ _id: "2020-02-01", event_count: 1 }],
      },
    },
    {
      description: "InstrumentScientist cannot get events dates without investigationId or instrumentName",
      user: anonymous.credential,
      populateUser: administrator.credential,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "this is my first",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 400,
      },
    },
  ],
  remove: [
    {
      description: "InvestigationUser is not allowed to remove events",
      remover: investigationUser,
      administrator,
      find: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 403,
        },
      },
    },
    {
      description: "Anonymous is not allowed to remove events",
      remover: anonymous,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 403,
        },
      },
    },
    {
      description: "InstrumentScientist is not allowed to remove events",
      remover: instrumentScientist,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 403,
        },
      },
    },
    {
      description: "Administrator removes and restores all events by investigationId",
      remover: administrator,
      restorer: administrator,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 200,
          restore: 200,
        },
      },
    },
    {
      description: "InvestigationUser is not allowed to restore events",
      remover: administrator,
      restorer: investigationUser,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 200,
          restore: 403,
        },
      },
    },
    {
      description: "Anonymous is not allowed to restore events",
      remover: administrator,
      restorer: anonymous,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 200,
          restore: 403,
        },
      },
    },
    {
      description: "InstrumentScientist  is not allowed to restore events",
      remover: administrator,
      restorer: instrumentScientist,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 200,
          restore: 403,
        },
      },
    },
    {
      description: "Administrator can not remove without investigationId",
      remover: administrator,
      restorer: administrator,
      administrator,
      removeBy: {
        search: "First",
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 400,
        },
      },
    },
    {
      description: "Administrator removes and restores all events by investigationId and search",
      remover: administrator,
      restorer: administrator,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        search: "First",
      },
      events: populatedlogbookTest1,
      expected: {
        status: {
          remove: 200,
          restore: 200,
        },
      },
    },
    {
      description: "Administrator removes and restores all events by investigationId and type",
      remover: administrator,
      restorer: administrator,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        types: "error",
      },
      events: populatedlogbookTest1,
      expected: {
        removeCount: getOccurencesByTypeCategory(
          populatedlogbookTest1.filter((e) => e.investigationId === investigationUser.investigations.participates[0].investigationId),
          "error"
        ),
        status: {
          remove: 200,
          restore: 200,
        },
      },
    },
    {
      description: "Administrator removes and restores all events by investigationId and category and type",
      remover: administrator,
      restorer: administrator,
      administrator,
      removeBy: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        types: "notification-error",
      },
      events: populatedlogbookTest1,
      expected: {
        removeCount: getOccurencesByTypeCategory(
          populatedlogbookTest1.filter((e) => e.investigationId === investigationUser.investigations.participates[0].investigationId),
          "notification-error"
        ),
        status: {
          remove: 200,
          restore: 200,
        },
      },
    },
  ],
  countEvents: [
    {
      description: "InvestigationUser should count events by investigationId",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "Administrator should count events by investigationId",
      user: administrator,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "InstrumentScientist should count events by investigationId",
      user: instrumentScientist,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "Anonymous can not count events by investigationId under embargo",
      user: anonymous,
      investigationId: anonymous.investigations.underEmbargo.investigationId,
      events: populatedlogbookTest1,
      types: "",
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous should count events by investigationId released",
      user: anonymous,
      investigationId: anonymous.investigations.released.investigationId,
      events: populatedReleasedLogbook,
      types: "",
      expected: {
        status: 200,
        occurrences: populatedReleasedLogbook.length,
      },
    },
    {
      description: "InvestigationUser should not count events by instrumentName",
      user: investigationUser,
      instrumentName,
      events: [
        {
          instrumentName,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist should count events by instrumentName",
      user: instrumentScientist,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "Administrator should count events by instrumentName",
      user: administrator,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "Anonymous should not count events by instrumentName",
      user: anonymous,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          creationDate: "2020-01-01",
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser should not count events without investigationId or instrumentName",
      user: investigationUser,
      events: populatedlogbookTest1,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser should count events by investigationId filtered by type",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "annotation",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.filter((e) => e.type === EVENT_TYPE_ANNOTATION).length,
      },
    },
    {
      description: "InvestigationUser should count events by investigationId with search",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      search: "first",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser should count events by investigationId filtered by date",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      useDate: true,
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "filterInvestigation should not change count events by investigationId",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      filterInvestigation: true,
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "InstrumentScientist should count events by instrumentName with filterInvestigation",
      user: instrumentScientist,
      filterInvestigation: true,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "investigation comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InstrumentScientist should count events by instrumentName without filterInvestigation",
      user: instrumentScientist,
      instrumentName: instrumentScientist.instrument.name,
      events: [
        {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "beamline comment",
            },
          ],
        },
        {
          instrumentName: instrumentScientist.instrument.name,
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "investigation comment",
            },
          ],
        },
      ],
      types: "",
      expected: {
        status: 200,
        occurrences: 2,
      },
    },
    {
      description: "InvestigationUser should count events by investigationId and search with no occurence",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      search: "NOTFOUND",
      expected: {
        status: 200,
        occurrences: 0,
      },
    },
    {
      description: "InvestigationUser should count events by investigationId and search and find result",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedlogbookTest1,
      types: "",
      search: "Comment",
      expected: {
        status: 200,
        occurrences: 1,
      },
    },
    {
      description: "InvestigationUser count events without types",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbook,
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbook.length,
      },
    },
    {
      description: "InvestigationUser should count events with tags",
      user: investigationUser,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      events: populatedInvestigationLogbookWithTags,
      prepopulatedTags: tags,
      tags: "CALIBRATION",
      expected: {
        status: 200,
        occurrences: populatedInvestigationLogbookWithTags.filter((e) => e.tag && e.tag.indexOf("CALIBRATION") > -1).length,
      },
    },
  ],
};
