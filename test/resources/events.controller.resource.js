module.exports = {
  translateEventContentToHtml: [
    {
      content: [
        {
          format: "plainText",
          text: "plaintext, no line break",
        },
      ],
      expected: [
        {
          format: "plainText",
          text: "plaintext, no line break",
        },
      ],
    },
    {
      content: [
        {
          format: "plainText",
          text: "plaintext, line one\nline two",
        },
      ],
      expected: [
        {
          format: "plainText",
          text: "plaintext, line one\nline two",
        },
        {
          format: "html",
          text: "plaintext, line one<br>line two",
        },
      ],
    },
    {
      content: [
        {
          format: "plainText",
          text: "plaintext, line one\nline two\nline three",
        },
      ],
      expected: [
        {
          format: "plainText",
          text: "plaintext, line one\nline two\nline three",
        },
        {
          format: "html",
          text: "plaintext, line one<br>line two<br>line three",
        },
      ],
    },
    {
      content: [
        {
          format: "html",
          text: "<p>html, some text </p>",
        },
      ],
      expected: [
        {
          format: "html",
          text: "<p>html, some text </p>",
        },
      ],
    },
    {
      content: [
        {
          format: "plainText",
          text: "plainText and html, some text",
        },
        {
          format: "html",
          text: "<p> plainText and html, some text </p>",
        },
      ],
      expected: [
        {
          format: "plainText",
          text: "plainText and html, some text",
        },
        {
          format: "html",
          text: "<p> plainText and html, some text </p>",
        },
      ],
    },
  ],
  replaceImageSrc: [
    {
      aboutThisTest: "this is a event extracted from mongo, src replacement occurs",
      events: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><strong><img src="http://kolla-vm026.esrf.fr:8000/investigations/135816585/events/5be53b805b6f9e6ceca42a7d/file?sessionId=141af1fa-02f9-4696-8095-4e002b4e7839" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
      sessionId: "527945f8-fa71-4e7b-a12c-3cc79d152b33",
      investigationId: "135816585",
      serverURI: "https://icatplus.esrf.fr",
      expected: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><strong><img src="https://icatplus.esrf.fr/resource/527945f8-fa71-4e7b-a12c-3cc79d152b33/file/id/5be53b805b6f9e6ceca42a7d/investigation/id/135816585/download" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
    },
    {
      aboutThisTest: "This is a event where replacement of image is useless but done anyway",
      events: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><strong><img src="https://icatplus.esrf.fr/resource/527945f8-fa71-4e7b-a12c-3cc79d152b33/file/id/5be53b805b6f9e6ceca42a7d/investigation/id/135816585/download" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
      sessionId: "527945f8-fa71-4e7b-a12c-3cc79d152b33",
      investigationId: "135816585",
      serverURI: "https://icatplus.esrf.fr",
      expected: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><strong><img src="https://icatplus.esrf.fr/resource/527945f8-fa71-4e7b-a12c-3cc79d152b33/file/id/5be53b805b6f9e6ceca42a7d/investigation/id/135816585/download" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
    },
    {
      aboutThisTest: "This is a event where there is no image, so no replacement is performed",
      events: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text: "<p></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>",
            },
          ],
          previousVersionEvent: null,
        },
      ],
      sessionId: "527945f8-fa71-4e7b-a12c-3cc79d152b33",
      investigationId: "135816585",
      serverURI: "https://icatplus.esrf.fr",
      expected: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text: "<p></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>",
            },
          ],
          previousVersionEvent: null,
        },
      ],
    },
    {
      aboutThisTest: "This is an event where img src has an unexpected value",
      events: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text: '<p><strong><img src="hello" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
      sessionId: "527945f8-fa71-4e7b-a12c-3cc79d152b33",
      investigationId: "135816585",
      serverURI: "https://icatplus.esrf.fr",
      expected: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: " Fe2 strepto r2 Main Root: XANES Points",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text: '<p><strong><img src="hello" width="527" height="408" /></strong></p>\n<p><strong>&nbsp;Fe2 strepto r2 Main Root: XANES Points</strong></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
    },
    {
      aboutThisTest: "This is an event where img has a style before src",
      events: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: "\n",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><img style="height: auto; width: auto; max-width: 100%;" src="http://icatplus.esrf.fr/investigations/142866117/events/5c092a58e0ec17001276d9cc/file?sessionId=eeaad50c-a9dc-47d3-bcb1-c71defa9c62c" alt="" width="1280" height="1024" /></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
      sessionId: "527945f8-fa71-4e7b-a12c-3cc79d152b33",
      investigationId: "135816585",
      serverURI: "https://icatplus.esrf.fr",
      expected: [
        {
          _id: "5be00615f7d77b703f89404c",
          tag: [],
          file: [],
          investigationName: "EV-280",
          investigationId: 135816585,
          datasetId: null,
          datasetName: "fe2streptor2_main_root",
          title: "",
          type: "notification",
          category: "commandLine",
          filename: null,
          fileSize: null,
          username: "reyesher",
          instrumentName: "id21",
          software: null,
          machine: null,
          creationDate: "2018-11-09T07:47:33.000Z",
          content: [
            {
              _id: "5be53b885b6f9e6ceca42a7f",
              format: "plainText",
              text: "\n",
            },
            {
              _id: "5be53b885b6f9e6ceca42a7e",
              format: "html",
              text:
                '<p><img style="height: auto; width: auto; max-width: 100%;" src="https://icatplus.esrf.fr/resource/527945f8-fa71-4e7b-a12c-3cc79d152b33/file/id/5c092a58e0ec17001276d9cc/investigation/id/135816585/download" alt="" width="1280" height="1024" /></p>',
            },
          ],
          previousVersionEvent: null,
        },
      ],
    },
  ],
};
