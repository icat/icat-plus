const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const principalInvestigator = globalResource.users.principalInvestigator;
const localContact = globalResource.users.localContact;

const logOut = {
  message: "Error when retrieving the session information. Your session might be expired or it is invalid.",
};

module.exports = {
  doLogout: [
    {
      description: "InstrumentScientist logs out",
      user: instrumentScientist.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "administrator logs out",
      user: administrator.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "anonymous logs out",
      user: anonymous.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
    {
      description: "investigationUser logs out",
      user: investigationUser.credential,
      expected: {
        status: 500,
        message: logOut.message,
      },
    },
  ],
  doLogin: [
    {
      description: "InstrumentScientist logs in",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
        isInstrumentScientist: true,
        isAdministrator: false,
        usersByPrefix: [],
        refreshStatus: 204,
      },
    },
    {
      description: "Administrator logs in",
      user: administrator.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: true,
        usersByPrefix: [],
        refreshStatus: 204,
      },
    },
    {
      description: "Anonymous logs in",
      user: anonymous.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: false,
        usersByPrefix: [],
        refreshStatus: 204,
      },
    },
    {
      description: "InvestigationUser logs in",
      user: investigationUser.credential,
      expected: {
        status: 200,
        isInstrumentScientist: false,
        isAdministrator: false,
        usersByPrefix: [],
        refreshStatus: 204,
      },
    },
  ],

  getUsers: [
    {
      description: "InstrumentScientist gets the list of users",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets the list of users",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous doesn't get the list of users",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser doesn't get the list of users",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
  ],
  getUserPreferences: [
    {
      description: "InvestigationUser can retrieve empty preferences if not exist",
      user: investigationUser.credential,
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, recentlyVisited: [], selection: { datasetIds: [], sampleIds: [] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can retrieve preferences if exists",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          selection: { datasetIds: [123] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: {
            autoProcessingRankingShell: "overall",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            autoProcessingRankingDisableSpaceGroup: true,
            scanTypeFilters: ["datacollection"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 } },
          },
          isGroupedBySample: false,
          pagination: [{ key: "investigations", value: 20 }],
        },
      ],
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: {
            autoProcessingRankingShell: "overall",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            autoProcessingRankingDisableSpaceGroup: true,
            scanTypeFilters: ["datacollection"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 } },
          },
          isGroupedBySample: false,
          pagination: [{ key: "investigations", value: 20 }],
        },
      },
    },
    {
      description: "InvestigationUser can retrieve preferences for the given user",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: administrator }, { user: investigationUser }],
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { datasetIds: [], sampleIds: [] }, pagination: [] },
      },
    },
  ],
  updateUserPreferences: [
    {
      description: "InvestigationUser can update user preferences",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          selection: { datasetIds: [123, 456] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: {
            autoProcessingRankingShell: "overall",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            scanTypeFilters: ["datacollection"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 } },
          },
          isGroupedBySample: false,
          pagination: [{ key: "investigations", value: 20 }],
        },
      ],
      userpreferences: {
        user: investigationUser.name,
        selection: { datasetIds: [123], sampleIds: [] },
        logbookFilters: { sortBy: "createdAt", sortOrder: -1, types: "notification-error" },
        recentlyVisited: [
          { url: "/investigation/1347829104/datasets", label: "Datasets", target: { investigationName: "test", instrumentName: "ID00", investigationStartDate: "2000-01-01" } },
          { url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } },
        ],
        mxsettings: {
          autoProcessingRankingShell: "inner",
          autoProcessingRankingParameter: "mean_I_over_sigI",
          scanTypeFilters: ["datacollection", "mesh"],
          cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 }, completeness: { outer: 10 } },
        },
        isGroupedBySample: true,
        pagination: [
          { key: "investigations", value: 50 },
          { key: "logbook", value: 200 },
        ],
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123], sampleIds: [] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1, types: "notification-error" },
          recentlyVisited: [
            { url: "/investigation/1347829104/datasets", label: "Datasets", target: { investigationName: "test", instrumentName: "ID00", investigationStartDate: "2000-01-01" } },
            { url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } },
          ],
          mxsettings: {
            autoProcessingRankingShell: "inner",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            scanTypeFilters: ["datacollection", "mesh"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 }, completeness: { outer: 10 } },
          },
          isGroupedBySample: true,
          pagination: [
            { key: "investigations", value: 50 },
            { key: "logbook", value: 200 },
          ],
        },
      },
    },
    {
      description: "InvestigationUser can update user preferences by deselecting datasetIds",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser, selection: { datasetIds: [123, 456] } }],
      userpreferences: {
        user: investigationUser.name,
        selection: { datasetIds: [] },
      },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { datasetIds: [] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can create user preferences with a selection of datasetIds",
      user: investigationUser.credential,
      userpreferences: { user: investigationUser.name, selection: { datasetIds: [123, 456] } },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { datasetIds: [123, 456] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can update user preferences by adding selectedSamples",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser }],
      userpreferences: {
        user: investigationUser.name,
        selection: { sampleIds: [123] },
      },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { sampleIds: [123] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can update user preferences by updating the selection",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser }],
      userpreferences: {
        user: investigationUser.name,
        selection: { datasetIds: [123], sampleIds: [456] },
      },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { datasetIds: [123], sampleIds: [456] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can update user preferences by clearing the selection",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser, selection: { datasetIds: [123], sampleIds: [456] } }],
      userpreferences: {
        user: investigationUser.name,
        selection: { datasetIds: [], sampleIds: [] },
      },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, selection: { datasetIds: [], sampleIds: [] }, pagination: [] },
      },
    },
    {
      description: "InvestigationUser can create user preferences with logbookFilters",
      user: investigationUser.credential,
      userpreferences: { user: investigationUser.name, logbookFilters: { sortBy: "createdAt", sortOrder: -1 } },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          selection: { datasetIds: [], sampleIds: [] },
          pagination: [],
        },
      },
    },
    {
      description: "InvestigationUser can update logbookFilters",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser, selection: { datasetIds: [123, 456] }, logbookFilters: { sortBy: "createdAt", sortOrder: -1 } }],
      userpreferences: { user: investigationUser.name, logbookFilters: { sortBy: "createdAt", sortOrder: 1, types: "notification-error" } },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123, 456], sampleIds: [] },
          pagination: [],
          logbookFilters: { sortBy: "createdAt", sortOrder: 1, types: "notification-error" },
        },
      },
    },
    {
      description: "InvestigationUser can update user preferences with empty recently visited",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          recentlyVisited: [
            {
              url: "/investigation/1347829104/datasets",
              label: "Datasets",
              target: { investigationName: "test", instrumentName: "ID00", investigationStartDate: "2000-01-01", investigationEndDate: "2000-01-02" },
            },
            { url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } },
          ],
        },
      ],
      userpreferences: {
        username: investigationUser.name,
        recentlyVisited: [],
      },
      expected: {
        status: 200,
        preferences: { username: investigationUser.name, recentlyVisited: [], selection: { datasetIds: [], sampleIds: [] }, pagination: [] },
      },
    },

    {
      description: "InvestigationUser can create user preferences with recentlyVisited",
      user: investigationUser.credential,
      userpreferences: {
        user: investigationUser.name,
        recentlyVisited: [
          { url: "/investigation/1347829104/datasets", label: "Datasets", target: { investigationName: "test", instrumentName: "ID00", investigationStartDate: "2000-01-01" } },
        ],
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [], sampleIds: [] },
          pagination: [],
          recentlyVisited: [
            { url: "/investigation/1347829104/datasets", label: "Datasets", target: { investigationName: "test", instrumentName: "ID00", investigationStartDate: "2000-01-01" } },
          ],
        },
      },
    },
    {
      description: "InvestigationUser can create user preferences with mxsettings",
      user: investigationUser.credential,
      userpreferences: {
        user: investigationUser.name,
        mxsettings: {
          autoProcessingRankingShell: "overall",
          autoProcessingRankingParameter: "mean_I_over_sigI",
          autoProcessingRankingDisableSpaceGroup: true,
          scanTypeFilters: ["datacollection", "mesh"],
        },
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [], sampleIds: [] },
          pagination: [],
          mxsettings: {
            autoProcessingRankingShell: "overall",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            autoProcessingRankingDisableSpaceGroup: true,
            scanTypeFilters: ["datacollection", "mesh"],
          },
        },
      },
    },
    {
      description: "InvestigationUser can update mxsettings",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          selection: { datasetIds: [123, 456] },
          mxsettings: {
            autoProcessingRankingShell: "overall",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            autoProcessingRankingDisableSpaceGroup: true,
            scanTypeFilters: ["datacollection", "mesh"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 } },
          },
        },
      ],
      userpreferences: {
        username: investigationUser.name,
        mxsettings: {
          autoProcessingRankingShell: "outer",
          autoProcessingRankingParameter: "mean_I_over_sigI",
          autoProcessingRankingDisableSpaceGroup: false,
          scanTypeFilters: ["datacollection"],
          cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 }, completeness: { outer: 10 } },
        },
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123, 456], sampleIds: [] },
          pagination: [],
          mxsettings: {
            autoProcessingRankingShell: "outer",
            autoProcessingRankingParameter: "mean_I_over_sigI",
            autoProcessingRankingDisableSpaceGroup: false,
            scanTypeFilters: ["datacollection"],
            cutoffs: { r_meas_all_IPlus_IMinus: { inner: 5 }, completeness: { outer: 10 } },
          },
        },
      },
    },
    {
      description: "InvestigationUser can create user preferences with isGroupedBySample",
      user: investigationUser.credential,
      userpreferences: { user: investigationUser.name, isGroupedBySample: true },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [], sampleIds: [] },
          pagination: [],
          isGroupedBySample: true,
        },
      },
    },
    {
      description: "InvestigationUser can update isGroupedBySample",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser, selection: { datasetIds: [123, 456] }, isGroupedBySample: true }],
      userpreferences: { user: investigationUser.name, isGroupedBySample: false },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123, 456], sampleIds: [] },
          pagination: [],
          isGroupedBySample: false,
        },
      },
    },
    {
      description: "InvestigationUser can create user preferences with pagination",
      user: investigationUser.credential,
      userpreferences: { user: investigationUser.name, pagination: [{ key: "logbook", value: 100 }] },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [], sampleIds: [] },
          pagination: [{ key: "logbook", value: 100 }],
        },
      },
    },
    {
      description: "InvestigationUser can update user preferences with pagination",
      user: investigationUser.credential,
      prepopulatedPreferences: [{ user: investigationUser, selection: { datasetIds: [123, 456] }, pagination: [{ key: "logbook", value: 100 }] }],
      userpreferences: {
        user: investigationUser.name,
        pagination: [
          { key: "logbook", value: 100 },
          { key: "parcels", value: 10 },
        ],
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          selection: { datasetIds: [123, 456], sampleIds: [] },
          pagination: [
            { key: "logbook", value: 100 },
            { key: "parcels", value: 10 },
          ],
        },
      },
    },
    {
      description: "InvestigationUser can update user preferences without changes",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          emailNotificationReportEnabled: true,
          selection: { datasetIds: [123, 456], sampleIds: [789] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: { autoProcessingRankingShell: "overall", autoProcessingRankingParameter: "mean_I_over_sigI" },
          isGroupedBySample: false,
          pagination: [{ key: "logbook", value: 100 }],
        },
      ],
      userpreferences: {
        user: investigationUser.name,
        emailNotificationReportEnabled: true,
        selection: { datasetIds: [123, 456], sampleIds: [789] },
        logbookFilters: { sortBy: "createdAt", sortOrder: -1, createdAt: "2023-12-04T11:54:02.462Z", updatedAt: "2023-12-04T13:04:38.283Z", __v: 3 },
        recentlyVisited: [
          {
            url: "/investigation/1338650741/logbook",
            label: "Logbook",
            target: { instrumentName: "ID00" },
            createdAt: "2023-12-04T11:54:02.462Z",
            updatedAt: "2023-12-04T13:04:38.283Z",
            __v: 3,
          },
        ],
        mxsettings: {
          autoProcessingRankingShell: "overall",
          autoProcessingRankingParameter: "mean_I_over_sigI",
          createdAt: "2023-12-04T11:54:02.462Z",
          updatedAt: "2023-12-04T13:04:38.283Z",
          __v: 3,
        },
        isGroupedBySample: false,
        pagination: [{ key: "logbook", value: 100 }],
        createdAt: "2023-12-04T11:54:02.462Z",
        updatedAt: "2023-12-04T13:04:38.283Z",
        __v: 3,
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          emailNotificationReportEnabled: true,
          selection: { datasetIds: [123, 456], sampleIds: [789] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: { autoProcessingRankingShell: "overall", autoProcessingRankingParameter: "mean_I_over_sigI" },
          isGroupedBySample: false,
          pagination: [{ key: "logbook", value: 100 }],
        },
      },
    },
  ],
  updateSimultaneouslyUserPreferences: [
    {
      description: "InvestigationUser can update user preferences simultaneously with same value",
      user: investigationUser.credential,
      prepopulatedPreferences: [
        {
          user: investigationUser,
          emailNotificationReportEnabled: true,
          selection: { datasetIds: [123, 456], sampleIds: [789] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: { autoProcessingRankingShell: "overall", autoProcessingRankingParameter: "mean_I_over_sigI" },
          isGroupedBySample: true,
          pagination: [{ key: "logbook", value: 100 }],
        },
      ],
      userpreferences: {
        user: investigationUser.name,
        emailNotificationReportEnabled: true,
        selection: { datasetIds: [123, 456], sampleIds: [789] },
        logbookFilters: { sortBy: "createdAt", sortOrder: -1, createdAt: "2023-12-04T11:54:02.462Z", updatedAt: "2023-12-04T13:04:38.283Z", __v: 3 },
        recentlyVisited: [
          {
            url: "/investigation/1338650741/logbook",
            label: "Logbook",
            target: { instrumentName: "ID00" },
            createdAt: "2023-12-04T11:54:02.462Z",
            updatedAt: "2023-12-04T13:04:38.283Z",
            __v: 3,
          },
        ],
        mxsettings: {
          autoProcessingRankingShell: "overall",
          autoProcessingRankingParameter: "mean_I_over_sigI",
          createdAt: "2023-12-04T11:54:02.462Z",
          updatedAt: "2023-12-04T13:04:38.283Z",
          __v: 3,
        },
        isGroupedBySample: true,
        pagination: [{ key: "logbook", value: 100 }],
        createdAt: "2023-12-04T11:54:02.462Z",
        updatedAt: "2023-12-04T13:04:38.283Z",
        __v: 3,
      },
      expected: {
        status: 200,
        preferences: {
          username: investigationUser.name,
          emailNotificationReportEnabled: true,
          selection: { datasetIds: [123, 456], sampleIds: [789] },
          logbookFilters: { sortBy: "createdAt", sortOrder: -1 },
          recentlyVisited: [{ url: "/investigation/1338650741/logbook", label: "Logbook", target: { instrumentName: "ID00" } }],
          mxsettings: { autoProcessingRankingShell: "overall", autoProcessingRankingParameter: "mean_I_over_sigI" },
          isGroupedBySample: true,
          pagination: [{ key: "logbook", value: 100 }],
        },
      },
    },
  ],
  getUsersForInvestigation: [
    {
      description: "InstrumentScientist gets the list of users for investigation",
      user: instrumentScientist.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets the list of users for investigation",
      user: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "PrincipalInvestigator gets the list of users for investigation",
      user: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "LocalContact gets the list of users for investigation",
      user: localContact.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot get the list of users for investigation",
      user: anonymous.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser cannot get the list of users for investigation",
      user: investigationUser.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot get the list of users for investigation if not instrumentScientist of the investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      expected: {
        status: 403,
      },
    },
  ],
};
