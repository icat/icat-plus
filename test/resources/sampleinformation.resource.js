const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  uploadFile: [
    {
      description: "Investigation user can upload file information",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user cannot upload file information if sample is not editable",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[0].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot upload file information if filename already exists for this sample",
      user: investigationUser.credential,
      prepopulate: {
        user: investigationUser.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "file.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user can upload file information if another file exist for this sample",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: investigationUser.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument Scientist can upload file information",
      user: instrumentScientist.credential,
      sampleId: investigationUser.investigations.participates[0].samples[1].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can upload file information",
      user: administrator.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot upload file information",
      user: anonymous.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user cannot upload file information if no access to investigation",
      user: anonymous.credential,
      sampleId: investigationUser.investigations.noAccess.samples[0].id,
      data: "file content",
      filename: "file.pdb",
      fileInformation: {
        fileType: "pdb",
      },
      expected: {
        status: 403,
      },
    },
  ],
  getFiles: [
    {
      description: "Investigation user can get all files for a given sample",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
        sampleId: investigationUser.investigations.participates[1].samples[1].id,
        resources: [
          {
            filename: "existingfile.pdb",
            fileType: "pdb",
          },
        ],
      },
    },
    {
      description: "Investigation user can get all files for a given sample when empty",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user can get all files filtering by groupName",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      groupName: "b",
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "a.pdb",
            fileType: "pdb",
            groupName: "A",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
          {
            data: "existing file",
            filename: "b.pdb",
            fileType: "pdb",
            groupName: "B",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
        sampleId: investigationUser.investigations.participates[1].samples[1].id,
        resources: [
          {
            filename: "b.pdb",
            fileType: "pdb",
            groupName: "B",
          },
        ],
      },
    },
    {
      description: "Anonymous cannot get files for sample",
      user: anonymous.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can get all files for a given sample",
      user: administrator.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
        sampleId: investigationUser.investigations.participates[1].samples[1].id,
        resources: [
          {
            filename: "existingfile.pdb",
            fileType: "pdb",
          },
        ],
      },
    },
    {
      description: "InstrumentScientist can get all files for a given sample",
      user: instrumentScientist.credential,
      sampleId: investigationUser.investigations.participates[0].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[0].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
        sampleId: investigationUser.investigations.participates[0].samples[1].id,
        resources: [
          {
            filename: "existingfile.pdb",
            fileType: "pdb",
          },
        ],
      },
    },
  ],
  downloadFile: [
    {
      description: "Investigation user can download file",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can download file",
      user: administrator.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument Scientist can download file",
      user: instrumentScientist.credential,
      sampleId: investigationUser.investigations.participates[0].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[0].samples[1].id,
          },
        ],
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot download file",
      user: anonymous.credential,
      sampleId: investigationUser.investigations.participates[1].samples[1].id,
      prepopulate: {
        user: administrator.credential,
        sampleInformations: [
          {
            data: "existing file",
            filename: "existingfile.pdb",
            fileType: "pdb",
            sampleId: investigationUser.investigations.participates[1].samples[1].id,
          },
        ],
      },
      expected: {
        status: 403,
      },
    },
  ],
};
