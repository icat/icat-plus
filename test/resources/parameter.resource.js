const globalResource = require("../env/global.resource.js");
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  getParameters: [
    {
      description: "Get all parameterTypes",
      expected: { status: 200 },
    },
    {
      description: "Get parameterType by id",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      expected: { status: 200 },
    },
    {
      description: "Get parameterType by name",
      name: "definition",
      expected: { status: 200 },
    },
    {
      description: "Get parameterType with invalid id",
      parameterTypeId: 12312312131,
      expected: { status: 400 },
    },
  ],
};
