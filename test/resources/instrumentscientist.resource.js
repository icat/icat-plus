const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const instruments = globalResource.instruments.map((i) => i.name);

module.exports = {
  getInstrumentScientist: [
    {
      description: "InstrumentScientist gets the list of instrumentscientists",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets the list of instrumentscientists",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous doesn't get the list of instrumentscientists",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser doesn't get the list of instrumentscientists",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist gets the list of instrumentscientists per beamline",
      user: instrumentScientist.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets the list of instrumentscientists per beamline in lower case",
      user: instrumentScientist.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`.toLowerCase(),
      expected: {
        status: 200,
      },
    },
  ],
  createInstrumentScientist: [
    {
      description: "Administrator can create and remove instrumentScientists",
      user: administrator.credential,
      instrumentScientist: {
        usernames: [instrumentScientist.name],
        instrumentnames: [instruments[1]],
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can create a remove instrumentScientists with the same instrument as him",
      user: instrumentScientist.credential,
      instrumentScientist: {
        usernames: [anonymous.name],
        instrumentnames: [instrumentScientist.instrument.name],
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can not create or remove instrumentScientists for a different instrument",
      user: instrumentScientist.credential,
      instrumentScientist: {
        usernames: [anonymous.name],
        // returns a instrument that is not associated to the instrumentScientist user
        instrumentnames: [instruments[1]],
      },
      expected: {
        status: 500,
      },
    },

    {
      description: "Fail to add a instrumentscientist by instrumentScientist for non-existing instrument",
      user: instrumentScientist.credential,
      instrumentScientist: {
        usernames: [anonymous.name],
        instrumentnames: ["NON-EXISTING-INSTRUMENT"],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Fail to add a instrumentscientist by a instrumentScientist for non-existing user name",
      user: instrumentScientist.credential,
      instrumentScientist: {
        usernames: ["NON-EXISTING-USER-NAME"],
        instrumentnames: [instrumentScientist.instrument.name],
      },
      expected: {
        status: 400,
      },
    },

    {
      description: "Fail to add a instrumentscientist by anonymous",
      user: anonymous.credential,
      instrumentScientist: {
        usernames: [anonymous.name],
        instrumentnames: [instruments[0]],
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Fail to add a instrumentscientist by investigationUser ",
      user: investigationUser.credential,
      instrumentScientist: {
        usernames: [investigationUser.name],
        instrumentnames: [instruments[0]],
      },
      expected: {
        status: 403,
      },
    },
  ],
};
