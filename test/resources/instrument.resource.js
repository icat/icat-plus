const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  getInstruments: [
    {
      description: "Anonymous retrieves instruments",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves instruments",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist retrieves instruments",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser retrieves instruments",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getInstrumentsBySessionId: [
    {
      description: "Anonymous retrieves instruments",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves instruments",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist retrieves instruments",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser retrieves instruments",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser retrieves instruments with filter participant",
      user: investigationUser.credential,
      filter: "participant",
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist retrieves instruments with filter instrumentScientist",
      user: instrumentScientist.credential,
      filter: "instrumentScientist",
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser retrieves instruments with invalid filter",
      user: investigationUser.credential,
      filter: "administrator",
      expected: {
        status: 500,
      },
    },
  ],
};
