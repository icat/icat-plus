const globalResource = require("../env/global.resource.js");
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;

const prepopulatedTags = [
  {
    name: "Investigation Tag",
    investigationId: investigationUser.investigations.participates[0].investigationId,
    user: investigationUser.credential,
  },
  {
    name: "Investigation Tag with no access to investigationUser",
    investigationId: investigationUser.investigations.underEmbargo.investigationId,
    user: administrator.credential,
  },
  {
    name: "Investigation Tag in another investigation",
    investigationId: investigationUser.investigations.participates[1].investigationId,
    user: administrator.credential,
  },
  {
    name: "Beamline Tag",
    instrumentName: instrumentScientist.instrument.name,
    user: instrumentScientist.credential,
  },
  {
    name: "Global Tag",
    instrumentName: instrumentScientist.instrument.name,
    user: administrator.credential,
  },
  {
    name: "Investigation Tag in no access investigation",
    investigationId: investigationUser.investigations.noAccess.investigationId,
    user: administrator.credential,
  },
];
module.exports = {
  getTags: [
    {
      description: "InvestigationUser gets the tags by investigation including the beamline tags",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        occurrences: prepopulatedTags.filter(
          (tag) =>
            tag.investigationId === investigationUser.investigations.participates[0].investigationId ||
            (tag.instrumentName === investigationUser.investigations.participates[0].instrumentName && tag.investigationId === undefined)
        ).length,
      },
    },
    {
      description: "InvestigationUser can not get the tags from investigation with no access",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not get the tags from beamline logbook",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser can not get the global",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous can not get the tags by investigation ",
      user: anonymous.credential,
      tags: prepopulatedTags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous can not get the tags from beamline logbook",
      user: anonymous.credential,
      tags: prepopulatedTags,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist gets the tags by investigation",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        occurrences: prepopulatedTags.filter(
          (tag) =>
            tag.investigationId === investigationUser.investigations.participates[0].investigationId ||
            (tag.instrumentName === investigationUser.investigations.participates[0].instrumentName && tag.investigationId === undefined)
        ).length,
      },
    },
    {
      description: "InstrumentScientist gets the tags by instrument",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        status: 200,
        occurrences: prepopulatedTags.filter((tag) => tag.instrumentName === investigationUser.investigations.participates[0].instrumentName).length,
      },
    },
    {
      description: "Administrator gets the tags by investigation",
      user: administrator.credential,
      tags: prepopulatedTags,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        occurrences: prepopulatedTags.filter(
          (tag) =>
            tag.investigationId === investigationUser.investigations.participates[0].investigationId ||
            (tag.instrumentName === investigationUser.investigations.participates[0].instrumentName && tag.investigationId === undefined)
        ).length,
      },
    },
    {
      description: "Administrator gets the tags by instrument",
      user: administrator.credential,
      tags: prepopulatedTags,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        status: 200,
        occurrences: prepopulatedTags.filter((tag) => tag.instrumentName === investigationUser.investigations.participates[0].instrumentName).length,
      },
    },
  ],
  createTag: [
    {
      description: "InvestigationUser creates a tag with name",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a tag name (with case)",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a Tag NamE",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a tag with name, description",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        investigationId: investigationUser.investigations.participates[0].investigationId,
        description: " this is a description of the tag",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a tag with name, investigationId, description, color",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        investigationId: investigationUser.investigations.participates[0].investigationId,
        description: " this is a description of the tag",
        color: "blue",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a tag with name, investigationId, instrumentName, description, color",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        description: " this is a description of the tag",
        color: "blue",
        investigationId: investigationUser.investigations.participates[0].investigationId,
        instrumentName: "id01",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can not create a global tag",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not create a beamline logbook tag",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        instrumentName: instrumentScientist.instrument.name,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can not create a tag in other's investigation",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        insvestigationId: investigationUser.investigations.noAccess.investigationId,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not create a tag if the tag already exists within the investigation",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "Investigation Tag",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not create a tag if the tag already exists within the beamline",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "Beamline Tag",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not create a tag if the tag already exists as global",
      user: investigationUser.credential,
      tags: prepopulatedTags,
      tag: {
        name: "Global Tag",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InstrumentScientist creates a beamline logbook tag with name, instrumentName",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        instrumentName: instrumentScientist.instrument.name,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist creates a a beamline logbook tag with name, instrumentName, description",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        instrumentName: instrumentScientist.instrument.name,
        description: " this is a description of the tag",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist creates a beamline logbook tag with name, instrumentName, description, color",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "A tag name",
        instrumentName: instrumentScientist.instrument.name,
        description: " this is a description of the tag",
        color: "blue",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist creates a tag with name, investigationId, description, color",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        description: " this is a description of the tag",
        color: "blue",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can not create a tag if the tag already exists as global",
      user: instrumentScientist.credential,
      tags: prepopulatedTags,
      tag: {
        name: "Global Tag",
        instrumentName: instrumentScientist.instrument.name,
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator creates a tag with name, investigationId, description, color",
      user: administrator.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        description: " this is a description of the tag",
        color: "blue",
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator creates a beamline logbook tag with name, instrumentName, description, color",
      user: administrator.credential,
      tags: prepopulatedTags,
      tag: {
        name: "A tag name",
        instrumentName: instrumentScientist.instrument.name,
        description: " this is a description of the tag",
        color: "blue",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can not create a tag if the tag already exists as global",
      user: administrator.credential,
      tags: prepopulatedTags,
      tag: {
        name: "Global Tag",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous can not create a tag",
      user: anonymous.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
        description: " this is a description of the tag",
        color: "blue",
        investigationId: 2483226,
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not create a global tag",
      user: anonymous.credential,
      tags: prepopulatedTags,
      tag: {
        name: "a tag name",
      },
      expected: {
        status: 400,
      },
    },
  ],
  updateTag: [
    {
      description: "Investigation user updates a tag",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      populate: {
        user: investigationUser.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      update: {
        user: investigationUser.credential,
        tag: {
          name: "an updated tag name",
          description: "updated description",
          color: "red",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser updates a tag and it is stored in lowercase",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      populate: {
        user: investigationUser.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      update: {
        user: investigationUser.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can not update an beamline tag",
      populate: {
        user: instrumentScientist.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      update: {
        user: investigationUser.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not update a tag",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      populate: {
        user: investigationUser.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      update: {
        user: anonymous.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not update an beamline tag",
      populate: {
        user: instrumentScientist.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      update: {
        user: anonymous.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientst can update a investigation tag",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      populate: {
        user: investigationUser.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      update: {
        user: instrumentScientist.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can update an beamline tag",
      populate: {
        user: instrumentScientist.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      update: {
        user: instrumentScientist.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can update a investigation tag",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      populate: {
        user: investigationUser.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      update: {
        user: administrator.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can update an beamline tag",
      populate: {
        user: instrumentScientist.credential,
        tag: {
          name: "a tag name",
          description: "tag to be created which will be updated.",
          color: "blue",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      update: {
        user: administrator.credential,
        tag: {
          name: "An Updated Tag Name",
          description: "updated description",
          color: "red",
          instrumentName: instrumentScientist.instrument.name,
        },
      },
      expected: {
        status: 200,
      },
    },
  ],
  e2e: {
    description: "End two end test that simulates a complete scenario",
    investigationUser: investigationUser.credential,
    administrator: administrator.credential,
    instrumentScientist: instrumentScientist.credential,
    tags: prepopulatedTags,
    investigationId: investigationUser.investigations.participates[0].investigationId,
    instrumentName: instrumentScientist.instrument.name,
  },
};
