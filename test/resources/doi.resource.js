const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const principalInvestigator = globalResource.users.principalInvestigator;

const doi = {
  data: globalResource.doi.public[2],
  dataRelatedIdentifier: globalResource.doi.public[3],
};
module.exports = {
  mint: [
    {
      description: "Principal investigator mints a DOI",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "A DOI should be minted with creatorName",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ creatorName: "principal, Investigator", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "mint a DOI without title should return an error",
      user: principalInvestigator.credential,
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", id: "principalInvestigator__0" }],
      expected: {
        status: 500,
      },
    },
    {
      description: "mint a DOI if title is empty should return an error",
      user: principalInvestigator.credential,
      title: "",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", id: "principalInvestigator__0" }],
      expected: {
        status: 500,
      },
    },
    {
      description: "mint a DOI without abstract should return an error",
      user: principalInvestigator.credential,
      title: "Title",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", id: "principalInvestigator__0" }],
      expected: {
        status: 500,
      },
    },
    {
      description: "mint a DOI if abstract is empty should return an error",
      user: principalInvestigator.credential,
      title: "Title",
      abstract: "",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", id: "principalInvestigator__0" }],
      expected: {
        status: 500,
      },
    },
    {
      description: "mint a DOI if authors is empty should return an error",
      user: principalInvestigator.credential,
      title: "Title",
      abstract: "Abstract",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [],
      expected: {
        status: 500,
      },
    },
    {
      description: "mint a DOI if no authors should return an error",
      user: principalInvestigator.credential,
      title: "Title",
      abstract: "Abstract",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 500,
      },
    },
    {
      description: "Anonymous is not allowed to mint a DOI",
      user: anonymous.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "investigationUser can mint a DOI",
      user: investigationUser.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser is not allowed to mint a DOI on dataset if he is not a participant of the investigation",
      user: investigationUser.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.noAccess.datasets[0].id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "instrumentScientist is not allowed to mint a DOI",
      user: instrumentScientist.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "administrator is not allowed to mint a DOI",
      user: administrator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "Principal investigator mints a DOI with orcid",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0", orcid: "0000-0002-1825-0097" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator mints a DOI with invalid orcid",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0", orcid: "X000-0002-1825-0097" }],
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator mints from a reserved DOI",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      reserveDataCollection: true,
      reservedDatasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 200,
      },
    },
    {
      description: "Should return an error if no datasetId list or dataCollection is provided",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator cannot mint a DOI from a reserved DOI",
      user: administrator.credential,
      title: "Test",
      abstract: "Test",
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      reserveDataCollection: true,
      reservedDatasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot mint a DOI from a reserved DOI",
      user: instrumentScientist.credential,
      title: "Test",
      abstract: "Test",
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      reserveDataCollection: true,
      reservedDatasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous cannot mint a DOI from a reserved DOI",
      user: instrumentScientist.credential,
      title: "Test",
      abstract: "Test",
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      reserveDataCollection: true,
      reservedDatasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 403,
      },
    },
    {
      description: "Principal investigator mints a DOI with keywords",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      keywords: ["keyword1", "keyword2"],
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator mints a DOI with citationPublicationDOI",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      citationPublicationDOI: "10.4232/10.ASEAS-5.2-1",
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator mints a DOI with referenceURL",
      user: principalInvestigator.credential,
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: [{ name: "principalInvestigator", surname: "", id: "principalInvestigator__0" }],
      referenceURL: "https://www.rcsb.org/structure/8JSR",
      expected: {
        status: 200,
      },
    },
  ],
  datacite: [
    {
      description: "JSON datacite returns expected doi data",
      prefix: doi.data.prefix,
      suffix: doi.data.suffix,
      expected: {
        status: 200,
        instrument: doi.data.instrument,
        creators: doi.data.creators,
        contributors: doi.data.contributors,
        proposalTypeDescription: doi.data.proposalTypeDescription,
        name: doi.data.name,
      },
    },
    {
      description: "JSON datacite returns doi data with related identifiers",
      prefix: doi.dataRelatedIdentifier.prefix,
      suffix: doi.dataRelatedIdentifier.suffix,
      expected: {
        status: 200,
        instrument: doi.dataRelatedIdentifier.instrument,
        creators: doi.dataRelatedIdentifier.creators,
        contributors: doi.dataRelatedIdentifier.contributors,
        proposalTypeDescription: doi.dataRelatedIdentifier.proposalTypeDescription,
        name: doi.dataRelatedIdentifier.name,
        relatedIdentifiers: doi.dataRelatedIdentifier.relatedIdentifiers,
      },
    },
    {
      description: "JSON datacite should return an error if DOI is not existing",
      prefix: "0000",
      suffix: "NO-1234",
      expected: {
        status: 500,
      },
    },
  ],
  expReports: [
    {
      description: "Experimental reports returns information for ES DOI",
      prefix: globalResource.doi.public[0].prefix,
      suffix: globalResource.doi.public[0].suffix,
      expected: {
        status: 200,
        reports: [
          {
            nbReports: 1,
          },
        ],
      },
    },
    {
      description: "Experimental reports returns information for DC DOI",
      prefix: globalResource.doi.public[1].prefix,
      suffix: globalResource.doi.public[1].suffix,
      expected: {
        status: 200,
        reports: [
          {
            nbReports: 1,
          },
        ],
      },
    },
  ],
  reserveDOI: [
    {
      description: "Principal investigator can reserve a DOI",
      user: principalInvestigator.credential,
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator cannot reserve a DOI if authorList is empty",
      user: principalInvestigator.credential,
      datasetIdList: [],
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator cannot reserve a DOI",
      user: administrator.credential,
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous cannot reserve a DOI",
      user: anonymous.credential,
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser cannot reserve a DOI on dataset if he/she is not a participant of the investigation",
      user: investigationUser.credential,
      datasetIdList: [investigationUser.investigations.noAccess.datasets[0].id],
      expected: {
        status: 403,
      },
    },
  ],
  getReservedDOIs: [
    {
      description: "Principal investigator can retrieve reserved DOIs",
      user: principalInvestigator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can retrieve reserved DOIs, but empty list",
      user: principalInvestigator.credential,
      expected: {
        status: 200,
        length: 0,
      },
    },
    {
      description: "InstrumentScientist can retrieve reserved DOIs",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve reserved DOIs",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
  ],
};
