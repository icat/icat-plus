const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  extractUserNamePrefix: [
    {
      description: "esrf username pattern",
      username: "test01234/11",
      expected: {
        username: "test01234",
      },
    },
    {
      description: "undefined username",
      username: undefined,
      expected: {
        username: "",
      },
    },
    {
      description: "db username pattern",
      username: "test01234",
      expected: {
        username: "test01234",
      },
    },
    {
      description: "empty username",
      username: "",
      expected: {
        username: "",
      },
    },
  ],
  getUserByUsernamePrefix: [
    {
      description: "db user",
      user: administrator.credential,
      username: "admin",
      expected: {
        status: 200,
        users: [],
      },
    },
    {
      description: "esrf user with another suffix",
      user: administrator.credential,
      username: "test123/0001",
      expected: {
        status: 200,
        users: [{ name: "test123/0003" }, { name: "test123/0002" }],
      },
    },
    {
      description: "esrf user without other suffix",
      user: administrator.credential,
      username: "test456/0011",
      expected: {
        status: 200,
        users: [],
      },
    },
  ],
  reload: [
    {
      description: "Administrator reloads the cache",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot reload the cache",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser cannot reload the cache",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot reload the cache",
      user: instrumentScientist.credential,
      expected: {
        status: 403,
      },
    },
  ],
};
