const globalResource = require("../env/global.resource.js");
const { createEventByAttributes } = require("./../helper/logbook.js");
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const anonymousUser = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;
const CONSTANTS = require("../../app/constants");

module.exports = {
  mint: [
    {
      description: "A DOI is minted per dataset with the API KEY",
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: "Daniyal J., Jafry; Maire, Bellier; Christopher, Werlan, 0000-0003-2021-5432;",
      expected: {
        status: 200,
      },
    },
    {
      description: "Mint a DOI with keywords",
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: "Daniyal J., Jafry; Maire, Bellier; Christopher, Werlan, 0000-0003-2021-5432;",
      keywords: ["keyword"],
      expected: {
        status: 200,
      },
    },
    {
      description: "Mint a DOI with citationPublicationDOI",
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: "Daniyal J., Jafry; Maire, Bellier; Christopher, Werlan, 0000-0003-2021-5432;",
      citationPublicationDOI: "10.4232/10.ASEAS-5.2-1",
      expected: {
        status: 200,
      },
    },
    {
      description: "Mint a DOI with referenceURL",
      title: "Test",
      abstract: "Test",
      datasetIdList: [investigationUser.investigations.participates[0].datasets.datasetWithSample.id],
      authors: "Daniyal J., Jafry; Maire, Bellier; Christopher, Werlan, 0000-0003-2021-5432;",
      referenceURL: "https://www.rcsb.org/structure/8JSR",
      expected: {
        status: 200,
      },
    },
  ],

  getDatasetByInvestigationId: [
    {
      description: "Data acquisition can retrieve datasets by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId with a limit to 1",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId with a search query",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      search: "t",
      expected: {
        status: 200,
      },
    },
    {
      description: "Data acquisition can retrieve datasets by investigationId sorted by name",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sortBy: "name",
      sortOrder: 1,
      expected: {
        status: 200,
      },
    },
  ],

  createBeamlineNotification: [
    {
      description: "Creation of a beamline notification",
      tagsToFillDB: [],
      event: {
        instrumentName: instrumentScientist.instrument.name,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Creation of a beamline notification with new tag",
      tagsToFillDB: [],
      adminUser: administrator.credential,
      event: {
        instrumentName: instrumentScientist.instrument.name,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "newtag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "newtag", instrumentName: instrumentScientist.instrument.name }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "newtag", instrumentName: instrumentScientist.instrument.name }],
      },
    },
    {
      description: "Creation of a beamline notification with existing global tag",
      tagsToFillDB: [{ name: "tag" }],
      adminUser: administrator.credential,
      event: {
        instrumentName: instrumentScientist.instrument.name,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag" }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "tag" }],
      },
    },
    {
      description: "Creation of a beamline notification with existing beamline tag",
      tagsToFillDB: [{ name: "tag", instrumentName: instrumentScientist.instrument.name }],
      adminUser: administrator.credential,
      event: {
        instrumentName: instrumentScientist.instrument.name,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag", instrumentName: instrumentScientist.instrument.name }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [{ name: "tag", instrumentName: instrumentScientist.instrument.name }],
      },
    },
    {
      description: "Creation of a beamline notification with existing another beamline tag",
      tagsToFillDB: [{ name: "tag", instrumentName: "ID00" }],
      adminUser: administrator.credential,
      event: {
        instrumentName: instrumentScientist.instrument.name,
        type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
        category: CONSTANTS.EVENT_CATEGORY_DEBUG,
        title: "a title",
        tag: [{ name: "tag" }],
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          instrumentName: instrumentScientist.instrument.name,
          type: CONSTANTS.EVENT_TYPE_NOTIFICATION,
          category: CONSTANTS.EVENT_CATEGORY_DEBUG,
          title: "a title",
          tag: [{ name: "tag", instrumentName: instrumentScientist.instrument.name }],
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
        tags: [
          { name: "tag", instrumentName: "ID00" },
          { name: "tag", instrumentName: instrumentScientist.instrument.name },
        ],
      },
    },
  ],
  createInvestigationNotification: [
    {
      description: "Event investigation name in lower case",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName.toLowerCase(),
        instrumentName: investigationUser.investigations.participates[0].instrumentName.toLowerCase(),
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event investigation name in capital letters",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with no creationDate",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 200,
        event: {
          type: "notification",
          title: "a title",
          category: "comment",
          content: [
            { format: "plainText", text: "this is my comment" },
            { format: "html", text: "<p>this is my comment</p>" },
          ],
        },
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, not in summer time",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, no offset, a date in summer time",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-04-09T10:18:22.749",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-04-09T10:18:22.749+02:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, with offset",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749+01:00",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749+01:00",
        }),
      },
    },
    {
      description: "Event with creationDate : ISO format, UTC",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "2020-03-09T10:18:22.749Z",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "2020-03-09T10:18:22.749Z",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon, 09 Mar 2020 09:51:54 GMT",
        }),
      },
    },
    {
      description: "Event with creationDate: RFC 2822 format)",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon 09 Mar 2020 15:24:38 +0400",
        }),
      },
    },
    {
      description: "Event with creationDate: unknown format)",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          creationDate: "Mon Mar 09 2020 15:24:38 GMT+0400 (CET)",
        }),
      },
    },
    {
      description: "Event with a tag with name",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "myTestTag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          tag: [
            {
              name: "mytesttag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag with ID",
      adminUser: administrator.credential,
      tagsToFillDB: [
        {
          name: "My TAG",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        tag: [{ name: "My TAG" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "my tag",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with a tag that comes as a string with ID",
      adminUser: administrator.credential,
      tagsToFillDB: [
        {
          name: "mytag",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      ],
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        category: "comment",
        tag: '[{"name":"global"}]',
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          type: "notification",
          category: "comment",
          tag: [
            {
              name: "global",
              investigationId: investigationUser.investigations.participates[0].investigationId,
            },
          ],
        }),
      },
    },
    {
      description: "Event with investigation name that can not be normalized",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "XX-III-ProposalMathilde",
        instrumentName: instrumentScientist.instrument.name,
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with wrong instrumentName",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        investigationName: "id010100",
        instrumentName: "not instrument",
        type: "notification",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with no instrumentName",
      adminUser: administrator.credential,
      tagsToFillDB: [],
      event: {
        type: "notification",
        investigationName: "id010100",
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Event with no type",
      adminUser: administrator.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        title: "a title",
        category: "comment",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
    {
      description: "Event with no category",
      adminUser: administrator.credential,
      event: {
        token: global.gServerConfig.server.API_KEY,
        investigationId: investigationUser.investigations.participates[0].investigationId,
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        title: "a title",
        content: [
          { format: "plainText", text: "this is my comment" },
          { format: "html", text: "<p>this is my comment</p>" },
        ],
        creationDate: "2018-09-01T00:00:01.000Z",
      },
      expected: {
        status: 422,
      },
    },
    {
      description: "Creation of an investigation notification with new tag",
      tagsToFillDB: [],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "newtag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: "notification",
          tag: [{ name: "newtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
        }),
        tags: [{ name: "newtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      },
    },
    {
      description: "Creation of an investigation notification with existing global tag",
      tagsToFillDB: [{ name: "globaltag" }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "globaltag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: "notification",
          tag: [{ name: "globaltag" }],
        }),
        tags: [{ name: "globaltag" }],
      },
    },
    {
      description: "Creation of an investigation notification with existing beamline tag",
      tagsToFillDB: [{ name: "beamlinetag", instrumentName: investigationUser.investigations.participates[0].instrumentName }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "beamlinetag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: "notification",
          tag: [{ name: "beamlinetag", instrumentName: investigationUser.investigations.participates[0].instrumentName }],
        }),
        tags: [{ name: "beamlinetag", instrumentName: investigationUser.investigations.participates[0].instrumentName }],
      },
    },
    {
      description: "Creation of an investigation notification with existing investigation tag",
      tagsToFillDB: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.participates[0].investigationName,
        instrumentName: investigationUser.investigations.participates[0].instrumentName,
        type: "notification",
        tag: [{ name: "investigationtag" }],
      }),
      expected: {
        status: 200,
        event: createEventByAttributes({
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: "notification",
          tag: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
        }),
        tags: [{ name: "investigationtag", investigationId: investigationUser.investigations.participates[0].investigationId }],
      },
    },
    {
      description: "Creation of an investigation notification in a released logbook should fail",
      adminUser: administrator.credential,
      investigationId: investigationUser.investigations.released.investigationId,
      event: createEventByAttributes({
        investigationName: investigationUser.investigations.released.investigationName,
        instrumentName: investigationUser.investigations.released.instrumentName,
        type: "notification",
      }),
      expected: {
        status: 403,
      },
    },
  ],
  createBroadcastEvent: [
    {
      description: "Creation of a broadcast notification",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_INFO,
        title: "machine title",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 200,
        event: {
          type: "broadcast",
          title: "machine title",
          category: "info",
          machine: "ebs",
          content: [{ format: "plainText", text: "machine comment" }],
        },
      },
    },
    {
      description: "Creation of a broadcast notification with tags",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "machine title",
        machine: "ebs",
        tag: [{ name: "machine" }, { name: "machine tag2" }],
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 200,
        event: {
          type: "broadcast",
          title: "machine title",
          category: "error",
          content: [{ format: "plainText", text: "machine comment" }],
          tag: [
            {
              name: "machine",
            },
            {
              name: "machine tag2",
            },
          ],
        },
      },
    },
    {
      description: "Creation of a broadcast notification fails if wrong sessionId",
      tagsToFillDB: [],
      sessionId: "wrong sessionId",
      event: {
        type: CONSTANTS.EVENT_TYPE_BROADCAST,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "machine title",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "Creation of a broadcast notification fails if type is not broadcast",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        type: CONSTANTS.EVENT_TYPE_ANNOTATION,
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "wrong type",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "Creation of a broadcast notification fails if type is undefined",
      tagsToFillDB: [],
      sessionId: global.gServerConfig.server.API_KEY,
      event: {
        category: CONSTANTS.EVENT_CATEGORY_ERROR,
        title: "no type",
        machine: "ebs",
        content: [{ format: "plainText", text: "machine comment" }],
      },
      expected: {
        status: 400,
      },
    },
  ],
  createFromBase64byBeamlineAndInstrumentName: [
    {
      description: "InvestigationUser creates a event with base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser creates a event with base64 and tag",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
        tag: [{ name: "myTag" }],
      },
      expected: {
        status: 200,
        tagCount: 1,
      },
    },
    {
      description: "InvestigationUser creates a event with base64 and tags",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
        tag: [{ name: "myTag1" }, { name: "myTag2" }],
      },
      expected: {
        status: 200,
        tagCount: 2,
      },
    },
    {
      description: "InvestigationUser creates a event with base64 and tags with name duplicated",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
        tag: [{ name: "myTag1" }, { name: "myTag1" }, { name: "myTag1" }, { name: "myTag2" }],
      },
      expected: {
        status: 200,
        tagCount: 2,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid instrument",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: "Invalid instrument",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with invalid investigation name",
      user: investigationUser.credential,
      investigationName: "TEST-XXXXX",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with wrong apiKey",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      apiKey: "WrongAPI",
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser creates a event with no base64",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no investigationName",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no instrumentName",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.participates[0].investigationName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser creates a event with no apiKey",
      user: investigationUser.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {},
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser cannot create an event in a released investigation",
      user: investigationUser.credential,
      investigationName: investigationUser.investigations.released.investigationName,
      instrumentName: investigationUser.investigations.released.instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist creates an event with base64 in beamline logbook",
      user: instrumentScientist.credential,
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      body: {
        base64: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQYV2NgYAAAAAMAAWgmWQ0AAAAASUVORK5CYII=",
      },
      expected: {
        status: 200,
      },
    },
  ],
  restore: [
    {
      description: "Restore data should trigger an error if no datasetId",
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [],
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data should return empty array if no entry to restore",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [],
      expected: {
        status: 200,
        nbUpdated: 0,
      },
    },
    {
      description: "Restore data should return expected values to restore",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [
        {
          datasetId: 123,
          user: investigationUser.name,
        },
      ],
      expected: {
        status: 200,
        nbUpdated: 1,
      },
    },
    {
      description: "Restore data should send email to several users for the same datasetId",
      datasetId: 123,
      level: "info",
      message: "Restoration Ok",
      dataAccessToFillDB: [
        {
          datasetId: 123,
          user: investigationUser.name,
        },
        {
          datasetId: 123,
          user: anonymousUser.name,
          email: "test@test.fr",
        },
        {
          datasetId: 456,
          user: anonymousUser.name,
          email: "test2@test.fr",
        },
      ],
      expected: {
        status: 200,
        nbUpdated: 2,
      },
    },
  ],
  moveEvents: [
    {
      description: "Move events should return an error if wrong API key",
      adminUser: administrator.credential,
      apiKey: "WrongAPI",
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
      ],
      expected: {
        status: 403,
      },
    },
    {
      description: "Events cannot be moved if no sourceInvestigationId",
      adminUser: administrator.credential,
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
      ],
      expected: {
        status: 400,
      },
    },
    {
      description: "Events cannot be moved if no destinationSourceId",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
      ],
      expected: {
        status: 400,
      },
    },
    {
      description: "Events cannot be moved if destinationSourceId does not match with an existing session",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      destinationInvestigationId: 123,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
      ],
      expected: {
        status: 400,
      },
    },
    {
      description: "Move events should work as expected",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "another comment",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[1].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment from another investigation",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        modified: 2,
      },
    },
    {
      description: "Move events does not modify events, if no event match the investigationId",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[1].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[1].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "another comment",
            },
          ],
        },
        {
          investigationId: investigationUser.investigations.participates[1].investigationId,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment from another investigation",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        modified: 0,
      },
    },
    {
      description: "Move events does not modify events, if no investigationId",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          beamlineName: investigationUser.investigations.participates[1].beamlineName,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
        {
          type: CONSTANTS.EVENT_TYPE_BROADCAST,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "broadcast comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        modified: 0,
      },
    },
    {
      description: "Move events should modify investigationId, investigationName and instrumentName",
      adminUser: administrator.credential,
      sourceInvestigationId: investigationUser.investigations.participates[0].investigationId,
      destinationInvestigationId: investigationUser.investigations.participates[1].investigationId,
      events: [
        {
          investigationId: investigationUser.investigations.participates[0].investigationId,
          investigationName: investigationUser.investigations.participates[0].investigationName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
          type: CONSTANTS.EVENT_TYPE_ANNOTATION,
          category: CONSTANTS.EVENT_CATEGORY_COMMENT,
          content: [
            {
              format: "plainText",
              text: "a comment",
            },
          ],
        },
      ],
      expected: {
        status: 200,
        modified: 1,
        events: [
          {
            investigationId: investigationUser.investigations.participates[1].investigationId,
            investigationName: investigationUser.investigations.participates[1].investigationName,
            instrumentName: investigationUser.investigations.participates[1].instrumentName,
            type: CONSTANTS.EVENT_TYPE_ANNOTATION,
            category: CONSTANTS.EVENT_CATEGORY_COMMENT,
            content: [
              {
                format: "plainText",
                text: "a comment",
              },
            ],
          },
        ],
      },
    },
  ],
};
