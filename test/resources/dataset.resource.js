const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

const doi = {
  investigation: {
    prefix: globalResource.doi.public[0].prefix,
    suffix: globalResource.doi.public[0].suffix,
  },
  dc: {
    prefix: globalResource.doi.public[1].prefix,
    suffix: globalResource.doi.public[1].suffix,
  },
};

module.exports = {
  getDataCollectionByDataSetId: [
    {
      description: "Retrieve a datacollection by datasetId",
      datasetId: "117610320",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getTimeline: [
    {
      description: "InvestigationUser gets timeline datasets by investigationId",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can not get timeline datasets without investigationId",
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser gets timeline datasets by investigationId and sampleId",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets timeline datasets by investigation",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can not get the timeline datasets for embargoed investigation",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous can get the timeline datasets for released investigation",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.released.investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetByDOI: [
    {
      description: "Administrator get datasets by DOI attached to an investigation",
      user: administrator.credential,
      doi: doi.investigation,
      limit: 25,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator get datasets by DOI (in lower case) attached to an investigation",
      user: administrator.credential,
      doi: JSON.parse(JSON.stringify(doi.investigation).toLowerCase()),
      limit: 25,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous get datasets by DOI attached to an investigation",
      user: anonymous.credential,
      doi: doi.investigation,
      limit: 25,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation",
      user: investigationUser.credential,
      doi: doi.investigation,
      limit: 25,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator get datasets by DOI attached to a data collection",
      user: administrator.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous get datasets by DOI attached to a data collection",
      user: anonymous.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection",
      user: investigationUser.credential,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection with a limit to 1",
      user: investigationUser.credential,
      limit: 1,
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection sorted by sampleName ",
      user: investigationUser.credential,
      sortBy: "SAMPLENAME",
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to a data collection with a search query ",
      user: investigationUser.credential,
      search: "test",
      doi: doi.dc,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation with a limit to 1",
      user: investigationUser.credential,
      limit: 1,
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user get datasets by DOI attached to an investigation sorted by sampleName ",
      user: investigationUser.credential,
      sortBy: "SAMPLENAME",
      doi: doi.investigation,
      limit: 25,
      expected: {
        status: 200,
      },
    },
    /*
    Skipped because of docker compose. TODO
    {
      description: "Investigation user get datasets by DOI attached to an investigation with a search query ",
      user: investigationUser.credential,
      search: "MX",
      doi: doi.investigation,
      expected: {
        status: 200,
      },
    },
    */
  ],
  getDatasetDocumentByDateRange: [
    {
      description: "Administrator retrieves a list of datasets, from beamlines which beamline name contains a dash,  by date range",
      startDate: "2018-08-20",
      endDate: "2018-08-22",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user can retrieve a list of datasets by date range",
      startDate: "2017-01-23",
      endDate: "2017-01-24",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous user can not retrieve a list of datasets by date range",
      startDate: "2017-01-23",
      endDate: "2017-01-24",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetByDatasetIds: [
    {
      description: "Retrieve a single dataset with NO Sample",
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieve a single dataset",
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieve a comma separated list of datasets",
      datasetIds: `${investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id}, ${investigationUser.investigations.participates[0].datasets.datasetWithSample.id}`,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetBy: [
    {
      description: "InvestigationUser can retrieve a single dataset by datasetId with NO Sample",
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by multiple datasetIds",
      datasetIds: `${investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id}, ${investigationUser.investigations.participates[0].datasets.datasetWithSample.id}`,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve a  dataset by datasetId with limit=1",
      datasetIds: `${investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id}, ${investigationUser.investigations.participates[0].datasets.datasetWithSample.id}`,
      user: investigationUser.credential,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId with a limit=1 and skip=1",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      limit: 1,
      skip: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId with a search query",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      search: "t",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId sorted by name",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      sortBy: "name",
      sortOrder: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and datasetType",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      datasetType: "acquisition",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and datasetType with processed data",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.processed[0].investigationId,
      datasetType: "acquisition",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and datasetType with processed data and nested",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.processed[0].investigationId,
      datasetType: "acquisition",
      nested: true,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and datasetType with processed data",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.processed[0].investigationId,
      datasetType: "processed",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot retrieve datasets by investigationId",
      user: anonymous.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve datasets by investigationId",
      user: administrator.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can retrieve datasets by investigationId",
      user: instrumentScientist.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and sampleId",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieving datasets without investigationId or datasetIds or sampleId should return an error",
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by sampleId",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and filter on parameters",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~like~test",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and filter on parameters with in",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~in~test;test2",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by investigationId and multiple filters on parameters",
      user: investigationUser.credential,
      investigationIds: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~like~test,__volume~gteq~2",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by sampleId and filter on parameters",
      user: investigationUser.credential,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      parameters: "sample_name~like~test",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by datasetIds and filter on parameters",
      user: investigationUser.credential,
      datasetIds: `${investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id}, ${investigationUser.investigations.participates[0].datasets.datasetWithSample.id}`,
      parameters: "sample_name~like~test",
      expected: {
        status: 200,
      },
    },
    {
      description: "Retrieving datasets without investigationId, sampleId, datasetIds or instrumentName should failed",
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by instrumentName",
      user: investigationUser.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve datasets by instrumentName",
      user: administrator.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientists can retrieve datasets by instrumentName",
      user: instrumentScientist.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      sortBy: "STARTDATE",
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientists can retrieve datasets by instrumentName with limit",
      user: instrumentScientist.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      limit: 20,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientists can retrieve datasets by instrumentName in lower case",
      user: instrumentScientist.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName.toLowerCase()}`,
      expected: {
        status: 200,
      },
    },
    {
      description: "Incorrect sortBy should throw an error",
      user: investigationUser.credential,
      instrumentName: `${investigationUser.investigations.participates[0].instrumentName}`,
      sortBy: "TEST",
      expected: {
        status: 500,
      },
    },
    {
      description: "InvestigationUser can retrieve datasets by multiple investigationIds",
      user: investigationUser.credential,
      investigationIds: `${investigationUser.investigations.participates[0].investigationId},${investigationUser.investigations.participates[1].investigationId}`,
      limit: 3,
      expected: {
        status: 200,
      },
    },
  ],
  getDataDownload: [
    {
      description: "Data download failed if no datasetIds and no datafileIds",
      user: investigationUser.credential,
      expected: {
        status: 404,
      },
    },
  ],
  getDataRestore: [
    {
      description: "Restore data failed if no datasetId ",
      user: investigationUser.credential,
      name: investigationUser.name,
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data failed if no name ",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 400,
      },
    },
    {
      description: "Restore data should record a dataset query",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: "63335951",
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: 63335951,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a dataset query with email",
      user: investigationUser.credential,
      name: anonymous.name,
      email: anonymous.email,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: anonymous.name,
          email: anonymous.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should not record a query if a query already exists for the same user, dataset, ongoing",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists but done",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: investigationUser.name,
        type: "RESTORE",
        status: "DONE",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another user",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
        user: instrumentScientist.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
    {
      description: "Restore data should record a query if a query already exists for another dataset",
      user: investigationUser.credential,
      name: investigationUser.name,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      prepopulate: {
        datasetId: 123,
        user: investigationUser.name,
        type: "RESTORE",
        status: "ONGOING",
      },
      expected: {
        status: 200,
        datasetAccess: {
          datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          user: investigationUser.name,
          email: investigationUser.email,
          type: "RESTORE",
          status: "ONGOING",
        },
      },
    },
  ],
};
