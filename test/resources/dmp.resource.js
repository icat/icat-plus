const globalResource = require("../env/global.resource.js");

const administrator = globalResource.users.administrator;
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  questionnaires: [
    {
      description: "should return uuid if questionnaire exists, for administrator user",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "exitingInvestigationName",
      expected: {
        status: 200,
        questionnaires: [{ uuid: "a-uuid" }],
      },
    },
    {
      description: "should return empty if questionnaire does not exist in DMP",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "nonExitingInvestigationName",
      expected: {
        status: 200,
        questionnaires: [],
      },
    },
    {
      description: "should return error if DMP endpoint failed",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "failed",
      expected: {
        status: 500,
      },
    },
    {
      description: "should return the questionnaire if instrumentscientist user",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "exitingInvestigationName",
      expected: {
        status: 200,
        questionnaires: [{ uuid: "a-uuid" }],
      },
    },
    {
      description: "should return the questionnaire if investigation user",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "exitingInvestigationName",
      expected: {
        status: 200,
        questionnaires: [{ uuid: "a-uuid" }],
      },
    },
    {
      description: "should return error if anonymous user",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      investigationName: "exitingInvestigationName",
      expected: {
        status: 403,
      },
    },
    {
      description: "should return error if investigation user has no access to the investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      investigationName: "exitingInvestigationName",
      expected: {
        status: 403,
      },
    },
  ],
};
