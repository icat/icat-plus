const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  byDataCollections: [
    {
      description: "Investigation user gets dataCollections",
      user: investigationUser.credential,
      limit: 500,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets dataCollections",
      user: anonymous.credential,
      limit: 500,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets dataCollections",
      user: administrator.credential,
      limit: 500,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist gets dataCollections",
      user: instrumentScientist.credential,
      limit: 500,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets dataCollections with search",
      user: anonymous.credential,
      search: "test",
      limit: 1500,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets dataCollections by datasetId",
      user: anonymous.credential,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser gets dataCollections by datasetId",
      user: investigationUser.credential,
      datasetId: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        status: 200,
      },
    },
  ],
};
