const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  updateSampleParameter: [
    {
      description: "Investigation user can update sample parameter if sample is editable and type is description",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user cannot update sample parameter if no access to investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].descriptionParameter.id,
      value: "sample desc",
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user cannot update sample parameter if no access to parameter",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.noAccess.samples[0].descriptionParameter.id,
      value: "sample desc",
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot update sample parameter if not sample description",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].otherParameter.id,
      value: "other param",
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot update sample parameter if sample not editable",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[1].descriptionParameter.id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 400,
      },
    },
    {
      description: "Instrument Scientist can update sample parameter if sample is editable and type is description",
      user: instrumentScientist.credential,
      investigationId: instrumentScientist.investigations.instrumentScientist.investigationId,
      parameterId: instrumentScientist.investigations.instrumentScientist.samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can update sample parameter if sample is editable and type is description",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot update sample parameter if sample is editable and type is description",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user can update sample parameter if type is comment",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].commentParameter.id,
      value: "test_sample_comment_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Instrument Scientist can update sample parameter if type is comment",
      user: instrumentScientist.credential,
      investigationId: instrumentScientist.investigations.instrumentScientist.investigationId,
      parameterId: instrumentScientist.investigations.instrumentScientist.samples[0].commentParameter.id,
      value: "test_sample_comment_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator user can update sample parameter if type is comment",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[2].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].commentParameter.id,
      value: "test_sample_comment_",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user cannot update sample description if the investigationId does not match the sample investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      parameterId: investigationUser.investigations.participates[2].samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot update sample description if the investigation is released",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      parameterId: investigationUser.investigations.participates[1].samples[0].descriptionParameter.id,
      value: "test_sample_desc_",
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user cannot update sample comment if the investigation is released",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      parameterId: investigationUser.investigations.participates[1].samples[0].commentParameter.id,
      value: "test_sample_comment_",
      expected: {
        status: 403,
      },
    },
  ],
  createSampleParameter: [
    {
      description: "Investigation user can create sample parameter",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user cannot create sample parameter if no access to investigation",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.noAccess.investigationId,
      sampleId: investigationUser.investigations.noAccess.samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 403,
      },
    },
    {
      description: "Investigation user cannot create sample parameter if no access to sample",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.noAccess.samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot create sample parameter if not sample description",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Other",
      value: "other param",
      expected: {
        status: 400,
      },
    },
    {
      description: "Investigation user cannot create sample parameter if sample not editable",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[1].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 400,
      },
    },
    {
      description: "InstrumentScientist can create sample parameter",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot create sample parameter",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can create sample parameter",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_description",
      value: "sample desc",
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user can create sample parameter for comment",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_notes",
      value: "sample comment",
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can create sample parameter for comment",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_notes",
      value: "sample comment",
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can create sample parameter for comment",
      user: administrator.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_notes",
      value: "sample comment",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous cannot create sample parameter for comment",
      user: anonymous.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      name: "Sample_notes",
      value: "sample comment",
      expected: {
        status: 403,
      },
    },
  ],
};
