const globalResource = require("../env/global.resource.js");

const administrator = globalResource.users.administrator;
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;

const message = {
  type: "INFORMATION",
  message: "we are on maintenance",
};

module.exports = {
  getMessages: [
    {
      description: "Retrieve message type information of exists",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      type: "INFORMATION",
      expected: {
        status: 200,
        message: [
          {
            type: "INFORMATION",
            message: "we are on maintenance",
          },
        ],
      },
    },
    {
      description: "Retrieve messages type information is empty list",
      prepopulate: {
        messages: [],
        user: administrator,
      },
      expected: {
        status: 200,
        message: [],
      },
    },
    {
      description: "Retrieve all messages without type",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      expected: {
        status: 200,
        message: [
          {
            type: "INFORMATION",
            message: "we are on maintenance",
          },
        ],
      },
    },
  ],
  updateInformationMessage: [
    {
      description: "Administrator can create information message",
      prepopulate: {
        messages: [],
        user: administrator,
      },
      user: administrator.credential,
      message: {
        type: "INFORMATION",
        message: "new information message",
      },
      expected: {
        status: 200,
        message: {
          type: "INFORMATION",
          message: "new information message",
        },
      },
    },
    {
      description: "Administrator can update information message",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      user: administrator.credential,
      message: {
        type: "INFORMATION",
        message: "not on maintenance anymore",
      },
      expected: {
        status: 200,
        message: {
          type: "INFORMATION",
          message: "not on maintenance anymore",
        },
      },
    },
    {
      description: "Anonymous cannot update information message",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      user: anonymous.credential,
      message: {
        type: "INFORMATION",
        message: "not on maintenance anymore",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InvestigationUser cannot update information message",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      user: investigationUser.credential,
      message: {
        type: "INFORMATION",
        message: "not on maintenance anymore",
      },
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot update information message",
      prepopulate: {
        messages: [message],
        user: administrator,
      },
      user: instrumentScientist.credential,
      message: {
        type: "INFORMATION",
        message: "not on maintenance anymore",
      },
      expected: {
        status: 403,
      },
    },
  ],
};
