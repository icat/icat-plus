module.exports = {
  users: {
    allowed: [
      {
        plugin: "db",
        username: "reader",
        password: "",
      },
    ],
    denied: [
      {
        plugin: "db",
        username: "reader",
        password: "badpassword",
      },
    ],
  },
};
