const globalResource = require("../env/global.resource.js");

const administrator = globalResource.users.administrator;
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  instrumentMetrics: [
    {
      description: "Administrator gets data metrics by instrument",
      user: administrator.credential,
      ranges: [
        {
          name: "2020",
          start: "2017-08-20",
          end: "2018-08-31",
        },
      ],
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets data metrics",
      user: administrator.credential,
      ranges: [
        {
          name: "2020",
          start: "2020-08-20",
          end: "2020-08-31",
        },
      ],
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can not access to data metrics",
      user: anonymous.credential,
      ranges: [
        {
          name: "2017",
          start: "2017-01-01",
          end: "2017-12-31",
        },
      ],
      expected: {
        status: 403,
      },
    },
    {
      description: "investigationUser can not access to data metrics",
      user: investigationUser.credential,
      ranges: [
        {
          name: "2017",
          start: "2017-01-01",
          end: "2017-12-31",
        },
      ],
      expected: {
        status: 403,
      },
    },
  ],
};
