const globalResource = require("../env/global.resource.js");
const anonymous = globalResource.users.anonymous;
module.exports = {
  request: [
    {
      endPoint: "/h5grove/meta",
      description: "Anoynmous can not get a file that is not public via /meta",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/attr",
      description: "Anoynmous can not get a file that is not public via /attr",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/data",
      description: "Anoynmous can not get a file that is not public via /data",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
    {
      endPoint: "/h5grove/stats",
      description: "Anoynmous can not get a file that is not public via /stats",
      user: anonymous.credential,
      datafileId: 123859093,
      expected: {
        status: 404,
      },
    },
  ],
};
