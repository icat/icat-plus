const globalResource = require("../env/global.resource.js");

const investigationUser = globalResource.users.investigationUser;
const principalInvestigator = globalResource.users.principalInvestigator;
const localContact = globalResource.users.localContact;
const anonymous = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  // Use to revoke permissions one the test failed so DB is cleaned
  revoker: administrator.credential,
  investigationusers: [
    {
      description: "Get investigation Users from a investigation by participant",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
      },
    },
  ],
  addInvestigationUser: [
    {
      description: "Anyonymous can not grant permissions",
      user: anonymous.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 400 },
    },
    {
      description: "InvestigationUser can not grant permissions",
      user: investigationUser.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 403,
      },
    },
    {
      description: "Principal investigator can add a collaborator",
      user: principalInvestigator.credential,
      revoker: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can add a collaborator",
      user: administrator.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 200,
      },
    },
    {
      description: "Local contact can add a collaborator",
      user: localContact.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can add a collaborator",
      user: instrumentScientist.credential,
      revoker: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 200,
      },
    },
    {
      description: "Principal investigator can not grant access to a non existing user",
      user: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "Non-existing-user" },
      expected: {
        status: 400,
      },
    },
    {
      description: "Principal investigator can not grant access to a non existing investigation",
      user: principalInvestigator.credential,
      investigationId: 1111000999888990,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 400,
      },
    },
    {
      description: "InstrumentScientist can not add a collaborator if not instrumentScientist of the investigation",
      user: instrumentScientist.credential,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 403,
      },
    },
  ],
  deleteInvestigationUser: [
    {
      description: "Anyonymous cannot revoke a collaborator",
      user: anonymous.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 400 },
    },
    {
      description: "InvestigationUser cannot revoke a collaborator",
      user: investigationUser.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 403 },
    },
    {
      description: "InstrumentScientist cannot revoke a collaborator if not instrumentScientist of the investigation",
      user: instrumentScientist.credential,
      granter: administrator.credential,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 403 },
    },
    {
      description: "InstrumentScientist can revoke a collaborator",
      user: instrumentScientist.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 200 },
    },
    {
      description: "PrincipalInvestigator can revoke a collaborator",
      user: principalInvestigator.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 200 },
    },
    {
      description: "Administrator can revoke a collaborator",
      user: administrator.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 200 },
    },
    {
      description: "LocalContact can revoke a collaborator",
      user: localContact.credential,
      granter: administrator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "CollaboratorUser" },
      expected: { status: 200 },
    },
    {
      description: "Principal investigator cannot revoke a non existing user",
      user: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: "Non-existing-user" },
      expected: {
        status: 400,
      },
    },
    {
      description: "Principal investigator cannot revoke a collaborator from an non existing investigation",
      user: principalInvestigator.credential,
      investigationId: 1111000999888990,
      collaborator: { name: "CollaboratorUser" },
      expected: {
        status: 400,
      },
    },
    {
      description: "Principal investigator cannot revoke a user which is not a collaborator",
      user: principalInvestigator.credential,
      investigationId: principalInvestigator.investigation.principalInvestigator.investigationId,
      collaborator: { name: investigationUser.name },
      expected: {
        status: 400,
      },
    },
  ],
};
