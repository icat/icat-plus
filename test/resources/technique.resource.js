const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const investigationUser = globalResource.users.investigationUser;

module.exports = {
  getTechniques: [
    {
      description: "Anonymous retrieves techniques",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves techniques",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist retrieves techniques",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser retrieves techniques",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
  ],
};
