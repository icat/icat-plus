const globalResource = require("../env/global.resource.js");
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const administrator = globalResource.users.administrator;
const anonymous = globalResource.users.anonymous;

module.exports = {
  upload: [
    {
      description: "User can upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: investigationUser.fullName,
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
    },
    {
      description: "Administrator can upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: administrator.fullName,
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
    },
    {
      description: "InstrumentScientist can upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: instrumentScientist,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: instrumentScientist.fullName,
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
    },
    {
      description: "Anonymous cannot upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: anonymous,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 403,
      },
    },
    {
      description: "ApiKey can upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      apiKey: global.gServerConfig.server.API_KEY,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: "API_KEY",
          investigationId: investigationUser.investigations.participates[0].investigationId,
        },
      },
    },
    {
      description: "Wrong apiKey cannot upload a file in a given investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      apiKey: "WRONG API_KEY",
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 400,
      },
    },
    {
      description: "User cannot upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: administrator.fullName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
        },
      },
    },
    {
      description: "InstrumentScientist can upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      user: instrumentScientist,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: instrumentScientist.fullName,
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
        },
      },
    },
    {
      description: "Anonymous cannot upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].investigationId,
      user: anonymous,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator cannot upload a file with unknown instrument",
      instrumentName: "unknown",
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot upload a file without investigationId or instrumentName",
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 400,
      },
    },
    {
      description: "ApiKey can upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      apiKey: global.gServerConfig.server.API_KEY,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
        event: {
          type: "attachment",
          category: "file",
          username: "API_KEY",
          instrumentName: investigationUser.investigations.participates[0].instrumentName,
        },
      },
    },
    {
      description: "Wrong apiKey cannot upload a file in a beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      apiKey: "WRONG API_KEY",
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 400,
      },
    },
  ],
  download: [
    {
      description: "User can download a file from an event in an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      uploadUser: instrumentScientist,
      user: investigationUser,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can download a file from an event in an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      uploadUser: instrumentScientist,
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can download a file from an event in an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      uploadUser: instrumentScientist,
      user: instrumentScientist,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can download a file from an event in an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      uploadUser: instrumentScientist,
      user: anonymous,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist cannot download a file from an unknown event in an investigation",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      uploadUser: instrumentScientist,
      user: instrumentScientist,
      eventId: "test",
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 500,
      },
    },
    {
      description: "User can download a file from an event in an beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      uploadUser: instrumentScientist,
      user: investigationUser,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can download a file from an event in an beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      uploadUser: instrumentScientist,
      user: instrumentScientist,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can download a file from an event in an beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      uploadUser: instrumentScientist,
      user: administrator,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous can download a file from an event in an beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      uploadUser: instrumentScientist,
      user: anonymous,
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist cannot download a file from an unknown event in an beamline logbook",
      instrumentName: investigationUser.investigations.participates[0].instrumentName,
      uploadUser: instrumentScientist,
      user: instrumentScientist,
      eventId: "test",
      file: new Buffer.alloc(128, "R0lGODlhDAAMAKIFAF5LAP/zxAAAANyuAP/gaP///wAAAAAAACH5BAEAAAUALAAAAAAMAAwAAAMlWLPcGjDKFYi9lxKBOaGcF35DhWHamZUW0K4mAbiwWtuf0uxFAgA7", "base64"),
      expected: {
        status: 500,
      },
    },
  ],
};
