const globalResource = require("../env/global.resource.js");

const instruments = globalResource.instruments;
const investigationTypes = globalResource.investigationTypes;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;
const anonymous = globalResource.users.anonymous;
const administrator = globalResource.users.administrator;

module.exports = {
  allocateInvestigations: [
    {
      user: administrator.credential,
      description: "Allocate proposal when time in between the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-26T10:00:00",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 6 hours before the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-26T02:00:00",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 8 hours before the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-26T00:00:00",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 8 hours and 1 second before the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-25T23:59:59",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is 48 hours and 1 second before the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-24T07:59:59",
      expected: {
        status: 200,
        visitId: undefined,
      },
    },
    {
      user: administrator.credential,
      description: "Allocate proposal when time is after the time slot",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-09-05T08:00:00",
      expected: {
        status: 200,
        visitId: undefined,
      },
    },
    {
      user: investigationUser.credential,
      description: "investigationUser can allocate proposal when time is in the range 48 hours",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-25T08:00:00",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
    {
      user: administrator.credential,
      description: "Administrator can allocate proposal when time is in the range 48 hours",
      investigationName: investigationUser.investigations.timeSlots.investigationName,
      instrumentName: investigationUser.investigations.timeSlots.instrumentName,
      time: "2020-08-25T08:00:00",
      expected: {
        status: 200,
        visitId: investigationUser.investigations.timeSlots.visitId,
      },
    },
  ],
  normalize: [
    {
      input: "20-011219",
      expected: {
        status: 200,
        name: "20-01-1219",
      },
    },
    {
      input: "MX0000",
      expected: {
        status: 200,
        name: "MX-0000",
      },
    },
    {
      input: "mx2260",
      expected: {
        status: 200,
        name: "MX-2260",
      },
    },
    {
      input: "ihls3203",
      expected: {
        status: 200,
        name: "IH-LS-3203",
      },
    },
    {
      input: "ihls3203",
      expected: {
        status: 200,
        name: "IH-LS-3203",
      },
    },
    {
      input: "hc4170",
      expected: {
        status: 200,
        name: "HC-4170",
      },
    },
    {
      input: "ih-ls-3203",
      expected: {
        status: 200,
        name: "IH-LS-3203",
      },
    },
    {
      input: "ev280",
      expected: {
        status: 200,
        name: "EV-280",
      },
    },
    {
      input: "ip0001",
      expected: {
        status: 200,
        name: "IP-0001",
      },
    },
    {
      input: "id000000",
      expected: {
        status: 200,
        name: "ID000000",
      },
    },
    {
      input: "bm230002",
      expected: {
        status: 200,
        name: "BM230002",
      },
    },
    {
      input: "bM23-0002",
      expected: {
        status: 200,
        name: "BM230002",
      },
    },
    {
      input: "BM230002",
      expected: {
        status: 200,
        name: "BM230002",
      },
    },
    {
      input: "id30a-12303",
      expected: {
        status: 200,
        name: "ID30A-12303",
      },
    },
    {
      input: "ID30A-10101",
      expected: {
        status: 200,
        name: "ID30A-10101",
      },
    },
    {
      input: "id30a10203",
      expected: {
        status: 200,
        name: "ID30A-10203",
      },
    },
    {
      input: "id992303",
      expected: {
        status: 500,
      },
    },
  ],
  getTimeline: [
    {
      description: "InvestigationUser can not get time line",
      user: investigationUser.credential,
      instrument: instruments[0],
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous can not get time line",
      user: investigationUser.credential,
      instrument: instruments[0],
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator get time line",
      user: administrator.credential,
      instrument: instruments[0],
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can not get time line without instrument",
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "InstrumentScientist get time line",
      user: instrumentScientist.credential,
      instrument: instruments[0],
      expected: {
        status: 200,
      },
    },
  ],
  investigations: [
    {
      description: "Investigation User  investigations by sessionId",
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets investigations by sessionId",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets investigations by sessionId",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getInvestigationByReleasedStatus: [
    {
      description: "Anonymous gets all released investigations",
      user: anonymous.credential,
      status: "released",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: `Investigation user gets all released investigations limit to 1 and instrumentName=${instruments[0].name}`,
      user: investigationUser.credential,
      status: "released",
      instrumentName: instruments[0].name,
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all released investigations",
      user: administrator.credential,
      status: "released",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all released investigations limit to 1",
      user: administrator.credential,
      status: "released",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },

    {
      description: "InvestigationUser gets all released investigations limit to 1",
      user: investigationUser.credential,
      status: "released",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations after a date",
      user: investigationUser.credential,
      status: "released",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations before a date",
      user: investigationUser.credential,
      status: "released",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations sorted by name",
      user: investigationUser.credential,
      status: "released",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all released investigations filtered by search",
      user: investigationUser.credential,
      status: "released",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationByEmbargoedStatus: [
    {
      description: "Anonymous gets all embargoed investigations",
      user: anonymous.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations",
      user: investigationUser.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all embargoed investigations",
      user: administrator.credential,
      status: "embargoed",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations limit to 1",
      user: investigationUser.credential,
      status: "embargoed",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: `Investigation user gets all embargoed investigations limit to 1 and instrumentName=${instruments[0].name}`,
      user: investigationUser.credential,
      status: "embargoed",
      instrumentName: instruments[0].name,
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all embargoed investigations limit to 1",
      user: administrator.credential,
      status: "embargoed",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations after a date",
      user: investigationUser.credential,
      status: "embargoed",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations before a date",
      user: investigationUser.credential,
      status: "embargoed",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations sorted by name",
      user: investigationUser.credential,
      status: "embargoed",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all embargoed investigations filtered by search",
      user: investigationUser.credential,
      status: "embargoed",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationByParticipantStatus: [
    {
      description: "Anonymous gets all participant investigations",
      user: anonymous.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations",
      user: investigationUser.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations with limit",
      user: investigationUser.credential,
      limit: 1,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: `Investigation user gets all participant investigations limit to 1 and instrumentName=${instruments[0].name}`,
      user: investigationUser.credential,
      status: "participant",
      instrumentName: instruments[0].name,
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Administrator gets all participant investigations",
      user: administrator.credential,
      status: "participant",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations limit to 1",
      user: investigationUser.credential,
      status: "participant",
      limit: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations after a date",
      user: investigationUser.credential,
      status: "participant",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations before a date",
      user: investigationUser.credential,
      status: "participant",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations sorted by name",
      user: investigationUser.credential,
      status: "participant",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all participant investigations filtered by search",
      user: investigationUser.credential,
      status: "participant",
      search: "title",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigationsByUsername: [
    {
      description: "Anonymous can retrieve open investigations where investigationUser is a participant",
      user: anonymous.credential,
      username: investigationUser.name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve investigations where investigationUser is a participant",
      user: administrator.credential,
      username: investigationUser.name,
      expected: {
        status: 200,
      },
    },
  ],
  getInvestigationByInstrument: [
    {
      description: "Investigation user gets all investigation by instrument",
      user: instrumentScientist.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigation by multiple instruments",
      user: instrumentScientist.credential,
      instrument: `${instruments[0].name},${instruments[1].name}`,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation by instrument",
      user: administrator.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigation by instrument limit to 1",
      user: instrumentScientist.credential,
      instrument: instruments[0].name,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation by instrument limit to 1",
      user: administrator.credential,
      instrument: instruments[0].name,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  after a date",
      user: investigationUser.credential,
      status: "participant",
      startDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  before a date",
      user: investigationUser.credential,
      status: "participant",
      endDate: "2019-01-01",
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument  sorted by name",
      user: investigationUser.credential,
      status: "participant",
      sortBy: "name",
      sortOrder: 1,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
    {
      description: "Investigation user gets all investigations by instrument filtered by search",
      user: investigationUser.credential,
      status: "participant",
      search: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        industrials: investigationTypes.industrials,
        status: 200,
      },
    },
  ],
  getInvestigations: [
    {
      description: "Investigation user gets investigation",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigation",
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument",
      user: investigationUser.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument in upper case",
      user: investigationUser.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations by instrument in lower case",
      user: investigationUser.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigations by instrument",
      user: administrator.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigations by instrument",
      user: anonymous.credential,
      instrument: instruments[0].name,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigations by instrument limit to 1",
      user: administrator.credential,
      instrument: instruments[0].name,
      limit: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "Investigation user gets investigations and withHasAccess",
      user: investigationUser.credential,
      instrument: instruments[0].name,
      withHasAccess: true,
      limit: 25,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser gets investigations and search by text",
      user: investigationUser.credential,
      search: investigationUser.investigations.participates[0].instrumentName,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser gets investigations and search by denormalized investigation",
      user: investigationUser.credential,
      search: investigationUser.investigations.participates[0].investigationName.replace("-", ""),
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can retrieve investigations by instrument name and filter on parameters",
      user: administrator.credential,
      instrument: instruments[0].name,
      limit: 25,
      parameters: "__fileCount~gteq~1",
      expected: {
        status: 200,
      },
    },
  ],
  getInvestigationsFilteredOnInstrument: [],
  getMultipleInvestigations: [
    {
      description: "Administrator gets multiple investigations",
      user: administrator.credential,
      ids: investigationUser.investigations.participates.map((inv) => inv.investigationId),
      expected: {
        status: 200,
        numberInvestigations: investigationUser.investigations.participates.length,
      },
    },
    {
      description: "User gets multiple investigations he has access to",
      user: investigationUser.credential,
      ids: investigationUser.investigations.participates.map((inv) => inv.investigationId),
      expected: {
        status: 200,
        numberInvestigations: investigationUser.investigations.participates.length,
      },
    },
    {
      description: "When asking for multiple investigations, the user gets only the ones he has access to",
      user: investigationUser.credential,
      ids: [investigationUser.investigations.participates[0].investigationId, investigationUser.investigations.noAccess.investigationId],
      expected: {
        status: 200,
        numberInvestigations: 1,
      },
    },
  ],
  getInvestigationsByPeriod: [
    {
      description: "Investigation user gets investigation on a period",
      startDate: "2020-07-01",
      endDate: "2020-07-31",
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator gets all investigation on a period",
      user: administrator.credential,
      startDate: "2020-07-01",
      endDate: "2020-07-31",
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous gets all investigation on a period",
      user: anonymous.credential,
      startDate: "2017-01-01",
      endDate: "2019-07-31",
      expected: {
        status: 200,
      },
    },
  ],
};
