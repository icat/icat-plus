const globalResource = require("../env/global.resource.js");

const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const administrator = globalResource.users.administrator;
const instrumentScientist = globalResource.users.instrumentScientist;

module.exports = {
  getSamplesByInvestigationId: [
    {
      description: "InvestigationUser retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: anonymous.credential,
      expected: {
        status: 200,
      },
    },
  ],
  getSamplesBy: [
    {
      description: "InvestigationUser retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Anonymous retrieves samples by investigationId",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: anonymous.credential,
      expected: {
        status: 200,
        count: 0,
      },
    },
    {
      description: "InvestigationUser retrieves samples by sampleId",
      sampleIds: investigationUser.investigations.participates[0].samples[0].id,
      user: investigationUser.credential,
      expected: {
        status: 200,
        count: 1,
      },
    },
    {
      description: "InvestigationUser retrieves samples by sampleId and investigationId",
      sampleIds: investigationUser.investigations.participates[0].samples[0].id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
        count: 1,
      },
    },
    {
      description: "InvestigationUser retrieves samples by sampleId and investigationId that does not correspond ",
      sampleIds: investigationUser.investigations.participates[0].samples[0].id,
      investigationId: investigationUser.investigations.participates[1].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
        count: 0,
      },
    },
    {
      description: "InvestigationUser retrieves samples by multiple sampleIds",
      sampleIds: [investigationUser.investigations.participates[0].samples[0].id, investigationUser.investigations.participates[0].samples[1].id],
      user: investigationUser.credential,
      expected: {
        status: 200,
        count: 2,
      },
    },
    {
      description: "InvestigationUser does not find any sample by search criteria and investigationId",
      search: "NotFindable",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      expected: {
        status: 200,
        count: 0,
      },
    },
    {
      description: "InvestigationUser can retrieve samples without filters",
      user: investigationUser.credential,
      limit: 10,
      expected: {
        status: 200,
        count: 10,
      },
    },
    {
      description: "InvestigationUser can retrieve samples with datasetType ",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.processed[0].investigationId,
      datasetType: "acquisition",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples with datasetType and nested",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.processed[0].investigationId,
      datasetType: "acquisition",
      nested: true,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples with datasetType equals to processed data",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.processed[0].investigationId,
      datasetType: "processed",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples with limit",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      user: investigationUser.credential,
      limit: 1,
      expected: {
        status: 200,
        count: 1,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by investigationId sorted by name",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sortBy: "name",
      sortOrder: 1,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by investigationId and filter on parameters",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~like~test",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by investigationId and filter on parameters with in",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~in~test;test2",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by investigationId and multiple filters on parameters",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      parameters: "sample_name~like~test,__volume~gteq~2",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by sampleId and filter on parameters",
      user: investigationUser.credential,
      sampleIds: investigationUser.investigations.participates[0].samples[0].id,
      parameters: "sample_name~like~test",
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser can retrieve samples by filtering on samples having acquisition datasets",
      user: investigationUser.credential,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      havingAcquisitionDatasets: true,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves samples by instrumentName",
      instrumentName: investigationUser.investigations.participates[1].instrumentName,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves samples by multiple instruments",
      instrumentName: `${investigationUser.investigations.participates[1].instrumentName},${investigationUser.investigations.participates[0].instrumentName}`,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
  ],
  page: [
    {
      description: "InvestigationUser can retrieve sample page",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Sample not found should return an error",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[1].samples[0].id,
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous cannot retrieve sample page if under embargo",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      user: anonymous.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can retrieve sample page",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can retrieve sample page",
      investigationId: investigationUser.investigations.participates[0].investigationId,
      sampleId: investigationUser.investigations.participates[0].samples[0].id,
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
  ],
};
