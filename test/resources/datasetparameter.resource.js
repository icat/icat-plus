const globalResource = require("../env/global.resource.js");
const instruments = globalResource.instruments;
const { anonymous, investigationUser, administrator, instrumentScientist } = globalResource.users;

module.exports = {
  getDatasetParametersValues: [
    {
      description: "InvestigationUser retrieves the values by investigationId and two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        name: "definition,definition",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves the values by investigationId, sampleId and two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        name: "definition,definition",
        sampleId: investigationUser.investigations.participates[0].samples[0].id,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves the values by investigationId and a filter on the parameters",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        name: "definition",
        parameters: "__fileCount~gt~0",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves the values by multiple investigationIds and two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: `${investigationUser.investigations.participates[0].investigationId}, ${investigationUser.investigations.participates[1].investigationId} `,
        name: "definition,definition",
      },
      expected: {
        status: 200,
      },
    },
  ],
  getCSVDatasetParameters: [
    {
      description: "InvestigationUser retrieves CSV with dataset parameters by investigationId and two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        format: "csv",
        name: "__volume,InstrumentSlitPrimary_vertical_offset",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves CSV with dataset parameters by investigationId and sampleId with two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        format: "csv",
        name: "__volume,InstrumentSlitPrimary_vertical_offset",
        sampleId: investigationUser.investigations.participates[0].samples[0].id,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser will fail to retrieve a CSV with a single parameter",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        format: "csv",
        name: "__volume",
      },
      expected: {
        status: 500,
      },
    },
    {
      description: "InvestigationUser retrieves the values in csv by multiple investigationIds and two parameters",
      user: investigationUser.credential,
      query: {
        investigationId: `${investigationUser.investigations.participates[0].investigationId}, ${investigationUser.investigations.participates[1].investigationId} `,
        format: "csv",
        name: "__volume,InstrumentSlitPrimary_vertical_offset",
      },
      expected: {
        status: 200,
      },
    },
  ],
  getDatasetParameters: [
    {
      description: "InvestigationUser retrieves dataset parameters by investigationId",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves dataset parameters by investigationId and sampleId",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        sampleId: investigationUser.investigations.participates[0].samples[0].id,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves dataset parameters by investigationId and sampleId and datasetType",
      user: investigationUser.credential,
      query: {
        datasetType: "acquisition",
        investigationId: investigationUser.investigations.participates[0].investigationId,
        sampleId: investigationUser.investigations.participates[0].samples[0].id,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves dataset parameters by investigationId and sampleId and wrong datasetType",
      user: investigationUser.credential,
      query: {
        datasetType: "Non-existing dataset Type",
        investigationId: investigationUser.investigations.participates[0].investigationId,
        sampleId: investigationUser.investigations.participates[0].samples[0].id,
      },
      expected: {
        status: 200,
        isEmpty: true,
      },
    },
    {
      description: "Administrator can not retrieve dataset parameters without investigationId and instrumentName",
      query: {},
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can not retrieve dataset parameters by instrumentName without start and end date",
      query: { instrumentName: instruments[0].name },
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "InvestigationUser can not retrieve dataset parameters by instrumentName without start and end date",
      query: { instrumentName: instruments[0].name },
      user: investigationUser.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can not retrieve dataset parameters by instrument without endDate",
      query: { instrumentName: instruments[0].name, startDate: "2024-05-01" },
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can retrieve dataset parameters by instrument",
      query: { instrumentName: instruments[0].name, startDate: "2011-05-01", endDate: "2024-05-01", name: "definition" },
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "InstrumentScientist can retrieve dataset parameters by instrument",
      query: { instrumentName: instruments[0].name, startDate: "2011-05-01", endDate: "2024-05-01", name: "definition" },
      user: instrumentScientist.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser can retrieve dataset parameters by instrument",
      query: { instrumentName: instruments[0].name, startDate: "2011-05-01", endDate: "2024-05-01", name: "definition" },
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "investigationUser can retrieve dataset parameters by instrument and unique",
      query: { instrumentName: instruments[0].name, startDate: "2011-05-01", endDate: "2024-05-01", name: "definition", unique: "true" },
      user: investigationUser.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator can not retrieve dataset parameters by instrument with incorrect start date format",
      query: { instrumentName: instruments[0].name, startDate: "01-05-2011", endDate: "2024-05-01", name: "definition" },
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator can not retrieve dataset parameters by instrument with incorrect end date format",
      query: { instrumentName: instruments[0].name, startDate: "2011-05-01", endDate: "05-01-2024", name: "definition" },
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Anonymous retrieve empty list of dataset parameters from a under embargo investigation",
      user: anonymous.credential,
      query: {
        investigationId: investigationUser.investigations.underEmbargo.investigationId,
      },
      expected: {
        status: 200,
        isEmpty: true,
      },
    },
    {
      description: "Anonymous gets an list of dataset parameters from a released investigation",
      user: anonymous.credential,
      query: {
        investigationId: investigationUser.investigations.released.investigationId,
      },
      expected: {
        status: 200,
        isEmpty: false,
      },
    },
    {
      description: "Anonymous gets a list of dataset parameters from a open investigationId with datasetType",
      user: anonymous.credential,
      query: {
        investigationId: investigationUser.investigations.released.investigationId,
        datasetType: "acquisition",
      },
      expected: {
        status: 200,
        isEmpty: false,
      },
    },
    {
      description: "instrumentScientist  retrieves dataset parameters by investigationId",
      user: instrumentScientist.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist  retrieves dataset parameters by investigationId with datasetType",
      user: instrumentScientist.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        datasetType: "acquisition",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "administrator  retrieves dataset parameters by investigationId",
      user: administrator.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "instrumentScientist  retrieves dataset parameters by investigationId with datasetType with Dataset Type",
      user: instrumentScientist.credential,
      query: {
        investigationId: investigationUser.investigations.participates[0].investigationId,
        datasetType: "acquisition",
      },
      expected: {
        status: 200,
      },
    },
    {
      description: "InvestigationUser retrieves dataset parameters by multiple investigationId",
      user: investigationUser.credential,
      query: {
        investigationId: `${investigationUser.investigations.participates[0].investigationId}, ${investigationUser.investigations.participates[1].investigationId}`,
      },
      expected: {
        status: 200,
        isEmpty: false,
      },
    },
    {
      description: "InvestigationUser cannot retrieve datasets parameters if not access to the investigation",
      user: investigationUser.credential,
      query: {
        investigationId: investigationUser.investigations.noAccess.investigationId,
      },
      expected: {
        status: 200,
        isEmpty: true,
      },
    },
  ],
  updateParameter: [
    {
      description: "InvestigationUser cannot update dataset parameter",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      value: "motors ",
      user: investigationUser.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Anonymous cannot update dataset parameter",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      value: "motors ",
      user: anonymous.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "InstrumentScientist cannot update dataset parameter",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      value: "motors ",
      user: instrumentScientist.credential,
      expected: {
        status: 403,
      },
    },
    {
      description: "Administrator can update dataset parameter",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      value: "motors ",
      user: administrator.credential,
      expected: {
        status: 200,
      },
    },
    {
      description: "Administrator cannot update dataset parameter without parameterId",
      parameterId: "",
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot update dataset parameter with undefined value",
      parameterId: investigationUser.investigations.participates[0].datasets.datasetWithSample.parameters[0].id,
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
    {
      description: "Administrator cannot update dataset parameter if parameter does not exist",
      parameterId: 12988898545,
      value: "motors ",
      user: administrator.credential,
      expected: {
        status: 400,
      },
    },
  ],
};
