const globalResource = require("../env/global.resource.js");

const releasedInvestigation = globalResource.users.anonymous.investigations.released;
module.exports = {
  getDBByInvestigationId: [
    {
      description: "Get panosc demonstrator DB items by investigationId",
      investigationId: releasedInvestigation.investigationId,
      expected: {
        status: 200,
      },
    },
    {
      description: "Get panosc demonstrator DB fails with no investigationId",
      expected: {
        status: 400,
      },
    },
  ],
  getItemsByDateRange: [
    {
      description: "Get items by date",
      startDate: "2019-05-05",
      endDate: "2019-06-05",
      expected: {
        status: 200,
      },
    },
    {
      description: "Get items by date with no startDate",
      endDate: "2019-06-05",
      expected: {
        status: 500,
      },
    },
    {
      description: "Get items by date with no startDate, endDate",
      expected: {
        status: 500,
      },
    },
  ],
};
