const globalResource = require("../env/global.resource.js");

const administrator = globalResource.users.administrator;
const anonymous = globalResource.users.anonymous;
const investigationUser = globalResource.users.investigationUser;
const instrumentScientist = globalResource.users.instrumentScientist;

const identifier = "MXDataReductionPipelines_ID23";
const items = {
  items: [
    {
      category: "MX",
      id: "MXDataReductionPipelines_ID23-1",
      input_schema: {
        description: "This is a simple example of Reprocessing by using Ewoks.  ",
        properties: {
          anomalous: { default: true, description: "", title: "Anomalous", type: "boolean" },
          end_image: { title: "End Image", type: "number" },
          forced_spacegroup: { title: "Forced spacegroup", type: "string" },
          high_res_limit: { title: "High resolution limit", type: "number" },
          low_res_limit: { title: "Low resolution limit", type: "number" },
          mx_pipeline_name: {
            default: ["EDNA_proc"],
            items: { enum: ["EDNA_proc", "autoPROC", "XIA2_DIALS", "grenades_fastproc", "grenades_parallelproc"], type: "string" },
            title: "Pipeline",
            type: "array",
            uniqueItems: true,
          },
          start_image: { title: "Start image", type: "number" },
        },
        title: "Demo POC",
        type: "object",
      },
      keywords: { cardinality: "1", definition: "*", instrumentName: "ID23-1", scanType: "*" },
      label: "MXDataReductionPipelines (ID23-1)",
      ui_schema: {
        mx_pipeline_name: { "ui:widget": "checkboxes" },
        "ui:order": ["mx_pipeline_name", "start_image", "end_image", "low_res_limit", "high_res_limit", "forced_spacegroup", "anomalous", "*"],
      },
    },
    { id: "workflow2", input_schema: {}, keywords: { cardinality: 1 }, label: "workflow2", ui_schema: {} },
    { category: "", id: "demo", label: "demo" },
  ],
};

module.exports = {
  getInputsLocation: [
    {
      description: "Raw Location of a dataset of type acquistion is its own location",
      user: administrator.credential,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      expected: {
        location: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.location,
      },
    },
  ],
  getWorkflows: [
    {
      description: "investigationUser submits a job",
      submitter: investigationUser.credential,
      reader: investigationUser.credential,
      items,
      identifier,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      queue: "test",
      updates: [
        { status: "STARTED" },
        { logs: { message: "Starting the process" } },
        { status: "RUNNING", step: { name: "INTEGRATION", status: "started" } },
        { step: { name: "INTEGRATION", status: "finished", message: "Integration finised successfully" } },
        { status: "FINISHED", logs: { message: "Processed successfully. The answer to life is 42" } },
      ],
      expected: {
        submitStatus: 200,
        readerStatus: 200,
        jobCount: 1,
      },
    },
    {
      description: "investigationUser submits a job to an investigation with no access",
      submitter: investigationUser.credential,
      reader: investigationUser.credential,
      items,
      identifier,
      datasetIds: investigationUser.investigations.noAccess.datasets[0].id,
      expected: {
        submitStatus: 403,
        readerStatus: 400,
      },
    },
    {
      description: "administrator submits a job and reader can list it because belongs to the same investigation",
      submitter: administrator.credential,
      reader: investigationUser.credential,
      items,
      identifier,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      all: true,
      queue: "test",
      expected: {
        submitStatus: 200,
        readerStatus: 200,
        jobCount: 1,
      },
    },

    {
      description: "investigationUser fails to run a workflow because no identifier",
      submitter: investigationUser.credential,
      reader: investigationUser.credential,
      items,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      queue: "test",
      expected: {
        submitStatus: 400,
        readerStatus: 200,
        jobCount: 0,
      },
    },
    {
      description: "anonymous can not run a workflow ",
      submitter: anonymous.credential,
      reader: anonymous.credential,
      identifier,
      items,
      datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
      queue: "test",
      expected: {
        submitStatus: 403,
      },
    },
  ],
  getJobs: [
    {
      description: "InvestigationUser can retrieve jobs by jobId",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      jobId: "jobId1",
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser should retrieve an empty jobs list if jobId does not exist",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      jobId: "jobIdNotExit",
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "Anonymous cannot retrieve jobs by jobId",
      user: anonymous.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      jobId: "jobId1",
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "Administrator cannot retrieve investigationUser jobs by jobId",
      user: administrator.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      jobId: "jobId1",
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "InstrumentScientist cannot retrieve investigationUser jobs by jobId",
      user: instrumentScientist.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      jobId: "jobId1",
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier1",
          },
          {
            identifier: "identifier2",
          },
        ],
      },
    },
    {
      description: "Administrator can retrieve investigationUser jobs with all param",
      user: administrator.credential,
      prepopulateUser: investigationUser.credential,
      all: true,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier1",
          },
          {
            identifier: "identifier2",
          },
        ],
      },
    },
    {
      description: "InstrumentScientist cannot retrieve investigationUser jobs with all param",
      user: instrumentScientist.credential,
      prepopulateUser: investigationUser.credential,
      all: true,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "Anonymous cannot retrieve jobs",
      user: anonymous.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs by investigationId",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[1].datasets[0].id,
          identifier: "identifier2",
        },
      ],
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs with skip and limit",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      limit: 1,
      skip: 1,
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier2",
          },
        ],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs with sort",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      sortBy: "_id",
      sortOrder: -1,
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier2",
          },
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser can retrieve all jobs for an investigationId",
      user: investigationUser.credential,
      prepopulateUser: administrator.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      all: true,
      sortBy: "_id",
      sortOrder: -1,
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier2",
          },
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser cannot retrieve jobs for an investigationId without all",
      user: investigationUser.credential,
      prepopulateUser: administrator.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      investigationId: investigationUser.investigations.participates[0].investigationId,
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs by filtering by status",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
          status: "CREATED",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
          status: "FINISHED",
        },
      ],
      sortBy: "_id",
      sortOrder: -1,
      status: "CREATED",
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs by multiple status",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
          status: "CREATED",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
          status: "FINISHED",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier3",
          status: "RUNNING",
        },
      ],
      sortBy: "_id",
      sortOrder: -1,
      status: "created,running",
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier3",
          },
          {
            identifier: "identifier1",
          },
        ],
      },
    },
    {
      description: "InvestigationUser can retrieve jobs by filtering by endDate",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
        },
      ],
      sortBy: "_id",
      sortOrder: -1,
      endDate: "2000-01-01",
      expected: {
        status: 200,
        jobs: [],
      },
    },
    {
      description: "InvestigationUser can search jobs",
      user: investigationUser.credential,
      prepopulateUser: investigationUser.credential,
      prepopulatedJobs: [
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier1",
          status: "CREATED",
        },
        {
          datasetIds: investigationUser.investigations.participates[0].datasets.datasetWithNoSample.id,
          identifier: "identifier2",
          status: "RUNNING",
        },
      ],
      sortBy: "_id",
      sortOrder: -1,
      search: "run",
      expected: {
        status: 200,
        jobs: [
          {
            identifier: "identifier2",
          },
        ],
      },
    },
  ],
};
