const { expect } = require("chai");
const { getUserByNamePattern } = require("../../app/cache/cache");
const cacheResource = require("./resources/cache.resource");

require("it-each")({ testPerIteration: true });
describe("Units tests on cache.js", () => {
  describe("getUserByNamePattern()", () => {
    it.each(cacheResource.getUserByNamePattern, "%s", ["description"], (element, next) => {
      const user = getUserByNamePattern(element.name);
      if (element.expected.name) {
        expect(user).not.to.be.undefined;
        expect(user.name).to.equal(element.expected.name);
      } else {
        expect(user).to.be.undefined;
      }
      next();
    });
  });
});
