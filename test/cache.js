require("it-each")({ testPerIteration: true });
process.env.NODE_ENV = "test";
//require("dotenv").config({ path: `.env.${process.env.NODE_ENV}`.toLowerCase() });
global.gServerConfig = require("../config/server.config.js");
const { expect } = require("chai");
const cache = require("../app/cache/cache.js");
const sessionHelper = require("./helper/session.js");
const cacheResource = require("./resources/cache.resource.js");

describe("Cache", () => {
  describe("extractUserNamePrefix", () => {
    it.each(cacheResource.extractUserNamePrefix, "%s", ["description"], (element) => {
      expect(cache.extractUserNamePrefix(element.username)).equal(element.expected.username);
    });
  });

  describe("getUserByUsernamePrefix", () => {
    it.each(cacheResource.getUserByUsernamePrefix, "%s", ["description"], async (element, next) => {
      try {
        const getSessionResponse = await sessionHelper.doGetSession(element.user);
        const { sessionId } = getSessionResponse.body;
        const users = await cache.getUserByUsernamePrefix(sessionId, element.username);
        expect(users.length).equal(element.expected.users.length);
        const expectedUsersSet = new Set(element.expected.users.map((e) => e.name));
        users.forEach((user) => {
          expectedUsersSet.has(user.name);
          expect(user.createTime).to.not.be.null;
        });
        return next();
      } catch (e) {
        return next(e);
      }
    });
  });
});
