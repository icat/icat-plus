module.exports = {
  investigationTypes: {
    industrials: ["FX", "IX", "IN", "IM"],
  },
  instruments: [
    {
      name: "BEAMLINE-01",
    },
    {
      name: "BEAMLINE-02",
    },
  ],
  doi: {
    public: [
      { prefix: "TESTING", suffix: "DOI-TEST-0003" },
      { prefix: "TESTING", suffix: "DOI-TEST-0003" },
      {
        prefix: "TESTING",
        suffix: "DOI-TEST-0005",
        instrument: "BEAMLINE-01",
        name: "TEST-0005",
        proposalTypeDescription: "Investigation type dedicated to software developments",
        creators: [
          { name: "First, Test" },
          { name: "Second, Test" },
          {
            name: "Third, Test",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0002-1825-0097",
              },
            ],
          },
        ],
        contributors: [
          { name: "Contact, Local", contributorType: "DataCollector" },
          { name: "First, Test", contributorType: "ProjectManager" },
        ],
      },
      {
        prefix: "TESTING",
        suffix: "DOI-TEST-0005",
        instrument: "BEAMLINE-01",
        name: "TEST-0005",
        proposalTypeDescription: "Investigation type dedicated to software developments",
        creators: [
          { name: "First, Test" },
          { name: "Second, Test" },
          { name: "Third, Test", nameIdentifiers: [{ schemeUri: "https://orcid.org", nameIdentifierScheme: "ORCID", nameIdentifier: "https://orcid.org/0000-0002-1825-0097" }] },
        ],
        contributors: [
          { name: "Contact, Local", contributorType: "DataCollector" },
          { name: "First, Test", contributorType: "ProjectManager" },
        ],
        relatedIdentifiers: [{ relatedIdentifier: "TESTING/DOI-DC-0001" }],
      },
    ],
  },
  addresses: {
    valid: {
      complete: {
        name: "Jonh",
        surname: "Doe",
        companyName: "ESRF",
        address: "RUE DE LA POSTE",
        city: "Grenoble",
        region: "Rhone-Alpes/Isere",
        postalCode: "38000",
        email: "esrf@esrf.fr",
        phoneNumber: "123123123",
        country: "France",
      },
      courier: {
        name: "Jonh",
        surname: "Doe",
        companyName: "ESRF",
        address: "RUE DE LA POSTE",
        city: "Grenoble",
        region: "Rhone-Alpes/Isere",
        postalCode: "38000",
        email: "esrf@esrf.fr",
        phoneNumber: "123123123",
        country: "France",
        defaultCourierCompany: "FEDEX",
        defaultCourierAccount: "X123456N",
      },
    },
  },
  users: {
    principalInvestigator: {
      credential: {
        plugin: "db",
        username: "principalInvestigator",
        password: "principalInvestigator",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 1,
        },
      },
    },
    localContact: {
      credential: {
        plugin: "db",
        username: "localContact",
        password: "localContact",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 1,
        },
      },
    },
    administrator: {
      name: "admin",
      fullName: "adminFullName",
      credential: {
        plugin: "db",
        username: "admin",
        password: "admin",
      },
      investigations: {
        administrator: {
          investigationId: 5,
        },
      },
    },
    anonymous: {
      name: "reader",
      email: "reader@reader.com",
      credential: {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
      investigations: {
        underEmbargo: {
          investigationId: 3,
        },
        released: {
          investigationId: 4,
        },
      },
    },
    minter: {
      name: "minter",
      credential: {
        plugin: "db",
        username: "minter",
        password: "minter",
      },
    },

    instrumentScientist: {
      name: "instrumentScientist",
      fullName: "instrumentScientistFullName",
      credential: {
        plugin: "db",
        username: "instrumentScientist",
        password: "instrumentScientist",
      },
      instrument: { name: "BEAMLINE-01" },
      investigations: {
        instrumentScientist: {
          investigationId: 1,
          instrumentName: "BEAMLINE-01",
          samples: [{ id: 3, descriptionParameter: { id: 3 }, commentParameter: { id: 4 } }],
        },
      },
    },
    investigationUser: {
      name: "investigationUser",
      fullName: "investigationUserFullName",
      email: "investigationUser@investigationUser.com",
      credential: {
        plugin: "db",
        username: "investigationUser",
        password: "investigationUser",
      },
      investigations: {
        processed: [
          {
            investigationId: 9,
          },
        ],
        participates: [
          {
            investigationId: 1,
            investigationName: "TEST-0001",
            instrumentName: "BEAMLINE-01",
            datasets: {
              datasetWithNoSample: {
                id: 1,
                fileCount: 1,
              },
              datasetWithSample: {
                id: 2,
                fileCount: 6,
                parameters: [
                  {
                    id: 2,
                  },
                ],
              },
              datasetWithNoSampleNoDataCollection: {
                id: 1,
                fileCount: 1,
              },
            },
            samples: [
              {
                id: 1,
              },
              {
                id: 2,
              },
            ],
          },
          {
            investigationId: 2,
            investigationName: "TEST-0001",
            instrumentName: "BEAMLINE-02",
            samples: [
              { id: 4, descriptionParameter: { id: 5 }, otherParameter: { id: 6 }, commentParameter: { id: 7 } },
              { id: 5, descriptionParameter: { id: 8 } },
            ],
            datasets: [{ id: 5 }],
          },
          {
            investigationId: 3,
            investigationName: "TEST-0002",
            instrumentName: "BEAMLINE-02",
            samples: [
              { id: 6, descriptionParameter: { id: 10 }, otherParameter: { id: 11 }, commentParameter: { id: 12 } },
              { id: 7, descriptionParameter: { id: 13 } },
            ],
            datasets: [{ id: 7 }, { id: 8 }],
          },
        ],
        noAccess: {
          investigationId: 5,
          samples: [
            {
              id: 10,
              descriptionParameter: { id: 15 },
            },
          ],
          datasets: [{ id: 10 }],
        },
        underEmbargo: {
          investigationId: 5,
        },
        released: {
          investigationId: 4,
          investigationName: "TEST-0003",
          instrumentName: "BEAMLINE-01",
        },
        timeSlots: {
          investigationId: 3,
          investigationName: "TEST-0002",
          instrumentName: "BEAMLINE-02",
          visitId: "26-08-2020 BEAMLINE-02",
        },
      },
    },

    allowed: [
      {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
    ],
    denied: [
      {
        plugin: "db",
        username: "reader",
        password: "badpassword",
      },
    ],
  },
};
