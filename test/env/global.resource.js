module.exports = !process.env.TEST_ENV ? require("./test.global.resource") : require(`./${process.env.TEST_ENV.toLowerCase()}.global.resource`);
