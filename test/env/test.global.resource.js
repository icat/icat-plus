module.exports = {
  investigationTypes: {
    industrials: ["FX", "IX", "IN", "IM"],
  },
  instruments: [
    {
      name: "ID01",
    },
    {
      name: "ID19",
    },
    {
      name: "ID21",
    },
  ],
  doi: {
    public: [
      { prefix: "10.15151", suffix: "ESRF-ES-34497655" },
      { prefix: "10.5072", suffix: "ESRF-DC-123753799" },
      {
        prefix: "10.15151",
        suffix: "ESRF-ES-117583838",
        instrument: "BM29",
        name: "MX-2077",
        proposalTypeDescription: "Macromolecular Crystallography",
        creators: [
          { name: "Brennich, Martha" },
          { name: "De Maria Antolinos, Alejandro" },
          {
            name: "Deepak Thankappan, Deepak Thankappan",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0002-0677-9444",
              },
            ],
          },
          {
            name: "Sharma, Yogita",
          },
          { name: "Verma, Garima" },
        ],
        contributors: [
          {
            name: "Bowler, Matthew",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0003-0465-3351",
              },
            ],
            contributorType: "DataCollector",
          },
          { name: "Mueller-Dieckmann, Christoph", contributorType: "DataCollector" },
          { name: "Popov, Alexander", contributorType: "DataCollector" },
          {
            name: "Royant, Antoine",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0002-1919-8649",
              },
            ],
            contributorType: "DataCollector",
          },
          { name: "De Maria Antolinos, Alejandro", contributorType: "ProjectManager" },
          {
            name: "Deepak Thankappan, Deepak Thankappan",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0002-0677-9444",
              },
            ],
            contributorType: "ProjectManager",
          },
          { name: "Choukate, Komal", contributorType: "ProjectMember" },
          { name: "Ruchir C. Bobde, Ruchir Chandrakant", contributorType: "ProjectMember" },
          {
            name: "Saxena, Ajay Kumar",
            nameIdentifiers: [
              {
                schemeUri: "https://orcid.org",
                nameIdentifierScheme: "ORCID",
                nameIdentifier: "https://orcid.org/0000-0001-6967-030X",
              },
            ],
            contributorType: "ProjectMember",
          },
          { name: "Varshney, Nishant Kumar", contributorType: "ProjectMember" },
          { name: "Verma, Garima", contributorType: "ProjectMember" },
        ],
      },
      {
        prefix: "10.15151",
        suffix: "ESRF-ES-117605227",
        instrument: "ID11",
        name: "MA-4196",
        proposalTypeDescription: "Applied Material Science",
        creators: [{ name: "Braz Fernandes, Francisco Manuel" }, { name: "Chaillet, Maxime" }, { name: "De Maria Antolinos, Alejandro" }],
        contributors: [
          { name: "Sedmak, Pavel", contributorType: "DataCollector" },
          { name: "Braz Fernandes, Francisco Manuel", contributorType: "ProjectManager" },
          { name: "Chaillet, Maxime", contributorType: "ProjectManager" },
          { name: "De Maria Antolinos, Alejandro", contributorType: "ProjectManager" },
          { name: "Camacho, Edgar", contributorType: "ProjectMember" },
          { name: "Cavaleiro, André", contributorType: "ProjectMember" },
          { name: "El Hachi, Younes", contributorType: "ProjectMember" },
        ],
        relatedIdentifiers: [
          { relatedIdentifier: "10.5072/ESRF-DC-123753799" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398411" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398420" },
          { relatedIdentifier: "10.5072/TEST-ESRF-DC-123398429" },
          { relatedIdentifier: "10.5072/test-esrf-123398540" },
          { relatedIdentifier: "10.5072/test-esrf-123398549" },
          { relatedIdentifier: "10.5072/test-esrf-123398585" },
          { relatedIdentifier: "10.5072/test-esrf-123398594" },
          { relatedIdentifier: "10.5072/test-esrf-123398597" },
          { relatedIdentifier: "10.5072/test-esrf-123399173" },
          { relatedIdentifier: "10.5072/test-esrf-123399182" },
          { relatedIdentifier: "10.5072/test-esrf-123399191" },
          { relatedIdentifier: "10.5072/test-esrf-123399218" },
          { relatedIdentifier: "10.5072/test-esrf-123399227" },
          { relatedIdentifier: "10.5072/test-esrf-123399236" },
          { relatedIdentifier: "10.5072/test-esrf-123399245" },
          { relatedIdentifier: "10.5072/test-esrf-123399254" },
          { relatedIdentifier: "10.5072/test-esrf-123399263" },
          { relatedIdentifier: "10.5072/test-esrf-123399272" },
          { relatedIdentifier: "10.5072/test-esrf-123399299" },
          { relatedIdentifier: "10.5072/test-esrf-123399308" },
          { relatedIdentifier: "10.5072/test-esrf-123399326" },
          { relatedIdentifier: "10.5072/test-esrf-123399335" },
          { relatedIdentifier: "10.5072/test-esrf-123753807" },
          { relatedIdentifier: "10.5072/test-esrf-123753815" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816708" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816716" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816732" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816740" },
          { relatedIdentifier: "10.81044/TEST-ESRF-123816748" },
          { relatedIdentifier: "123399290" },
        ],
      },
    ],
  },
  addresses: {
    valid: {
      complete: {
        name: "Jonh",
        surname: "Doe",
        companyName: "ESRF",
        address: "RUE DE LA POSTE",
        city: "Grenoble",
        region: "Rhone-Alpes/Isere",
        postalCode: "38000",
        email: "esrf@esrf.fr",
        phoneNumber: "123123123",
        country: "France",
      },
      courier: {
        name: "Jonh",
        surname: "Doe",
        companyName: "ESRF",
        address: "RUE DE LA POSTE",
        city: "Grenoble",
        region: "Rhone-Alpes/Isere",
        postalCode: "38000",
        email: "esrf@esrf.fr",
        phoneNumber: "123123123",
        country: "France",
        defaultCourierCompany: "FEDEX",
        defaultCourierAccount: "X123456N",
      },
    },
  },
  users: {
    principalInvestigator: {
      credential: {
        plugin: "db",
        username: "principalInvestigator",
        password: "principalInvestigator",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 63335100,
        },
      },
    },
    localContact: {
      credential: {
        plugin: "db",
        username: "localContact",
        password: "localContact",
      },
      investigation: {
        principalInvestigator: {
          investigationId: 63335100,
        },
      },
    },
    administrator: {
      name: "admin",
      fullName: "adminFullName",
      credential: {
        plugin: "db",
        username: "admin",
        password: "test_admin",
      },
      investigations: {
        administrator: {
          investigationId: 123398449, //any investigationId would fit
        },
      },
    },
    anonymous: {
      name: "reader",
      credential: {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
      investigations: {
        underEmbargo: {
          investigationId: 69107436,
        },
        released: {
          investigationId: 123845948,
        },
      },
    },
    minter: {
      name: "minter",
      credential: {
        plugin: "db",
        username: "minter",
        password: "givemeadoi",
      },
    },

    instrumentScientist: {
      name: "instrumentScientist",
      fullName: "instrumentScientist",
      credential: {
        plugin: "db",
        username: "instrumentScientist",
        password: "instrumentScientist",
      },
      instrument: { name: "ID01" },
      investigations: {
        instrumentScientist: {
          investigationId: 63335100,
          instrumentName: "ID01",
          samples: [{ id: 123974017, descriptionParameter: { id: 124490872 }, commentParameter: { id: 125214374 } }],
        },
      },
    },
    investigationUser: {
      name: "investigationUser",
      fullName: "investigationUserFullName",
      email: "investigationUser@test.fr",
      credential: {
        plugin: "db",
        username: "investigationUser",
        password: "investigationUser",
      },
      investigations: {
        processed: [{ investigationId: 124404803 }],
        participates: [
          {
            investigationId: 63335100,
            investigationName: "ID010100",
            instrumentName: "ID01",
            datasets: {
              datasetWithNoSample: {
                id: 123755487,
                fileCount: 1,
                location: "/location",
              },
              datasetWithSample: {
                id: 63335951,
                fileCount: 6,
                parameters: [
                  {
                    id: 123595808,
                  },
                ],
              },
              datasetWithNoSampleNoDataCollection: {
                id: 123970335,
                fileCount: 1,
              },
            },
            samples: [
              {
                id: 63335950,
              },
              {
                id: 123973919,
              },
            ],
          },
          {
            investigationId: 116934142,
            investigationName: "EV-358",
            instrumentName: "ID21",
            samples: [
              { id: 117992029, descriptionParameter: { id: 124490870 }, otherParameter: { id: 123974015 }, commentParameter: { id: 125245766 } },
              { id: 117581283, descriptionParameter: { id: 124490871 } },
            ],
            datasets: [{ id: 118042662 }],
          },
          {
            investigationId: 69107436,
            investigationName: "MD-1114",
            instrumentName: "ID21",
            samples: [
              { id: 69924010, descriptionParameter: { id: 125246113 }, otherParameter: { id: 125246115 }, commentParameter: { id: 125246116 } },
              { id: 69270458, descriptionParameter: { id: 125246114 } },
            ],
            datasets: [{ id: 118042662 }],
          },
        ],
        noAccess: {
          investigationId: 2483226,
          samples: [
            {
              id: 2487435,
              descriptionParameter: { id: 123974016 },
            },
          ],
          datasets: [{ id: 2487670 }],
        },
        underEmbargo: {
          investigationId: 124524182,
        },
        released: {
          investigationId: 116934142,
          investigationName: "EV-358",
          instrumentName: "ID21",
        },
        timeSlots: {
          investigationId: 3,
          investigationName: "HC4234",
          instrumentName: "ID15B",
          visitId: "HC/4234 ID15B 26-08-2020/01-09-2020",
        },
      },
    },

    allowed: [
      {
        plugin: "db",
        username: "reader",
        password: "reader",
      },
    ],
    denied: [
      {
        plugin: "db",
        username: "reader",
        password: "badpassword",
      },
    ],
  },
};
