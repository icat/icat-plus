exports.getMessages = async (type) => {
  const params = new URLSearchParams();
  if (type) params.set("type", type);
  return global.gRequester.get(`/messages?${params}`).set("Content-Type", "application/json").send();
};
