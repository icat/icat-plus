const sessionHelper = require("./session.js");

/**
 * This method will login and will create a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function createParcel(user, parcel, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return (
    global.gRequester
      //.post(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/parcel`)
      .put(`/tracking/${getSessionResponse.body.sessionId}/parcel?investigationId=${investigationId}&shipmentId=${shipmentId}`)
      .set("Content-Type", "application/json")
      .send(parcel)
  );
}

/**
 * This method will login and will create a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function createOrUpdateParcel(user, parcel, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  if (investigationId !== undefined) params.set("investigationId", investigationId);
  if (shipmentId !== undefined) params.set("shipmentId", shipmentId);
  return global.gRequester.put(`/tracking/${getSessionResponse.body.sessionId}/parcel?${params.toString()}`).set("Content-Type", "application/json").send(parcel);
}

/**
 * This method will login and will create a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function deleteParcel(user, parcelId, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .delete(`/tracking/${getSessionResponse.body.sessionId}/parcel?investigationId=${investigationId}&shipmentId=${shipmentId}`)
    .set("Content-Type", "application/json")
    .send(parcelId);
}

/**
 * This method will login and will get the parcels by a shipmentId
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelsByShipmentId(user, investigationId, shipmentId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/shipment/id/${shipmentId}/parcel`)
    .set("Content-Type", "application/json")
    .send();
}

/**
 * This method will login and will get the parcels by a shipmentId
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelBySessionId(user, queryParams) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/parcel?${params.toString()}`).set("Content-Type", "application/json").send();
}

/**
 * This method will login and will get a parcel by id
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelById(user, investigationId, parcelId) {
  const getSessionResponse = await sessionHelper.doGetSession(user);

  return (
    global.gRequester
      //.get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}`)
      .get(`/tracking/${getSessionResponse.body.sessionId}/parcel?investigationId=${investigationId}&parcelId=${parcelId}`)
      .set("Content-Type", "application/json")
      .send()
  );
}

/**
 * This method will login and will get a parcel by id
 * @param {*} user
 * @param {*} shipment
 */
async function getParcelBy(user, params = {}) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const searchParams = new URLSearchParams();

  for (const element in params) {
    if (params[element]) searchParams.set(element, params[element]);
  }
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/parcel?${searchParams.toString()}`).set("Content-Type", "application/json").send();
}

/**
 * This method will login and will set the status of a parcel
 * @param {*} user
 * @param {*} shipment
 */
async function setParcelStatus(user, investigationId, parcelId, status, parcel) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .put(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}/status/${status}`)
    .set("Content-Type", "application/json")
    .send(parcel);
}

module.exports = {
  createParcel,
  deleteParcel,
  getParcelsByShipmentId,
  getParcelById,
  setParcelStatus,

  getParcelBy,
  getParcelBySessionId,
  createOrUpdateParcel,
};
