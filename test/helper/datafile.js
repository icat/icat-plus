const sessionHelper = require("./session.js");

exports.getDatafilesByDatasetId = async (user, datasetId, limit, skip, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", skip);
  if (search) params.set("search", search);
  return global.gRequester.get(`/catalogue/${sessionId}/dataset/id/${datasetId}/datafile?${params.toString()}`).set("Content-Type", "application/json").send();
};

exports.getDatafiles = async (user, datasetId, limit, skip, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (skip) params.set("skip", skip);
  if (datasetId) params.set("datasetId", datasetId);
  if (search) params.set("search", search);
  return global.gRequester.get(`/catalogue/${sessionId}/datafile?${params.toString()}`).set("Content-Type", "application/json").send();
};

exports.download = async (sessionId, investigationId, datasetId, filepath) => {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  if (datasetId) params.set("datasetId", datasetId);
  if (filepath) params.set("filepath", filepath);
  return global.gRequester.get(`/catalogue/${sessionId}/datafile/download?${params.toString()}`).set("Content-Type", "application/json").send();
};
