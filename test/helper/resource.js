const sessionHelper = require("./session.js");

async function getSessionId(user, apiKey) {
  if (user) {
    const getSessionResponse = await sessionHelper.doGetSession(user.credential);
    const { sessionId } = getSessionResponse.body;
    return sessionId;
  }
  return apiKey;
}

exports.uploadFile = async (user, apiKey, file, investigationId, instrumentName) => {
  const sessionId = await getSessionId(user, apiKey);
  const param = investigationId ? `investigationId=${investigationId}` : `instrumentName=${instrumentName}`;
  const response = await global.gRequester
    .post(`/resource/${sessionId}/file/upload?${param}`)
    .field("type", "attachment")
    .field("category", "file")
    .attach("file", file, "test.png");
  return response;
};
