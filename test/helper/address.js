const sessionHelper = require("./session.js");

/**
 * This method will login and will create a shipment
 * @param {*} user
 * @param {*} shipment
 */
const createAddress = async (user, address, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.post(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/address`).set("Content-Type", "application/json").send(address);
};

const deleteAddress = async (user, address, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .delete(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/address`)
    .set("Content-Type", "application/json")
    .send(address);
};

const updateAddress = async (user, address, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.put(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/address`).set("Content-Type", "application/json").send(address);
};

const getAddressByInvestigationId = async (user, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/address`).set("Content-Type", "application/json").send();
};

const getAddress = async (user) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/address`).set("Content-Type", "application/json").send();
};

module.exports = {
  createAddress,
  updateAddress,
  deleteAddress,
  getAddressByInvestigationId,
  getAddress,
};
