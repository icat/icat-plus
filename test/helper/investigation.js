const sessionHelper = require("./session.js");

exports.getInvestigationBy = async (user, queryParams) => {
  const params = new URLSearchParams();
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const request = `/catalogue/${getSessionResponse.body.sessionId}/investigation?${params.toString()}`;
  return global.gRequester.get(request).set("Content-Type", "application/json").send();
};

exports.getTimelineBy = async (user, parameters) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  const { instrument } = parameters;
  if (instrument) params.set("instrument", instrument);
  return global.gRequester.get(`/catalogue/${sessionId}/investigation/timeline?${params.toString()}`).set("Content-Type", "application/json").send();
};
