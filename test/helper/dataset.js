const sessionHelper = require("./session.js");

exports.getDatasetDocumentByDatasetIds = async (user, datasetIds) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/dataset/id/${datasetIds}/dataset_document`).set("Content-Type", "application/json").send();
};

exports.getDatasetDocumentByDates = async (user, startDate, endDate) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .get(`/catalogue/${getSessionResponse.body.sessionId}/dataset/startdate/${startDate}/enddate/${endDate}/dataset_document`)
    .set("Content-Type", "application/json")
    .send();
};

exports.getTimelineBy = async (user, parameters) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  const { sampleId, investigationId } = parameters;
  if (sampleId) params.set("sampleId", sampleId);
  if (investigationId) params.set("investigationId", investigationId);
  return global.gRequester.get(`/catalogue/${sessionId}/dataset/timeline?${params.toString()}`).set("Content-Type", "application/json").send();
};

exports.getDatasetsByDOI = async (user, prefix, suffix, limit, sortBy, sortOrder, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const { sessionId } = getSessionResponse.body;
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  return global.gRequester.get(`/doi/${prefix}/${suffix}/datasets?sessionId=/${sessionId}&${params.toString()}`).set("Content-Type", "application/json").send();
};

/** To be removed for deprecation */
exports.getDeprecatedDatasetsByInvestigationId = async (user, investigationId, limit, sortBy, sortOrder, search) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  if (limit) params.set("limit", limit);
  if (sortBy) params.set("sortBy", sortBy);
  if (sortOrder) params.set("sortOrder", sortOrder);
  if (search) params.set("search", search);
  return global.gRequester
    .get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/dataset?${params.toString()}`)
    .set("Content-Type", "application/json")
    .send();
};

exports.getDatasetsBy = async (user, params) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const urlParams = new URLSearchParams();
  Object.keys(params).forEach((key) => {
    if (params[key]) {
      urlParams.set(key, params[key]);
    }
  });
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/dataset?${urlParams.toString()}`).set("Content-Type", "application/json").send();
};

exports.downloadData = async (user, datasetIds, datafilesIds) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  if (datasetIds) params.set("datasetIds", datasetIds);
  if (datafilesIds) params.set("datafilesIds", datafilesIds);
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/data/download?${params.toString()}`).set("Content-Type", "application/json").send();
};

exports.restoreData = async (sessionId, datasetId, name, email) => {
  const params = new URLSearchParams();
  if (datasetId) params.set("datasetId", datasetId);
  const request = `/ids/${sessionId}/dataset/restore?${params.toString()}`;
  return global.gRequester.post(request).set("Content-Type", "application/json").send({ name, email });
};
