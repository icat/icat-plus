const _ = require("lodash");
const sessionHelper = require("./session.js");

/**
 * Test function which tests that an actual event corresponds to the expected event.
 * @param {object} actualEvent the event to test
 * @param {object} expectedEvent the expected event
 */
exports.testEvent = function (actualEvent, expectedEvent) {
  expect(actualEvent._id).to.be.a("string");
  if (actualEvent.investigationId) {
    expect(actualEvent.investigationId).to.be.an("number");
  }
  expect(actualEvent.type).to.be.a("string").to.equal(expectedEvent.type);
  expect(actualEvent.category).to.be.a("string").to.equal(expectedEvent.category);

  if (expectedEvent.creationDate) {
    expect(new Date(actualEvent.creationDate).toISOString()).to.equal(new Date(expectedEvent.creationDate).toISOString());
  }
  expect(new Date(actualEvent.creationDate)).to.be.not.null;
  if (expectedEvent.title) {
    expect(actualEvent.title).to.be.a("string").to.equal(expectedEvent.title);
  }

  /** TODO: Expectations for Content */

  // Test expectations on tags.
  if (expectedEvent.tag) {
    expect(actualEvent.tag.length).to.equal(expectedEvent.tag.length);
    expectedEvent.tag.forEach((expectedTag) => {
      const actualTag = _.find(actualEvent.tag, (actualTag) => actualTag.name === expectedTag.name);
      if (actualTag) {
        testTag(actualTag, expectedTag);
      } else {
        expect.fail("At least one expected tag was not found.");
      }
    });
  }

  // Tests username
  if (expectedEvent.username) {
    expect(actualEvent.username).to.equal(expectedEvent.username);
  }

  if (expectedEvent.instrumentName) {
    expect(actualEvent.instrumentName).to.equal(expectedEvent.instrumentName);
  }
};

/**
 * Test function which tests that an actual tag corresponds to the expected tag.
 * @param {object} actualTag the tag to test
 * @param {object} expectedTag the expected tag
 */
function testTag(actualTag, expectedTag) {
  expect(actualTag._id).to.be.a("string");
  expect(actualTag.name).to.equal(expectedTag.name.toLowerCase());

  if (expectedTag.investigationId) {
    expect(actualTag.investigationId).to.be.a("number").to.equal(Number(expectedTag.investigationId));
    /* An investigation tag has also the instrument attached */
    //expect(actualTag.instrumentName).to.be.oneOf([null]);
  }

  if (!expectedTag.investigationId && !expectedTag.instrumentName) {
    expect(actualTag.investigationId).to.equal(expectedTag.investigationId);
    expect(actualTag.instrumentName).to.equal(expectedTag.instrumentName);
  }

  if (!expectedTag.investigationId && expectedTag.instrumentName) {
    expect(actualTag.investigationId).to.be.oneOf([null, undefined]);
    expect(actualTag.instrumentName).to.equal(expectedTag.instrumentName.toUpperCase());
  }
}
exports.testTag = testTag;

/** Create an event by attributes.
 * @param {object} attributesObj. An object containing one or several of the attribute the event will have. Example: {category: 'error'}. Full description of the available
 * properties [here](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/models/event.model.js)
 * @return {object} Event with provided attributes.
 * Attributes which are not provided are set by default as shown below:
 * _id : 'defaultId',
 * category: 'comment',
 * content : [ { format: 'plainText', text: 'default content' }, { format: 'html', text: '<p> default content </p>' }],
 * createdAt: '2018-01-01T00:00:01.000Z',
 * creationDate : '2018-01-01T00:00:00.000Z',
 * datasetId: null,
 * file : [],
 * fileSize: null,
 * filename : null,
 * investigationId : 'defaultInvestigationId',
 * machine: null,
 * previousVersionEvent : null,
 * software : null,
 * tag : [],
 * title : null,
 * type : 'annotation',
 * updatedAt : "2018-01-01T00:00:01.000Z";
 * username : null,
 */
exports.createEventByAttributes = function (attributesObj) {
  return {
    _id: attributesObj._id ? attributesObj._id : "defaultId",
    category: attributesObj.category ? attributesObj.category : "comment",
    content: attributesObj.content
      ? attributesObj.content
      : [
          { format: "plainText", text: "default content" },
          { format: "html", text: "<p> default content </p>" },
        ],
    createdAt: attributesObj.createdAt ? attributesObj.createdAt : "2018-01-01T00:00:01.000Z",
    creationDate: attributesObj.creationDate ? attributesObj.creationDate : "2018-01-01T00:00:00.000Z",
    datasetId: attributesObj.datasetId ? attributesObj.datasetId : null,
    file: attributesObj.file ? attributesObj.file : [],
    fileSize: attributesObj.fileSize ? attributesObj.fileSize : null,
    filename: attributesObj.filename ? attributesObj.filename : null,
    investigationId: attributesObj.investigationId ? attributesObj.investigationId : "defaultInvestigationId",
    machine: attributesObj.machine ? attributesObj.machine : null,
    previousVersionEvent: attributesObj.previousVersionEvent ? attributesObj.previousVersionEvent : null,
    software: attributesObj.software ? attributesObj.software : null,
    tag: attributesObj.tag ? attributesObj.tag : [],
    title: attributesObj.title ? attributesObj.title : "",
    type: attributesObj.type ? attributesObj.type : "annotation",
    updatedAt: attributesObj.updatedAt ? attributesObj.updatedAt : "2018-01-01T00:00:01.000Z",
    username: attributesObj.username ? attributesObj.username : null,
    investigationName: attributesObj.investigationName ? attributesObj.investigationName : null,
    instrumentName: attributesObj.instrumentName ? attributesObj.instrumentName : null,
  };
};

exports.createEventFrombase64 = async (user, body, investigationId, instrumentName) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = investigationId ? `investigationId=${investigationId}` : `instrumentName=${instrumentName}`;
  const request = `/logbook/${getSessionResponse.body.sessionId}/event/createfrombase64?${params}`;
  return global.gRequester.post(request).set("Content-Type", "application/json").send(body);
};

exports.doFindEventsDates = async function (sessionId, investigationId, types, search, instrumentName) {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  if (types) params.set("types", types);
  if (search) params.set("search", search);
  if (instrumentName) params.set("instrumentName", instrumentName);
  const url = `/logbook/${sessionId}/event/dates?${params.toString()}`;
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};

exports.doMoveEvents = async function (apiKey, sourceInvestigationId, destinationInvestigationId) {
  const params = new URLSearchParams();
  if (sourceInvestigationId) params.set("sourceInvestigationId", sourceInvestigationId);
  if (destinationInvestigationId) params.set("destinationInvestigationId", destinationInvestigationId);
  const request = `/dataacquisition/${apiKey}/events/move?${params.toString()}`;
  global.gLogger.debug(`url ${request}`);
  return global.gRequester.post(request).set("Content-Type", "application/json").send();
};

/**
 * It returns the url with all the parameters needed for the /logbook/{sessionId}/event endpoint
 * @param {*} sessionId
 * @param {*} queryParams
 * @param {*} actions Add a string to the path. Example: "/remove" will do /event/remove
 * @returns
 */
exports.getEventEndPointURL = function (sessionId, queryParams, action) {
  const params = new URLSearchParams();
  params.set("sessionId", sessionId);
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return `/logbook/${sessionId}/event${action ? action : ""}?${params.toString()}`;
};

exports.doFindEvent = async function (sessionId, queryParams) {
  const url = this.getEventEndPointURL(sessionId, queryParams);
  return global.gRequester.get(url).set("Content-Type", "application/json").send();
};

exports.doRemoveEvent = async function (sessionId, queryParams) {
  const url = this.getEventEndPointURL(sessionId, queryParams, "/remove");
  return global.gRequester.put(url).set("Content-Type", "application/json").send();
};

exports.doRestoreEvent = async function (sessionId, queryParams) {
  const url = this.getEventEndPointURL(sessionId, queryParams, "/restore");
  return global.gRequester.put(url).set("Content-Type", "application/json").send();
};
