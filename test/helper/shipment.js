const sessionHelper = require("./session.js");

/**
 * This method will login and will create a shipment
 * @param {*} user
 * @param {*} shipment
 */
exports.createShipment = async (user, shipment, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .post(`/tracking/${getSessionResponse.body.sessionId}/shipment?investigationId=${investigationId}`)
    .set("Content-Type", "application/json")
    .send(shipment);
};

/**
 * This method will login and will create a shipment
 * @param {*} user
 * @param {*} shipment
 */
exports.deleteShipment = async (user, shipment, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .delete(`/tracking/${getSessionResponse.body.sessionId}/shipment?investigationId=${investigationId}`)
    .set("Content-Type", "application/json")
    .send(shipment);
};

/**
 * This method will login and will get a Shipment by query params
 * @param {*} user
 * @param {*} shipment
 */
exports.getShipmentBy = async (user, queryParams) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return global.gRequester.get(`/tracking/${getSessionResponse.body.sessionId}/shipment?${params.toString()}`).set("Content-Type", "application/json").send();
};
