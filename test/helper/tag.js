const Tag = require("../../app/models/tag.model.js");

exports.createTag = async (sessionId, investigationId, instrumentName, tag) => {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  if (instrumentName) params.set("instrumentName", instrumentName);
  const response = await global.gRequester.post(`/logbook/${sessionId}/tag?${params}`).send(tag);
  return response;
};

exports.getTags = async (sessionId, investigationId, instrumentName) => {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  if (instrumentName) params.set("instrumentName", instrumentName);
  const response = await global.gRequester.get(`/logbook/${sessionId}/tag?${params}`).send();
  return response;
};

exports.prepopulateTags = async (tags) => {
  const tagList = [];
  if (tags) {
    if (tags.length > 0) {
      for (const tag of tags) {
        // Create direclty the tag in the DB, in order to have the possiblity to create global tags
        let tagToCreate = new Tag({
          name: tag.name.toLowerCase(),
          description: tag.description,
          color: tag.color,
          instrumentName: tag.instrumentName,
          investigationId: tag.investigationId,
        });
        tagToCreate = await tagToCreate.save();
        tagList.push(tagToCreate);
      }
    }
  }
  return tagList;
};
