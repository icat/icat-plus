exports.getTechniques = async () => {
  return global.gRequester.get(`/catalogue/techniques`).set("Content-Type", "application/json").send();
};
