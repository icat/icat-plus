const sessionHelper = require("./session.js");

exports.getSamplesByInvestigationId = async (user, investigationId) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/sample`).set("Content-Type", "application/json").send();
};

exports.getSamplesBy = async (user, queryParams) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/samples?${params.toString()}`).set("Content-Type", "application/json").send();
};

exports.samplePage = async (user, sampleId, queryParams) => {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  const params = new URLSearchParams();
  for (const param in queryParams) {
    if (Object.hasOwnProperty.call(queryParams, param)) {
      const element = queryParams[param];
      if (element) {
        params.set(param, element);
      }
    }
  }
  return global.gRequester.get(`/catalogue/${getSessionResponse.body.sessionId}/samples/${sampleId}/page?${params.toString()}`).set("Content-Type", "application/json").send();
};
