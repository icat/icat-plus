exports.updateSampleParameter = async (sessionId, investigationId, parameterId, value) => {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  const request = `/catalogue/${sessionId}/sampleParameters/${parameterId}?${params.toString()}`;
  return global.gRequester.put(request).set("Content-Type", "application/json").send({ value });
};

exports.createSampleParameter = async (sessionId, investigationId, sampleId, name, value) => {
  const params = new URLSearchParams();
  if (investigationId) params.set("investigationId", investigationId);
  const request = `/catalogue/${sessionId}/samples/${sampleId}/sampleParameters?${params.toString()}`;
  return global.gRequester.post(request).set("Content-Type", "application/json").send({ name, value });
};
