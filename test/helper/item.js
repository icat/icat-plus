const sessionHelper = require("./session.js");

/**
 * This method will login and will add a new item to a parcel
 */
function addItem(user, investigationId, parcelId, item) {
  return new Promise((resolve, reject) => {
    sessionHelper.doGetSession(user).then((getSessionResponse) => {
      global.gRequester
        .post(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}/item`)
        .set("Content-Type", "application/json")
        .send(item)
        .end((err, response) => {
          if (err) {
            reject(err);
          } else {
            resolve(response);
          }
        });
    });
  });
}

/**
 * This method will login and edit an item
 */
async function editItem(user, investigationId, parcelId, itemId, item) {
  const getSessionResponse = await sessionHelper.doGetSession(user);
  return global.gRequester
    .put(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}/item/${itemId}`)
    .set("Content-Type", "application/json")
    .send(item);
}

/**
 * This method will login and will remove an item
 */
function removeItem(user, investigationId, parcelId, itemId) {
  return new Promise((resolve, reject) => {
    sessionHelper.doGetSession(user).then((getSessionResponse) => {
      global.gRequester
        .delete(`/tracking/${getSessionResponse.body.sessionId}/investigation/id/${investigationId}/parcel/id/${parcelId}/item/${itemId}`)
        .set("Content-Type", "application/json")
        .send()
        .end((err, response) => {
          if (err) {
            reject(err);
          } else {
            resolve(response);
          }
        });
    });
  });
}

module.exports = {
  addItem,
  editItem,
  removeItem,
};
