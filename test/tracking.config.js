const MXExperimentPlan = [
  {
    header: "Space G.",
    name: "space_group",
    type: "select",
    values: ["True", "False"],
  },
  {
    header: "a",
    name: "unit_cell_a",
    type: "string",
  },
  {
    header: "b",
    name: "unit_cell_b",
    type: "number",
  },
  {
    header: "c",
    name: "unit_cell_c",
    type: "number",
  },
  {
    header: "Alpha",
    name: "unit_cell_alpha",
    type: "string",
  },
  {
    header: "Beta",
    name: "unit_cell_beta",
    type: "string",
  },
  {
    header: "Gamma",
    name: "unit_cell_gamma",
    type: "string",
  },
  {
    header: "Exp. Type",
    name: "workflowType",
    type: "select",
    values: ["MXPressE", "MXPressF", "MXPressO", "MXPressI", "MXPressE_SAD", "MXScore", "MXPressM", "MXPressP", "MXPressP_SAD"],
  },
  {
    header: "Aimed resolution",
    name: "aimed_resolution",
    type: "string",
  },
  {
    header: "Required resolution",
    name: "required_resolution",
    type: "string",
  },
  {
    header: "Beam diameter",
    name: "beam_diameter",
    type: "string",
  },
  {
    header: "Number of positions",
    name: "number_positions",
    type: "string",
  },
  {
    header: "Aimed multiplicity",
    name: "aimed_multiplicity",
    type: "string",
  },
  {
    header: "Aimed completeness",
    name: "aimed_completeness",
    type: "string",
  },
  {
    header: "Force S.G",
    name: "forceSpaceGroup",
    type: "select",
    values: ["True", "False"],
  },
  {
    header: "Radiation sensivity",
    name: "radiation_sensivity",
    type: "string",
  },
  {
    header: "Smiles",
    name: "smiles",
    type: "string",
  },
  {
    header: "Tot. Rot. Angle",
    name: "total_rotation_angle",
    type: "string",
  },
  {
    header: "Min. Osc. Angle",
    name: "min_osc_angle",
    type: "string",
  },
];

const MXProcessingPlan = [
  {
    name: "pipeline",
    type: "select",
    values: ["autoproc", "edna_proc", "grenades"],
  },
];

const MXContainersType = [
  { name: "SPINE", capacity: 10 },
  { name: "UNIPUCK", capacity: 16 },
  { name: "BOX", capacity: 1 },
];

const STATUS = {
  CREATED: "CREATED",
  SCHEDULED: "SCHEDULED",
  READY: "READY",
  APPROVED: "APPROVED",
  REFUSED: "REFUSED",
  INPUT: "INPUT",
  SENT: "SENT",
  STORES: "STORES",
  BEAMLINE: "BEAMLINE",
  BACK_STORES: "BACK_STORES",
  ON_HOLD: "ON_HOLD",
  BACK_USER: "BACK_USER",
  DESTROYED: "DESTROYED",
  REMOVED: "REMOVED",
};

const FlexHCDDualChangerType = [{ name: "FlexHCDDual" }];
const FlexHCDUnipuckPlate = [{ name: "FlexHCDUnipuckPlate" }];

/**
 * sample tracking configuration file
 */
module.exports = {
  STATUS,
  sampleChangerTypes: {
    "ID23-1": FlexHCDDualChangerType,
    "ID23-2": FlexHCDUnipuckPlate,
    "ID30A-1": FlexHCDUnipuckPlate,
    "ID30A-2": FlexHCDDualChangerType,
    "ID30A-3": MXContainersType,
    ID30B: FlexHCDDualChangerType,
    ID29: FlexHCDDualChangerType,
    BM29: [],
  },
  containerTypes: {
    ID01: MXContainersType,
    "ID23-1": MXContainersType,
    "ID23-2": MXContainersType,
    "ID30A-1": MXContainersType,
    "ID30A-2": MXContainersType,
    "ID30A-3": MXContainersType,
    ID30B: MXContainersType,
    ID29: MXContainersType,
    BM29: MXContainersType,
  },
  experimentPlan: {
    "ID23-1": MXExperimentPlan,
    "ID23-2": MXExperimentPlan,
    "ID30A-1": MXExperimentPlan,
    "ID30A-3": MXExperimentPlan,
    ID29: MXExperimentPlan,
    BM29: MXExperimentPlan,
  },
  processingPlan: {
    "ID23-1": MXProcessingPlan,
    "ID23-2": MXProcessingPlan,
    "ID30A-1": MXProcessingPlan,
    "ID30A-3": MXProcessingPlan,
    ID29: MXProcessingPlan,
    BM29: MXProcessingPlan,
  },
  qrCode: {
    server: "https://data.esrf.fr",
  },
  /** Address of the facility where the experiments will be conducted */
  facilityAddress: {
    name: "ESRF Stores",
    surname: "",
    address: "71 avenue des Martyrs",
    city: "Grenoble",
    postalCode: "38000",
    email: "",
    phoneNumber: "+33 (0)4 76 88 2733",
    fax: "+33 (0)4 76 88 2347",
    country: "France",
  },

  notifications: {
    enabled: process.env.TRACKING_ENABLED ? !!+process.env.TRACKING_ENABLED : true,
    fromAddress: "dataportal@esrf.fr",
    replyTo: "sampletransport@esrf.fr",
    smtp: {
      host: "smtp.esrf.fr",
      port: 25,
    },
    siteName: "ESRF",
    subjectPrefix: "ESRF Tracking",
    storesEmail: "sampletransport@esrf.fr",
    proposalTypeEmails: [
      { proposal: ["IN", "IM"], email: "industry@esrf.fr" },
      { proposal: ["IX", "FX", "OA"], email: "mxind@esrf.fr" },
    ],
    parcelURLTemplate: "https://data.esrf.fr/investigation/{investigationId}/logistics/parcel/{parcelId}",
    logisticsURLTemplate: "https://data.esrf.fr/investigation/{investigationId}/logistics",
    /**
     * statuses describe to whom the emails should be sent for each different status of a parcel
     */
    statuses: {
      [STATUS.SENT]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
      [STATUS.STORES]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: false,
        replyToRoles: ["Local contact"],
      },
      [STATUS.BEAMLINE]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: false,
        replyToRoles: ["Local contact"],
      },
      [STATUS.BACK_STORES]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
      [STATUS.BACK_USER]: {
        roles: ["Participant", "Principal investigator", "Scientist", "Local contact"],
        isEmailToStores: true,
      },
    },
  },
};
