/**
 * investigation configuration file
 */
module.exports = {
  // start of industrial proposals name
  industrialProposals: ["IX", "FX", "IN", "IM"],
  //
  participantVisitId: ["PROPOSAL"],
};
