let envPath = `.env.${process.env.NODE_ENV}`.toLowerCase();
if (process.env.NODE_ENV.toUpperCase() === "TEST" && process.env.TEST_ENV) {
  envPath = `.env.${process.env.NODE_ENV}.${process.env.TEST_ENV}`.toLowerCase();
}

require("dotenv").config({ path: envPath });

/**
 * ICAT+ configuration file
 */
/** @module confguration */
module.exports = {
  /** ICAT+ server configuration section */
  server: {
    /** HTTP port used by ICAT+ */
    port: 8000,
    /** ICAT+ URL. Used by the PDF generator and SWAGGER */
    url: "https://icatplus.esrf.fr",
    /** API_KEY to short circuit default authentication mechanism. Distribute this key to trusted clients. */
    API_KEY: process.env.LOGBOOK_API_KEY,
    /** Temporal folder used by the application in case of need for writting files. Example: create base64 images*/
    temporalFolder: "/tmp",
  },
  /** Stores the request done to the instance in a database calculating their duration */
  profiler: {
    enabled: !!+process.env.PROFILER_ENABLED || true,
    /** Only queries longer that queries durantion will be logged */
    slow_queries_duration: process.env.PROFILER_SLOW_QUERIES_DURATION || 100,
  },
  user: {
    /** The API will empty the user's email */
    showEmail: false,
  },
  session: {
    /** if true it will allow icat+ to refresh the icat token */
    allowRefresh: true,
  },
  /** Database configuration section */
  database: {
    /** URI to connect to mongoDB: mongodb://[USER]:[PASSWORD]@[HOST]:[PORT]/[DATABASE]?authSource=admin*/
    uri: process.env.MONGO_DB_URL || undefined,
    isMongoUnitEnabled: !!+process.env.MONGO_UNIT_ENABLED,
  },
  /** Logging configuration section */
  logging: {
    /** Logging to graylog */
    graylog: {
      /** If TRUE enables the sending of logs to graylog server */
      enabled: !!+process.env.GRAYLOG_ENABLED,
      /** Graylog server URL */
      host: "graylog-dau.esrf.fr",
      /** Source of the logs */
      hostname: "ICAT+",
      /** HTTP port to use on the host side where graylog server is listening */
      port: 12205,
      /** Facility */
      facility: "ESRF",
    },
    /** Logging to the terminal */
    console: {
      /** Level of verbosity. Use '0' for no logs ; Use 'silly' to see all logs */
      level: process.env.LOGGING_LEVEL || "silly",
    },
    /** Logging to file */
    file: {
      /** If TRUE, logs are written to a file */
      enabled: !!+process.env.LOGGING_FILE_ENABLED,
      /** Absolute filepath where the logs will be stored */
      filename: "/tmp/server.log",
      /** Verbosity level. See logging/console/level for details*/
      level: "silly",
    },
  },
  /** ICAT data service configuration section */
  ids: {
    /** IDS server URL : http://[SERVER]:[PORT]*/
    server: process.env.IDS_SERVER,
  },
  /** ICAT metadata catalogue configuration section */
  icat: {
    /** ICAT server URL */
    server: `${process.env.ICAT_SERVER_URL}:${process.env.ICAT_SERVER_PORT}`,
    /** Maximum number of events that can be returned from the elogbook by a single HTTP request */
    maxQueryLimit: 10000,
    /** Authorization management section*/
    authorizations: {
      adminUsersReader: {
        /** Name of the group for users with special privileges (usually the admin group) */
        administrationGroups: ["admin"],
        /**
         * User is capable to get the list of users that are administrators
         * User needs access to tables user and userGroup in order to know who are administrators
         */
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: process.env.ICAT_ADMIN_USERNAME }, { password: process.env.ICAT_ADMIN_PASSWORD }],
        },
      },
      /**
       * This user is allowed to mint DOI's and needs some permission on ICAT tables
       * User has the permission to create and read datacollections in the ICAT database.
       */
      minting: {
        group: process.env.ICAT_MINTING_GROUP,
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: process.env.ICAT_MINTING_USERNAME }, { password: process.env.ICAT_MINTING_PASSWORD }],
        },
      },
      /**
       * This user can read the list of parameter types from ICAT
       */
      parameterListReader: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: process.env.ICAT_LISTREADER_USERNAME }, { password: process.env.ICAT_LISTREADER_PASSWORD }],
        },
      },
      /**
       * This user can read the list of investigations from ICAT
       */
      investigationReader: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_INVESTIGATIONS_READER_USERNAME }, { password: process.env.ICAT_INVESTIGATIONS_READER_PASSWORD }],
        },
      },
      /**
       * notifier needs to get access to all investigations as it will convert investigationName and beamline into investigationId
       */
      notifier: {
        user: {
          /** Authentication mechanism used by ICAT server */
          plugin: "db",
          credentials: [{ username: process.env.ICAT_NOTIFIER_USERNAME }, { password: process.env.ICAT_NOTIFIER_PASSWORD }],
        },
      },
      dataacquisition: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_DATAACQUISITION_USERNAME }, { password: process.env.ICAT_DATAACQUISITION_PASSWORD }],
        },
      },
      ingester: {
        user: {
          plugin: "db",
          credentials: [{ username: process.env.ICAT_INGESTER_USERNAME }, { password: process.env.ICAT_INGESTER_PASSWORD }],
        },
      },
    },
    anonymous: {
      /** Authentication mechanism used by ICAT server */
      plugin: "db",
      credentials: [{ username: process.env.ICAT_ANONYMOUS_USERNAME }, { password: process.env.ICAT_ANONYMOUS_PASSWORD }],
    },
  },
  /** Datacite configuration section. Datacite is an organization which creates DOIs (Digital Object Identifiers).
   *  We use this third-party services to create DOI for our data. More info [here](https://www.datacite.org/)*/
  datacite: {
    /** DOI prefix for your institution */
    prefix: process.env.DATACITE_PREFIX,
    /** Fixed part in the DOI suffix. */
    suffix: process.env.DATACITE_SUFFFIX,
    /** Username used for the authentication on datacite services */
    username: process.env.DATACITE_USERNAME,
    /** Password used for authentication on datacite services */
    password: process.env.DATACITE_PASSWORD,
    /** Datacite metadata store (MDS) URL */
    mds: process.env.DATACITE_MDS,
    /** Common part of the URL shared by all DOI landing pages */
    landingPage: "https://doi.esrf.fr/",
    publisher: "European Synchrotron Radiation Facility",
    /** Proxi settings */
    proxy: {
      host: "proxy.esrf.fr",
      port: "3128",
    },
  },
  dmp: {
    enabled: true,
    server: process.env.DMP_SERVER,
    credentials: { email: process.env.DMP_EMAIL, password: process.env.DMP_PASSWORD },
  },
  ewoks: {
    enabled: true,
    server: process.env.EWOKS_SERVER,
  },
  /** Elastic search configuration section */
  elasticsearch: {
    /** If enabled elastic search will send queries to the server */
    enabled: true,
    /** Server and pord where elasticsearch is running */
    server: process.env.ELASTIC_SEARCH_SERVER,
  },
  /** H5Grove (HDF5 reader) */
  h5grove: {
    /** If not enabled then operation will not be run **/
    enabled: true,
    server: {
      /** Server that actually implements h5grove and requests will be transfered to */
      url: process.env.H5GROVE_SERVER,
    },
  },
  /** OAI-PMH Section */
  oaipmh: {
    /** If not enabled then operation will not be run **/
    enabled: true,
    server: {
      /** Server that actually implements OAI-PMH and requests will be transfered to */
      url: process.env.OAIPMH_SERVER,
      port: process.env.OAIPMH_PORT,
    },
  },
  panosc: {
    /** If not enabled then operation will not be run **/
    search: {
      enabled: true,
      server: {
        /** Server that actually implements search API and requests will be transfered to */
        url: process.env.PANOSC_SEARCH_SERVER,
        port: process.env.PANOSC_SEARCH_PORT,
      },
    },
    humanOrgans: {
      enabled: true,
      server: {
        /** Server that actually implements search API and requests will be transfered to */
        url: process.env.PANOSC_HUMANORGANS_SERVER,
        port: process.env.PANOSC_HUMANORGANS_PORT,
      },
    },
  },
  experimentalReports: {
    server: process.env.EXPERIMENTAL_REPORTS_SERVER,
  },
};
