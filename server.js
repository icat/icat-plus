/** Remove this in production */
"use strict";
process.env.NODE_ENV = (process.env.NODE_ENV || "dev").toUpperCase();

// Setting the NODE_TLS_REJECT_UNAUTHORIZED environment variable to '0' makes TLS connections and HTTPS requests insecure by disabling certificate verification
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const express = require("express");
const bodyParser = require("body-parser");

const swaggerUi = require("swagger-ui-express");
const app = express();
const cors = require("cors");

const configPath = "./config/server.config.js";
global.gServerConfig = require(configPath);

global.gInvestigationConfig = require("./app/investigation.config.js");
global.gRestoreConfig = require("./app/restore.config.js");
global.gMessageConfig = require("./app/message.config.js");

global.gLogger = require("./app/logging/logger.js").logger;
global.appCache = null;
global.API_KEY = global.gServerConfig.server.API_KEY;

global.profilerEnabled = global.gServerConfig.profiler ? global.gServerConfig.profiler.enabled : true;
global.slowQueriesDuration = global.gServerConfig.profiler ? global.gServerConfig.profiler.slow_queries_duration : 100;

global.gLogger.info(`Profiler. enabled: ${global.profilerEnabled} duration:${global.slowQueriesDuration} ms`);

const { saveRequest } = require("./app/controllers/request.controller.js");

const mongoose = require("mongoose");
const cache = require("./app/cache/appCache.js");

global.gLogger.info(`Environment is: ${process.env.NODE_ENV}`);

global.gLogger.info(`ICAT server is: ${global.gServerConfig.icat.server}`);
global.gLogger.info(`configPath: ${configPath}`);
global.gLogger.debug(`Test environment:${process.env.TEST_ENV}`);
global.gLogger.info(`Ewoks server is: ${global.gServerConfig.ewoks.server}`);

/**
 * Configure where the tracking configuration is (tracking.config.js)
 *    - deployment" the file should be on /app
 *    - test: the file should be on /test/{TEST_ENV}.tracking.config.js
 */
const trackingConfigPath = process.env.NODE_ENV === "TEST" ? `./test/${process.env.TEST_ENV ? `${process.env.TEST_ENV}.` : ""}tracking.config.js` : "./app/tracking.config.js";
global.gTrackingConfig = require(trackingConfigPath);
global.gLogger.info(`trackingConfigPath is: ${trackingConfigPath}`);

app.use(bodyParser.urlencoded({ limit: "500mb", extended: true }));
app.use(bodyParser.text({ type: "application/x-ndjson" })); // Needed for elastic search msearch
app.use(bodyParser.json({ limit: "500mb", extended: true }));

/** For static content */
app.use(express.static("public"));
app.use("/static", express.static("public"));

app.use((req, res, next) => {
  const start = process.hrtime();

  res.on("close", async () => {
    if (global.profilerEnabled) {
      saveRequest(start, req);
    }
  });

  next();
});

/**mongoose */
mongoose.Promise = global.Promise;

// Connecting to the database
const uri = global.gServerConfig.database.uri;

if (process.env.NODE_ENV === "TEST") {
  if (global.gServerConfig.database.isMongoUnitEnabled) {
    global.gLogger.debug("Mongounit enabled: ", { uri });
  } else {
    global.gLogger.debug("Mongounit disabled. Using MonDB instance: ", { uri });
  }
}

mongoose.set("debug", false);
mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);

mongoose.connection.on("connecting", () => {
  global.gLogger.debug("Connecting to MongoDB...", { uri });
});
mongoose.connection.on("reconnected", () => {
  global.gLogger.debug(" Reconnected to MongoDB");
});

mongoose.connection.on("disconnected", () => {
  global.gLogger.debug("Disconnected from MongoDB", { uri });
  connectDB();
});

function connectDB() {
  mongoose
    .connect(uri, { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
      global.gLogger.debug("Successfully connected to the database", { uri });
    })
    .catch((err) => {
      global.gLogger.error("Could not connect to the database. Exiting now...", { error: err, uri });
      process.exit();
    });
}
connectDB();

// use it before all route definitions
app.use(cors({ origin: "*" }));

/** Routes */
app.get("/", (req, res) => {
  res.json({ message: "Welcome to ICAT+" });
});

global.gLogger.debug("Loading routes...");

[
  "h5grove",
  "dataacquisition",
  "actions",
  "resource",
  "session",
  "cache",
  "logbook",
  "catalogue",
  "elasticsearch",
  "oaipmh",
  "panosc",
  "doi",
  "shipment",
  "parcel",
  "address",
  "item",
  "dmp",
  "ids",
  "ewoks",
  "userpreferences",
  "messages",
].forEach((route) => {
  const routePath = `./app/routes/${route}.routes.js`;
  try {
    require(routePath)(app);
    global.gLogger.debug(`${route} is loaded`);
  } catch (e) {
    global.gLogger.error(`Error loading ${route}`);
    global.gLogger.error(e);
  }
});

// listen for requests
const server = app.listen(global.gServerConfig.server.port, () => {
  global.gLogger.info(`ICAT+ is listening on port ${global.gServerConfig.server.port}`);
});

server.onCacheInitialized = function () {};

global.gLogger.debug("Init cache...");
/** Init cache */
cache
  .init()
  .then(() => {
    server.onCacheInitialized();
  })
  .catch((error) => {
    global.gLogger.error("[cache] Cache could not be initialized", { error: error.message });

    if (error.response && error.response.data && error.response.data.message) {
      global.gLogger.error("[cache] Cache could not be initialized", { error: error.response.data.message });
    }
    process.exit();
  });

const swaggerJSDoc = require("swagger-jsdoc");

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "ICAT+ API",
    description: "ICAT+ RESTful API with Swagger",
    version: "1.0.0",
    contact: {
      name: " Alejandro De Maria Antolinos",
      email: "demariaa@esrf.fr",
    },
    license: {
      name: "MIT",
      url: "https://opensource.org/licenses/MIT",
    },
  },
  servers: [
    {
      url: global.gServerConfig.server.url,
    },
    {
      url: "http://localhost:8000", // For developments purposes is useful
    },
  ],
  tags: [
    {
      name: "Session",
      description: "all API methods related to session",
    },
  ],
};

global.gLogger.info(`Tracking notifications enabled ${global.gTrackingConfig.notifications.enabled}`);
global.gLogger.info(`Restore notifications enabled ${global.gRestoreConfig.enabled}`);
global.gLogger.info(`Message notifications enabled ${global.gMessageConfig.enabled}`);
// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["./documentation/swagger/**/*.js", "./**/routes/*.js"], // pass all in array
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);

app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

/** Swagger */
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec, { exporer: true }));

module.exports = server;
