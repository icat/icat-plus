# Installation

## Download the source code from github repository

## Install dependencies

```bash
npm install
```

## A MongoDB instance is required. The following command will download and install a docker image containing mongoDB database. Then it will start a mongoDB instance listening at port 27017.

```bash
docker run  -p 27017:27017 mongo
```

## Install the PDF generator (**optional and only needed for elogbook reporting**)

For Debian it is necessary to install wkhtmltopdf as described [here](https://wkhtmltopdf.org/downloads.html). It seems that the version they provided has more features than the one provided by apt-get in the central Debian repositories.

Example:

```bash
wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.focal_amd64.deb
dpkg -i wkhtmltox_0.12.6-1.focal_amd64.deb
```

## Run

```bash
npm start
```

Or run with nodemon. This will update the server automatically:

```bash
 nodemon npm start
```
