# Developments

## Tests

### Configuration

ICAT+ can run the tests by using mongoUnit (mongoDB in memory) or against a MongoDB instance. By default MongoUnit will be used except if in the configuration file config/server.config.js is set to true in the section of databases:

```
    database: {
        uri: 'mongodb://localhost:27017/icatplus?authSource=admin',
        isMongoUnitEnabled : true /
    },

```

If `isMongoUnitEnabled = false` then tests will use the **uri** defined in the database section. You can use the environment variable `MONGO_UNIT_ENABLED` to specify the value of this variable.

An instance of Mongo can be run with docker:

```bash
docker run  -p 27017:27017 mongo
```

Create the environment file related to the test: `.env.test`. This default context will use by default the test values defined in [test.global.resource.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/test/env/test.global.resource.js).
You can define another environment test:

- Export the test environment: `export TEST_ENV=compose`
- Define the test values in a file prefixed with the test environment name: `compose.global.resource.js`
- Execute the tests: `npm test`

### Run tests

```bash
npm test

```

For running a single test then:

```bash

npm test -- --grep "logbook/:sessionId/investigation/id/:investigationId/tag"

```

## Coverage

NYC (successor of [Instambul](https://github.com/istanbuljs/nyc)) is used for coverage support and a report is generated when it is run.

The coverage report from the main branch is available from [here](https://icat.gitlab-pages.esrf.fr/icat-plus/coverage/).

- In order to run the coverage tool and update the report run:

```bash

npm run-script test:coverage

```

## Code quality

- `npm run lint` - run linting and code formatting commands
- `npm run lint:eslint` - lint all JS files with ESLint (this command will report warnings as errors)
- `npm run lint:prettier` - check that all files have been formatted with Prettier

### Automatic fixing and formatting

- `npm run lint:eslint -- --fix` - auto-fix linting issues
- `npm run lint:prettier -- --write` - format all files with Prettier

## Debugging

- Install Node.js inspector Manager on Chrome/Chromium
- Open Inspector Manager and Developer Tools of the browser
- Run

```bash
node --inspect-brk server.js
```

Debug mode is also available for tests by:

```bash
./node_modules/mocha/bin/mocha test --opts test/mocha.opts --inspect-brk
```
