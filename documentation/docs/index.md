# ICAT+

ICAT+ is a NodeJS application that exposes a RestFul API and uses a MongoDB database. It allows:

- Logbook: Register and read events from the e-logbook database.
- Sample tracking
- Managing external resources such as images or any file.
- Controlled access to the resources by using a Metadata Catalogue (currently [ICAT](https://icatproject.org/))
- Minting of DOIs (by list of datasets)

ICAT+ is a collaborative project and licensed under the MIT license.

## API

ICAT+ exposes a set of restful webservices. The detailed documentation of these API methods is browsable at <http://server:8000/api-docs> once you have started the application. Additional information can also be foud in the [api page](./api.md)
