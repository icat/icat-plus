# API

ICAT+ exposes a set of restful webservices. The detailed documentation of these API methods is browsable at <http://server:8000/api-docs> once the application is started.

## Retrieve datasets by filtering on datasets parameters

The end point is: `GET /catalogue/{sessionId}/dataset`.

They can be filtered by dataset parameters via the query parameter `parameters`. This is a list of parameters separated by comma. A filter is composed by the parameter's name, and optionally an operator and a value, separated by `~`.

The parameter's name is mandatory and case-insensitive. The parameters's name can be a semicolon separated list to enable the search over different parameters (as an OR).

The following operators can be used as part of the parameter:

| Operator               | Notation | Example                                                                                                                             |
| ---------------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| Equals                 | ~eq~     | Use the equals operator to perform an exact match. `definition~eq~fluo` will filter all datasets with a definition equals to `fluo` |
| Like                   | ~like~   | `definition~like~flu` will filter all datasets for which the definition contains `fluo`                                             |
| Greater than           | ~gt~     | `emctf_angle~gt~33`                                                                                                                 |
| Greater than or equals | ~gteq~   | `emctf_angle~gteq~33`                                                                                                               |
| Less than              | ~lt~     | `emctf_angle~lt~33`                                                                                                                 |
| Less than or equals    | ~lteq~   | `emctf_angle~lteq~33`                                                                                                               |
| In                     | ~in~     | `samplepatient_organ_name~in~lung;heart` semicolon separated list                                                                   |

### Examples

#### Queyring by dataset parameter name without an operator

The following example queries for datasets which have a project name parameter:

`GET /catalogue/{sessionId}/dataset?datasetIds=571998122,142796047&parameters=project_name`

It returns only 1 dataset.

#### Queyring by organ

The following example queries for datasets related to `lung` organ:

`GET /catalogue/{sessionId}/dataset?datasetIds=571998122,572244401,572062098&parameters=samplepatient_organ_name~like~lung`

It returns 2 datasets.

If the request uses the equals operator, no datasets is returned for the query request, because the operator enforces an exact match (and the organ in this case is `left lung`)

#### Using multiple parameters

The following example shows the use of multiple parameters to query for datasets with patient age that is greater or equals than 60 and a VOI dose less than 4:

`GET /catalogue/{sessionId}/dataset?datasetIds=571998122,572244401,572062098&parameters=samplepatient_age~gteq~60,TOMO_total_voi_dose~lt~4`

#### Using multiple parameters names

The following example shows the use of multiple parameters names to query for datasets by searching 'hung' in the organ name or in the patient information :

`GET /catalogue/{sessionId}/dataset?datasetIds=571998122,572244401,572062098&parameters=samplepatient_info;samplepatient_organ~like~hung`
