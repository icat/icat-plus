# Configuration

## ICAT+ configuration

The configuration settings are in [server.config.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/config/server.config.example.js). An [example file](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/config/server.config.example.js) is provided and needs to be copied into configuration file:

```bash
npm run configure:all
```

Edit the file 'server.config.js' with your favorite text editor.

### Environment variables

Some configuration properties are based on environment variables. These variables are listed in the [.env](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/.env) file. You can define different environment files that you could decide to use for your env variables. For example:

- `.env.prod`: specific to the production environment. The command `npm run prod` will start the server based on this environment.
- `.env.dev`: specific to the test (or dev or pre-prod) environment. The command `npm run dev` will start the server based on this environment.
- `.env.local`: specific to the local environment. The command `npm run local` will start the server based on this environment.

You can define any environment:

- Export the `NODE_ENV` variable `export NODE_ENV=example`
- Create the associated file with the same extension: `.env.example`
- Run the command `npm start`

The `test` environment is dedicated to run the tests, please refer to the [developments](./developments.md) section.

## ICAT configuration

ICAT+ currently depends on [ICAT](https://icatproject.org/).

The ICAT's functionalities which are used:

- User authentication
- User's permission to access and/or store metadata related to investigations, datasets and datafiles.

For this reason, ICAT metadata catalogue also needs to be configured to support ICAT+. This includes the following:
ICAT+ needs to make some operations on ICAT then it needs to have some right to be configured on it:

1. [Minting](#minting)
2. [Elogbook Permissions](#elogbook-permissions)
3. [Notifiers](#notifiers)
4. [Logging](#logging)

### Minting

ICAT+ allows to mint DOIs given a **list of datasets, authors, title and abstract**.

ICAT+ will create in the Metadata Catalogue a new DataCollection entry with the selected datasets so DOI will be stored in the field 'doi' of the DataCollection table.

Then ICAT+ will fill up the tables **DataCollection, DataCollectionDataset and DataCollectionParameter** of the Metadata Catalogue. To make this as safe as possible, an user's account with the proper rights needs to be configured.

Example of valid configuration rules:

**minter** is a user that belongs to the group **minters**

```
User minter
UserGroup minters
```

Rules to be included in ICAT:

```
CRU minters DataCollection
CR minters DataCollectionParameter
CR minters DataCollectionDataset
R  minters Dataset
R  minters ParameterType
R  DataCollection

```

Also a set of parameterTypes are needed (ApplicableToDataCollection):

```text
title
abstract
mintedByFullName
mintedByName
investigationNames
```

### Elogbook Permissions

#### Administrator

Elogbook's permissions are not directly managed by ICAT since there is not events (finest logbook granularity level) as entity in ICAT. So, ICAT+ will allow read and write access to the participants of the investigation and the administrators during the embargo period. A participant is formally described as a investigationUser linked to a investigation.

After the embargo period write access is removed and read access is enabled for everybody.

Then ICAT+ needs to know who are the administrators so it needs to use an user's account properly configured in order to get the list of users that belong to the group administrators.

Example of valid configuration rules:

**userGroupReader** is a user that belongs to the group **userGroupReaders**

```
User userGroupReader
UserGroup userGroupReaders
```

Rules to be included in ICAT:

```
R userGroupReaders User
R userGroupReaders UserGroup

```

Take into account that the name of the group of administrator is a parameter in the configuration file

### Notifiers

During the data acquisition any software can send notifications (a event of the logbook) to ICAT+. Problem is that such software not always have the possibility to log into ICAT, it means that they have not got access to username and password.
So, implementation has been done with a **magic** API token that will allow store notification on any investigation by given the investigation name and the instrument name. As ICAT+ is linking that notification with the investigationId then it needs to use an user's account properly configured in order to get the investigation given the investigation name and instrument name.

Example of valid configuration rules:

**notifier** is a user that belongs to the group **notifiers**

```
User notifier
UserGroup notifiers
```

Rules to be included in ICAT:

```
R Investigation
R Instrument
R InvestigationInstrument

```

## Additional configuration

- The sample tracking configuration settings are in [tracking.config.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/tracking.config.example.js).
- The investigation configuration settings are in [investigation.config.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/investigation.config.example.js).
- The message configuration settings are in [message.config.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/message.config.example.js).
- The restore email configuration settings are in [restore.config.js](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/tracking.config.example.js)
  The restoration email template is located in [email.restore.handlbars](https://gitlab.esrf.fr/icat/icat-plus/-/blob/master/app/views/email.restore.handlebars).

To create the files:

```bash
npm run configure:all
```

Edit these files with your favorite text editor.

### logging

Winston is used on ICAT+ for logging. Winston is a simple and universal loggin library with support for multiple transports. The configuration of winston is done on the `server.config.js` under the section `logging`

There are currently three three transport mechanisms supported: `graylog`, `console` and `file`

Example of the logging section in `server.config.js`:

```
logging: {
    /** Configuration to graylog */
    graylog: {
      /** If false graylog will be ignored */
      enabled: false,
      /** Graylog server host */
      host: "graylog.esrf.fr",
      /** Graylog server port */
      port: 12200,
      /** Name of the facility */
      facility: "ESRF",
    },
    console: {
      level: "silly",
    },
    file: {
      enabled: true,
      filename: "/tmp/server.log",
      level: "silly",
    },
  },
```
