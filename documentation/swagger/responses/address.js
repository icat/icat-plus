/**
 * @swagger
 *
 * components:
 *   responses:
 *     addresses:
 *       description: List of address
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/address'
 *     address:
 *       description: An address
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/address'
 */
