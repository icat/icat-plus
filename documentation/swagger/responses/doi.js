/**
 * @swagger
 *
 * components:
 *   responses:
 *     doiCreated:
 *        description: DOI was created succesfully
 *        content:
 *          application/json:
 *             schema:
 *             type: object
 *             description: the new DOI created
 *             properties:
 *               message:
 *                 type: string
 *             example:
 *               message: 10.15151/ESRF-ES-90632078
 *
 */
