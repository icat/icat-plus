/**
 * @swagger
 *
 * components:
 *   responses:
 *     error500:
 *       description: Internal server error.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string
 *             example: 'An error occured on the server side.'
 *     error501:
 *       description: Not implemented.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string
 *             example: 'Not implemented.'
 *     error404:
 *       description: Not found.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string
 *             example: 'Not found.'
 */
