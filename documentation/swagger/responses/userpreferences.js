/**
 * @swagger
 *
 * components:
 *   responses:
 *     userpreferences:
 *       description: user's preferences
 *       content:
 *         application/json:
 *           schema:
 *              $ref: '#/components/schemas/userpreferences'
 */
