/**
 * @swagger
 *
 * components:
 *   responses:
 *     sessionInformation:
 *       description: Information about the session
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               remainingMinutes:
 *                 type: number
 *                 format: float
 *               userName:
 *                 type: string
 *               affiliation:
 *                 type: string
 *               fullName:
 *                 type: string
 *               user:
 *                 $ref: '#/components/schemas/session'
 *           example:
 *             remainingMinutes: 2.1
 *             username: "reader"
 *             user: {
 *                   name: "reader",
 *                   username: "reader",
 *                   sessionId: "b1a04f33-3989-4a12-81b8-6d589ff0c01a",
 *                   fullName: "Anonymous",
 *                   affiliation: "MyFacility",
 *                   lifeTimeMinutes: 720,
 *                   expirationTime: "2022-11-01T03:57:00.364Z",
 *                   isAdministrator: false,
 *                   isInstrumentScientist: false,
 *                   isMinter: false,
 *                   usersByPrefix: []
 *                   }
 */
