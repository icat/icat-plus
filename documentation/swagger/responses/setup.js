/**
 * @swagger
 *
 * components:
 *   responses:
 *     setup:
 *       description: Return the setup allowed for sample tracking by investigation
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/setup'
 */
