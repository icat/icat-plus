/**
 * @swagger
 *
 * components:
 *   responses:
 *     shipments:
 *       description: List of shipments
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/shipment'
 *     shipment:
 *       description: A Shipment
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/shipment'
 */
