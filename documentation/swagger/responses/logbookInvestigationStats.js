/**
 * @swagger
 *
 * components:
 *   responses:
 *     logbookInvestigationStats:
 *       description: List of logbookInvestigationStat
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/logbookInvestigationStat'
 */
