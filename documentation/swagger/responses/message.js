/**
 * @swagger
 *
 * components:
 *   responses:
 *     message:
 *       description: message from DRAC
 *       content:
 *         application/json:
 *           schema:
 *              $ref: '#/components/schemas/message'
 */
