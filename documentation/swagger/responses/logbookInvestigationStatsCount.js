/**
 * @swagger
 *
 * components:
 *   responses:
 *     logbookInvestigationStatsCount:
 *       description: List of logbookInvestigationStatsCount
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/logbookInvestigationStatCount'
 */
