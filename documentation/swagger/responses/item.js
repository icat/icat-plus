/**
 * @swagger
 *
 * components:
 *   responses:
 *     items:
 *       description: List of items
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/item'
 *     item:
 *       description: An item
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/item'
 */
