/**
 * @swagger
 *
 * components:
 *   responses:
 *     actions:
 *       description: result of the action
 *       content:
 *         application/json:
 *           schema:
 *              $ref: '#/components/schemas/actions'
 */
