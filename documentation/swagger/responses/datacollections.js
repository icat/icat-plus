/**
 * @swagger
 *
 * components:
 *   responses:
 *     datacollections:
 *       description: List of data collections
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/datacollection'
 */
