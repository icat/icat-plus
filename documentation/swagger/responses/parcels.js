/**
 * @swagger
 *
 * components:
 *   responses:
 *     parcels:
 *       description: List of parcels
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/parcel'
 *     parcel:
 *       description: A parcel
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/parcel'
 */
