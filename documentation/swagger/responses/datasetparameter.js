/**
 * @swagger
 *
 * components:
 *   responses:
 *     datasetparameters:
 *       description: List of datasetparameter
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/datasetparameter'
 *     datasetparameter:
 *       description: A datasetparameters
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/datasetparameter'
 */
