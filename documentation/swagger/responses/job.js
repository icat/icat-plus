/**
 * @swagger
 *
 * components:
 *   responses:
 *     jobs:
 *       description: List of job
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/job'
 *     job:
 *       description: An job
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/job'
 */
