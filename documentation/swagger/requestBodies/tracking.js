/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     parcelIdList:
 *       description: List of [_id]
 *       content:
 *         'application/json':
 *           schema:
 *             type: string
 *     shipment:
 *       description: shipment
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/shipment'
 *     parcels:
 *       description: parcel
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parcels'
 *     parcel:
 *       description: parcel
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parcel'
 *     parcelStatus:
 *       description: fields updated with parcel status
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parcelStatus'
 *     item:
 *       description: item
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/item'
 */
