/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     updateSampleParameter:
 *       description: parameter to update
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parameter'
 */
