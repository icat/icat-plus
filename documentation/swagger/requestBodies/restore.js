/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     restoreInfo:
 *       description: restoration status
 *       content:
 *         'application/json':
 *           schema:
 *             type: object
 *             properties:
 *               level:
 *                 description: Level
 *                 type: string
 *               message:
 *                 description: Message from the restoration system
 *                 type: string
 */
