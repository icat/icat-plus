/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     updateParameter:
 *       description: parameter to update
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/parameter'
 */
