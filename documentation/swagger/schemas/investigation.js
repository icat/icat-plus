/**
 * @swagger
 *
 * components:
 *   schemas:
 *     investigation:
 *         required:
 *           - "id"
 *           - "name"
 *           - "startDate"
 *         properties:
 *           id:
 *             type: "number"
 *           name:
 *             type: "string"
 *           startDate:
 *             type: "string"
 *           endDate:
 *             type: "string"
 *           doi:
 *             type: "string"
 *           title:
 *             type: "string"
 *           visitId:
 *             type: "string"
 *           releaseDate:
 *             type: "string"
 *           summary:
 *             type: "string"
 *           parameters:
 *             type: "array"
 *             items:
 *               type: "object"
 *           instrument:
 *             type: "object"
 *             required:
 *                - "id"
 *                - "name"
 *             properties:
 *                id:
 *                  type: "number"
 *                name:
 *                  type: "string"
 *                instrumentScientists:
 *                  type: "array"
 *                  required:
 *                   - "name"
 *                  items:
 *                     type: "object"
 *                     properties:
 *                        name:
 *                          type: "string"
 *           investigationUsers:
 *             type: "array"
 *             items:
 *               type: "object"
 *               required:
 *                  - "role"
 *                  - "user"
 *               properties:
 *                 role:
 *                   type: "string"
 *                 user:
 *                   type: "object"
 *                   required:
 *                      - "name"
 *                   properties:
 *                      email:
 *                        type: "string"
 *                      fullName:
 *                        type: "string"
 *                      familyName:
 *                        type: "string"
 *                      name:
 *                        type: "string"
 *                      givenName:
 *                        type: "string"
 *           meta:
 *             $ref: '#/components/schemas/meta'
 *           type:
 *             type: "object"
 *             required:
 *                - "id"
 *                - "name"
 *             properties:
 *                  id:
 *                    type: "string"
 *                  createId:
 *                    type: "string"
 *                  createTime:
 *                    type: "string"
 *                  modId:
 *                    type: "string"
 *                  modTime:
 *                    type: "string"
 *                  description:
 *                    type: "string"
 *                  name:
 *                    type: "string"
 *           canAccessDatasets:
 *             type: "boolean"
 *
 */
