/**
 * @swagger
 *
 * components:
 *   schemas:
 *     job:
 *       description: a reprocessing job
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *         datasetIds:
 *           type: "array"
 *           items:
 *             type: "string"
 *         sampleIds:
 *           type: "array"
 *           items:
 *             type: "string"
 *         userNames:
 *           type: "array"
 *           items:
 *             type: "string"
 *         investigations:
 *           type: "array"
 *           items:
 *             properties:
 *               _id:
 *                 type: "string"
 *               name:
 *                 type: "string"
 *               id:
 *                 type: "string"
 *         identifier:
 *           type: string
 *         input:
 *           type: object
 *         logs:
 *           type: "array"
 *           items:
 *             type: "object"
 *         status:
 *           type: string
 *         statuses:
 *           type: "array"
 *           items:
 *             type: "string"
 *         step:
 *           type: string
 *         steps:
 *           type: "array"
 *           items:
 *             type: "string"
 *         jobId:
 *           type: string
 *         workflow:
 *           type: object
 *         createdAt:
 *           type: string
 *         updatedAt:
 *           type: string
 */
