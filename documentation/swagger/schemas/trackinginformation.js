/**
 * @swagger
 *
 * components:
 *   schemas:
 *     trackingInformation:
 *       description: aditional tracking information
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *         trackingNumber:
 *           type: string
 *         forwarder:
 *           type: string
 */
