/**
 * @swagger
 *
 * components:
 *   schemas:
 *     dmpquestionnaire:
 *       required:
 *         - "uuid"
 *       properties:
 *         uuid:
 *           type: "string"
 *
 */
