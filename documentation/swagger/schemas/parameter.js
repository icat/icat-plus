/**
 * @swagger
 *
 * components:
 *   schemas:
 *     parameter:
 *       description: a parameter
 *       type: object
 *       required:
 *         - name
 *         - value
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         value:
 *           type: string
 *         units:
 *           type: string
 *         description:
 *           type: string
 */
