/**
 * @swagger
 *
 * components:
 *   schemas:
 *     sample:
 *         required:
 *           - "id"
 *           - "name"
 *         properties:
 *           id:
 *             type: "number"
 *           name:
 *             type: "string"
 *           modTime:
 *             type: "string"
 *           investigation:
 *             type: "object"
 *             required:
 *               - "id"
 *               - "name"
 *               - "startDate"
 *             properties:
 *               id:
 *                 type: "number"
 *               name:
 *                 type: "string"
 *               startDate:
 *                 type: "string"
 *               endDate:
 *                 type: "string"
 *               title:
 *                 type: "string"
 *               visitId:
 *                 type: "string"
 *               instrument:
 *                 type: "object"
 *                 required:
 *                  - "id"
 *                  - "name"
 *                 properties:
 *                  id:
 *                    type: "number"
 *                  name:
 *                    type: "string"
 *           datasets:
 *             type: "array"
 *             items:
 *               type: "object"
 *               required:
 *                 - "id"
 *                 - "name"
 *                 - "type"
 *               properties:
 *                 id:
 *                   type: "number"
 *                 name:
 *                   type: "string"
 *                 startDate:
 *                   type: "string"
 *                 endDate:
 *                   type: "string"
 *                 location:
 *                   type: "string"
 *                 type:
 *                   type: "string"
 *                 parameters:
 *                   type: "array"
 *                   items:
 *                     type: "object"
 *                     required:
 *                       - "id"
 *                       - "name"
 *                       - "value"
 *                       - "units"
 *                     properties:
 *                      id:
 *                        type: "number"
 *                      name:
 *                        type: "string"
 *                      value:
 *                        type: "string"
 *                      units:
 *                        type: "string"
 *           parameters:
 *             type: "array"
 *             items:
 *               type: "object"
 *               required:
 *                  - "id"
 *                  - "name"
 *                  - "value"
 *                  - "units"
 *               properties:
 *                 id:
 *                   type: "number"
 *                 name:
 *                   type: "string"
 *                 value:
 *                   type: "string"
 *                 units:
 *                   type: "string"
 *           meta:
 *              $ref: '#/components/schemas/meta'
 */
