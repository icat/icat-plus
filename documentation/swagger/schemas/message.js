/**
 * @swagger
 *
 * components:
 *   schemas:
 *     message:
 *       description: message from DRAC
 *       type: object
 *       required:
 *         - type
 *       properties:
 *         _id:
 *           type: string
 *         type:
 *           type: string
 *         message:
 *           type: string
 *
 */
