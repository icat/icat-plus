/**
 * @swagger
 *
 * components:
 *   schemas:
 *     user:
 *         properties:
 *           orcid:
 *             type: "string"
 *           name:
 *             type: "string"
 *           givenName:
 *             type: "string"
 *           affilation:
 *             type: "string"
 *           familyName:
 *             type: "string"
 *           email:
 *             type: "string"
 *           fullName:
 *             type: "string"
 *           id:
 *             type: "number"
 */
