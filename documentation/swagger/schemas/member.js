/**
 * @swagger
 *
 * components:
 *   schemas:
 *     member:
 *         properties:
 *           orcid:
 *             type: "string"
 *           name:
 *             type: "string"
 *           surname:
 *             type: "string"
 *           affilation:
 *             type: "string"
 *           role:
 *             type: "string"
 */
