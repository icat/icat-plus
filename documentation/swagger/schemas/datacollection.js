/**
 * @swagger
 *
 * components:
 *   schemas:
 *     datacollection:
 *       required:
 *         - "id"
 *       properties:
 *         id:
 *           type: "number"
 *         createId:
 *           type: "string"
 *         createTime:
 *           type: "string"
 *         modId:
 *           type: "string"
 *         modTime:
 *           type: "string"
 *         dataCollectionDatasets:
 *           type: "array"
 *           items:
 *             type: "object"
 *             properties:
 *               dataset:
 *                 type: "object"
 *                 properties:
 *                   id:
 *                     type: "number"
 *                   name:
 *                     type: "string"
 *                   startDate:
 *                     type: "string"
 *                   endDate:
 *                     type: "string"
 *                   location:
 *                     type: "string"
 *                   sampleName:
 *                     type: "string"
 */
