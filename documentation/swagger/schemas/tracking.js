/**
 * @swagger
 *
 * components:
 *   schemas:
 *     shipment:
 *       description: a shipment
 *       type: object
 *       required:
 *         - name
 *         - investigationName
 *       properties:
 *         _id:
 *           type: string
 *         investigationId:
 *           type: integer
 *         investigationName:
 *           type: string
 *         name:
 *           type: string
 *         defaultReturnAddress:
 *           description: a postal address that will be used by default as returning address of the parcels
 *           $ref: '#/components/schemas/address'
 *         defaultShippingAddress:
 *           $ref: '#/components/schemas/address'
 *         parcels:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/parcel'
 *     returnParcelInformation:
 *       description: information needed for the return of the parcel
 *       type: object
 *       required:
 *         - returnType
 *       properties:
 *         _id:
 *           type: string
 *         returnType:
 *           type: string
 *           enum: [USER_MANAGE_PAPERS, USER_ORGANIZE_RETURN]
 *         forwarderName:
 *           type: string
 *           description: forwarder name if returnType is USER_ORGANIZE_RETURN
 *         forwarderAccount:
 *           type: string
 *           description: forwarder account if returnType is USER_ORGANIZE_RETURN (optional)
 *         plannedPickupDate:
 *           type: string
 *           description: planned pickup date if returnType is USER_ORGANIZE_RETURN
 *     parcelStatus:
 *       description: fields updated with parcel status
 *       type: object
 *       properties:
 *         shippingTrackingInformation:
 *           $ref: '#/components/schemas/trackingInformation'
 *         returnTrackingInformation:
 *           $ref: '#/components/schemas/trackingInformation'
 *         containsDangerousGoods:
 *           type: boolean
 *     parcel:
 *       description: A box that will be sent by courier to the faciity with items inside and will be tracked independently.
 *       type: object
 *       required:
 *         - name
 *         - investigationName
 *         - investigationId
 *       properties:
 *         _id:
 *           type: string
 *         shipmentId:
 *           type: string
 *         name:
 *           type: string
 *         investigationId:
 *           type: number
 *         investigationName:
 *           type: string
 *         storageConditions:
 *           type: string
 *         isReimbursed:
 *           type: boolean
 *         reimbursedCustomsValue:
 *           type: number
 *         returnInstructions:
 *           type: string
 *         returnParcelInformation:
 *           $ref: '#/components/schemas/returnParcelInformation'
 *         comments:
 *           type: string
 *         containsDangerousGoods:
 *           type: boolean
 *         statuses:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/status'
 *         shippingAddress:
 *           $ref: '#/components/schemas/address'
 *         returnAddress:
 *           $ref: '#/components/schemas/address'
 *         shippingTrackingInformation:
 *           $ref: '#/components/schemas/trackingInformation'
 *         returnTrackingInformation:
 *           $ref: '#/components/schemas/trackingInformation'
 *         description:
 *           type: string
 *         type:
 *           type: string
 *         currentStatus:
 *           type: string
 *         items:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/item'
 *         localContactNames:
 *           type: array
 *           items:
 *             type: "string"
 *         content:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/item'
 *         createdAt:
 *           type: string
 *         updatedAt:
 *           type: string
 *         id:
 *           type: string
 *         investigation:
 *            $ref: '#/components/schemas/investigation'
 *         meta:
 *           $ref: '#/components/schemas/meta'
 *     parcels:
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/parcel'
 *     address:
 *       description: a postal address of a person for shipments
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         surname:
 *           type: string
 *         companyName:
 *           type: string
 *         address:
 *           type: string
 *         city:
 *           type: string
 *         region:
 *           type: string
 *         postalCode:
 *           type: string
 *         country:
 *           type: string
 *         email:
 *           type: string
 *         phoneNumber:
 *           type: string
 *         defaultCourierAccount:
 *           type: string
 *         defaultCourierCompany:
 *           type: string
 *     tracking:
 *       description: a tracking object is composed by a postal address and a tracking number
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         trackingId:
 *           type: string
 *         address:
 *           $ref: '#/components/schemas/address'
 *     item:
 *       description: an item that can be contained in a parcel
 *       type: object
 *       required:
 *         - name
 *         - type
 *         - id
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         type:
 *           type: string
 *         description:
 *           type: string
 *         comments:
 *           type: string
 *     status:
 *       description: status of a parcel
 *       type: object
 *       required:
 *         - status
 *         - createdBy
 *         - _id
 *       properties:
 *         _id:
 *           type: string
 *         status:
 *           type: string
 *         createdBy:
 *           type: string
 *         createdAt:
 *           type: string
 *         updatedAt:
 *           type: string
 *         createdByFullName:
 *           type: string
 *         createdByEmail:
 *           type: string
 *
 *
 */
