/**
 * @swagger
 *
 * components:
 *   schemas:
 *     setup:
 *       description: setup allowed for a parcel depending on the investigation
 *       type: object
 *       required:
 *         - containerTypes
 *         - experimentPlan
 *         - processingPlan
 *         - instrumentName
 *       properties:
 *         containerTypes:
 *           type: array
 *           items:
 *             type: string
 *         experimentPlan:
 *           type: array
 *           items:
 *             type: string
 *         processingPlan:
 *           type: array
 *           items:
 *             type: string
 *         instrumentName:
 *           type: string
 *         samplesheets:
 *           type: array
 *           items:
 *             type: object
 */
