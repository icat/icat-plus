/**
 * @swagger
 *
 * components:
 *   schemas:
 *     meta:
 *       type: "object"
 *       properties:
 *         page:
 *           type: "object"
 *           properties:
 *             total:
 *                type: "number"
 *             totalPages:
 *                type: "number"
 *             currentPage:
 *                type: "number"
 */
