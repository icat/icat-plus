/**
 * @swagger
 *
 * components:
 *   schemas:
 *     investigationusers:
 *       required:
 *         - "name"
 *         - "fullName"
 *         - "role"
 *         - "investigationName"
 *         - "investigationId"
 *       properties:
 *         id:
 *           type: "number"
 *         name:
 *           type: "string"
 *         fullName:
 *           type: "string"
 *         role:
 *           type: "string"
 *         investigationName:
 *           type: "string"
 *         investigationId:
 *           type: "number"
 *         familyName:
 *           type: "string"
 *         givenName:
 *           type: "string"
 */
