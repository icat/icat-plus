/**
 * @swagger
 *
 * components:
 *   schemas:
 *     doiData:
 *       type: object
 *       properties:
 *           title:
 *             type: string
 *           abstract:
 *             type: string
 *           datasetIdList:
 *             type: array
 *             items:
 *               type: number
 *           dataCollectionId:
 *             type: number
 *           keywords:
 *             type: array
 *             items:
 *               type: string
 *           citationPublicationDOI:
 *             type: string
 *           referenceURL:
 *             type: string
 *       required:
 *           - title
 *           - abstract
 *     authors:
 *       type: array
 *       required: true
 *       items:
 *        type: object
 *        properties:
 *           createName:
 *             type: string
 *           name:
 *             type: string
 *           surname:
 *             type: string
 *           orcid:
 *             type: string
 *           id:
 *             type: string
 *     authorslist:
 *       type: string
 *       description: semicolon-separated list. Each author has firstname, name, orcid (not mandatory) separated by comma.
 *       required: true
 *
 *
 */
