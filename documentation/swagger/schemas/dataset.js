/**
 * @swagger
 *
 * components:
 *   schemas:
 *     dataset:
 *       required:
 *         - "parameters"
 *         - "investigation"
 *         - "id"
 *         - "name"
 *         - "startDate"
 *         - "endDate"
 *         - "location"
 *         - "sampleName"
 *       properties:
 *         parameters:
 *           type: "array"
 *           items:
 *             $ref: '#/components/schemas/datasetparameter'
 *         investigation:
 *           required:
 *             - "name"
 *             - "id"
 *           properties:
 *             name:
 *               type: "string"
 *             id:
 *               type: "number"
 *           type: "object"
 *         id:
 *           type: "number"
 *         name:
 *           type: "string"
 *         startDate:
 *           type: "string"
 *         endDate:
 *           type: "string"
 *         location:
 *           type: "string"
 *         sampleName:
 *           type: "string"
 *         techniques:
 *           type: "array"
 *           items:
 *             type: "object"
 *             required:
 *               - "id"
 *               - "pid"
 *               - "name"
 *             properties:
 *               id:
 *                 type: "number"
 *               datasetId:
 *                 type: "number"
 *               pid:
 *                 type: "string"
 *               name:
 *                 type: "string"
 *               description:
 *                 type: "string"
 */
