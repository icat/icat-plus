/**
 * @swagger
 *
 * components:
 *   schemas:
 *     logbookInvestigationStatCount:
 *       description: a data structure with statistics about the count of notifications and annotations of the logbook
 *       type: object
 *       required:
 *         - _id
 *       properties:
 *         _id:
 *           type: string
 *         count:
 *           type: number
 */
