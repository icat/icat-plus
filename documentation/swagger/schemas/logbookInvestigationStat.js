/**
 * @swagger
 *
 * components:
 *   schemas:
 *     logbookInvestigationStat:
 *       description: a data structure with statistics about the logbook for a investigation
 *       type: object
 *       required:
 *         - investigationId
 *       properties:
 *         investigationId:
 *           type: string
 *         count:
 *           type: number
 *         title:
 *           type: string
 *         name:
 *           type: string
 *         __fileCount:
 *           type: string
 *         __volume:
 *           type: string
 *         __datasetCount:
 *           type: string
 *         __sampleCount:
 *           type: string
 *         __elapsedTime:
 *           type: string
 *         startDate:
 *           type: string
 *         endDate:
 *           type: string
 *         instrument:
 *           type: string
 *         annotations:
 *           type: number
 *         notifications:
 *           type: number
 */
