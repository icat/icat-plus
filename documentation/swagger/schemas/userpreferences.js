/**
 * @swagger
 *
 * components:
 *   schemas:
 *     userpreferences:
 *       description: user's preferences
 *       type: object
 *       required:
 *         - username
 *       properties:
 *         _id:
 *           type: string
 *         username:
 *           type: string
 *         selection:
 *           type: object
 *           properties:
 *             datasetIds:
 *               type: array
 *               items:
 *                  type: number
 *             sampleIds:
 *               type: array
 *               items:
 *                  type: number
 *         logbookFilters:
 *           $ref: '#/components/schemas/logbookFilters'
 *         recentlyVisited:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/recentlyVisited'
 *         mxsettings:
 *            $ref: '#/components/schemas/mxsettings'
 *         isGroupedBySample:
 *            type: boolean
 *         pagination:
 *            $ref: '#/components/schemas/pagination'
 *     logbookFilters:
 *       description: logbook filters
 *       type: object
 *       properties:
 *         sortBy:
 *           type: string
 *         sortOrder:
 *           type: number
 *         types:
 *           type: string
 *     recentlyVisited:
 *       description: recently visited items
 *       type: object
 *       required:
 *         - url
 *         - label
 *         - target
 *       properties:
 *         url:
 *           type: string
 *         label:
 *           type: string
 *         target:
 *           $ref: '#/components/schemas/target'
 *     target:
 *       description: recently visited target description
 *       type: object
 *       properties:
 *         instrumentName:
 *           type: string
 *         investigationName:
 *           type: string
 *         investigationStartDate:
 *           type: string
 *         investigationEndDate:
 *           type: string
 *     mxshell:
 *       description: mxshell
 *       type: object
 *       properties:
 *         inner:
 *           type: number
 *         outer:
 *           type: number
 *         overall:
 *           type: number
 *     mxcutoffs:
 *       description: mx filter cutoffs
 *       type: object
 *       properties:
 *         completeness:
 *           $ref: '#/components/schemas/mxshell'
 *         resolution_limit_low:
 *           $ref: '#/components/schemas/mxshell'
 *         resolution_limit_high:
 *           $ref: '#/components/schemas/mxshell'
 *         r_meas_all_IPlus_IMinus:
 *           $ref: '#/components/schemas/mxshell'
 *         mean_I_over_sigI:
 *           $ref: '#/components/schemas/mxshell'
 *         cc_half:
 *           $ref: '#/components/schemas/mxshell'
 *         cc_ano:
 *           $ref: '#/components/schemas/mxshell'
 *     mxsettings:
 *       description: mxsettings
 *       type: object
 *       required:
 *         - autoProcessingRankingShell
 *         - autoProcessingRankingParameter
 *         - scanTypeFilters
 *         - cutoffs
 *       properties:
 *         autoProcessingRankingShell:
 *           type: string
 *         autoProcessingRankingParameter:
 *           type: string
 *         autoProcessingRankingDisableSpaceGroup:
 *           type: boolean
 *         scanTypeFilters:
 *           type: array
 *           items:
 *             type: string
 *         cutoffs:
 *             $ref: '#/components/schemas/mxcutoffs'
 *     isGroupedBySample:
 *       description: data view grouped by sample or datasets
 *       type: boolean
 *     pagination:
 *       description: page size for the different tables
 *       type: array
 *       items:
 *         type: object
 *         properties:
 *           key:
 *             type: string
 *           value:
 *             type: number
 *
 */
