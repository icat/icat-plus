/**
 * @swagger
 *
 * components:
 *   schemas:
 *     datafile:
 *       required:
 *         - "id"
 *       properties:
 *         id:
 *           type: "number"
 *         createId:
 *           type: "string"
 *         createTime:
 *           type: "string"
 *         modId:
 *           type: "string"
 *         modTime:
 *           type: "string"
 *         destDatafiles:
 *           type: "array"
 *           items:
 *             type: "string"
 *         fileSize:
 *           type: "number"
 *         location:
 *           type: "string"
 *         name:
 *           type: "string"
 */
