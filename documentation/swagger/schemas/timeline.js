/**
 * @swagger
 *
 * components:
 *   schemas:
 *     timeline:
 *       properties:
 *         startDate:
 *           type: "string"
 *         endDate:
 *           type: "string"
 *         investigationId:
 *           type: "number"
 *         investigationName:
 *           type: "string"
 *         datasetName:
 *           type: "string"
 *         datasetId:
 *           type: "number"
 *         sampleId:
 *           type: "number"
 *         sampleName:
 *           type: "string"
 *         datasetType:
 *           type: "string"
 *         __fileCount:
 *           type: "string"
 *         __volume:
 *           type: "string"
 *         __elapsedTime:
 *           type: "string"
 */
