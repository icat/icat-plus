/**
 * @swagger
 *
 * components:
 *   schemas:
 *     actions:
 *       description: result of the actions
 *       type: object
 *       required:
 *         - missing
 *         - count
 *         - updated
 *       properties:
 *         missing:
 *           type: number
 *         count:
 *           type: number
 *         updated:
 *           type: number
 */
