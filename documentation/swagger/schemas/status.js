/**
 * @swagger
 *
 * components:
 *   schemas:
 *     status:
 *       type: string
 *       enum: [ONLINE, RESTORING, ARCHIVED]
 */
