/**
 * @swagger
 *
 * components:
 *   schemas:
 *     technique:
 *       required:
 *         - "pid"
 *       properties:
 *         id:
 *           type: "number"
 *         pid:
 *           type: "string"
 *         name:
 *           type: "string"
 *         description:
 *           type: "string"
 */
