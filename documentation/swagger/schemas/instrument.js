/**
 * @swagger
 *
 * components:
 *   schemas:
 *     instrument:
 *       required:
 *         - "name"
 *       properties:
 *         id:
 *           type: "number"
 *         name:
 *           type: "string"
 *         description:
 *           type: "string"
 */
