/**
 * @swagger
 *
 * components:
 *   schemas:
 *     datasetparameter:
 *       required:
 *         - "id"
 *         - "name"
 *         - "value"
 *         - "units"
 *         - "datasetId"
 *         - "sampleId"
 *         - "sampleName"
 *       properties:
 *         id:
 *           type: "number"
 *         name:
 *           type: "string"
 *         value:
 *           type: "string"
 *         units:
 *           type: "string"
 *         datasetId:
 *           type: "number"
 *         sampleName:
 *           type: "string"
 *         sampleId:
 *           type: "number"
 *         createTime:
 *           type: "string"
 */
