/**
 * @swagger
 *
 * components:
 *   schemas:
 *     datasetaccess:
 *       description: a dataset access
 *       type: object
 *       required:
 *         - datasetId
 *         - name
 *       properties:
 *         _id:
 *           type: string
 *         name:
 *           type: string
 *         datasetId:
 *           type: number
 *         level:
 *           type: string
 *         type:
 *           type: string
 *         creationDate:
 *           type: string
 *           format: date
 */
