/**
 * @swagger
 *
 * components:
 *   schemas:
 *     sampleinformation:
 *       description: additional sample information
 *       type: object
 *       required:
 *         - _id
 *         - sampleId
 *         - investigationId
 *       properties:
 *         _id:
 *           type: string
 *         investigationId:
 *           type: integer
 *         sampleId:
 *           type: integer
 *         resources:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/resourceinformation'
 *     resourceinformation:
 *       description: resource information related to a file
 *       type: object
 *       required:
 *         - _id
 *         - filename
 *       properties:
 *         _id:
 *           type: string
 *         filename:
 *           type: string
 *         fileType:
 *           type: string
 *         groupName:
 *           type: string
 *         multiplicity:
 *           type: number
 *         file:
 *           type: string
 */
