/**
 * @swagger
 *
 * components:
 *   parameters:
 *     search:
 *       in: query
 *       name: search
 *       description : text to be used for filtering the entities
 *       schema:
 *         type : string
 *     skip:
 *       in: query
 *       name: skip
 *       description : the number of entities that will be skipped
 *       schema:
 *          type : string
 *     limit:
 *       in: query
 *       name: limit
 *       description: limit the number of entities retrieved
 *       schema:
 *         type: string
 *     sortOrder:
 *       in: query
 *       name: sortOrder
 *       description: order applied to the sorting (-1 or 1) (ascending or descending)
 *       schema:
 *         type: string
 *     sampleId:
 *       in: query
 *       description: Metadata catalogue sample identifier
 *       name: sampleId
 *       schema:
 *         type: string
 *       example: 148845606
 *     pathRequiredSampleId:
 *       in: path
 *       description: Metadata catalogue sample identifier
 *       name: sampleId
 *       schema:
 *         type: string
 *       example: 148845606
 *       required: true
 *     queryRequiredSampleId:
 *       in: query
 *       description: Metadata catalogue sample identifier
 *       name: sampleId
 *       schema:
 *         type: string
 *       example: 148845606
 *       required: true
 *     querySampleIds:
 *       in: query
 *       description: Metadata catalogue sample identifiers
 *       name: sampleIds
 *       schema:
 *         type: string
 *       example: 148845606,148845607
 *     queryJobId:
 *       in: query
 *       description: The ewoks job identifier
 *       name: jobId
 *       schema:
 *         type: string
 *       example: 148845606
 *     startDate:
 *       in: path
 *       description: start date
 *       name: startDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *       required: true
 *     endDate:
 *       in: path
 *       description: start date
 *       name: endDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *       required: true
 *     startDateQueryRequired:
 *       in: query
 *       description: start date
 *       name: startDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *       required: true
 *     endDateQueryRequired:
 *       in: query
 *       description: start date
 *       name: endDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *       required: true
 *     investigationId:
 *       in: path
 *       description: Metadata catalogue investigation identifier
 *       name: investigationId
 *       schema:
 *         type: string
 *       example: 148845606
 *       required: true
 *     queryInvestigationId:
 *       in: query
 *       description: Metadata catalogue investigation identifier
 *       name: investigationId
 *       schema:
 *         type: string
 *       example: 148845606
 *     queryInvestigationIdList:
 *       in: query
 *       description:  Metadata catalogue investigation identifier list (comma separated)
 *       name: investigationId
 *       schema:
 *         type: string
 *     requiredQueryInvestigationId:
 *       in: query
 *       required : true
 *       description: Metadata catalogue investigation identifier
 *       name: investigationId
 *       schema:
 *         type: string
 *       example: 148845606
 *     queryInstrumentName:
 *       in: query
 *       description: the name of the instrument or instrument (comma separated)
 *       name: instrumentName
 *       schema:
 *         type: string
 *       example: ID01,ID23-1,ID30B
 *     toInvestigationId:
 *       in: path
 *       description: Metadata catalogue investigation identifier
 *       name: toInvestigationId
 *       schema:
 *         type: string
 *       example: 148845606
 *       required: true
 *     parcelId:
 *       in: path
 *       description: parcel identifier
 *       name: parcelId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *       required: true
 *     itemId:
 *       in: path
 *       description: item identifier
 *       name: itemId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *       required: true
 *     queryParcelId:
 *       in: query
 *       description: parcel identifier
 *       name: parcelId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *       required: true
 *     shipmentId:
 *       in: path
 *       description: shipment identifier
 *       name: shipmentId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *       required: true
 *     requiredQueryShipmentId:
 *       in: query
 *       description: shipment identifier
 *       name: shipmentId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *       required: true
 *     queryShipmentId:
 *       in: query
 *       description: shipment identifier
 *       name: shipmentId
 *       schema:
 *         type: string
 *       example: 5ee8c1f2e3e70701b5ebe383
 *     sessionId:
 *       in: path
 *       description: session identifier provided by the metadata catalogue
 *       name: sessionId
 *       schema:
 *         type: string
 *       example: 83825a51-69b8-4eg58-b5fb-bcf79bb412be
 *       required: true
 *     querySessionId:
 *       in: query
 *       description: session identifier provided by the metadata catalogue
 *       name: sessionId
 *       schema:
 *         type: string
 *       example: 83825a51-69b8-4eg58-b5fb-bcf79bb412be
 *       required: true
 *     apiKey:
 *       in: path
 *       description: API key is a simple encrypted string that identifies an application without any principal.
 *       name: apiKey
 *       schema:
 *         type: string
 *       example: 83825a51-69b8-4eg58-b5fb-bcf79bb412be
 *       required: true
 *     investigationName:
 *       in: path
 *       description: the name of the investigation
 *       name: investigationName
 *       schema:
 *         type: string
 *       example: MD-837
 *       required: true
 *     instrumentName:
 *       in: path
 *       description: the name of the beamline
 *       name: instrumentName
 *       schema:
 *         type: string
 *       example: ID01
 *       required: true
 *     queryInstrument:
 *       in: query
 *       description: the name of the beamline
 *       name: instrument
 *       schema:
 *         type: string
 *       example: ID01
 *       required: true
 *     datasetId:
 *       in: path
 *       description : Metadata catalogue dataset identifier
 *       name: datasetId
 *       schema :
 *         type : string
 *       example : 132123
 *       required: true
 *     datasetIds:
 *       in: path
 *       description : Metadata catalogue dataset identifier list (comma separated)
 *       name: datasetIds
 *       schema :
 *         type : string
 *       example : 132123,1523433
 *       required: true
 *     queryDatasetIds:
 *       in: query
 *       description : Metadata catalogue dataset identifier list (comma separated)
 *       name: datasetIds
 *       schema :
 *         type : string
 *       example : 132123,1523433
 *     queryDatafileIds:
 *       in: query
 *       description : Metadata catalogue datafile identifier list (comma separated)
 *       name: datafileIds
 *       schema :
 *         type : string
 *       example : 132123,1523433
 *     queryDatasetId:
 *       in: query
 *       description : Metadata catalogue dataset identifier
 *       name: datasetId
 *       schema :
 *         type : string
 *       example : 132123
 *     tagId:
 *       in: path
 *       description: logbook tag identifier
 *       name: tagId
 *       schema:
 *         type: string
 *       example: 5c8fbc5c8504536783dc2bc5
 *       required: true
 *     tagIdQuery:
 *       in: query
 *       description: logbook tag identifier
 *       name: tagId
 *       schema:
 *         type: string
 *       example: 5c8fbc5c8504536783dc2bc5
 *       required: true
 *     tags:
 *       in: query
 *       description: comma-separated list of tag names. Example system,calibration
 *       name: tags
 *       schema:
 *         type: string
 *     startTimeQuery:
 *       in: query
 *       description: items after start time - format YYYY-MM-DDTHH:mm:ss
 *       name: startTime
 *       schema:
 *         type: string
 *       example: 2022-05-16T08:26:00
 *     endTimeQuery:
 *       in: query
 *       description: items before end time - format YYYY-MM-DDTHH:mm:ss
 *       name: endTime
 *       schema:
 *         type: string
 *       example: 2022-05-16T08:26:00
 *     startDateQuery:
 *       in: query
 *       description: start date
 *       name: startDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *     endDateQuery:
 *       in: query
 *       description: end date
 *       name: endDate
 *       schema:
 *         type: string
 *       example: 2022-05-16
 *     datasetTypeQuery:
 *       in: query
 *       description: type of the dataset
 *       name: datasetType
 *       schema:
 *         type: string
 *     havingAcquisitionDatasetsQuery:
 *       in: query
 *       description: filter on samples having acquisition datasets (false by default)
 *       name: havingAcquisitionDatasets
 *       schema:
 *         type: boolean
 *         default: false
 *     includeDatasetsQuery:
 *       in: query
 *       description: true if the datasets are also loaded (false by default)
 *       name: includeDatasets
 *       schema:
 *         type: boolean
 *         default: false
 *     nestedQuery:
 *       in: query
 *       description: true if processed datasets will be populated
 *       name: nested
 *       schema:
 *         type: boolean
 *         default: false
 *     elementParametersQuery:
 *       in: query
 *       description: dataset or investigation parameters filter list, separated by comma. A filter is composed by the parameter's name, and optionally an operator and a value, separated by ~. The accepted list of operators is [eq, like, gt, gteq, lt, lteq, in]. See icat+ documentation for more details
 *       name: parameters
 *       schema:
 *         type: string
 *         example: definition~eq~fluo,tomo_energy~gteq~17.00,tomo_energy~lt~19.100
 *     parcelStatusQuery:
 *       in: query
 *       description: status of the parcel
 *       name: status
 *       schema:
 *         type: string
 *     parcelStatus:
 *       in: path
 *       description: status of the parcel
 *       name: status
 *       schema:
 *         type: string
 *       required: true
 *     key:
 *       in: path
 *       description: Cache key.
 *       name: key
 *       schema:
 *         type: string
 *       required: true
 */
